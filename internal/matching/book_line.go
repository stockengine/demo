package matching

import (
	"sync"

	"gitlab.com/stockengine/demo/internal/proto"
)

func NewBookLine() *BookLineV2 {
	blp := bookLinePool.Get().(*BookLineV2)
	blp.reset()
	return blp
	//return &BookLineV2{}
}

func CloseBookLine(bl *BookLineV2) {
	//bl.reset()
	bookLinePool.Put(bl)
}

var bookLinePool = sync.Pool{
	New: func() interface{} {
		return &BookLineV2{}
	},
}

//
//type BookLine struct {
//	intList singlylinkedlist.List //    88.0 ns/op
//	//doublylinkedlist.List // 125 ns/op
//}
//
//func (bl *BookLine) Reset() {
//	bl.intList.Clear()
//}
//
//func (bl *BookLine) Get() (o *Order, found bool) {
//	e, ok := bl.intList.Get(0)
//	if ok {
//		return e.(*Order), true
//	} else {
//		return nil, false
//	}
//}
//
//func (bl *BookLine) Remove() {
//	bl.intList.Remove(0)
//}
//func (bl *BookLine) Empty() bool {
//	return bl.intList.Empty()
//}
//func (bl *BookLine) Append(o *Order) {
//	bl.intList.Append(o)
//}
//func (bl *BookLine) Iterator() singlylinkedlist.Iterator {
//	return bl.intList.Iterator()
//}

// ----------------------  BookLine v2 ----------------------

type BookLineV2Elem struct {
	prev, next *BookLineV2Elem // prev нужен для удаление ордера из середины списка
	order      *proto.NewOrder
}

func (t *BookLineV2Elem) Order() *proto.NewOrder {
	return t.order
}

type BookLineV2 struct {
	first, last *BookLineV2Elem
}

func (bl *BookLineV2) Get() (le *BookLineV2Elem, found bool) {
	if bl.first != nil {
		return bl.first, true
	}
	return nil, false
}

// remove first elem
func (bl *BookLineV2) Remove() {
	old := bl.first
	if old != nil {
		nxt := bl.first.next
		bl.first = nxt
		if nxt != nil {
			nxt.prev = nil
		} else {
			bl.last = nil
		}

		//prepare to GC
		deleteBookLineV2Elem(old)
	}
}
func (bl *BookLineV2) Empty() bool {
	if (bl.first == nil && bl.last != nil) || (bl.first != nil && bl.last == nil) {
		panic("OMFG!!! List broken :( need more autotests") // защита от дурака т.к. от меня
	}
	return bl.first == nil
}

// add elem to end
func (bl *BookLineV2) Append(o *proto.NewOrder) (le *BookLineV2Elem) {
	//leIn := bookLineElemPool.Get()
	le = newBookLineV2Elem()
	le.prev = bl.last
	le.order = o
	if bl.last != nil {
		bl.last.next = le
		bl.last = le
	} else {
		bl.first, bl.last = le, le
	}
	return
}

//приватный метод т.к. нет чеков на корректность
func (bl *BookLineV2) delete(blPtr *BookLineV2Elem) {
	prv := blPtr.prev
	nxt := blPtr.next
	if prv != nil { // не первый
		prv.next = nxt
	} else { // первый
		bl.first = nxt
	}
	if nxt != nil { // не последний
		nxt.prev = prv
	} else { // последний
		bl.last = prv
	}
	deleteBookLineV2Elem(blPtr)
}

func (bl *BookLineV2) reset() {
	bl.last = nil
	bl.first = nil
}

// BookLineElem Pool  --------
//var cntBookLineElem int64
//var cntBookLineNew int64
var bookLineElemPool = sync.Pool{
	New: func() interface{} {
		//cntBookLineNew++
		return &BookLineV2Elem{}
	},
}

func newBookLineV2Elem() (ret *BookLineV2Elem) {
	//cntBookLineElem++
	ret = bookLineElemPool.Get().(*BookLineV2Elem)
	ret.reset()
	return
	//return new(BookLineV2Elem)
}

func deleteBookLineV2Elem(le *BookLineV2Elem) {
	//cntBookLineElem--
	bookLineElemPool.Put(le)
}

func (t *BookLineV2Elem) reset() {
	t.prev, t.next, t.order = nil, nil, nil
}

// -------------------  Iterator -------------------------

func (bl *BookLineV2) Iterator() BookLineV2Iterator {
	return BookLineV2Iterator{
		bookLine: bl,
		cur:      nil,
	}
}

type BookLineV2Iterator struct {
	bookLine *BookLineV2
	cur      *BookLineV2Elem
}

func (bli *BookLineV2Iterator) Next() bool {
	if bli.cur == nil {
		bli.cur = bli.bookLine.first
	} else {
		bli.cur = bli.cur.next
	}
	return bli.cur != nil
}

func (bli *BookLineV2Iterator) Value() *proto.NewOrder {
	return bli.cur.Order()
}

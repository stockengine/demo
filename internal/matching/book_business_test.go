package matching

import (
	"testing"

	"gitlab.com/stockengine/demo/internal/lib/subcommand"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/stockengine/demo/internal/proto"
)

//func TestBook_Proceed(t *testing.T) {
//	book := NewBook()
//	tx := &TransactionLogger{}
//	order := &NewOrder{
//		Type:      Type_Limit,
//		Operation: Operation_Buy,
//		Amount:    NewAmountStringMust("100"),
//		Price:     NewPrice(250, 0),
//		Identification: Identification{
//			Owner: Owner{123),
//			ExtID: ExtID{567),
//		},
//	}
//	book.Proceed(order, tx)
//	book.PrintLog()
//}

func TestBookOrderProceedNewOrder(t *testing.T) {
	bs := NewBook()

	tests := []struct {
		order     NewOrder
		tx        []TxDeal
		buy, sell []NewOrder
		state     OrderState
	}{
		{ //0
			order: NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     567,
			},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     567,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //1
			order: NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     678,
			},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     567,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     678,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //2
			order: NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Sell,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     789,
			},
			tx: []TxDeal{
				{
					Maker: OrderChange{
						MarketSide:   MarketSide_Maker,
						AmountChange: NewAmountStringMust("100"),
						OrderStatus:  OrderStatus_Closed,
						DealPrice:    NewPriceMust("250"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    Amount{}, // после сделки остаток ордера = 0
							Price:     NewPriceMust("250"),
							OwnerID:   123,
							ExtID:     567,
						},
					},
					Taker: OrderChange{
						MarketSide:   MarketSide_Taker,
						AmountChange: NewAmountStringMust("100"),
						OrderStatus:  OrderStatus_Closed,
						DealPrice:    NewPriceMust("250"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Sell,
							Amount:    Amount{}, // после сделки остаток ордера = 0
							Price:     NewPriceMust("250"),
							OwnerID:   123,
							ExtID:     789,
						},
					},
				},
			},
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     678,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("0"),
				OrderStatus: OrderStatus_Closed,
			},
		},
		{ //3
			order: NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     987,
			},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     678,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     987,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ // 4
			order: NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     987,
			},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     678,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123,
				ExtID:     987,
			}},
			sell: nil,
			state: OrderState{
				OrderStatus: OrderStatus_IgnoredDublicateId,
			},
		},
	}
	for i, tst := range tests {
		trans := NewTransaction()
		order := tst.order // copy
		bs.Proceed(&SubCommands{New: &order}, trans)
		assert.Equalf(t, tst.order, *trans.Command.SubCommands.New, "case(%d)", i)
		assert.Equalf(t, tst.tx, trans.TxList, "case(%d)", i)
		assert.Equalf(t, tst.sell, bs.Get(Operation_Sell), "case(%d)", i)
		assert.Equalf(t, tst.buy, bs.Get(Operation_Buy), "case(%d)", i)
		assert.Equalf(t, tst.state, trans.OrderFinalState, "case(%d)", i)
		CloseTransaction(trans)
	}
}

func TestBookOrderProceedEdit(t *testing.T) {
	bs := NewBook()

	tests := []struct {
		cmd       SubCommands
		tx        []TxDeal
		buy, sell []NewOrder
		state     OrderState
	}{
		{ //0 add
			cmd: SubCommands{New: &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 567,
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 567,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //1 edit bad id
			cmd: SubCommands{Edit: &EditOrder{
				NewAmount: NewAmountStringMust("13"),
				NewPrice:  NewPriceMust("777"),
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 567,
			}},
			sell: nil,
			state: OrderState{
				OrderStatus: OrderStatus_IgnoredInvalidIdentification,
			},
		},
		{ //2 edit bad id
			cmd: SubCommands{Edit: &EditOrder{
				OwnerID: 1234, ExtID: 567,
				NewAmount: NewAmountStringMust("13"),
				NewPrice:  NewPriceMust("777"),
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 567,
			}},
			sell: nil,
			state: OrderState{
				OrderStatus: OrderStatus_IgnoredIdentificationNotFound,
			},
		},
		{ //3 edit bad id
			cmd: SubCommands{Edit: &EditOrder{
				OwnerID: 123, ExtID: 5678,
				NewAmount: NewAmountStringMust("13"),
				NewPrice:  NewPriceMust("777"),
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 567,
			}},
			sell: nil,
			state: OrderState{
				OrderStatus: OrderStatus_IgnoredIdentificationNotFound,
			},
		},
		{ //4 edit amount
			cmd: SubCommands{Edit: &EditOrder{
				OwnerID: 123, ExtID: 567,
				NewAmount: NewAmountStringMust("13"),
			}},
			tx: []TxDeal{{Maker: OrderChange{
				MarketSide:   MarketSide_Maker,
				AmountChange: NewAmountStringMust("100"),
				OrderStatus:  OrderStatus_ChangedAmount,
				DealPrice:    NewPriceMust("250"),
				Order: NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("13"),
					Price:     NewPriceMust("250"),
					OwnerID:   123, ExtID: 567,
				},
			}}},
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("13"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 567,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("13"),
				OrderStatus: OrderStatus_ChangedAmount,
			},
		},
		{ //5 edit price
			cmd: SubCommands{Edit: &EditOrder{
				OwnerID: 123, ExtID: 567,
				NewPrice: NewPriceMust("777"),
			}},
			tx: []TxDeal{
				{Maker: OrderChange{
					MarketSide:  MarketSide_Maker,
					OrderStatus: OrderStatus_Cancel,
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("13"),
						Price:     NewPriceMust("250"),
						OwnerID:   123, ExtID: 567,
					},
				}},
				{Maker: OrderChange{
					MarketSide:   MarketSide_Maker,
					AmountChange: NewAmountStringMust("13"),
					OrderStatus:  OrderStatus_ChangedPrice,
					DealPrice:    NewPriceMust("250"),
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("13"),
						Price:     NewPriceMust("777"),
						OwnerID:   123, ExtID: 567,
					},
				}},
			},
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("13"),
				Price:     NewPriceMust("777"),
				OwnerID:   123, ExtID: 567,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("13"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //6 add opposite order
			cmd: SubCommands{New: &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Sell,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("800"),
				OwnerID:   345, ExtID: 789,
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("13"),
				Price:     NewPriceMust("777"),
				OwnerID:   123, ExtID: 567,
			}},
			sell: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Sell,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("800"),
				OwnerID:   345, ExtID: 789,
			}},
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //7 move to deal :)
			cmd: SubCommands{Edit: &EditOrder{
				OwnerID: 123, ExtID: 567,
				NewPrice: NewPriceMust("900"),
			}},
			tx: []TxDeal{
				{Maker: OrderChange{
					MarketSide:  MarketSide_Maker,
					OrderStatus: OrderStatus_Cancel,
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("13"),
						Price:     NewPriceMust("777"),
						OwnerID:   123, ExtID: 567,
					},
				}},
				{Maker: OrderChange{
					MarketSide:   MarketSide_Maker,
					AmountChange: NewAmountStringMust("13"),
					OrderStatus:  OrderStatus_ChangedPrice,
					DealPrice:    NewPriceMust("777"),
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("13"),
						Price:     NewPriceMust("900"),
						OwnerID:   123, ExtID: 567,
					},
				}},
				{
					Maker: OrderChange{
						MarketSide:   MarketSide_Maker,
						AmountChange: NewAmountStringMust("13"),
						OrderStatus:  OrderStatus_ClosedPartial,
						DealPrice:    NewPriceMust("800"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Sell,
							Amount:    NewAmountStringMust("87"),
							Price:     NewPriceMust("800"),
							OwnerID:   345, ExtID: 789,
						},
					},
					Taker: OrderChange{
						MarketSide:   MarketSide_Taker,
						AmountChange: NewAmountStringMust("13"),
						OrderStatus:  OrderStatus_Closed,
						DealPrice:    NewPriceMust("800"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("0"),
							Price:     NewPriceMust("900"),
							OwnerID:   123, ExtID: 567,
						},
					},
				},
			},
			buy: nil,
			sell: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Sell,
				Amount:    NewAmountStringMust("87"),
				Price:     NewPriceMust("800"),
				OwnerID:   345, ExtID: 789,
			}},
			state: OrderState{
				AmountFinal: NewAmountStringMust("0"),
				OrderStatus: OrderStatus_Closed,
			},
		},
	}
	for i, tst := range tests {
		trans := NewTransaction()
		oldCmd := subcommand.NewSubCommand(tst.cmd)
		bs.Proceed(&tst.cmd, trans)
		assert.Equalf(t, oldCmd, tst.cmd, "case(%d)", i)
		assert.Equalf(t, tst.tx, trans.TxList, "case(%d)", i)
		assert.Equalf(t, tst.sell, bs.Get(Operation_Sell), "case(%d)", i)
		assert.Equalf(t, tst.buy, bs.Get(Operation_Buy), "case(%d)", i)
		assert.Equalf(t, tst.state, trans.OrderFinalState, "case(%d)", i)
		CloseTransaction(trans)
	}
}

func TestBookOrderProceedDelete(t *testing.T) {
	bs := NewBook()

	tests := []struct {
		cmd       SubCommands
		tx        []TxDeal
		buy, sell []NewOrder
		state     OrderState
	}{
		{ //0 add
			cmd: SubCommands{New: &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 1,
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 1,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //1 add
			cmd: SubCommands{New: &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("200"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 2,
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 1,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("200"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 2,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("200"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //2 add
			cmd: SubCommands{New: &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("300"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 3,
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 1,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("200"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 2,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("300"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 3,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("300"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ //3 add
			cmd: SubCommands{New: &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("400"),
				Price:     NewPriceMust("350"),
				OwnerID:   234, ExtID: 1,
			}},
			tx: nil,
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("400"),
				Price:     NewPriceMust("350"),
				OwnerID:   234, ExtID: 1,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("100"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 1,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("200"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 2,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("300"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 3,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("400"),
				OrderStatus: OrderStatus_ToBook,
			},
		},
		{ // delete one
			cmd: SubCommands{
				Delete: &DeleteOrder{OwnerID: 123, ExtID: 1}},
			tx: []TxDeal{{
				Maker: OrderChange{
					MarketSide:   MarketSide_Maker,
					AmountChange: NewAmountStringMust("0"),
					OrderStatus:  OrderStatus_Cancel,
					DealPrice:    NewPriceMust("0"),
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("100"),
						Price:     NewPriceMust("250"),
						OwnerID:   123, ExtID: 1,
					},
				},
			}},
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("400"),
				Price:     NewPriceMust("350"),
				OwnerID:   234, ExtID: 1,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("200"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 2,
			}, {
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("300"),
				Price:     NewPriceMust("250"),
				OwnerID:   123, ExtID: 3,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"),
				OrderStatus: OrderStatus_Cancel,
			},
		},
		//ToDo: delete multy имеет недетерминированный порядок т.к. в мапе лежат ордера пользователя
		{ // delete multi
			cmd: SubCommands{
				Delete: &DeleteOrder{OwnerID: 123}},
			tx: []TxDeal{{
				Maker: OrderChange{
					MarketSide:   MarketSide_Maker,
					AmountChange: NewAmountStringMust("0"),
					OrderStatus:  OrderStatus_Cancel,
					DealPrice:    NewPriceMust("0"),
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("200"),
						Price:     NewPriceMust("250"),
						OwnerID:   123, ExtID: 2,
					},
				},
			}, {
				Maker: OrderChange{
					MarketSide:   MarketSide_Maker,
					AmountChange: NewAmountStringMust("0"),
					OrderStatus:  OrderStatus_Cancel,
					DealPrice:    NewPriceMust("0"),
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("300"),
						Price:     NewPriceMust("250"),
						OwnerID:   123, ExtID: 3,
					},
				},
			}},
			buy: []NewOrder{{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmountStringMust("400"),
				Price:     NewPriceMust("350"),
				OwnerID:   234, ExtID: 1,
			}},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("0"),
				OrderStatus: OrderStatus_Cancel,
			},
		},
	}
	for i, tst := range tests {
		trans := NewTransaction()
		oldCmd := subcommand.NewSubCommand(tst.cmd)
		bs.Proceed(&tst.cmd, trans)
		assert.Equalf(t, oldCmd, tst.cmd, "case(%d)", i)
		assert.Equalf(t, tst.tx, trans.TxList, "case(%d)", i)
		assert.Equalf(t, tst.sell, bs.Get(Operation_Sell), "case(%d)", i)
		assert.Equalf(t, tst.buy, bs.Get(Operation_Buy), "case(%d)", i)
		assert.Equalf(t, tst.state, trans.OrderFinalState, "case(%d)", i)
		CloseTransaction(trans)
	}
}

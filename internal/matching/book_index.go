package matching

import "gitlab.com/stockengine/demo/internal/proto"

func (t *Book) inIndex(o *proto.NewOrder) bool {
	if o.OwnerID == 0 ||
		o.ExtID == 0 {
		return false
	}

	return t.indexByUserID.AlreadyExist(o.OwnerID, o.ExtID)
}

func (t *Book) indexAdd(le *BookLineV2Elem) {
	if le != nil { // задан id
		t.indexByUserID.Append(le)
	}
}

// +build howtocode

package matching

import (
	"sync"
	"testing"
)

/*
BenchmarkAllocStackOrder-8              2000000000               0.26 ns/op
BenchmarkAllocHeapOrder-8               50000000                28.8 ns/op
BenchmarkAllocPoolOrder-8               100000000               14.3 ns/op
BenchmarkAllocStackBookLineElem-8       2000000000               0.26 ns/op
BenchmarkAllocHeapBookLineElem-8        50000000                29.1 ns/op
BenchmarkAllocPoolBookLineElem-8        100000000               15.6 ns/op
BenchmarkAllocStackAmount-8             2000000000               0.26 ns/op
BenchmarkAllocHeapAmount-8              100000000               22.9 ns/op
BenchmarkAllocPoolAmount-8              100000000               15.8 ns/op

Вывод - использование пула всегда быстрее чем аллокация объектов в куче.
200+нс возможны только в случае если объект не возвращается в пул т.е. идёт утечка
*/

//func BenchmarkAllocStackOrder(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		o := &Order{}
//		_ = o
//	}
//}
//
//var o *Order
//
//func BenchmarkAllocHeapOrder(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		o = &Order{}
//		_ = o
//	}
//}
//
//var orderPool = sync.Pool{
//	New: func() interface{} {
//		return &Order{}
//	},
//}
//
//func BenchmarkAllocPoolOrder(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		o = orderPool.Get().(*Order)
//		_ = o
//		orderPool.Put(o)
//	}
//}

func BenchmarkAllocStackBookLineElem(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ble := &BookLineV2Elem{}
		_ = ble
	}
}

var ble *BookLineV2Elem

func BenchmarkAllocHeapBookLineElem(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ble = &BookLineV2Elem{}
		_ = ble
	}
}

var blePool = sync.Pool{
	New: func() interface{} {
		return &BookLineV2Elem{}
	},
}

func BenchmarkAllocPoolBookLineElem(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ble = blePool.Get().(*BookLineV2Elem)
		_ = ble
		blePool.Put(ble)
	}
}

//func BenchmarkAllocStackAmount(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		am := &Amount{}
//		_ = am
//	}
//}
//
//var am *Amount
//
//func BenchmarkAllocHeapAmount(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		am = &Amount{}
//		_ = am
//	}
//}
//
//var amountPool = sync.Pool{
//	New: func() interface{} {
//		return &Amount{}
//	},
//}
//
//func BenchmarkAllocPoolAmount(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		am = amountPool.Get().(*Amount)
//		_ = am
//		amountPool.Put(am)
//	}
//}

//noinspection GoUnusedGlobalVariable
var glblLe *BookLineV2Elem

func BenchmarkPoolBookLineElem(b *testing.B) {
	for i := 0; i < b.N; i++ {
		le := newBookLineV2Elem()
		glblLe = le
		deleteBookLineV2Elem(le)
	}
}

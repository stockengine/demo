package matching

import (
	"bytes"
	"math/big"
	"math/rand"
	"testing"

	"gitlab.com/stockengine/demo/internal/proto"

	"github.com/stretchr/testify/assert"
)

func TestAmountString(t *testing.T) {
	am := proto.Amount{Byte0: 10, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0}
	assert.Equal(t, "10", proto.AmountToString(&am))
	//t.Errorf("%s\n", am)
}

func TestAmountCmp(t *testing.T) {
	testData := []struct {
		a1, a2 proto.Amount
		cmp    int
	}{{
		a1:  proto.Amount{Byte0: 10, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0},
		a2:  proto.Amount{Byte0: 10, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0},
		cmp: 0,
	}, {
		a1:  proto.Amount{Byte0: 15, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0},
		a2:  proto.Amount{Byte0: 10, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0},
		cmp: 1,
	}, {
		a1:  proto.Amount{Byte0: 10, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0},
		a2:  proto.Amount{Byte0: 15, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0},
		cmp: -1,
	}, {
		a1:  proto.Amount{Byte0: 15, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 5, Byte5: 0},
		a2:  proto.Amount{Byte0: 10, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0},
		cmp: 1,
	}, {
		a1:  proto.Amount{Byte0: 15, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 5, Byte5: 0},
		a2:  proto.Amount{Byte0: 10, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 5},
		cmp: -1,
	}}

	for i, e := range testData {
		assert.Equalf(t, e.cmp, proto.AmountCmp(&e.a1, &e.a2), "i->%d, a1->%s, a2->%s", i, e.a1, e.a2)
	}

}

func TestAmount_SetFromString(t *testing.T) {
	a := proto.Amount{}
	val := "1234567890123456789012345678901234567890123456789012345678901234567890"
	assert.NoError(t, proto.AmountSetFromString(&a, val))
	res := proto.AmountToString(&a)

	assert.Equal(t, val, res)
}

func TestAmount_Mul64(t *testing.T) {
	a := proto.Amount{}
	assert.NoError(t, proto.AmountSetFromString(&a, "12345678901234567890"))
	_ = proto.AmountMul64(&a, 10)
	assert.Equal(t, proto.Amount{Byte0: 0xB14E9F812F366C34, Byte1: 6, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0}, a)
	assert.Equal(t, "123456789012345678900", proto.AmountToString(&a))
}

func TestAmountExtractDigit(t *testing.T) {
	a := proto.Amount{Byte0: 0xB14E9F812F366C34, Byte1: 6, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0}
	assert.Equal(t, "123456789012345678900", proto.AmountToString(&a))
	assert.Equal(t, uint64(0), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(0), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(9), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(8), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(7), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(6), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(5), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(4), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(3), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(2), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(1), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(0), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(9), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(8), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(7), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(6), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(5), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(4), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(3), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(2), proto.AmountExtractDigit(&a))
	assert.Equal(t, uint64(1), proto.AmountExtractDigit(&a))

}

func TestAmount_Mul(t *testing.T) {
	ba, bb := big.Int{}, big.Int{}
	fa, fb := proto.Amount{}, proto.Amount{}
	rnd := rand.New(rand.NewSource(666))

	oneRound := func() {
		getRndStr := func(n int64) string {
			rLen := int(rnd.Int63n(n)) + 1
			var valStr bytes.Buffer
			for i := 0; i < rLen; i++ {
				valStr.WriteRune('0' + rune(rnd.Int63n(10)))
			}
			return valStr.String()
		}
		str1, str2 := getRndStr(50), getRndStr(8)
		ba.SetString(str1, 10)
		bb.SetString(str2, 10)
		assert.NoError(t, proto.AmountSetFromString(&fa, str1))
		assert.NoError(t, proto.AmountSetFromString(&fb, str2))

		bc := ba.Mul(&ba, &bb)
		_ = proto.AmountMul64(&fa, fb.Byte0)

		assert.Equalf(t, bc.String(), proto.AmountToString(&fa), "str1 -> '%s', str2 -> '%s', mul1 -> '%s' mul2 -> '%s'\n", str1, str2, bc.String(), fa.String())
	}
	for i := 0; i < 1e4; i++ {
		oneRound()
	}
}

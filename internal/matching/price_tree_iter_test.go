package matching

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/stockengine/demo/internal/proto"
)

//кейс - в стакане на покупку в начале не самый дорогой ордер и когда приходит ордер на продажу, то продаётся самый невыгодный
//func NewBookSide(op proto.Operation) *BookSide {
// buy - начинается с самой высокой цены (я прихожу продавать и продаю самым выгодным - дорогим)
// sell - начинается с самой низкой (я прихожу покупать и покупаю самых выгодных - самых дешёвых)
func TestBookTree_BuyOrder(t *testing.T) {
	book := NewBook()
	book.buy.PriceList.Put(proto.NewPriceMust("15"), NewBookLine())
	book.buy.PriceList.Put(proto.NewPriceMust("5"), NewBookLine())
	book.buy.PriceList.Put(proto.NewPriceMust("25"), NewBookLine())

	it := book.Iterator(proto.Operation_Buy)
	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("25"), it.Key())
	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("15"), it.Key())
	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("5"), it.Key())
}

func TestBookTree_SellOrder(t *testing.T) {
	book := NewBook()
	book.sell.PriceList.Put(proto.NewPriceMust("15"), NewBookLine())
	book.sell.PriceList.Put(proto.NewPriceMust("5"), NewBookLine())
	book.sell.PriceList.Put(proto.NewPriceMust("25"), NewBookLine())

	it := book.Iterator(proto.Operation_Sell)
	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("5"), it.Key())
	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("15"), it.Key())
	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("25"), it.Key())
}

func TestBookTree_RemoveOrder(t *testing.T) {
	book := NewBook()
	book.sell.PriceList.Put(proto.NewPriceMust("15"), NewBookLine())
	book.sell.PriceList.Put(proto.NewPriceMust("5"), NewBookLine())
	book.sell.PriceList.Put(proto.NewPriceMust("25"), NewBookLine())

	it := book.Iterator(proto.Operation_Sell)

	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("5"), it.Key())
	it.Value().(*BookLineV2).Remove()
	assert.True(t, it.Value().(*BookLineV2).Empty())
	defer book.sell.closeBookLine(proto.NewPriceMust("5"), it.Value().(*BookLineV2))

	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("15"), it.Key())
	it.Value().(*BookLineV2).Remove()
	assert.True(t, it.Value().(*BookLineV2).Empty())
	defer book.sell.closeBookLine(proto.NewPriceMust("15"), it.Value().(*BookLineV2))

	assert.True(t, it.Next())
	assert.Equal(t, proto.NewPriceMust("25"), it.Key())
	assert.True(t, it.Value().(*BookLineV2).Empty())
	defer book.sell.closeBookLine(proto.NewPriceMust("25"), it.Value().(*BookLineV2))
}

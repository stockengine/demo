package matching

import (
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/stockengine/demo/internal/proto"
)

// тесты по проблемам найденные пользователями

// тест имени Димы
// https://gitlab.com/stockengine/demo/issues/16
//
//[✓] > matching Create -Operation=Buy -Amount=10 -Price=10 -Type=Limit
//[✓] > matching Create -Operation=Sell -Amount=5 -Price=5 -Type=Limit
//[✓] > matching Create -Operation=Sell -Amount=5 -Price=12 -Type=Limit
//[✓] > matching Create -Operation=Buy -Amount=5 -Price=15 -Type=Limit
//[✓] > matching Create -Operation=Buy -Amount=100 -Price=15 -Type=Limit
//[✓] > matching Create -Operation=Sell -Amount=50 -Price=5 -Type=Limit
//[✓] > matching Create -Operation=Sell -Amount=50 -Price=5 -Type=Limit
//[✓] > storage Load
//Command                                                    Original                            Final
//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
//1        1       0          0         0             0 Limit       Buy       10 10.0000000000     0     0     10        ToBook
//2        2       0          0         0             0 Limit      Sell        5  5.0000000000     0     0      0        Closed
//						  Taker           10.0000000000 Limit      Sell        5  5.0000000000     0     0      0        Closed
//						  Maker           10.0000000000 Limit       Buy        5 10.0000000000     0     0      5 ClosedPartial
//3        3       0          0         0             0 Limit      Sell        5 12.0000000000     0     0      5        ToBook
//4        4       0          0         0             0 Limit       Buy        5 15.0000000000     0     0      0        Closed
//						  Taker           12.0000000000 Limit       Buy        5 15.0000000000     0     0      0        Closed
//						  Maker           12.0000000000 Limit      Sell        5 12.0000000000     0     0      0        Closed
//5        5       0          0         0             0 Limit       Buy      100 15.0000000000     0     0    100        ToBook
//6        6       0          0         0             0 Limit      Sell       50  5.0000000000     0     0     45 ClosedPartial
//						  Taker           10.0000000000 Limit      Sell        5  5.0000000000     0     0     45 ClosedPartial
//						  Maker           10.0000000000 Limit       Buy        5 10.0000000000     0     0      0        Closed
//7        7       0          0         0             0 Limit      Sell       50  5.0000000000     0     0      0        Closed
//						  Taker           15.0000000000 Limit      Sell       50  5.0000000000     0     0      0        Closed
//						  Maker           15.0000000000 Limit       Buy       50 15.0000000000     0     0     50 ClosedPartial
//total 7 tx
//
// PS = FIXED

func TestMatchingIgnoreOrders(t *testing.T) {
	bs := NewBook()

	tests := []struct {
		order     NewOrder
		tx        []TxDeal
		buy, sell []NewOrder
		state     OrderState
	}{
		{ //0 --- matching Create -Operation=Buy -Amount=10 -Price=10 -Type=Limit
			//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
			//1        1       0          0         0             0 Limit       Buy       10 10.0000000000     0     0     10        ToBook
			order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("10"), Price: NewPriceMust("10")},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("10"), Price: NewPriceMust("10")},
			},
			sell:  nil,
			state: OrderState{AmountFinal: NewAmountStringMust("10"), OrderStatus: OrderStatus_ToBook},
		},
		{ //1 --- matching Create -Operation=Sell -Amount=5 -Price=5 -Type=Limit
			//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
			//2        2       0          0         0             0 Limit      Sell        5  5.0000000000     0     0      0        Closed
			//						  Taker           10.0000000000 Limit      Sell        5  5.0000000000     0     0      0        Closed
			//						  Maker           10.0000000000 Limit       Buy        5 10.0000000000     0     0      5 ClosedPartial
			order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("5"), Price: NewPriceMust("5")},
			tx: []TxDeal{
				{
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("5"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("10"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("10")}},
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("5"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("10"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("5")}},
				}},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("10")},
			},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed,
			},
		},
		{ //2 --- matching Create -Operation=Sell -Amount=5 -Price=12 -Type=Limit
			//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
			//3        3       0          0         0             0 Limit      Sell        5 12.0000000000     0     0      5        ToBook
			order: NewOrder{
				Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("5"), Price: NewPriceMust("12"),
			},
			tx: nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("10")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("5"), Price: NewPriceMust("12")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("5"), OrderStatus: OrderStatus_ToBook},
		},
		{ //3 --- matching Create -Operation=Buy -Amount=5 -Price=15 -Type=Limit
			//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
			//4        4       0          0         0             0 Limit       Buy        5 15.0000000000     0     0      0        Closed
			//						  Taker           12.0000000000 Limit       Buy        5 15.0000000000     0     0      0        Closed
			//						  Maker           12.0000000000 Limit      Sell        5 12.0000000000     0     0      0        Closed
			order: NewOrder{
				Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("15"),
			},
			tx: []TxDeal{
				{
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("5"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("12"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("12")}},
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("5"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("12"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: NewPriceMust("15")}},
				}},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("10")},
			},
			sell:  nil,
			state: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
		},
		{ // 4 --- matching Create -Operation=Buy -Amount=100 -Price=15 -Type=Limit
			//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
			//5        5       0          0         0             0 Limit       Buy      100 15.0000000000     0     0    100        ToBook
			order: NewOrder{
				Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("100"), Price: NewPriceMust("15"),
			},
			tx: nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("100"), Price: NewPriceMust("15")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("10")},
			},
			sell:  nil,
			state: OrderState{AmountFinal: NewAmountStringMust("100"), OrderStatus: OrderStatus_ToBook},
		},
		{ //5 --- matching Create -Operation=Sell -Amount=50 -Price=5 -Type=Limit
			//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
			//6        6       0          0         0             0 Limit      Sell       50  5.0000000000     0     0     45 ClosedPartial
			//						  Taker           10.0000000000 Limit      Sell        5  5.0000000000     0     0     45 ClosedPartial
			//						  Maker           10.0000000000 Limit       Buy        5 10.0000000000     0     0      0        Closed
			order: NewOrder{
				Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("50"), Price: NewPriceMust("5"),
			},
			tx: []TxDeal{
				{
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("50"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("15"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("50"), Price: NewPriceMust("15")}},
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("50"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("15"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("5")}},
				}},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("50"), Price: NewPriceMust("15")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("10")},
			},
			sell:  nil,
			state: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
		},
		{ //6 --- matching Create -Operation=Sell -Amount=50 -Price=5 -Type=Limit
			//### CommitID   IPLow SrcDstPort ConnectTS     AcceptCnt  Type Operation   Amount         Price Owner ExtID Amount        Status
			//7        7       0          0         0             0 Limit      Sell       50  5.0000000000     0     0      0        Closed
			//						  Taker           15.0000000000 Limit      Sell       50  5.0000000000     0     0      0        Closed
			//						  Maker           15.0000000000 Limit       Buy       50 15.0000000000     0     0     50 ClosedPartial
			order: NewOrder{
				Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("50"), Price: NewPriceMust("5"),
			},
			tx: []TxDeal{
				{
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("50"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("15"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: NewPriceMust("15")}},
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("50"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("15"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("5")}},
				}},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("5"), Price: NewPriceMust("10")},
			},
			sell:  nil,
			state: OrderState{OrderStatus: OrderStatus_Closed},
		},
	}
	for i, tst := range tests {
		trans := NewTransaction()
		order := tst.order // copy
		bs.Proceed(&SubCommands{New: &order}, trans)
		assert.Equalf(t, tst.tx, trans.TxList, "case(%d)", i)
		assert.Equalf(t, tst.sell, bs.Get(Operation_Sell), "case(%d)", i)
		assert.Equalf(t, tst.buy, bs.Get(Operation_Buy), "case(%d)", i)
		assert.Equalf(t, tst.state, trans.OrderFinalState, "case(%d)", i)
		CloseTransaction(trans)
	}
}

// Второй тест имени Димы
//
//https://gitlab.com/stockengine/demo/issues/18
//
//matching Create -Amount=100 -Operation=Buy -Price=100 -Type=Limit
//matching Create -Amount=10 -Operation=Sell -Price=120 -Type=Limit
//matching Create -Amount=10 -Operation=Sell -Price=90 -Type=Limit
//matching Create -Amount=20 -Operation=Buy -Price=130 -Type=Limit
//matching Create -Amount=200 -Operation=Sell -Price=95 -Type=Limit
//
//storage Load
//Command                                                     Original                             Final
//### CommitID   IPLow SrcDstPort ConnectTS      AcceptCnt  Type Operation   Amount          Price Owner ExtID Amount        Status
//  1        1       0          0         0              0 Limit       Buy      100 100.0000000000     0     0    100        ToBook
//  2        2       0          0         0              0 Limit      Sell       10 120.0000000000     0     0     10        ToBook
//  3        3       0          0         0              0 Limit      Sell       10  90.0000000000     0     0      0        Closed
//                          Taker           100.0000000000 Limit      Sell       10  90.0000000000     0     0      0        Closed
//                          Maker           100.0000000000 Limit       Buy       10 100.0000000000     0     0     90 ClosedPartial
//  4        4       0          0         0              0 Limit       Buy       20 130.0000000000     0     0     10 ClosedPartial
//                          Taker           120.0000000000 Limit       Buy       10 130.0000000000     0     0     10 ClosedPartial
//                          Maker           120.0000000000 Limit      Sell       10 120.0000000000     0     0      0        Closed
//  5        5       0          0         0              0 Limit      Sell      200  95.0000000000     0     0    190 ClosedPartial
//                          Taker           130.0000000000 Limit      Sell       10  95.0000000000     0     0    190 ClosedPartial
//                          Maker           130.0000000000 Limit       Buy       10 130.0000000000     0     0      0        Closed

func TestMatchingIgnoreOrders2(t *testing.T) {
	bs := NewBook()

	tests := []struct {
		order     NewOrder
		tx        []TxDeal
		buy, sell []NewOrder
		state     OrderState
	}{
		{
			//     Command                                                     Original                             Final
			//### CommitID   IPLow SrcDstPort ConnectTS      AcceptCnt  Type Operation   Amount          Price Owner ExtID Amount        Status
			//  1        1       0          0         0              0 Limit       Buy      100 100.0000000000     0     0    100        ToBook
			order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("100"), Price: NewPriceMust("100")},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("100"), Price: NewPriceMust("100")},
			},
			sell:  nil,
			state: OrderState{AmountFinal: NewAmountStringMust("100"), OrderStatus: OrderStatus_ToBook},
		},
		{
			//### CommitID   IPLow SrcDstPort ConnectTS      AcceptCnt  Type Operation   Amount          Price Owner ExtID Amount        Status
			//  2        2       0          0         0              0 Limit      Sell       10 120.0000000000     0     0     10        ToBook
			order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10"), Price: NewPriceMust("120")},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("100"), Price: NewPriceMust("100")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10"), Price: NewPriceMust("120")},
			},
			state: OrderState{
				AmountFinal: NewAmountStringMust("10"), OrderStatus: OrderStatus_ToBook,
			},
		},
		{
			//### CommitID   IPLow SrcDstPort ConnectTS      AcceptCnt  Type Operation   Amount          Price Owner ExtID Amount        Status
			//  3        3       0          0         0              0 Limit      Sell       10  90.0000000000     0     0      0        Closed
			//                          Taker           100.0000000000 Limit      Sell       10  90.0000000000     0     0      0        Closed
			//                          Maker           100.0000000000 Limit       Buy       10 100.0000000000     0     0     90 ClosedPartial
			order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10"), Price: NewPriceMust("90")},
			tx: []TxDeal{
				{
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("100"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("90"), Price: NewPriceMust("100")}},
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("100"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("90")}},
				}},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("90"), Price: NewPriceMust("100")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10"), Price: NewPriceMust("120")},
			},
			state: OrderState{
				AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed,
			},
		},
		{
			//### CommitID   IPLow SrcDstPort ConnectTS      AcceptCnt  Type Operation   Amount          Price Owner ExtID Amount        Status
			//  4        4       0          0         0              0 Limit       Buy       20 130.0000000000     0     0     10 ClosedPartial
			//                          Taker           120.0000000000 Limit       Buy       10 130.0000000000     0     0     10 ClosedPartial
			//                          Maker           120.0000000000 Limit      Sell       10 120.0000000000     0     0      0        Closed
			order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20"), Price: NewPriceMust("130")},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("120"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("10"), Price: NewPriceMust("130")}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("120"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("120")}},
				}},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("10"), Price: NewPriceMust("130")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("90"), Price: NewPriceMust("100")},
			},
			sell: nil,
			state: OrderState{
				AmountFinal: NewAmountStringMust("10"), OrderStatus: OrderStatus_ClosedPartial,
			},
		},
		{
			//### CommitID   IPLow SrcDstPort ConnectTS      AcceptCnt  Type Operation   Amount          Price Owner ExtID Amount        Status
			//  5        5       0          0         0              0 Limit      Sell      200  95.0000000000     0     0    190 ClosedPartial
			//                          Taker           130.0000000000 Limit      Sell       10  95.0000000000     0     0    190 ClosedPartial
			//                          Maker           130.0000000000 Limit       Buy       10 130.0000000000     0     0      0        Closed
			/// отсутствует вторая сделка
			order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("200"), Price: NewPriceMust("95")},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("130"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("190"), Price: NewPriceMust("95")}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("130"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: NewPriceMust("130")}},
				},
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("90"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("100"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("100"), Price: NewPriceMust("95")}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("90"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("100"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: NewPriceMust("100")}},
				},
			},
			buy: nil,
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("100"), Price: NewPriceMust("95")},
			},
			state: OrderState{
				AmountFinal: NewAmountStringMust("100"), OrderStatus: OrderStatus_ClosedPartial,
			},
		},
	}
	for i, tst := range tests {
		trans := NewTransaction()
		order := tst.order // copy
		bs.Proceed(&SubCommands{New: &order}, trans)
		assert.Equalf(t, tst.tx, trans.TxList, "case(%d)", i)
		assert.Equalf(t, tst.sell, bs.Get(Operation_Sell), "case(%d)", i)
		assert.Equalf(t, tst.buy, bs.Get(Operation_Buy), "case(%d)", i)
		assert.Equalf(t, tst.state, trans.OrderFinalState, "case(%d)", i)
		CloseTransaction(trans)
	}
}

// третий тест имени Димы
// на этой последовательности команд генерируется такая последовательность сделок, что DOM падает
// в матчинге проверяем генерацию транзакций и корректность ордербук
// в ДОМ проверяем уже по списку транзакций генерацию стаканов

func TestMatchingDOMSubzero(t *testing.T) {
	bs := NewBook()

	tests := []struct {
		order     NewOrder
		tx        []TxDeal
		buy, sell []NewOrder
		state     OrderState
	}{
		{ // 0
			order: NewOrder{Price: NewPriceMust("3070405"), Amount: NewAmountStringMust("25838"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy:   nil,
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("25838"), Price: NewPriceMust("3070405")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("25838"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 1
			order: NewOrder{Price: NewPriceMust("2789318"), Amount: NewAmountStringMust("1925"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("25838"), Price: NewPriceMust("3070405")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("1925"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 2
			order: NewOrder{Price: NewPriceMust("8412466"), Amount: NewAmountStringMust("38913"), Operation: Operation_Buy, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("25838"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("3070405"),
						Order: NewOrder{Price: NewPriceMust("8412466"), Amount: NewAmountStringMust("13075"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("25838"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("3070405"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("3070405")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13075"), Price: NewPriceMust("8412466")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell:  nil,
			state: OrderState{AmountFinal: NewAmountStringMust("13075"), OrderStatus: OrderStatus_ClosedPartial},
		},
		{ // 3
			order: NewOrder{Price: NewPriceMust("11809479"), Amount: NewAmountStringMust("27839"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13075"), Price: NewPriceMust("8412466")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("27839"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 4
			order: NewOrder{Price: NewPriceMust("2890670"), Amount: NewAmountStringMust("33610"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13075"), Price: NewPriceMust("8412466")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("33610"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 5
			order: NewOrder{Price: NewPriceMust("2894377"), Amount: NewAmountStringMust("36170"), Operation: Operation_Sell, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("13075"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("8412466"),
						Order: NewOrder{Price: NewPriceMust("2894377"), Amount: NewAmountStringMust("23095"), Operation: Operation_Sell, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("13075"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("8412466"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: NewPriceMust("8412466")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("23095"), Price: NewPriceMust("2894377")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("23095"), OrderStatus: OrderStatus_ClosedPartial},
		},
		{ // 6
			order: NewOrder{Price: NewPriceMust("10478015"), Amount: NewAmountStringMust("23317"), Operation: Operation_Buy, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("23095"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("2894377"),
						Order: NewOrder{Price: NewPriceMust("10478015"), Amount: NewAmountStringMust("222"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("23095"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("2894377"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("2894377")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("222"), Price: NewPriceMust("10478015")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("222"), OrderStatus: OrderStatus_ClosedPartial},
		},
		{ // 7
			order: NewOrder{Price: NewPriceMust("7155806"), Amount: NewAmountStringMust("26728"), Operation: Operation_Sell, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("222"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("10478015"),
						Order: NewOrder{Price: NewPriceMust("7155806"), Amount: NewAmountStringMust("26506"), Operation: Operation_Sell, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("222"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("10478015"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: NewPriceMust("10478015")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("26506"), OrderStatus: OrderStatus_ClosedPartial},
		},
		{ // 8
			order: NewOrder{Price: NewPriceMust("12176831"), Amount: NewAmountStringMust("3992"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("3992"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 9
			order: NewOrder{Price: NewPriceMust("3711427"), Amount: NewAmountStringMust("32406"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("32406"), Price: NewPriceMust("3711427")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("32406"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 10
			order: NewOrder{Price: NewPriceMust("2569820"), Amount: NewAmountStringMust("33154"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("32406"), Price: NewPriceMust("3711427")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("33154"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 11
			order: NewOrder{Price: NewPriceMust("6451376"), Amount: NewAmountStringMust("4704"), Operation: Operation_Buy, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("4704"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("3711427"),
						Order: NewOrder{Price: NewPriceMust("6451376"), Amount: NewAmountStringMust("0"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("4704"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("3711427"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27702"), Price: NewPriceMust("3711427")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27702"), Price: NewPriceMust("3711427")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
		},
		{ // 12
			order: NewOrder{Price: NewPriceMust("2737598"), Amount: NewAmountStringMust("33704"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27702"), Price: NewPriceMust("3711427")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("33704"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 13
			order: NewOrder{Price: NewPriceMust("12421826"), Amount: NewAmountStringMust("1419"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27702"), Price: NewPriceMust("3711427")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("1419"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 14
			order: NewOrder{Price: NewPriceMust("2005019"), Amount: NewAmountStringMust("18496"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27702"), Price: NewPriceMust("3711427")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("18496"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 15
			order: NewOrder{Price: NewPriceMust("3013567"), Amount: NewAmountStringMust("20425"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27702"), Price: NewPriceMust("3711427")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("26506"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("20425"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 16
			order: NewOrder{Price: NewPriceMust("14675912"), Amount: NewAmountStringMust("39340"), Operation: Operation_Buy, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("27702"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("3711427"),
						Order: NewOrder{Price: NewPriceMust("14675912"), Amount: NewAmountStringMust("11638"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("27702"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("3711427"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("3711427")}},
				},
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("11638"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("7155806"),
						Order: NewOrder{Price: NewPriceMust("14675912"), Amount: NewAmountStringMust("0"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("11638"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("7155806"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("14868"), Price: NewPriceMust("7155806")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("14868"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
		},
		{ // 17
			order: NewOrder{Price: NewPriceMust("7705742"), Amount: NewAmountStringMust("14084"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("14868"), Price: NewPriceMust("7155806")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("14084"), Price: NewPriceMust("7705742")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("14084"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 18
			order: NewOrder{Price: NewPriceMust("10719777"), Amount: NewAmountStringMust("42321"), Operation: Operation_Buy, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("14868"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("7155806"),
						Order: NewOrder{Price: NewPriceMust("10719777"), Amount: NewAmountStringMust("27453"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("14868"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("7155806"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("7155806")}},
				},
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("14084"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("7705742"),
						Order: NewOrder{Price: NewPriceMust("10719777"), Amount: NewAmountStringMust("13369"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("14084"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("7705742"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: NewPriceMust("7705742")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("13369"), OrderStatus: OrderStatus_ClosedPartial},
		},
		{ // 19
			order: NewOrder{Price: NewPriceMust("10778051"), Amount: NewAmountStringMust("36040"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("36040"), Price: NewPriceMust("10778051")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("36040"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 20
			order: NewOrder{Price: NewPriceMust("12289841"), Amount: NewAmountStringMust("15167"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("36040"), Price: NewPriceMust("10778051")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("15167"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 21
			order: NewOrder{Price: NewPriceMust("5494828"), Amount: NewAmountStringMust("6340"), Operation: Operation_Sell, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("6340"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("10778051"),
						Order: NewOrder{Price: NewPriceMust("5494828"), Amount: NewAmountStringMust("0"), Operation: Operation_Sell, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("6340"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("10778051"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("29700"), Price: NewPriceMust("10778051")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("29700"), Price: NewPriceMust("10778051")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
		},
		{ // 22
			order: NewOrder{Price: NewPriceMust("11754040"), Amount: NewAmountStringMust("48336"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("29700"), Price: NewPriceMust("10778051")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("48336"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("48336"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 23
			order: NewOrder{Price: NewPriceMust("12202905"), Amount: NewAmountStringMust("18959"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("29700"), Price: NewPriceMust("10778051")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("48336"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("18959"), Price: NewPriceMust("12202905")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("18959"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 24
			order: NewOrder{Price: NewPriceMust("14091355"), Amount: NewAmountStringMust("38140"), Operation: Operation_Buy, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("38140"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("11754040"),
						Order: NewOrder{Price: NewPriceMust("14091355"), Amount: NewAmountStringMust("0"), Operation: Operation_Buy, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("38140"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("11754040"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10196"), Price: NewPriceMust("11754040")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("29700"), Price: NewPriceMust("10778051")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10196"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("18959"), Price: NewPriceMust("12202905")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
		},
		{ // 25
			order: NewOrder{Price: NewPriceMust("1616847"), Amount: NewAmountStringMust("6463"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("29700"), Price: NewPriceMust("10778051")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("13369"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("6463"), Price: NewPriceMust("1616847")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10196"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("18959"), Price: NewPriceMust("12202905")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("6463"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 26
			order: NewOrder{Price: NewPriceMust("8104085"), Amount: NewAmountStringMust("41051"), Operation: Operation_Sell, Type: Type_Limit},
			tx: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("29700"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("10778051"),
						Order: NewOrder{Price: NewPriceMust("8104085"), Amount: NewAmountStringMust("11351"), Operation: Operation_Sell, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("29700"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("10778051"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: NewPriceMust("10778051")}},
				},
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("11351"), OrderStatus: OrderStatus_Closed, DealPrice: NewPriceMust("10719777"),
						Order: NewOrder{Price: NewPriceMust("8104085"), Amount: NewAmountStringMust("0"), Operation: Operation_Sell, Type: Type_Limit}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("11351"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: NewPriceMust("10719777"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("2018"), Price: NewPriceMust("10719777")}},
				},
			},
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("2018"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("6463"), Price: NewPriceMust("1616847")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10196"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("18959"), Price: NewPriceMust("12202905")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
		},
		{ // 27
			order: NewOrder{Price: NewPriceMust("12223991"), Amount: NewAmountStringMust("23798"), Operation: Operation_Sell, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("2018"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("6463"), Price: NewPriceMust("1616847")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10196"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("18959"), Price: NewPriceMust("12202905")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("23798"), Price: NewPriceMust("12223991")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("23798"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 28
			order: NewOrder{Price: NewPriceMust("9495624"), Amount: NewAmountStringMust("3156"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("2018"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("3156"), Price: NewPriceMust("9495624")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("6463"), Price: NewPriceMust("1616847")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10196"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("18959"), Price: NewPriceMust("12202905")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("23798"), Price: NewPriceMust("12223991")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("3156"), OrderStatus: OrderStatus_ToBook},
		},
		{ // 29
			order: NewOrder{Price: NewPriceMust("10367930"), Amount: NewAmountStringMust("43654"), Operation: Operation_Buy, Type: Type_Limit},
			tx:    nil,
			buy: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("2018"), Price: NewPriceMust("10719777")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("43654"), Price: NewPriceMust("10367930")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("3156"), Price: NewPriceMust("9495624")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("20425"), Price: NewPriceMust("3013567")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33610"), Price: NewPriceMust("2890670")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("1925"), Price: NewPriceMust("2789318")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33704"), Price: NewPriceMust("2737598")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("33154"), Price: NewPriceMust("2569820")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("18496"), Price: NewPriceMust("2005019")},
				{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("6463"), Price: NewPriceMust("1616847")},
			},
			sell: []NewOrder{
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("10196"), Price: NewPriceMust("11754040")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27839"), Price: NewPriceMust("11809479")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("3992"), Price: NewPriceMust("12176831")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("18959"), Price: NewPriceMust("12202905")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("23798"), Price: NewPriceMust("12223991")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("15167"), Price: NewPriceMust("12289841")},
				{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("1419"), Price: NewPriceMust("12421826")},
			},
			state: OrderState{AmountFinal: NewAmountStringMust("43654"), OrderStatus: OrderStatus_ToBook},
		},
	}
	for i, tst := range tests {
		trans := NewTransaction()
		order := tst.order // copy
		bs.Proceed(&SubCommands{New: &order}, trans)
		if !assert.Equalf(t, tst.tx, trans.TxList, "case(%d)", i) {
			t.Fatalf("%#v\n", trans.TxList)
		}
		if !assert.Equalf(t, tst.sell, bs.Get(Operation_Sell), "case(%d)", i) {
			t.Fatalf("%#v\n", bs.Get(Operation_Sell))
		}
		if !assert.Equalf(t, tst.buy, bs.Get(Operation_Buy), "case(%d)", i) {
			t.Fatalf("%#v\n", bs.Get(Operation_Buy))
		}
		if !assert.Equalf(t, tst.state, trans.OrderFinalState, "case(%d)", i) {
			t.Fatalf("%#v\n", trans.OrderFinalState)
		}
		CloseTransaction(trans)
	}
}

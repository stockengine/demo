package matching

import (
	"sync"

	"gitlab.com/stockengine/demo/internal/proto"
)

//собирает все изменения для последующей сериализации и отправки одним блоком
// without pool
//BenchmarkProceed10OrderBook-8           10000000               161 ns/op             276 B/op          3 allocs/op
//BenchmarkProceed100OrderBook-8          10000000               170 ns/op             276 B/op          3 allocs/op
//BenchmarkProceed1000OrderBook-8         10000000               181 ns/op             276 B/op          3 allocs/op

//with pool
//BenchmarkProceed10OrderBook-8           10000000               145 ns/op             116 B/op          3 allocs/op
//BenchmarkProceed100OrderBook-8          10000000               144 ns/op             116 B/op          3 allocs/op
//BenchmarkProceed1000OrderBook-8         10000000               138 ns/op             116 B/op          3 allocs/op

// с исправленной багой в тесте и без пула
//BenchmarkProceed10OrderBook-8           10000000               158 ns/op             276 B/op          3 allocs/op
//BenchmarkProceed100OrderBook-8          10000000               165 ns/op             276 B/op          3 allocs/op
//BenchmarkProceed1000OrderBook-8         10000000               169 ns/op             276 B/op          3 allocs/op

//должна работать с пулом транзакций для минимизации аллокаций
func NewTransaction() *proto.Transaction {
	tx := txPool.Get().(*proto.Transaction)
	tx.TxList = tx.TxList[:0] // reset declared via protobuf and trash slice :( , so do handmade clear
	tx.OrderFinalState = proto.OrderState{}
	tx.Command = proto.Command{}
	return tx
	//return &Transaction{}
	//return getTx()
}
func CloseTransaction(t *proto.Transaction) {
	txPool.Put(t)
	//putTx(t)
}

var txPool = sync.Pool{
	New: func() interface{} {
		return &proto.Transaction{
			TxList: []proto.TxDeal{},
		}
	},
}

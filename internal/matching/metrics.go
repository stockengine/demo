package matching

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/stockengine/demo/internal/proto"
)

const unknown = "unknown"

func cmdToLabel(cmd *proto.Command) string {
	switch {
	case cmd.SubCommands.New != nil:
		return "new"
	case cmd.SubCommands.Edit != nil:
		return "edit"
	case cmd.SubCommands.Delete != nil:
		return "delete"
	default:
		return unknown
	}
}

func txToMetrics(tx *proto.Transaction, counter prometheus.CounterVec) {
	//{"type", "side", "operation", "status"}
	var tpe, side, op, status string
	switch {
	case tx.Command.SubCommands.New != nil:
		side = "taker"
		op = tx.Command.SubCommands.New.Operation.String()
	case tx.Command.SubCommands.Edit != nil, tx.Command.SubCommands.Delete != nil:
		side = "maker"
		op = unknown
	default:
		side = unknown
		op = unknown
	}
	//maker
	status = tx.OrderFinalState.OrderStatus.String()
	tpe = "limit"
	counter.WithLabelValues(tpe, side, op, status).Inc()

	side = "maker"
	for i := range tx.TxList {
		status = tx.TxList[i].Maker.OrderStatus.String()
		op = tx.TxList[i].Maker.Order.Operation.String()
		counter.WithLabelValues(tpe, side, op, status).Inc()
	}
}

func (t *Engine) prometheusMetrics() {
	t.wg.Add(1)
	go func() {
		defer t.wg.Done()
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				bs := t.book.buy.PriceList.Size()  //ToDo: unsafe - race condition.. fix it later
				ss := t.book.sell.PriceList.Size() //ToDo: unsafe - race condition.. fix it later
				t.promOrderTotal.WithLabelValues("buy").Set(float64(bs))
				t.promOrderTotal.WithLabelValues("sell").Set(float64(ss))
			case <-t.quit:
				return
			}
		}
	}()
}

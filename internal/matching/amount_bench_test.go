package matching

import (
	"fmt"
	"math/big"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/mailru/easyjson"

	"gitlab.com/stockengine/demo/internal/proto"
)

// вариант с передачей суммы по указателю работает быстрее...
//
// amountCmpCopy.txt -> передача суммы по копии
// amountCmpPtr.txt -> передача суммы по указателю
//
//$ benchcmp amountCmpCopy.txt amountCmpPtr.txt
//benchmark                           old ns/op     new ns/op     delta
//BenchmarkAmountCmp-8                3.93          3.41          -13.23%
//BenchmarkAmountCmpBigInt-8          5.59          5.60          +0.18%
//BenchmarkAmountSetSub-8             5.56          5.55          -0.18%
//BenchmarkAmountSetAdd-8             4.77          4.78          +0.21%
//BenchmarkAmountSetAddBigInt-8       17.3          17.4          +0.58%
//BenchmarkAmountSetSubTillZero-8     15.0          12.1          -19.33%
//BenchmarkAmountIsZeroFalse-8        0.25          0.26          +0.79%
//BenchmarkAmountIsZeroTrue-8         0.51          0.52          +1.38%
//BenchmarkAmountMul64-8              27.7          27.5          -0.72%
//BenchmarkAmountMulBigInt-8          20.8          20.8          +0.00%
//
//benchmark                           old allocs     new allocs     delta
//BenchmarkAmountCmp-8                0              0              +0.00%
//BenchmarkAmountCmpBigInt-8          0              0              +0.00%
//BenchmarkAmountSetSub-8             0              0              +0.00%
//BenchmarkAmountSetAdd-8             0              0              +0.00%
//BenchmarkAmountSetAddBigInt-8       0              0              +0.00%
//BenchmarkAmountSetSubTillZero-8     0              0              +0.00%
//BenchmarkAmountMul64-8              0              0              +0.00%
//BenchmarkAmountMulBigInt-8          0              0              +0.00%
//
//benchmark                           old bytes     new bytes     delta
//BenchmarkAmountCmp-8                0             0             +0.00%
//BenchmarkAmountCmpBigInt-8          0             0             +0.00%
//BenchmarkAmountSetSub-8             0             0             +0.00%
//BenchmarkAmountSetAdd-8             0             0             +0.00%
//BenchmarkAmountSetAddBigInt-8       0             0             +0.00%
//BenchmarkAmountSetSubTillZero-8     0             0             +0.00%
//BenchmarkAmountMul64-8              0             0             +0.00%
//BenchmarkAmountMulBigInt-8          0             0             +0.00%

func BenchmarkAmountCmp(b *testing.B) {
	x := proto.NewAmount(13)
	y := proto.NewAmount(12)
	ptrX := &x
	ptrY := &y
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		proto.AmountCmp(ptrX, ptrY)
	}
}
func BenchmarkAmountCmpBigInt(b *testing.B) {
	x := big.NewInt(13)
	y := big.NewInt(12)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		x.Cmp(y)
	}
}

//func BenchmarkAmountSet(b *testing.B) {
//	x := NewAmount(12)
//	z := proto.Amount{}
//	_ = z // remove warning
//	b.ReportAllocs()
//	b.ResetTimer()
//	for i := 0; i < b.N; i++ {
//		z = x
//	}
//}

func BenchmarkAmountSetSub(b *testing.B) {
	x := proto.NewAmount(1312)
	y := proto.NewAmount(1213)
	z := proto.Amount{}
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		z = x
		proto.AmountSub(&z, &y)
	}
}
func BenchmarkAmountSetAdd(b *testing.B) {
	x := proto.NewAmount(12)
	y := proto.NewAmount(13)
	z := proto.Amount{}
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		z = x
		proto.AmountAdd(&z, &y)
	}
}
func BenchmarkAmountSetAddBigInt(b *testing.B) {
	x := big.NewInt(1312)
	y := big.NewInt(1213)
	z := &big.Int{}
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		z.Set(x)
		z.Add(z, y)
	}
}

func BenchmarkAmountSetSubTillZero(b *testing.B) {
	x := proto.NewAmount(12)
	y := proto.NewAmount(13)
	z := proto.Amount{}
	deal := proto.Amount{}
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		z = x
		proto.AmountSubTillZero(&z, &y, &deal)
	}
}

func BenchmarkAmountIsZeroFalse(b *testing.B) {
	x := proto.NewAmount(12)
	for i := 0; i < b.N; i++ {
		proto.AmountIsZero(&x)
	}
}
func BenchmarkAmountIsZeroTrue(b *testing.B) {
	x := proto.Amount{}
	for i := 0; i < b.N; i++ {
		proto.AmountIsZero(&x)
	}
}

func BenchmarkAmountMul64(b *testing.B) {
	x := proto.NewAmount(12)
	var y uint64 = 10
	z := proto.Amount{}
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		z = x
		_ = proto.AmountMul64(&z, y)
	}
}
func BenchmarkAmountMulBigInt(b *testing.B) {
	x := big.NewInt(12)
	y := big.NewInt(10)
	z := big.Int{}
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		z.Set(x)
		z.Mul(x, y)
	}
}

// тест отсутствия проблем при Amount vs AmountMethod
type amtm struct{}
type amnt2 amtm

func (amtm) z()  {}
func (amnt2) z() {}
func BenchmarkAmtm(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var z amtm
		z.z()
	}
}
func BenchmarkAmtm2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var z amtm
		amnt2(z).z()
	}
}

func Benchmark_EasyJSONUnmarshal(b *testing.B) {
	// Benchmark_EasyJSONMarshal-8   	  396945	      3118 ns/op	     208 B/op	       2 allocs/op
	// Benchmark_EasyJSONMarshal-8        179130	      6765 ns/op											//2019-12-19
	a := &proto.Amount{}
	val := "1234567890123456789012345678901234567890123456789012345678901234567890"
	valJSON := []byte(fmt.Sprintf("%s", val))

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if err := easyjson.Unmarshal(valJSON, a); err != nil {
			assert.NoError(b, err)
			return
		}
	}
}

func Benchmark_EasyJSONMarshal(b *testing.B) {
	// Benchmark_EasyJSONUnmarshal-8   	  133813	      7989 ns/op	     896 B/op	       7 allocs/op
	// Benchmark_EasyJSONUnmarshal-8       50496	     24880 ns/op											//2019-12-19
	a := &proto.Amount{}
	val := "1234567890123456789012345678901234567890123456789012345678901234567890"
	valJSON := []byte(fmt.Sprintf("%s", val))
	err := easyjson.Unmarshal(valJSON, a)
	if !assert.NoError(b, err) {
		return
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = easyjson.Marshal(a)
		if err != nil {
			assert.NoError(b, err)
			return
		}
	}
}

//func BenchmarkAmountMul64Long(b *testing.B) {
//	x := NewAmount(12)
//	assert.NoError(b, x.SetFromString("123456789012345678901234567890123456789012345678901234567890"))
//	var y uint64 = 10
//	z := proto.Amount{}
//	b.ReportAllocs()
//	b.ResetTimer()
//	for i := 0; i < b.N; i++ {
//		z.Set(&x)
//		z.Mul64(y)
//	}
//}
//func BenchmarkAmountMulBigIntLong(b *testing.B) {
//	x := &big.Int{}
//	x.SetString("123456789012345678901234567890123456789012345678901234567890", 10)
//	y := big.NewInt(10)
//	z := big.Int{}
//	b.ReportAllocs()
//	b.ResetTimer()
//	for i := 0; i < b.N; i++ {
//		z.Set(x)
//		z.Mul(x, y)
//	}
//}
//func BenchmarkAmountMul64LongLong(b *testing.B) {
//	x := NewAmount(12)
//	assert.NoError(b, x.SetFromString("123456789012345678901234567890123456789012345678901234567890"))
//	var y uint64 = 1234567890123456789
//	z := proto.Amount{}
//	b.ReportAllocs()
//	b.ResetTimer()
//	for i := 0; i < b.N; i++ {
//		z.Set(&x)
//		z.Mul64(y)
//	}
//}
//func BenchmarkAmountMulBigIntLongLong(b *testing.B) {
//	x := &big.Int{}
//	x.SetString("123456789012345678901234567890123456789012345678901234567890", 10)
//	y := big.NewInt(1234567890123456789)
//	z := big.Int{}
//	b.ReportAllocs()
//	b.ResetTimer()
//	for i := 0; i < b.N; i++ {
//		z.Set(x)
//		z.Mul(x, y)
//	}
//}

package matching

import (
	"context"
	"net"
	"reflect"
	"sync"
	"sync/atomic"
	"time"

	prometheus2 "gitlab.com/stockengine/demo/internal/lib/prometheus"

	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/forwarder"

	"google.golang.org/grpc"

	"gitlab.com/stockengine/demo/internal/proto"
)

type Engine struct {
	wg     sync.WaitGroup
	inCmd  *forwarder.ManyToOne
	outMgs *forwarder.OneToMany

	addr        string
	storageAddr string

	connStorage   *grpc.ClientConn
	clientStorage proto.StorageEngineClient         // для начальной загрузки истории ордеров
	clientTx      proto.StorageEngine_StoreTxClient // для записи результатов сделок

	cancelClientStoreTx context.CancelFunc
	cancelLoadTx        context.CancelFunc // для отмены длительной начальной загрузки

	srv *grpc.Server
	lis net.Listener

	book *Book

	prometheusBind      string
	prom                *prometheus2.Prometheus
	promCmdIn           prometheus.CounterVec
	promOrderProcessing prometheus.HistogramVec
	promOrderStatus     prometheus.CounterVec
	promOrderTotal      prometheus.GaugeVec
	quit                chan struct{}
}

type Option interface {
	apply(*Engine)
}

type OptionForwarder struct {
	forwarder.ManyToOneOptions
	forwarder.OneToManyOptions
}

func (t OptionForwarder) apply(m *Engine) {
	m.inCmd = forwarder.NewManyToOne(t.ManyToOneOptions)
	m.outMgs = forwarder.NewOneToMany(t.OneToManyOptions)
}

type OptionServerAddr struct {
	Addr string
}

func (t OptionServerAddr) apply(m *Engine) {
	m.addr = t.Addr
}

type OptionStorageAddr struct {
	Addr string
}

func (t OptionStorageAddr) apply(m *Engine) {
	m.storageAddr = t.Addr
}

type OptionPrometheusBind struct {
	Addr string
}

func (t OptionPrometheusBind) apply(m *Engine) {
	m.prometheusBind = t.Addr
}

type ErrorNeedChanPool struct{}

func (ErrorNeedChanPool) Error() string {
	return "Need ChanPool"
}

func NewEngine(opts ...Option) (*Engine, error) {
	var err error
	me := &Engine{
		book: NewBook(),
		quit: make(chan struct{}),
	}
	for _, o := range opts {
		o.apply(me)
	}
	if me.inCmd == nil || me.outMgs == nil {
		return nil, ErrorNeedChanPool{}
	}

	prom, err := prometheus2.New(me.prometheusBind)
	if err != nil {
		return nil, err
	}

	me.prom = prom
	me.promCmdIn = prom.NewCounterVec("command", "incoming commands counters",
		[]string{"operation"})
	me.promOrderProcessing = prom.NewHistogramVec("processing", "order processing latency & etc",
		[]string{"operation"}, prometheus2.DefBucketsUltraLowLat)
	me.promOrderStatus = prom.NewCounterVec("status", "order processing status",
		[]string{"type", "side", "operation", "status"})
	me.promOrderTotal = prom.NewGaugeVec("count", "order count",
		[]string{"side"})

	me.prometheusMetrics()

	me.wg.Add(1)
	go func() {
		defer me.wg.Done()
		ch := me.inCmd.Get()
		for msg := range ch {
			start := time.Now()
			cmd := msg.(*proto.Command)
			tx := &proto.Transaction{ //ToDo: replace with pool
				Command: proto.Command{
					ReqID: cmd.ReqID,
				},
			} //NewTransaction()
			me.book.Proceed(&cmd.SubCommands, tx)
			elapsed := time.Since(start)
			me.promOrderProcessing.WithLabelValues(cmdToLabel(cmd)).Observe(elapsed.Seconds()) //ToDo: optimize it - its too slow :(
			me.outMgs.SendEvent(tx)
			txToMetrics(tx, me.promOrderStatus) //ToDo: too slow.....
		}
		me.outMgs.Close() // больше не будем отправлять
	}()

	me.wg.Add(1)
	go func() {
		defer me.wg.Done()
		msgCh := me.outMgs.NewSubscriber()

		defer me.outMgs.RemoveSubscriber(msgCh)
		for msg := range msgCh {
			tx := msg.(*proto.Transaction)
			if logger.ErrorLogHelper(me.clientTx.Send(tx), "clientTx.Send") {
				break
			}
		}
		//закрываем коннект
		logger.ErrorLogHelper(me.clientTx.CloseSend(), "clientTx.CloseSend()")
	}()

	//connect to storage server. its prerequsite
	var ctx context.Context
	me.connStorage, err = config.GRPCClientDial(me.storageAddr, true)
	if err != nil {
		return nil, err
	}
	me.clientStorage = proto.NewStorageEngineClient(me.connStorage)

	//reading history
	if err := me.loadHistoricalData(); err != nil {
		return nil, err
	}
	ctx, me.cancelClientStoreTx = context.WithCancel(context.Background())
	me.clientTx, err = me.clientStorage.StoreTx(ctx)
	if err != nil {
		return nil, err
	}

	me.lis, me.srv, err = config.CreateGRPCListen(me.addr)
	if err != nil {
		return nil, err
	}

	proto.RegisterMatchingEngineServer(me.srv, me)

	go func() {
		err := me.srv.Serve(me.lis)
		logger.ErrorLogHelper(err, "Serve")
	}()

	return me, err
}

/*
задача:
- считаем что ордеров у нас нет (при старте и так пусто в памяти)
- надо считать все ордера из хранилища (и в транзакции добавить исходную команду)
- повторно выполнить матчинг
- считать что финальное состояние совпадает с финальным состоянием при остановке
*/
func (t *Engine) loadHistoricalData() error {
	logger.Log.Info("orders load started")
	start := time.Now()
	var loaded int64
	quit := make(chan struct{})
	defer close(quit) // dont waste
	go func() {       // reporting routine
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		var old int64
		for {
			select {
			case <-ticker.C:
				cnt := atomic.LoadInt64(&loaded)
				delta := cnt - old
				old = cnt
				logger.Log.Info("loading...", zap.Int64("orders", cnt), zap.Int64("order per second", delta))
			case <-quit:
				return
			}
		}
	}()
	if t.book.Size() != 0 {
		return ErrorOrdersExists{}
	}

	var ctx context.Context
	ctx, t.cancelLoadTx = context.WithCancel(context.Background())
	ltx, err := t.clientStorage.LoadTx(ctx, &proto.EmptyMessage{})
	if err != nil {
		return err
	}
	defer func() {
		if err := ltx.CloseSend(); err != nil {
			logger.Log.Error("ltx.CloseSend()", zap.Error(err))
		}
		// и контекст освобождаем, в Stop он ещё раз освободится, ничего страшного
		t.cancelLoadTx()
	}()
	var tx proto.Transaction
loadTxLoop:
	for {
		ctx, err := ltx.Recv()
		needBrake, isGood := logger.ErrorLogHelper2(err, "LoadTx.Recv()")
		switch {
		case needBrake && isGood:
			break loadTxLoop
		case needBrake && !isGood:
			return err
		case !needBrake:
			tx.Reset()
			t.book.Proceed(&ctx.Transaction.Command.SubCommands, &tx)
			if !reflect.DeepEqual(ctx.Transaction.TxList, tx.TxList) {
				return &ErrorOrderAndTxMismatch{
					ctx: *ctx,
					tx:  tx,
				}
			}
			atomic.AddInt64(&loaded, 1)
		default:
			panic("OMFG!!!")
		}
	}
	logger.Log.Info("loading complete", zap.Int64("total loaded", loaded), zap.Duration("elapsed", time.Since(start)))
	return nil
}

func (t *Engine) Stop() {
	t.cancelLoadTx() // останавливаем загрузку истории из хранилища (если идёт)

	//просим остановить приём команд
	// - прекращаем принимать новые соединения
	// - все обработчики узнают о завершении и получают io.EOF
	logger.Log.Debug("srv.Stop")
	t.srv.Stop() // гасим сервер,
	//t.srv.GracefulStop() // graceful не подходит т.к. у нас входящий поток, а grafecul ждёт завершения потока

	//жду завершения потока матчинга
	logger.Log.Debug("wg.Wait")
	t.inCmd.Close() //т.к. приём остановлен, то никто больше ничего не запишет
	// каскадом запускаются остановка отправка ответов, закрываются соединение с хранилищем
	close(t.quit) //для метрик прометиуса
	t.wg.Wait()

	logger.ErrorLogHelper(t.connStorage.Close(), "t.connStorage.Stop()")
}

// метод запускается в отдельной го-рутине при каждом вызове. задача - принимать запросы и класть в очередь к ядру
func (t *Engine) Proceed(srv proto.MatchingEngine_ProceedServer) error {
	t.wg.Add(1)
	defer t.wg.Done() // вынужден так делать т.к. GracefulStop не пригоден, потому сам и учитываю
	log := logger.LogNewConnect(srv.Context(), "Proceed")
	defer log.Info("stream closed")

L:
	for {
		cmd, err := srv.Recv()
		needBrake, isGood := logger.ErrorLogHelper4(err, "srv.Recv()", log)
		switch {
		case !needBrake && isGood:
			t.inCmd.Put(cmd)
			t.promCmdIn.WithLabelValues(cmdToLabel(cmd)).Inc()
		default:
			break L
		}
	}
	return nil
}

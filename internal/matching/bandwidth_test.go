// +build heavy

package matching

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/stockengine/demo/internal/proto"
)

// https://github.com/mzheravin/exchange-core
//
//3,000,000 inbound messages are distributed as follows:
// 9% limit
// 3% market new orders,
// 6% cancel operations,
// 82% move operations.
// About 6% commands are causing trades.

// в нашем тесте делаем начальное наполнение 3000000 buy ордерами, а далее рандомно:
// 12% лимитные ордера (6% buy, 6% sell)
//  6% удаление
// 82% изменение объёма
//
// BenchmarkBandwidth-8     3000000               588 ns/op               5 B/op          0 allocs/op
//после перехода на структуры генерируемые gogoproto
// BenchmarkBandwidth-8                     3000000               625 ns/op               4 B/op          0 allocs/op
//go1.13.3, генарация транзакций, дерево переведено на interface значения и в библиотеку
// BenchmarkBandwidth-8                     2049652               604 ns/op               6 B/op          0 allocs/op
//
// 2019-10-29 добавил move операцию, профиль теста сменил на
// 12% лимитные ордера (6% buy, 6% sell)
//  6% удаление
// 82% изменение цены ордера
// BenchmarkBandwidth-8      690996              1867 ns/op              72 B/op          0 allocs/op
// и контрольный бенч с изменением только объёма
// BenchmarkBandwidth-8     1559563               809 ns/op              71 B/op          0 allocs/op

func BenchmarkBandwidth(b *testing.B) {
	id := make(map[Identification]struct{})
	book := NewBook()
	rnd := rand.New(rand.NewSource(666))
	//начальная заливка данных - лимитные ордера с последовательными id
	const baseOrderCnt = int(3e6)
	for i := 0; i < baseOrderCnt; i++ {
		tx := NewTransaction()
		order := &NewOrder{
			Type:      Type_Limit,
			Operation: Operation_Buy,
			Amount:    NewAmount(uint64((rnd.Int63n(100000) + 1) * 100)),
			Price:     NewPriceUint64(uint64((rnd.Int63n(100000) + 1) * 1000000)),
			Identification: Identification{
				Owner: Owner{Owner: 1},
				ExtID: ExtID{ExtID: uint64(i + 1)},
			},
		}
		id[order.Identification] = struct{}{}
		book.Proceed(&SubCommands{New: order}, tx)
		CloseTransaction(tx)
	}

	assert.Equal(b, baseOrderCnt, len(book.Get(Operation_Buy)))

	b.ReportAllocs()
	b.ResetTimer()
	b.StopTimer()

	for i := 0; i < b.N; i++ {
		r := rnd.Int63n(100)
		switch {
		case r < 6: // 6% buy = append to order book
			order := &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Buy,
				Amount:    NewAmount(uint64((rnd.Int63n(100000) + 1) * 100)),
				Price:     NewPriceUint64(uint64((rnd.Int63n(100000) + 1) * 1000000)),
				Identification: Identification{
					Owner: Owner{Owner: 2},
					ExtID: ExtID{ExtID: uint64(i + 1)},
				},
			}
			tx := NewTransaction()
			b.StartTimer()
			book.Proceed(&SubCommands{New: order}, tx)
			b.StopTimer()
			CloseTransaction(tx)
			id[order.Identification] = struct{}{}

		case r < 12: // 6% sell = make deal (unknown number or deals
			order := &NewOrder{
				Type:      Type_Limit,
				Operation: Operation_Sell,
				Amount:    NewAmount(uint64((rnd.Int63n(100000) + 1) * 100)),
				Price:     NewPriceUint64(uint64((rnd.Int63n(100000) + 1) * 100000)), // на один ноль меньше чем покупка, гарантируем одну сделку
				Identification: Identification{
					Owner: Owner{Owner: 2},
					ExtID: ExtID{ExtID: uint64(i + 1)},
				},
			}
			tx := NewTransaction()
			b.StartTimer()
			book.Proceed(&SubCommands{New: order}, tx)
			b.StopTimer()
			removeClosed := func(change OrderChange) {
				switch change.OrderStatus {
				case OrderStatus_Closed:
					delete(id, change.Order.Identification)
				case OrderStatus_ClosedPartial:
				default:
					b.Fatalf("unexpected order status -> %+v, change -> %+v\n", change.OrderStatus, change)
				}
			}
			for _, v := range tx.TxList {
				removeClosed(v.Taker)
				removeClosed(v.Maker)
			}
			//assert.Falsef(b, len(tx.Get()) == 0, "no one deal :( order", order, "book buy", book.Get(OrderBuy))
			CloseTransaction(tx)
		case r < 18: // 6% remove
			for k := range id { // пользуемся свойством рандомности map
				tx := NewTransaction()
				b.StartTimer()
				book.Proceed(&SubCommands{Delete: &DeleteOrder{Identification: k}}, tx)
				b.StopTimer()
				delete(id, k)
				CloseTransaction(tx)
				break // на первом же элементе останавливаемся
			}
		default: // 82% change price
			for k := range id { // пользуемся свойством рандомности map
				tx := NewTransaction()
				price := NewPriceUint64(uint64((rnd.Int63n(100000) + 1) * 1000000))
				amount := NewAmount(uint64((rnd.Int63n(100000) + 1) * 100))
				b.StartTimer()
				book.Proceed(&SubCommands{Edit: &EditOrder{
					Identification: k,
					Price:          price,
					Amount:         amount,
				}}, tx)
				b.StopTimer()
				CloseTransaction(tx)
				break
			}

		}
	}
}

package matching

import (
	"sync"

	"gitlab.com/stockengine/demo/internal/proto"
)

//type Order struct {
//	Type
//	Operation
//	//CommissionType
//	Amount
//	Price
//	//Commission
//	Identification
//	//Pair
//}
//
func OrderGetStatus(order *proto.NewOrder) proto.OrderStatus {
	switch proto.AmountIsZero(&order.Amount) {
	case true:
		return proto.OrderStatus_Closed
	case false:
		return proto.OrderStatus_ClosedPartial
	default:
		panic("OMFG!!!")
	}
}

var orderPool = sync.Pool{
	New: func() interface{} {
		return &proto.NewOrder{}
	},
}

//noinspection GoUnusedExportedFunction
//func GetOrder() (o *proto.NewOrder) {
//	o = orderPool.Get().(*proto.NewOrder)
//	*o = proto.NewOrder{}
//	return
//}

func NewOrderCopy(target *proto.NewOrder) (o *proto.NewOrder) {
	o = orderPool.Get().(*proto.NewOrder)
	*o = *target
	return
}

func PutOrder(o *proto.NewOrder) {
	orderPool.Put(o)
}

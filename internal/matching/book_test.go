package matching

import (
	"testing"

	"gitlab.com/stockengine/demo/internal/proto"
	. "gitlab.com/stockengine/demo/internal/proto"
)

// задача - понять какая структура лучше всего для представление ордербук на 1-м уровне
// победитель RedBlackTree
/*
BenchmarkStdMap-8                       20000000                56.0 ns/op             0 B/op          0 allocs/op
BenchmarkStdMapViaInterface-8           20000000                74.9 ns/op             7 B/op          0 allocs/op
BenchmarkTreeMap-8                      10000000               157 ns/op              18 B/op          1 allocs/op
BenchmarkAVLTree-8                      10000000               173 ns/op              18 B/op          1 allocs/op
BenchmarkRedBlackTree-8                 10000000               155 ns/op              18 B/op          1 allocs/op
BenchmarkBTreeO3-8                       5000000               288 ns/op              44 B/op          2 allocs/op
BenchmarkBTreeO4-8                       5000000               248 ns/op              26 B/op          1 allocs/op
BenchmarkBTreeO5-8                      10000000               239 ns/op              33 B/op          1 allocs/op
BenchmarkBTreeO6-8                      10000000               221 ns/op              24 B/op          1 allocs/op
BenchmarkBTreeO7-8                      10000000               217 ns/op              28 B/op          1 allocs/op
BenchmarkBTreeO8-8                      10000000               207 ns/op              23 B/op          1 allocs/op

*/

/*
BookV1 :
BenchmarkBookAppend-8                   20000000                82.0 ns/op            40 B/op          2 allocs/op
BenchmarkProceed10OrderBook-8           10000000               152 ns/op             136 B/op          3 allocs/op
BenchmarkProceed100OrderBook-8          10000000               146 ns/op             136 B/op          3 allocs/op
BenchmarkProceed1000OrderBook-8         10000000               148 ns/op             136 B/op          3 allocs/op

BookV2 with Pool:
BenchmarkBookAppend-8                    5000000               290 ns/op             120 B/op          2 allocs/op
BenchmarkProceed10OrderBook-8            5000000               324 ns/op             216 B/op          3 allocs/op
BenchmarkProceed100OrderBook-8           5000000               331 ns/op             216 B/op          3 allocs/op
BenchmarkProceed1000OrderBook-8          5000000               317 ns/op             216 B/op          3 allocs/op
// удивительно! внутри pool используется lock/ unlock, которые просаживают производительность. маленькие объекты нет смысла хранить в пулах :(

BookV2 without Pool:
BenchmarkBookAppend-8           20000000                99.7 ns/op           120 B/op          2 allocs/op
// заменил список на односвязанный
BenchmarkBookAppend-8           20000000                92.2 ns/op           104 B/op          2 allocs/op
// заменил ордер на указатель на ордер
BenchmarkBookAppend-8           20000000                81.9 ns/op            24 B/op          2 allocs/op
// да и в документации написано что стоит делать свой собственный аллокатор
On the other hand, a free list maintained as part of a short-lived object is not a suitable use for a Pool,
since the overhead does not amortize well in that scenario.
It is more efficient to have such objects implement their own free list.

// BookLineV2Elem with pool
BenchmarkProceed0OrderBook-8             5000000               361 ns/op             444 B/op          7 allocs/op
BenchmarkProceed1OrderBook-8             5000000               369 ns/op             444 B/op          7 allocs/op
BenchmarkProceed10OrderBook-8            5000000               311 ns/op             396 B/op          5 allocs/op
BenchmarkProceed100OrderBook-8           5000000               305 ns/op             396 B/op          5 allocs/op
BenchmarkProceed1000OrderBook-8          5000000               307 ns/op             396 B/op          5 allocs/op

// BookLineV2Elem without pool
BenchmarkProceed0OrderBook-8             5000000               361 ns/op             460 B/op          7 allocs/op
BenchmarkProceed1OrderBook-8             5000000               383 ns/op             460 B/op          7 allocs/op
BenchmarkProceed10OrderBook-8            5000000               281 ns/op             412 B/op          5 allocs/op
BenchmarkProceed100OrderBook-8           5000000               297 ns/op             412 B/op          5 allocs/op
BenchmarkProceed1000OrderBook-8          5000000               313 ns/op             412 B/op          5 allocs/op

// RBTree without interface
BenchmarkProceed0OrderBook-8             5000000               315 ns/op             416 B/op          4 allocs/op
BenchmarkProceed1OrderBook-8             5000000               353 ns/op             416 B/op          4 allocs/op
BenchmarkProceed10OrderBook-8            5000000               268 ns/op             384 B/op          3 allocs/op
BenchmarkProceed100OrderBook-8           5000000               287 ns/op             384 B/op          3 allocs/op
BenchmarkProceed1000OrderBook-8          5000000               267 ns/op             384 B/op          3 allocs/op

// TxDeal with pool
BenchmarkProceed0OrderBook-8            10000000               288 ns/op             224 B/op          3 allocs/op
BenchmarkProceed1OrderBook-8             5000000               261 ns/op             224 B/op          3 allocs/op
BenchmarkProceed10OrderBook-8           10000000               228 ns/op             192 B/op          2 allocs/op
BenchmarkProceed100OrderBook-8          10000000               220 ns/op             192 B/op          2 allocs/op
BenchmarkProceed1000OrderBook-8         10000000               206 ns/op             192 B/op          2 allocs/op

// BookUserIndexID with pool
BenchmarkProceed0OrderBook-8            10000000               224 ns/op             128 B/op          2 allocs/op
BenchmarkProceed1OrderBook-8            10000000               247 ns/op             128 B/op          2 allocs/op
BenchmarkProceed10OrderBook-8           10000000               164 ns/op              96 B/op          1 allocs/op
BenchmarkProceed100OrderBook-8          10000000               158 ns/op              96 B/op          1 allocs/op
BenchmarkProceed1000OrderBook-8         10000000               160 ns/op              96 B/op          1 allocs/op

// Order with Pool (alloc var in test variable)
BenchmarkProceed0OrderBook-8            10000000               181 ns/op              32 B/op          1 allocs/op
BenchmarkProceed1OrderBook-8            10000000               179 ns/op              32 B/op          1 allocs/op
BenchmarkProceed10OrderBook-8           10000000               130 ns/op               0 B/op          0 allocs/op
BenchmarkProceed100OrderBook-8          10000000               129 ns/op               0 B/op          0 allocs/op
BenchmarkProceed1000OrderBook-8         10000000               131 ns/op               0 B/op          0 allocs/op

// добавил пул в дерево, в создание списов
BenchmarkProceed0OrderBook-8            10000000               158 ns/op               0 B/op          0 allocs/op
BenchmarkProceed1OrderBook-8            10000000               157 ns/op               0 B/op          0 allocs/op
BenchmarkProceed10OrderBook-8           10000000               135 ns/op               0 B/op          0 allocs/op
BenchmarkProceed100OrderBook-8          10000000               132 ns/op               0 B/op          0 allocs/op
BenchmarkProceed1000OrderBook-8         10000000               132 ns/op               0 B/op          0 allocs/op

// перевёл на использование классов сгенерированных gogoproto
BenchmarkProceed0OrderBook-8            20000000               115 ns/op               0 B/op          0 allocs/op
BenchmarkProceed1OrderBook-8            20000000               111 ns/op               0 B/op          0 allocs/op
BenchmarkProceed10OrderBook-8           20000000                94.2 ns/op             0 B/op          0 allocs/op
BenchmarkProceed100OrderBook-8          20000000                91.7 ns/op             0 B/op          0 allocs/op
BenchmarkProceed1000OrderBook-8         20000000                91.4 ns/op             0 B/op          0 allocs/op

// go1.13.3 & перешел на библиотеку lib/treeprice + корректно начал заполнять транзакции
BenchmarkProceed0OrderBook-8             7597041               160 ns/op              32 B/op          0 allocs/op
BenchmarkProceed1OrderBook-8             7097131               161 ns/op              32 B/op          0 allocs/op
BenchmarkProceed10OrderBook-8           12945428                89.1 ns/op             0 B/op          0 allocs/op
BenchmarkProceed100OrderBook-8          12402975                89.7 ns/op             0 B/op          0 allocs/op
BenchmarkProceed1000OrderBook-8         11693785                90.6 ns/op             0 B/op          0 allocs/op

// стал передавать копию в ProcessOrder
BenchmarkProceed0OrderBook-8             4833993               245 ns/op             272 B/op          2 allocs/op
BenchmarkProceed1OrderBook-8             4821970               245 ns/op             272 B/op          2 allocs/op
BenchmarkProceed10OrderBook-8            4871690               243 ns/op             270 B/op          1 allocs/op
BenchmarkProceed100OrderBook-8           4915536               243 ns/op             271 B/op          1 allocs/op
BenchmarkProceed1000OrderBook-8          4814740               246 ns/op             272 B/op          1 allocs/op


*/

//// тест штатной го мапы - для референтных значений
//type StdMap map[Price]interface{}
//
//func (t StdMap) Put(key interface{}, value interface{}) {
//	t[key.(Price)] = value
//}
//func (t StdMap) Get(key interface{}) (value interface{}, found bool) {
//	value, found = t[key.(Price)]
//	return
//}
//func (t StdMap) Remove(key interface{}) {
//	delete(t, key.(Price))
//}

//func BenchmarkStdMap(b *testing.B) {
//	m := make(map[Price]interface{})
//	rnd := rand.New(rand.NewSource(666))
//
//	b.ReportAllocs()
//	b.ResetTimer()
//
//	for i := 0; i < b.N; i++ {
//		op := rnd.Int31n(3)
//		k := rnd.Int31n(1000)
//		key := NewPriceUint64(uint64(k))
//		switch op {
//		case 0: // insert or update
//			m[key] = &struct{}{}
//		case 1: // delete
//			delete(m, key)
//		case 2: // read
//			_ = m[key]
//		}
//	}
//}

//type benchMap interface {
//	Put(key interface{}, value interface{})
//	Get(key interface{}) (value interface{}, found bool)
//	Remove(key interface{})
//}

//func helperBenchBook(b *testing.B, m benchMap) {
//	rnd := rand.New(rand.NewSource(666))
//
//	b.ReportAllocs()
//	b.ResetTimer()
//
//	for i := 0; i < b.N; i++ {
//		op := rnd.Int31n(3)
//		k := rnd.Int31n(1000)
//		key := NewPriceUint64(uint64(k))
//		switch op {
//		case 0: // insert or update
//			m.Put(key, &struct{}{})
//		case 1: // delete
//			m.Remove(key)
//		case 2: // read
//			_, _ = m.Get(key)
//		}
//	}
//
//}

//type benchList interface {
//	Get(index int) (interface{}, bool)
//	Remove(index int)
//	Add(values ...interface{})
//}
//
//func helperBenchList(b *testing.B, l benchList) {
//	rnd := rand.New(rand.NewSource(666))
//
//	b.ReportAllocs()
//	b.ResetTimer()
//
//	for i := 0; i < b.N; i++ {
//		op := rnd.Int31n(3)
//		switch op {
//		case 0:
//			l.Add(&struct{}{})
//		case 1:
//			_, _ = l.Get(0)
//		case 2:
//			l.Remove(0)
//		}
//	}
//
//}
//
//// тесто что б убедиться что от интерфейса ничего не пострадало
//func BenchmarkStdMapViaInterface(b *testing.B) {
//	m := StdMap(make(map[Price]interface{}))
//	helperBenchBook(b, m)
//}
//
//func helperCmp(a, b interface{}) int {
//	aa := a.(Price)
//	bb := b.(Price)
//	switch {
//	case aa.price == bb.price:
//		return 0
//	case aa.price < bb.price:
//		return -1
//	case aa.price > bb.price:
//		return 1
//	}
//	panic("OMFG its impossible")
//}
//
//func BenchmarkTreeMap(b *testing.B) {
//	m := treemap.NewWith(helperCmp)
//	helperBenchBook(b, m)
//}
//
//func BenchmarkAVLTree(b *testing.B) {
//	m := avltree.NewWith(helperCmp)
//	helperBenchBook(b, m)
//}
//
//func BenchmarkRedBlackTree(b *testing.B) {
//	m := redblacktree.NewWith(helperCmp)
//	helperBenchBook(b, m)
//}
//
//func BenchmarkBTreeO3(b *testing.B) {
//	m := btree.NewWith(3, helperCmp)
//	helperBenchBook(b, m)
//}
//func BenchmarkBTreeO4(b *testing.B) {
//	m := btree.NewWith(4, helperCmp)
//	helperBenchBook(b, m)
//}
//func BenchmarkBTreeO5(b *testing.B) {
//	m := btree.NewWith(5, helperCmp)
//	helperBenchBook(b, m)
//}
//func BenchmarkBTreeO6(b *testing.B) {
//	m := btree.NewWith(6, helperCmp)
//	helperBenchBook(b, m)
//}
//func BenchmarkBTreeO7(b *testing.B) {
//	m := btree.NewWith(7, helperCmp)
//	helperBenchBook(b, m)
//}
//func BenchmarkBTreeO8(b *testing.B) {
//	m := btree.NewWith(8, helperCmp)
//	helperBenchBook(b, m)
//}

//func BenchmarkArrayList(b *testing.B) {
//	list := arraylist.New()
//	helperBenchList(b, list)
//}
//func BenchmarkDoubleLinkedList(b *testing.B) {
//	list := doublylinkedlist.New()
//	helperBenchList(b, list)
//}
//func BenchmarkSinglyLinkedList(b *testing.B) {
//	list := singlylinkedlist.New()
//	helperBenchList(b, list)
//
//}

// with pool
// BenchmarkBookSideAppendRemove-8         20000000                60.0 ns/op            16 B/op          2 allocs/op
//
// without pool
// BenchmarkBookSideAppendRemove-8         20000000                77.2 ns/op            48 B/op          3 allocs/op
//
// with local tree without interface
// BenchmarkBookSideAppendRemove-8         50000000                29.9 ns/op             0 B/op          0 allocs/op
//
// with lib/pricetree & go1.13.3 + корректная генерация транзакций
// BenchmarkBookSideAppendRemove-8         25275639                44.1 ns/op             0 B/op          0 allocs/op

func BenchmarkBookSideAppendRemove(b *testing.B) {
	order := &NewOrder{
		Type:      Type_Limit,
		Operation: Operation_Buy,
		Amount:    NewAmount(10),
		Price:     NewPriceMust("1"),
		OwnerID:   123,
		ExtID:     567,
	}
	bs := NewBookSide(order.Operation)
	bs.AppendOrder(order, &proto.Transaction{}, order.Amount) // что бы не срабатывала очистка линии

	b.ReportAllocs()
	b.ResetTimer()
	//beg := cntBookLineElem
	for i := 0; i < b.N; i++ {
		bs.AppendOrder(order, &proto.Transaction{}, order.Amount)
		bl := bs.getLine(order.Price)
		bl.Remove()
	}
}

func helperOrderBookProceedBench(b *testing.B, ordersCnt int) {
	book := NewBook()
	orderBuy := NewOrder{
		Type:      Type_Limit,
		Operation: Operation_Buy,
		Amount:    NewAmount(10),
		Price:     NewPriceMust("10"),
		//Owner: Owner{123},
		//ExtID: ExtID{567},
	}
	orderSell := orderBuy
	orderSell.Operation = Operation_Sell

	for i := 0; i < ordersCnt; i++ {
		or := orderBuy
		tx := NewTransaction()
		book.Proceed(&SubCommands{New: &or}, tx)
		CloseTransaction(tx)
	}

	b.ReportAllocs()
	b.ResetTimer()

	//beg := cntBookLineElem
	for i := 0; i < b.N; i++ {
		var order *NewOrder
		tx := NewTransaction()
		switch i % 2 {
		case 1:
			order = NewOrderCopy(&orderBuy)
		case 0:
			order = NewOrderCopy(&orderSell)
		}
		book.Proceed(&SubCommands{New: order}, tx)
		if len(tx.TxList) == 0 {
			//b.Fatalf("empty deal :(")
		} else {
			switch tx.TxList[0].Taker.OrderStatus {
			case OrderStatus_Closed:
			case OrderStatus_ClosedPartial:
			default:
				b.Fatalf("bad order status -> %s. tx -> %+v\n", tx.TxList[0].Taker.OrderStatus, tx.TxList)
			}
			switch tx.TxList[0].Maker.OrderStatus {
			case OrderStatus_Closed:
			case OrderStatus_ClosedPartial:
			default:
				b.Fatalf("bad order status -> %s. tx -> %+v\n", tx.TxList[0].Maker.OrderStatus, tx.TxList)
			}
		}
		CloseTransaction(tx)
		PutOrder(order)
	}
}

func BenchmarkProceed0OrderBook(b *testing.B) {
	helperOrderBookProceedBench(b, 0)
}
func BenchmarkProceed1OrderBook(b *testing.B) {
	helperOrderBookProceedBench(b, 1)
}
func BenchmarkProceed10OrderBook(b *testing.B) {
	helperOrderBookProceedBench(b, 10)
}
func BenchmarkProceed100OrderBook(b *testing.B) {
	helperOrderBookProceedBench(b, 100)
}
func BenchmarkProceed1000OrderBook(b *testing.B) {
	helperOrderBookProceedBench(b, 1000)
}

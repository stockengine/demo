package matching

import (
	"testing"
)

/*
результаты бенчей какие то низкие, надо бы уже на серверах тестироваться

BenchmarkChanPool-8                              2000000               925 ns/op             208 B/op          4 allocs/op
BenchmarkGrpcMatchingEngineFakeClient1-8          100000             13159 ns/op            3376 B/op         65 allocs/op
BenchmarkGrpcMatchingEngineFakeClient3-8           20000            105792 ns/op           24980 B/op        472 allocs/op
BenchmarkGrpcMatchingEngineFakeClient10-8           1000           1123180 ns/op          263610 B/op       4819 allocs/op
BenchmarkGrpcMatchingEngineFakeClient100-8            10         115272073 ns/op        23951376 B/op     465544 allocs/op

*/

// кейс 1 - один сервер, один клиент, отправляем 1 запрос, ожидаем получить 1 ответа
func TestGrpcMatchingEngineCase1(t *testing.T) {
	//ToDo: сделать тест с проверкой отправки сделки в хранилище + бенч
}

//func getServerAddr(t testing.TB) net.Addr {
//	//run server
//	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
//	assert.NoError(t, err)
//	return addr
//}

package matching

import (
	"sync"
)

type BookUserIndex map[uint64]*BookUserIndexID //Key -> UserID
type BookUserIndexID struct {
	idx map[uint64]*BookLineV2Elem   // тут держим все ордера с Identification. Key -> ExtID
	all map[*BookLineV2Elem]struct{} //  тут все ордера без Identification
}

var bookUserIndexIDPool = sync.Pool{
	New: func() interface{} {
		return &BookUserIndexID{
			idx: make(map[uint64]*BookLineV2Elem), //Key -> ExtID
			all: make(map[*BookLineV2Elem]struct{}),
		}
	},
}

func newBookUserIndexID() (ret *BookUserIndexID) {
	ret = bookUserIndexIDPool.Get().(*BookUserIndexID)
	return
}

func deleteBookUserIndexID(bui *BookUserIndexID) {
	bookUserIndexIDPool.Put(bui)
}

func (t BookUserIndex) AlreadyExist(ownerID, extID uint64) bool {
	if ow, ok := t[ownerID]; ok {
		_, ok := ow.idx[extID]
		return ok
	}
	return false
}

func (t BookUserIndex) Append(o *BookLineV2Elem) bool {
	if o.Order().OwnerID == 0 {
		return false
	}
	if ow, ok := t[o.order.OwnerID]; ok { // есть такой пользователь
		if o.Order().ExtID != 0 { // задали extId
			if _, ok := ow.idx[o.order.ExtID]; ok {
				// уже есть с таким ID.... отказываем во вставке
				return false
			}
		}
		//нет, норм вставляем
		addToIndex(ow, o)
		return true
	}
	// нет такого пользователя, добавляем
	bui := newBookUserIndexID()
	t[o.order.OwnerID] = bui
	addToIndex(bui, o)
	return true
}

func addToIndex(id *BookUserIndexID, o *BookLineV2Elem) {
	if o.Order().ExtID != 0 {
		//добавляем по ExtID
		id.idx[o.Order().ExtID] = o
	} else {
		id.all[o] = struct{}{}
	}
}

func (t BookUserIndex) GetByID(ownerID, extID uint64) *BookLineV2Elem {
	if ow, ok := t[ownerID]; ok {
		if oi, ok := ow.idx[extID]; ok {
			return oi
		}
	}
	return nil // ну нету
}

func (t BookUserIndex) GetByOwner(owner uint64) (ret []*BookLineV2Elem) {
	if ow, ok := t[owner]; ok {
		for k := range ow.all {
			ret = append(ret, k)
		}
		for _, v := range ow.idx {
			ret = append(ret, v)
		}
	}
	return
}

func (t BookUserIndex) Remove(le *BookLineV2Elem) {
	if le.Order().OwnerID != 0 && le.Order().ExtID != 0 {
		// удаляем по id
		t.RemoveByID(le.Order().OwnerID, le.Order().ExtID)
	} else {
		t.removeByPtr(le)
	}
}

func (t BookUserIndex) RemoveByID(ownerID, extID uint64) (o *BookLineV2Elem) {
	if ow, ok := t[ownerID]; ok {
		return t.removeID(ownerID, extID, ow)
	}
	return
}

func (t BookUserIndex) removeID(ownerID, extID uint64, ow *BookUserIndexID) (o *BookLineV2Elem) {
	if oi, ok := ow.idx[extID]; ok {
		o = oi
		delete(ow.idx, extID)
		t.cleanLine(ow, ownerID)
	}
	return
}

func (t BookUserIndex) removeByPtr(le *BookLineV2Elem) {
	if ow, ok := t[le.Order().OwnerID]; ok {
		delete(ow.all, le)
	}
}

func (t BookUserIndex) cleanLine(ow *BookUserIndexID, ownerID uint64) {
	if len(ow.idx) == 0 && len(ow.all) == 0 { //удалили последний, чистим всю линию
		delete(t, ownerID)
		deleteBookUserIndexID(ow)
	}
}

type BookIndexOwnerFunc func(ble *BookLineV2Elem)

func (t BookUserIndex) RemoveByOwner(owner uint64, fb BookIndexOwnerFunc) {
	if ow, ok := t[owner]; ok {
		//сначала удаляем все с ExtID
		for _, v := range ow.idx {
			fb(v)
			// нет необходимости удалять поштучно - удалим всю мапу
			//t.removeID(Identification{
			//	Owner: owner,
			//	ExtID: extId,
			//}, ow)
		}
		// а теперь и без ExtID
		for le := range ow.all {
			fb(le)
		}
		delete(t, owner)
	}
}

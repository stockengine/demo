package matching

import (
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/stockengine/demo/internal/proto"
)

func TestOrderCopy(t *testing.T) {
	order := NewOrder{
		Type:      Type_Limit,
		Operation: Operation_Buy,
		Amount:    NewAmount(10),
		Price:     NewPriceMust("1"),
		OwnerID:   123,
		ExtID:     567,
	}
	orderCopy := order

	orderCopy.Operation = Operation_Sell
	orderCopy.Amount.Byte3 = 1

	assert.Equal(t, order, NewOrder{
		Type:      Type_Limit,
		Operation: Operation_Buy,
		Amount:    NewAmount(10),
		Price:     NewPriceMust("1"),
		OwnerID:   123,
		ExtID:     567,
	})
}

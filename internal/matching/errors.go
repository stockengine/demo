package matching

import (
	"errors"
	"fmt"

	"gitlab.com/stockengine/demo/internal/proto"
)

type ErrorOrdersExists struct{}

func (e ErrorOrdersExists) Error() string {
	return "ErrorOrdersExists"
}

type ErrorOrderAndTxMismatch struct {
	ctx proto.CommitedTransaction
	tx  proto.Transaction
}

func (e *ErrorOrderAndTxMismatch) Error() string {
	return fmt.Sprintf("Order and assotiated transaction mismatch. ctx: [%+v] tx: [%+v]", e.ctx, e.tx)
}

var ErrorShutdownInProcess = errors.New("ErrorShutdownInProcess")

package matching

import (
	"sort"

	"gitlab.com/stockengine/demo/internal/lib/subcommand"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/pricetree"

	"gitlab.com/stockengine/demo/internal/proto"
)

type Book struct {
	buy, sell     *BookSide
	indexByUserID BookUserIndex
}

// its an order book side - bid or ask
type BookSide struct {
	PriceList pricetree.Tree
}

func NewBook() *Book {
	return &Book{
		buy:           NewBookSide(proto.Operation_Buy),
		sell:          NewBookSide(proto.Operation_Sell),
		indexByUserID: BookUserIndex{},
	}
}

func NewBookSide(op proto.Operation) *BookSide {
	// buy - начинается с самой высокой цены (я прихожу продавать и продаю самым выгодным - дорогим) => с инверсией
	// sell - начинается с самой низкой (я прихожу покупать и покупаю самых выгодных - самых дешёвых) => без инверсии
	return &BookSide{
		PriceList: *pricetree.NewTree(op == proto.Operation_Buy),
	}
}

func (t *Book) Proceed(cmd *proto.SubCommands, tx *proto.Transaction) {
	*tx = proto.Transaction{ //store original command
		Command: proto.Command{
			SubCommands: subcommand.NewSubCommand(*cmd),
			ReqID:       tx.Command.ReqID,
		},
	} //NewTransaction()
	switch {
	case cmd.New != nil:
		t.ProceedOrder(cmd.New, tx)
	case cmd.Delete != nil:
		t.DeleteOrder(cmd.Delete.OwnerID, cmd.Delete.ExtID, tx)
	case cmd.Edit != nil:
		t.EditOrder(cmd.Edit, tx)
	default:
		panic("OMFG!!!111 oneof dont works")
	}
}

// поддержаны операции:
// - доабвление ордера и проведение по сделкам
// матчинг ордера по встречному стакану
func (t *Book) ProceedOrder(order *proto.NewOrder, tx *proto.Transaction) {
	//validate order
	switch {
	case order.Price.Price == 0:
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredInvalidPrice
		return
	case proto.AmountIsZero(&order.Amount):
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredInvalidAmount
		return
	}

	var proceedPtr func(*proto.NewOrder, *proto.Transaction, *BookUserIndex)
	var appendPtr func(*proto.NewOrder, *proto.Transaction, proto.Amount) *BookLineV2Elem

	switch op := order.Operation; op {
	case proto.Operation_Buy:
		proceedPtr = t.sell.ProceedOrder
		appendPtr = t.buy.AppendOrder
	case proto.Operation_Sell:
		proceedPtr = t.buy.ProceedOrder
		appendPtr = t.sell.AppendOrder
	default:
		panic("OMFG... unknown proto.Operation")
	}

	// если ордер с таким индексом уже есть, то отказываем в обработке
	if t.inIndex(order) {
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredDublicateId
		return
	}

	var initialAmount = order.Amount
	proceedPtr(order, tx, &t.indexByUserID)
	le := appendPtr(order, tx, initialAmount)
	// в индекс добавляем только если сделка прошла
	if le != nil {
		t.indexAdd(le)
	}
}

func (t *Book) Iterator(op proto.Operation) pricetree.Iterator {
	switch op {
	case proto.Operation_Buy:
		return t.buy.Iterator()
	case proto.Operation_Sell:
		return t.sell.Iterator()
	default:
		panic("OMFG!!!")
	}
}

func (t *Book) Get(op proto.Operation) (ret []proto.NewOrder) {
	it := t.Iterator(op)
	for it.Next() {
		bl := it.Value().(*BookLineV2)
		blIt := bl.Iterator()
		for blIt.Next() {
			or := blIt.Value()
			ret = append(ret, *or)
		}
	}
	return
}

func (t *BookSide) closeBookLine(opPrice proto.Price, bl *BookLineV2) {
	t.PriceList.Remove(opPrice) // убираем что б больше не спотыкаться о неё
	CloseBookLine(bl)
}

func (t *BookSide) getLine(price proto.Price) *BookLineV2 {
	bl, ok := t.PriceList.Get(price)
	if !ok {
		return nil
	}
	return bl.(*BookLineV2)
}

func (t *BookSide) ProceedOrder(order *proto.NewOrder, tx *proto.Transaction, idx *BookUserIndex) {
	//идём и торгуемся
	for it := t.PriceList.Iterator(); it.Next() && !proto.AmountIsZero(&order.Amount); {
		OpPrice, bl := it.Key(), it.Value().(*BookLineV2)
		// и так, суть сделки
		// - если ордер в стакане имеет лучшую или такую же цену как наш, то
		// - закрываем наш ордер на сумму ордера в стакане и если наш ордер ещё не нулевой, то повторяем
		// если ордер в стакане хуже, то кладём наш ордер в наш стакан
		switch cmp := t.PriceList.Comparator(order.Price, OpPrice); cmp {
		case 1, 0:
			// ордер в стакане такой же или лучше чем наш
			//вычитаем (с насыщением) из нашего ордера сумму ордера стакана и фиксируем эту транзакцию
			// далее смотрим кто обнулился - наш ордер или ордер стакана:
			// -- если наш, то завершаемся
			// -- если в стакане, то удаляем из стакана и продолжаем
			for !proto.AmountIsZero(&order.Amount) {
				OpLineElemPtr, found := bl.Get()
				if !found { // аварийная ситуация, пустая линия, в нормальных условиях не возможно
					logger.Log.Warn("Garbage detected... :(", zap.String("price", proto.PriceToString(OpPrice)))
					t.closeBookLine(OpPrice, bl)
					break // переходим к следующему элементу
				}
				deal := proto.Amount{} // пул вроде бы нет смысла использовать
				proto.AmountSubTillZero(&order.Amount, &OpLineElemPtr.Order().Amount, &deal)

				txDeal := proto.TxDeal{
					Maker: proto.OrderChange{
						MarketSide:   proto.MarketSide_Maker,
						AmountChange: deal,
						OrderStatus:  OrderGetStatus(OpLineElemPtr.Order()),
						DealPrice:    OpPrice,
						Order:        *OpLineElemPtr.Order(),
					},
					Taker: proto.OrderChange{
						MarketSide:   proto.MarketSide_Taker,
						AmountChange: deal,
						OrderStatus:  OrderGetStatus(order),
						DealPrice:    OpPrice,
						Order:        *order,
					},
				}
				tx.TxList = append(tx.TxList, txDeal)

				// эта функция после сделки только по причине необходимости прервать цикл при удалении последнего элемента
				// окей, "кончился" встречный ордер, удаляем его из стакана
				if proto.AmountIsZero(&OpLineElemPtr.Order().Amount) {
					bl.Remove()
					//чистим индексы
					idx.Remove(OpLineElemPtr)
					//чистим мусор - вдруг ордер был последний на линии
					if bl.Empty() {
						//ToDo: это грязный фикс, стоит оценить производительность и переделать на нормальный, например заново обходить дерево и удалять пустые первые элементы
						defer t.closeBookLine(OpPrice, bl) // откладываем удаление из книги до завершения обработки, а то ломается итератор
						break                              // переходим на следующую линию
					}
				}
			}
		case -1:
			// ордер в стакане хуже чем наш, сделки не будет
			return
		default:
			panic("OMFG... impossible case")
		}
	}
}

// добавление ордера в свой стакан
func (t *BookSide) AppendOrder(order *proto.NewOrder, tx *proto.Transaction, initialAmount proto.Amount) (le *BookLineV2Elem) {
	// сохраняем финальное состояние ордераю сейчас записываем финальный баланс, а статус определим чуть ниже
	tx.OrderFinalState.AmountFinal = order.Amount

	cmpAmountChange := proto.AmountCmp(&initialAmount, &order.Amount)

	//анализируем и выставляем статус
	switch {
	case proto.AmountIsZero(&order.Amount):
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_Closed
		return
	case cmpAmountChange > 0:
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_ClosedPartial
	case cmpAmountChange == 0:
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_ToBook
	default:
		panic("OMFG!!!111 impossible case")
	}

	// проверяем наличие записей с такой же ценой
	bookLine := t.getLine(order.Price)
	switch bookLine != nil {
	case true: // да, с такой ценой уже есть записи
		// подписываем в конец списка
		le = bookLine.Append(order)
	case false: // нет, с такой ценой нет. добавляем
		bl := NewBookLine()
		le = bl.Append(order)
		t.PriceList.Put(order.Price, bl)
	}

	return
}

func (t *BookSide) Iterator() pricetree.Iterator {
	return t.PriceList.Iterator()
}

func (t *BookSide) PrintLog() {
	it := t.PriceList.Iterator()
	for it.Next() {
		price := it.Key()
		bl := it.Value().(*BookLineV2)
		logger.Log.Debug("", zap.String("price", proto.PriceToString(price)))
		blIt := bl.Iterator()
		for i := 0; blIt.Next(); i++ {
			order := blIt.Value()
			logger.Log.Debug("", zap.Reflect("order", order))
		}
	}
}

func (t *Book) PrintLog() {
	logger.Log.Debug("buy size")
	t.buy.PrintLog()

	logger.Log.Debug("sell size")
	t.sell.PrintLog()
}

func (t *Book) DeleteOrder(ownerID, extID uint64, tx *proto.Transaction) {
	switch {
	case ownerID != 0 && extID != 0: // try to delete concrete order
		t.DeleteOrderByID(ownerID, extID, tx)
	case ownerID != 0: //delete all owner orders
		t.DeleteOrderByOwner(ownerID, tx)
	default: // invalid command
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredInvalidIdentification
	}
}

// удаление конкретного ордера у конкретного пользователя
func (t *Book) DeleteOrderByID(ownerID, extID uint64, tx *proto.Transaction) {
	le := t.indexByUserID.RemoveByID(ownerID, extID)
	if le == nil {
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredIdentificationNotFound
		return
	}
	t.deleteByLe(le)
	tx.OrderFinalState = proto.OrderState{
		AmountFinal: le.order.Amount,
		OrderStatus: proto.OrderStatus_Cancel,
	}
	tx.TxList = append(tx.TxList, proto.TxDeal{
		Maker: proto.OrderChange{
			MarketSide:  proto.MarketSide_Maker,
			OrderStatus: proto.OrderStatus_Cancel,
			Order:       *le.order,
		},
	})
}

func (t *Book) DeleteOrderByOwner(owner uint64, tx *proto.Transaction) {
	t.indexByUserID.RemoveByOwner(owner, func(ble *BookLineV2Elem) {
		t.deleteByLe(ble)
		tx.TxList = append(tx.TxList, proto.TxDeal{
			Maker: proto.OrderChange{
				MarketSide:  proto.MarketSide_Maker,
				OrderStatus: proto.OrderStatus_Cancel,
				Order:       *ble.order,
			},
		})
	})
	switch len(tx.TxList) {
	case 0:
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredIdentificationNotFound
	default:
		//задолбался что автотесты спонтанно дают ошибку т.к. порядок удаления не детерминирован. упорядочил
		sort.Slice(tx.TxList, func(i, j int) bool {
			iElem, jElem := &tx.TxList[i].Maker.Order, &tx.TxList[j].Maker.Order
			return iElem.ExtID < jElem.ExtID ||
				iElem.Price.Price < jElem.Price.Price ||
				proto.AmountCmp(&iElem.Amount, &jElem.Amount) == -1 ||
				iElem.Type < jElem.Type
		})
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_Cancel
	}
}

func (t *Book) deleteByLe(le *BookLineV2Elem) {
	bs := t.getBookSide(le.Order().Operation)
	bl := bs.getLine(le.Order().Price)
	bl.delete(le)
	if bl.Empty() {
		bs.closeBookLine(le.Order().Price, bl)
	}
}

func (t *Book) getBookSide(op proto.Operation) *BookSide {
	switch op {
	case proto.Operation_Buy:
		return t.buy
	case proto.Operation_Sell:
		return t.sell
	default:
		panic("OMFG!!! unknown op")
	}
}

//Todo: бенч
func (t *Book) EditOrder(change *proto.EditOrder, tx *proto.Transaction) {
	switch {
	case change.OwnerID == 0 || change.ExtID == 0:
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredInvalidIdentification
		return
	case change.NewPrice.Price == 0 && proto.AmountIsZero(&change.NewAmount):
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredInvalidAmountPrice
		return
	}

	le := t.indexByUserID.GetByID(change.OwnerID, change.ExtID)
	if le == nil {
		tx.OrderFinalState.OrderStatus = proto.OrderStatus_IgnoredIdentificationNotFound
		return
	}

	//всё хорошо, можно редактировать ордер
	changeAmount := !proto.AmountIsZero(&change.NewAmount) && proto.AmountCmp(&change.NewAmount, &le.order.Amount) != 0
	changePrice := change.NewPrice.Price != 0 && change.NewPrice.Price != le.order.Price.Price
	txDeal := proto.TxDeal{
		Maker: proto.OrderChange{
			MarketSide:   proto.MarketSide_Maker,
			AmountChange: le.order.Amount, // старый объём
			DealPrice:    le.order.Price,  // старая цена
		},
	}

	if changePrice {
		//удаляем ордер и заново заносим в стакан. финальный статус будет установлен proceedorder
		// первая часть - удаление
		t.DeleteOrderByID(change.OwnerID, change.ExtID, tx)
	}

	// обновляем состояние ордера (он уже исключен из своего дерева, можно править прямо по указателю
	var finalStatus proto.OrderStatus
	switch {
	case changeAmount && changePrice:
		finalStatus = proto.OrderStatus_ChangedAmountPrice
		le.order.Price = change.NewPrice
		le.order.Amount = change.NewAmount
	case changeAmount:
		finalStatus = proto.OrderStatus_ChangedAmount
		le.order.Amount = change.NewAmount
	case changePrice:
		finalStatus = proto.OrderStatus_ChangedPrice
		le.order.Price = change.NewPrice
	}

	txDeal.Maker.Order = *le.order
	txDeal.Maker.OrderStatus = finalStatus
	tx.TxList = append(tx.TxList, txDeal)

	if changePrice {
		//удаляем ордер и заново заносим в стакан. финальный статус будет установлен proceedorder
		// вторая часть - запись заново
		t.ProceedOrder(le.order, tx)
	} else {
		tx.OrderFinalState.AmountFinal = le.order.Amount
		tx.OrderFinalState.OrderStatus = finalStatus
	}
}

func (t *Book) Size() int {
	return t.buy.PriceList.Size() + t.sell.PriceList.Size()
}

//ToDo:
// и пакетные операции - их любят маркет-мейкеры
// - удалить массив ордеров
// - отредактировать массив ордеров

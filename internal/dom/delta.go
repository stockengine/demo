package dom

import (
	"fmt"
	"sort"

	"gitlab.com/stockengine/demo/internal/proto"
)

type DeltaLog struct {
	proto.DOMDelta
}

type internalDeltaMap map[proto.Price]internalDeltaMapElem
type internalDeltaMapElem struct {
	Count  uint64
	Amount proto.Amount
	Type   proto.DOMDeltaType
}

// херовая реализация... слишком много частных случаев... когда-нибудь потом надо будет что-то получше придумать, а пока это не создаёт проблем
func NewDeltaLog(book *proto.CommitedTransaction) (ret *DeltaLog) {
	switch {
	case book.Transaction.Command.SubCommands.New != nil:
		return NewDeltaLogNew(book)
	case book.Transaction.Command.SubCommands.Edit != nil:
		return NewDeltaLogEdit(book)
	case book.Transaction.Command.SubCommands.Delete != nil:
		return NewDeltaLogDelete(book)
	default:
		panic(fmt.Sprintf("OMFG!!! book.Command -> %+v", book.Transaction.Command.SubCommands))
	}
}

func initDeltaMap() []internalDeltaMap {
	deltaMap := make([]internalDeltaMap, len(proto.Operation_name)-1) //да это магия
	for i := range deltaMap {
		deltaMap[i] = make(internalDeltaMap)
	}
	return deltaMap
}

func initDeltaLog(commitID uint64, deltaMap []internalDeltaMap) (ret *DeltaLog) {
	ret = &DeltaLog{
		DOMDelta: proto.DOMDelta{
			CommitID: commitID,
			Bid:      nil,
			Ask:      nil,
		},
	}

	for side, sideMap := range deltaMap {
		var ptrSide *[]proto.DOMDeltaElem
		switch proto.Operation(side + int(proto.Operation_Buy)) { //проклятая черная магия
		case proto.Operation_Buy:
			ptrSide = &ret.Ask
		case proto.Operation_Sell:
			ptrSide = &ret.Bid
		default:
			panic(fmt.Sprintf("OMFG!!!111 unknown operation [%s]", proto.Operation(side).String()))
		}
		*ptrSide = make([]proto.DOMDeltaElem, 0, len(sideMap)) // make preallocation
		for price, delta := range sideMap {
			*ptrSide = append(*ptrSide, proto.DOMDeltaElem{
				DOMOrdersLine: proto.DOMLine{
					Count:  delta.Count,
					Price:  price,
					Amount: delta.Amount,
				},
				Type: delta.Type,
			})
		}
		sort.Slice(*ptrSide, func(i, j int) bool {
			return (*ptrSide)[i].DOMOrdersLine.Price.Price > (*ptrSide)[j].DOMOrdersLine.Price.Price
		})
	}

	return
}

func DeltaMapOperationIndex(op proto.Operation) int {
	switch op {
	case proto.Operation_Buy:
		return 0
	case proto.Operation_Sell:
		return 1
	default:
		panic(fmt.Sprintf("OMFG!!!111 unknown operation [%s]", op.String()))
	}
}

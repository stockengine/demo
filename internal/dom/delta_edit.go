package dom

import (
	"fmt"

	"gitlab.com/stockengine/demo/internal/proto"
)

func NewDeltaLogEdit(book *proto.CommitedTransaction) *DeltaLog {
	//идём по массиву закоммиченных транзакций и формируем дельту стакана
	deltaMap := initDeltaMap()

	// отбрасываем пустые случаи
	switch book.Transaction.OrderFinalState.OrderStatus {
	case proto.OrderStatus_IgnoredIdentificationNotFound,
		proto.OrderStatus_IgnoredInvalidIdentification,
		proto.OrderStatus_IgnoredInvalidAmount,
		proto.OrderStatus_IgnoredInvalidPrice,
		proto.OrderStatus_IgnoredInvalidAmountPrice:
		return nil
	}

	// возможны следующие варианты - редактирование суммы, редактирование цены, редактирование цены и суммы
	// к сожалению, проверки не алгоритмические...
	// 0 - удаление (если менялась цена)
	// 1 - изменение параметров ордера - объём и цена
	// 2 - возмоные сделки или ToBook если менялась цена

	var workIndex int

	if workIndex >= len(book.Transaction.TxList) {
		panic(fmt.Sprintf("OMFG!!! %d >= %d", workIndex, len(book.Transaction.TxList)))
	}

	//case 0
	if book.Transaction.TxList[workIndex].Maker.OrderStatus == proto.OrderStatus_Cancel {
		// это фиктивная отмена, просто пропускаем эту запись
		workIndex++
	}

	//case 1
	order := book.Transaction.TxList[workIndex].Maker
	oldAm := order.AmountChange // старое состояние ордера
	newAm := order.Order.Amount // новое состояние ордера

	switch order.OrderStatus {
	case proto.OrderStatus_ChangedAmount: // делает + или - к конкретной строке
		delta := internalDeltaMapElem{}
		cmp := proto.AmountCmp(&newAm, &oldAm)
		switch cmp {
		case -1: //amount dec => oldAm > newAm
			delta.Amount = oldAm
			proto.AmountSub(&delta.Amount, &newAm)
			delta.Type = proto.DOMDeltaType_Sub
		case 1: //amount inc => newAm > oldAm
			delta.Amount = newAm
			proto.AmountSub(&delta.Amount, &oldAm)
			delta.Type = proto.DOMDeltaType_Add
		default: //принцип нулевой терпимости к ошибкам. конкретно эту ситуацию можно было бы и обработать, но её не надо допускать впринципе
			panic(fmt.Sprintf("OMFG!!!111 bad testing, bad project control... its necessary to rewrite code for stupid :( cmp == %d", cmp))
		}
		deltaMap[DeltaMapOperationIndex(order.Order.Operation)][order.DealPrice] = delta
	case proto.OrderStatus_ChangedAmountPrice, // делаем "-" на старую сумму к старой строке и "+" на всю сумму к новой строке
		proto.OrderStatus_ChangedPrice: // делаем "-" на всю сумму к старой строке и "+" на всю сумму к новой строке
		// плюс не делаем т.к. его потом сделает обработка сделок
		oldPrice := order.DealPrice
		deltaMap[DeltaMapOperationIndex(order.Order.Operation)][oldPrice] = internalDeltaMapElem{
			Count:  1,
			Amount: oldAm,
			Type:   proto.DOMDeltaType_Sub,
		}
		//костыль...
		if workIndex+1 >= len(book.Transaction.TxList) { //мда... торгов то и не было, добавляем ручками ордер
			newPrice := order.Order.Price
			deltaMap[DeltaMapOperationIndex(order.Order.Operation)][newPrice] = internalDeltaMapElem{
				Count:  1,
				Amount: newAm,
				Type:   proto.DOMDeltaType_Add,
			}
		}
	default:
		panic(fmt.Sprintf("OMFG!!! status[%s]", book.Transaction.TxList[workIndex].Maker.OrderStatus))
	}

	workIndex++
	if workIndex >= len(book.Transaction.TxList) {
		return initDeltaLog(book.CommitID, deltaMap)
	}

	//ToDo: stupid slow path :( optimize it
	//case 2 пробуем поторговать
	// убираем уже обработанные транзакции
	book.Transaction.TxList = book.Transaction.TxList[workIndex:]
	book.Transaction.Command.SubCommands.New = &proto.NewOrder{}
	*book.Transaction.Command.SubCommands.New = order.Order
	// и отправляем на повторную обработку
	matchLog := NewDeltaLogNew(book)

	//сначала идут операции по редактированию, а то при обратном порядке при обработке может быть уход в минус
	firstLog := initDeltaLog(book.CommitID, deltaMap)
	firstLog.Ask = append(firstLog.Ask, matchLog.Ask...)
	firstLog.Bid = append(firstLog.Bid, matchLog.Bid...)

	return firstLog
}

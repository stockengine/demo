# DOM = Depth of Market 

https://www.investopedia.com/terms/d/depth-of-market.asp

Стакан с анонимными и обобщёнными ордерами. Сервис выполняет хранение, агрегацию и выдачу стакана по запросу. Сервис высоконагруженный, многопоточный.

Подписывается на поток подтверждённых сделок от storage, предоставляет grpc для запросов.
Одновременно может быть запущено множество dom. 

Какого-либо слоя постоянного хранения не предусмотрено - при старте вычитывает всё из storage, при потере соединения со storage прекращает работу.

## внутренняя структура обработки потока

```mermaid
graph LR
subgraph
se.storage--> |new commited tx| se.dom
se.dom-->deltaDook
deltaBook-->addToFullBook
addToFullBook--> |PreparedBook| SubscribeBook
deltaBook-->addToShortBook
addToShortBook-->|PreparedShortBook|SubscribeShortBook
deltaBook-->prepareDelta
prepareDelta-->|PreparedDelta|SubscribeDelta
```

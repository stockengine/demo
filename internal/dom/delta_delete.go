package dom

import (
	"gitlab.com/stockengine/demo/internal/proto"
)

func NewDeltaLogDelete(book *proto.CommitedTransaction) (ret *DeltaLog) {
	deltaMap := initDeltaMap()

	for _, change := range book.Transaction.TxList {
		order := change.Maker
		if order.OrderStatus != proto.OrderStatus_Cancel {
			panic("OMFG!!!111")
		}
		delta := deltaMap[DeltaMapOperationIndex(order.Order.Operation)][order.Order.Price]
		delta.Count++
		delta.Type = proto.DOMDeltaType_Sub
		proto.AmountAdd(&delta.Amount, &order.Order.Amount)
		deltaMap[DeltaMapOperationIndex(order.Order.Operation)][order.Order.Price] = delta
	}

	return initDeltaLog(book.CommitID, deltaMap)
}

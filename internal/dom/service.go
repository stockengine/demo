package dom

import (
	"context"
	"net"
	"sync"
	"sync/atomic"
	"time"

	prometheus2 "github.com/prometheus/client_golang/prometheus"
	"gitlab.com/stockengine/demo/internal/lib/prometheus"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/forwarder"

	"gitlab.com/stockengine/demo/internal/lib/pricetree"

	"gitlab.com/stockengine/demo/internal/proto"
	"google.golang.org/grpc"
)

type Options struct {
	StorageServiceHostPort string
	BindHostPort           string
	FullUpdateInterval     time.Duration
	Top100UpdateInterval   time.Duration
	DeltaBuffer            int
	UpdateBuffer           int
	PrometheusBind         string
}

type Engine struct {
	opts    Options
	bookInt internalDOMBook

	connStorage *grpc.ClientConn

	clientStorage     proto.StorageEngineClient
	methodSubscribeTx proto.StorageEngine_SubscribeTxClient
	cancelSubscribeTx context.CancelFunc

	cancelLoadTx context.CancelFunc // для отмены загрузки при старте

	lis net.Listener
	srv *grpc.Server

	ctxChan chan *proto.CommitedTransaction
	wg      sync.WaitGroup
	quit    chan struct{}

	fullDOMSubscribers, shortDOMSubscribers, deltaSubscribers *forwarder.OneToMany

	prom               *prometheus.Prometheus
	promBookIntCommits prometheus2.GaugeVec
	promBookIntSize    prometheus2.GaugeVec
	promBookIntPrices  prometheus2.GaugeVec
	promTxReceived     prometheus2.Counter
	promDomProcessing  prometheus2.HistogramVec
	promDeltaLogCnt    prometheus2.CounterVec
}

// только для корректности с автотестами
func initIntBook() internalDOMBook {
	return internalDOMBook{
		Bid: pricetree.NewTree(true),
		Ask: pricetree.NewTree(false),
	}
}

func NewEngine(opts Options) (*Engine, error) {
	forwarderOpts := forwarder.OneToManyOptions{
		ChannelSize:        opts.DeltaBuffer,
		DropSubscribersMsg: true,
		ReportLock:         true,
	}

	prom, err := prometheus.New(opts.PrometheusBind)
	if err != nil {
		return nil, err
	}

	ret := &Engine{
		opts:                opts,
		ctxChan:             make(chan *proto.CommitedTransaction, opts.UpdateBuffer),
		bookInt:             initIntBook(),
		fullDOMSubscribers:  forwarder.NewOneToMany(forwarderOpts),
		shortDOMSubscribers: forwarder.NewOneToMany(forwarderOpts),
		deltaSubscribers:    forwarder.NewOneToMany(forwarderOpts),
		quit:                make(chan struct{}),
		prom:                prom,
		promBookIntCommits:  prom.NewGaugeVec("commitID", "last commit ID", nil),
		promBookIntSize:     prom.NewGaugeVec("size", "book size (in lines count", []string{"side"}),
		promBookIntPrices:   prom.NewGaugeVec("prices", "book top & bottom prices by side", []string{"side", "position"}),
		promTxReceived:      prom.NewCounter("TxReceived", "count of received tx"),
		promDomProcessing:   prom.NewHistogramVec("processing", "dom processing latency", []string{"operation"}, prometheus.DefBucketsLowLat),
		promDeltaLogCnt:     prom.NewCounterVec("DeltaLogCnt", "order count changes", []string{"side", "direction"}),
	}

	ret.wg.Add(1)
	go ret.addToBookConsumer()
	//ret.wg.Add(1)
	//go ret.reportFullDOM() //disabled besause it may be too big and no one exchange shows full DOM
	ret.wg.Add(1)
	go ret.reportShortDOM()
	ret.prometheusReporter()

	if err := ret.load(opts.StorageServiceHostPort); err != nil {
		return nil, err
	}
	if err := ret.bind(opts.BindHostPort); err != nil {
		return nil, err
	}
	return ret, nil
}

func (t *Engine) load(hostPort string) (err error) {
	//connect to storage server. its prerequsite
	t.connStorage, err = config.GRPCClientDial(hostPort, true)
	if err != nil {
		return
	}
	t.clientStorage = proto.NewStorageEngineClient(t.connStorage)
	//ToDo: stockengine/demo#24 продумать одновременную загрузку ордеров вместе с приёмом новых сделок (storage будет скипать при медленном приёме)

	if err = t.loadHistorical(); err != nil {
		return
	}

	if err = t.connectToNew(); err != nil {
		return
	}

	return
}

func (t *Engine) Stop() {
	logger.Log.Debug("shutdown goroutines...")
	start := time.Now()

	t.cancelLoadTx() // если что-то загружалось, а нас попросили остановиться, значит можно остановиться
	t.cancelSubscribeTx()
	close(t.quit) // останавливаем воркеры
	//останавливаем сервера с подписчиками
	t.shortDOMSubscribers.Close()
	t.fullDOMSubscribers.Close()
	t.deltaSubscribers.Close()
	t.srv.GracefulStop()

	t.wg.Wait()
	close(t.ctxChan) //bad :( need more checks

	logger.ErrorLogHelper(t.connStorage.Close(), "connStorage.Stop()")

	elapsed := time.Since(start)
	logger.Log.Debug("shutdown done", zap.Duration("elapsed", elapsed))
	if err := t.prom.Close(); err != nil {
		logger.Log.Error("prometheus close", zap.Error(err))
	}
}

func (t *Engine) loadHistorical() (err error) {
	var ctx context.Context
	ctx, t.cancelLoadTx = context.WithCancel(context.Background())
	ltx, err := t.clientStorage.LoadTx(ctx, &proto.EmptyMessage{})
	if err != nil {
		return err
	}
	defer t.cancelLoadTx()

	//ToDo: такой хелпер уже в 2-3 местах встречается, надо бы вынести в общую библиотеку
	logger.Log.Info("orders load started")
	start := time.Now()
	var loaded int64
	quit := make(chan struct{})
	defer close(quit) // dont waste
	go func() {       // reporting routine
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		var old int64
		for {
			select {
			case <-ticker.C:
				cnt := atomic.LoadInt64(&loaded)
				delta := cnt - old
				old = cnt
				logger.Log.Info("loading...", zap.Int64("total orders", cnt), zap.Int64("orders per second", delta))
			case <-quit:
				return
			}
		}
	}()

EndlessLoop:
	for {
		ctx, errLoc := ltx.Recv()
		needBreake, isGood := logger.ErrorLogHelper2(errLoc, "loadTx.Recv")
		switch {
		case needBreake && isGood:
			break EndlessLoop
		case needBreake && !isGood:
			err = errLoc
			break EndlessLoop
		default:
			t.addToBook(ctx)
			_ = atomic.AddInt64(&loaded, 1)
		}
	}
	logger.Log.Info("loading complete", zap.Int64("total loaded", loaded), zap.Duration("elapsed", time.Since(start)))

	return err
}

func (t *Engine) connectToNew() (err error) {
	var ctx context.Context
	ctx, t.cancelSubscribeTx = context.WithCancel(context.Background())
	t.methodSubscribeTx, err = t.clientStorage.SubscribeTx(ctx, &proto.EmptyMessage{})
	if err != nil {
		return
	}
	t.wg.Add(1)
	go t.newConsumer()
	return nil
}

func (t *Engine) newConsumer() {
	defer t.wg.Done()

EndlessLoop:
	for {
		ctx, err := t.methodSubscribeTx.Recv()
		if logger.ErrorLogHelper(err, "methodSubscribeTx.Recv()") {
			break EndlessLoop
		}
		t.addToBook(ctx)
		t.promTxReceived.Inc() //optimized for a very hot path
	}
}

func (t *Engine) bind(hostPort string) (err error) {
	t.lis, t.srv, err = config.CreateGRPCListen(hostPort)
	if err != nil {
		logger.Log.Error("CreateGRPCListen", zap.Error(err))
		return
	}

	proto.RegisterDOMServer(t.srv, t)
	t.wg.Add(1)
	go func() {
		defer t.wg.Done()
		err := t.srv.Serve(t.lis)
		logger.ErrorLogHelper(err, "grpc serve")
	}()

	return
}

//DRY
func helperTickerHandler(interval time.Duration, wg *sync.WaitGroup, name string, quit chan struct{}, fn func(), prom prometheus2.HistogramVec) {
	defer wg.Done()

	if interval == 0 {
		logger.Log.Warn("update interval is zero. So stop update routine", zap.String("name", name))
		return
	}
	ticker := time.NewTicker(interval)
	defer ticker.Stop()
L:
	for {
		select {
		case <-ticker.C:
			start := time.Now()
			fn()
			elapsed := time.Since(start)
			prom.WithLabelValues(name).Observe(elapsed.Seconds())
		case <-quit:
			break L
		}
	}
}

//func (t *Engine) reportFullDOM() {
//	helperTickerHandler(t.opts.FullUpdateInterval, &t.wg, "FullDOM", t.quit, func() {
//		dom := t.bookInt.GetDOMBook(-1)
//		t.fullDOMSubscribers.SendEvent(dom) // escape to heap?
//	}, t.promDomProcessing)
//}

func (t *Engine) reportShortDOM() {
	helperTickerHandler(t.opts.Top100UpdateInterval, &t.wg, "ShortDOM", t.quit, func() {
		dom := t.bookInt.GetDOMBook(100)
		t.shortDOMSubscribers.SendEvent(dom) // escape to heap?
	}, t.promDomProcessing)
}

//DRY
func helperSubscribe(ctx context.Context, method string, fw *forwarder.OneToMany, quit chan struct{}, fn func(msg interface{}) error) error {
	log := logger.LogNewConnect(ctx, method)
	ch := fw.NewSubscriber()
	defer fw.RemoveSubscriber(ch)
L:
	for {
		select {
		case msg, ok := <-ch: // при закрытии сокета срабатывает эта ветка с ok = false
			switch {
			case !ok:
				break L
			case logger.ErrorLogHelper3(fn(msg), "book.Send", log):
				break L
			}
		case <-quit:
			break L
		}
	}
	return nil
}

//func (t *Engine) SubscribeBook(message *proto.EmptyMessage, book proto.DOM_SubscribeBookServer) error {
//	return helperSubscribe(book.Context(), "SubscribeBook", t.fullDOMSubscribers, t.quit, func(msg interface{}) error {
//		domBook := msg.(*proto.DOMBook)
//		return book.Send(domBook)
//	})
//}

func (t *Engine) SubscribeShortBook(message *proto.EmptyMessage, book proto.DOM_SubscribeShortBookServer) error {
	return helperSubscribe(book.Context(), "SubscribeShortBook", t.shortDOMSubscribers, t.quit, func(msg interface{}) error {
		domBook := msg.(*proto.DOMBook)
		return book.Send(domBook)
	})
}

func (t *Engine) SubscribeDelta(message *proto.EmptyMessage, book proto.DOM_SubscribeDeltaServer) error {
	return helperSubscribe(book.Context(), "SubscribeShortBook", t.deltaSubscribers, t.quit, func(msg interface{}) error {
		domBook := msg.(*proto.DOMDelta)
		return book.Send(domBook)
	})
}

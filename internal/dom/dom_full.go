package dom

import (
	"fmt"

	"gitlab.com/stockengine/demo/internal/lib/pricetree"
	"gitlab.com/stockengine/demo/internal/proto"
)

func (dom *internalDOMBook) updateFullDOM(delta *DeltaLog) {
	dom.rwMux.Lock()
	defer dom.rwMux.Unlock()

	dom.CommitID = delta.CommitID
	dom.updateSide(delta.Bid, dom.Bid)
	dom.updateSide(delta.Ask, dom.Ask)
}

func (dom *internalDOMBook) updateSide(delta []proto.DOMDeltaElem, tree *pricetree.Tree) {
	for _, deltaElem := range delta {
		//take
		var elem internalDOMOrdersLine
		iVal, ok := tree.Get(deltaElem.DOMOrdersLine.Price)
		if ok {
			elem = iVal.(internalDOMOrdersLine)
		}

		//proceed
		switch deltaElem.Type {
		case proto.DOMDeltaType_Add:
			elem.Count += deltaElem.DOMOrdersLine.Count
			proto.AmountAdd(&elem.Amount, &deltaElem.DOMOrdersLine.Amount)
		case proto.DOMDeltaType_Sub:
			if !ok || elem.Count < deltaElem.DOMOrdersLine.Count {
				panic(fmt.Sprintf("OMFG!!!111 trying decrease non existed or less element. exist[%t] elem.Count[%d] delta.Count[%d]", ok, elem.Count, deltaElem.DOMOrdersLine.Count))
			}
			elem.Count -= deltaElem.DOMOrdersLine.Count
			proto.AmountSub(&elem.Amount, &deltaElem.DOMOrdersLine.Amount)
		default:
			panic("OMFG!!!111")
		}

		//update
		if elem.Count > 0 {
			tree.Put(deltaElem.DOMOrdersLine.Price, elem)
		} else {
			tree.Remove(deltaElem.DOMOrdersLine.Price)
		}
	}
}

func (dom *internalDOMBook) GetDOMBook(limit int) *proto.DOMBook {
	ret := &proto.DOMBook{
		CommitID: dom.CommitID,
		Bid:      make([]proto.DOMLine, 0, dom.Bid.Size()),
		Ask:      make([]proto.DOMLine, 0, dom.Ask.Size()),
	}

	dom.rwMux.RLock()
	copyDOMBookSide(limit, &ret.Ask, dom.Ask)
	copyDOMBookSide(limit, &ret.Bid, dom.Bid)
	dom.rwMux.RUnlock()

	return ret
}

func copyDOMBookSide(limit int, dl *[]proto.DOMLine, pt *pricetree.Tree) {
	for iter := pt.Iterator(); iter.Next() && limit != 0; limit-- { //проверка на ноль только для того, что б -1 означал любое число элементов (не больше чем int_max/2, но это мелочи)
		val := iter.Value().(internalDOMOrdersLine)
		*dl = append(*dl, proto.DOMLine{
			Count:  val.Count,
			Price:  iter.Key(),
			Amount: val.Amount,
		})
	}
}

//func logDom(dom proto.DOMBook) {
//	var w = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.AlignRight)
//	defer w.Flush()
//
//	const formatLeft = "%s\t%s\t%s\t\t\t\t\n"
//	const formatRight = "\t\t\t%[3]s\t%[2]s\t%[1]s\t\n"
//
//	_, _ = fmt.Fprintf(w, "last CommitID %d", dom.CommitID)
//	helper := func(format string, side []proto.DOMLine) {
//		for _, elem := range side {
//			cStr := strconv.FormatUint(elem.Count, 10)
//			pStr := proto.PriceToString(elem.Price)
//			aStr := matching.AmountToString(&elem.Amount)
//			_, _ = fmt.Fprintf(w, format, cStr, aStr, pStr)
//		}
//	}
//	_, _ = fmt.Fprintf(w, "sell entries [%d]:\n", len(dom.Ask))
//	_, _ = fmt.Fprintf(w, formatLeft, "count", "total amount", "price")
//	helper(formatLeft, dom.Ask)
//	_, _ = fmt.Fprintf(w, "buy  entries [%d]:\n", len(dom.Bid))
//	_, _ = fmt.Fprintf(w, formatRight, "count", "total amount", "price")
//	helper(formatRight, dom.Bid)
//}

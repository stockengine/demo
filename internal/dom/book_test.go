package dom

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/stockengine/demo/internal/proto"
	. "gitlab.com/stockengine/demo/internal/proto"
)

//BenchmarkDOMAdd-8                1591946               735 ns/op            1312 B/op         14 allocs/op
//BenchmarkDOMEdit-8               1446009               817 ns/op            1408 B/op         16 allocs/op
//BenchmarkDOMAddDelete-8          1595269               755 ns/op            1312 B/op         14 allocs/op
//
//2020-01-02
//BenchmarkDOMAdd-8         	 1461802	       758 ns/op	    1088 B/op	      11 allocs/op
//BenchmarkDOMEdit-8        	 1509555	       797 ns/op	    1088 B/op	      11 allocs/op
//BenchmarkDOMAddDelete-8   	 1430367	       774 ns/op	    1088 B/op	      11 allocs/op
//BenchmarkNewDeltaLog-8    	  898792	      1186 ns/op	    2000 B/op	      16 allocs/op

func TestDOMSubZero(t *testing.T) {
	tests := []struct {
		ctx CommitedTransaction
		dom DOMBook
	}{
		{ // 0
			ctx: CommitedTransaction{
				CommitID: 0,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("3070405"), Amount: NewAmountStringMust("25838"), Operation: Operation_Sell, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("25838"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 0,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("3070405"), Amount: NewAmountStringMust("25838")},
				},
				Ask: nil,
			},
		},
		{ // 1
			ctx: CommitedTransaction{
				CommitID: 1,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("1925"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 1,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("3070405"), Amount: NewAmountStringMust("25838")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
				},
			},
		},
		{ // 2
			ctx: CommitedTransaction{
				CommitID: 2,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("8412466"), Amount: NewAmountStringMust("38913"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("13075"), OrderStatus: OrderStatus_ClosedPartial},
					TxList: []TxDeal{
						{
							Taker: OrderChange{
								MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("25838"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("3070405"),
								Order: NewOrder{Price: proto.NewPriceMust("8412466"), Amount: NewAmountStringMust("13075"), Operation: Operation_Buy, Type: Type_Limit}},
							Maker: OrderChange{
								MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("25838"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("3070405"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("3070405")}},
						},
					},
				},
			},
			dom: DOMBook{
				CommitID: 2,
				Bid:      []DOMLine{},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("8412466"), Amount: NewAmountStringMust("13075")},
				},
			},
		},
		{ // 3
			ctx: CommitedTransaction{
				CommitID: 3,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839"), Operation: Operation_Sell, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("27839"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 3,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("8412466"), Amount: NewAmountStringMust("13075")},
				},
			},
		},
		{ // 4
			ctx: CommitedTransaction{
				CommitID: 4,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("33610"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 4,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
					{Count: 1, Price: proto.NewPriceMust("8412466"), Amount: NewAmountStringMust("13075")},
				},
			},
		},
		{ // 5
			ctx: CommitedTransaction{
				CommitID: 5,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("2894377"), Amount: NewAmountStringMust("36170"), Operation: Operation_Sell, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("23095"), OrderStatus: OrderStatus_ClosedPartial},
					TxList: []TxDeal{
						{
							Taker: OrderChange{
								MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("13075"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("8412466"),
								Order: NewOrder{Price: proto.NewPriceMust("2894377"), Amount: NewAmountStringMust("23095"), Operation: Operation_Sell, Type: Type_Limit}},
							Maker: OrderChange{
								MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("13075"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("8412466"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("8412466")}},
						},
					},
				},
			},
			dom: DOMBook{
				CommitID: 5,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("2894377"), Amount: NewAmountStringMust("23095")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 6
			ctx: CommitedTransaction{
				CommitID: 6,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("10478015"), Amount: NewAmountStringMust("23317"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("222"), OrderStatus: OrderStatus_ClosedPartial},
					TxList: []TxDeal{
						{
							Taker: OrderChange{
								MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("23095"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("2894377"),
								Order: NewOrder{Price: proto.NewPriceMust("10478015"), Amount: NewAmountStringMust("222"), Operation: Operation_Buy, Type: Type_Limit}},
							Maker: OrderChange{
								MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("23095"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("2894377"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("2894377")}},
						},
					},
				},
			},
			dom: DOMBook{
				CommitID: 6,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
					{Count: 1, Price: proto.NewPriceMust("10478015"), Amount: NewAmountStringMust("222")},
				},
			},
		},
		{ // 7
			ctx: CommitedTransaction{
				CommitID: 7,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26728"), Operation: Operation_Sell, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("26506"), OrderStatus: OrderStatus_ClosedPartial},
					TxList: []TxDeal{
						{
							Taker: OrderChange{
								MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("222"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("10478015"),
								Order: NewOrder{Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506"), Operation: Operation_Sell, Type: Type_Limit}},
							Maker: OrderChange{
								MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("222"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("10478015"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("10478015")}},
						},
					},
				},
			},
			dom: DOMBook{
				CommitID: 7,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 8
			ctx: CommitedTransaction{
				CommitID: 8,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992"), Operation: Operation_Sell, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("3992"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 8,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 9
			ctx: CommitedTransaction{
				CommitID: 9,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("32406"), Operation: Operation_Sell, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("32406"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 9,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
					{Count: 1, Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("32406")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 10
			ctx: CommitedTransaction{
				CommitID: 10,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("2569820"), Amount: NewAmountStringMust("33154"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("33154"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 10,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
					{Count: 1, Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("32406")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2569820"), Amount: NewAmountStringMust("33154")},
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 11
			ctx: CommitedTransaction{
				CommitID: 11,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("6451376"), Amount: NewAmountStringMust("4704"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("0"), OrderStatus: OrderStatus_Closed},
					TxList: []TxDeal{
						{
							Taker: OrderChange{
								MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("4704"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("3711427"),
								Order: NewOrder{Price: proto.NewPriceMust("6451376"), Amount: NewAmountStringMust("0"), Operation: Operation_Buy, Type: Type_Limit}},
							Maker: OrderChange{
								MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("4704"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("3711427"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("27702"), Price: proto.NewPriceMust("3711427")}},
						},
					},
				},
			},
			dom: DOMBook{
				CommitID: 11,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
					{Count: 1, Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("27702")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2569820"), Amount: NewAmountStringMust("33154")},
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 12
			ctx: CommitedTransaction{
				CommitID: 12,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("2737598"), Amount: NewAmountStringMust("33704"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("33704"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 12,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
					{Count: 1, Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("27702")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2569820"), Amount: NewAmountStringMust("33154")},
					{Count: 1, Price: proto.NewPriceMust("2737598"), Amount: NewAmountStringMust("33704")},
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 13
			ctx: CommitedTransaction{
				CommitID: 13,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("12421826"), Amount: NewAmountStringMust("1419"), Operation: Operation_Sell, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("1419"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 13,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12421826"), Amount: NewAmountStringMust("1419")},
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
					{Count: 1, Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("27702")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2569820"), Amount: NewAmountStringMust("33154")},
					{Count: 1, Price: proto.NewPriceMust("2737598"), Amount: NewAmountStringMust("33704")},
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 14
			ctx: CommitedTransaction{
				CommitID: 14,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("2005019"), Amount: NewAmountStringMust("18496"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("18496"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 14,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12421826"), Amount: NewAmountStringMust("1419")},
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
					{Count: 1, Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("27702")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2005019"), Amount: NewAmountStringMust("18496")},
					{Count: 1, Price: proto.NewPriceMust("2569820"), Amount: NewAmountStringMust("33154")},
					{Count: 1, Price: proto.NewPriceMust("2737598"), Amount: NewAmountStringMust("33704")},
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
				},
			},
		},
		{ // 15
			ctx: CommitedTransaction{
				CommitID: 15,
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Price: proto.NewPriceMust("3013567"), Amount: NewAmountStringMust("20425"), Operation: Operation_Buy, Type: Type_Limit}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("20425"), OrderStatus: OrderStatus_ToBook},
					TxList:          nil,
				},
			},
			dom: DOMBook{
				CommitID: 15,
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("12421826"), Amount: NewAmountStringMust("1419")},
					{Count: 1, Price: proto.NewPriceMust("12176831"), Amount: NewAmountStringMust("3992")},
					{Count: 1, Price: proto.NewPriceMust("11809479"), Amount: NewAmountStringMust("27839")},
					{Count: 1, Price: proto.NewPriceMust("7155806"), Amount: NewAmountStringMust("26506")},
					{Count: 1, Price: proto.NewPriceMust("3711427"), Amount: NewAmountStringMust("27702")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("2005019"), Amount: NewAmountStringMust("18496")},
					{Count: 1, Price: proto.NewPriceMust("2569820"), Amount: NewAmountStringMust("33154")},
					{Count: 1, Price: proto.NewPriceMust("2737598"), Amount: NewAmountStringMust("33704")},
					{Count: 1, Price: proto.NewPriceMust("2789318"), Amount: NewAmountStringMust("1925")},
					{Count: 1, Price: proto.NewPriceMust("2890670"), Amount: NewAmountStringMust("33610")},
					{Count: 1, Price: proto.NewPriceMust("3013567"), Amount: NewAmountStringMust("20425")},
				},
			},
		},
	}

	service := &Engine{
		bookInt: initIntBook(),
	}

	//yep. its bad design.
	for i, test := range tests {
		delta := NewDeltaLog(&test.ctx) // вычисляем дельту

		service.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево
		book := service.bookInt.GetDOMBook(-1)

		if test.dom.Ask == nil {
			test.dom.Ask = []DOMLine{}
		}
		if test.dom.Bid == nil {
			test.dom.Bid = []DOMLine{}
		}

		if !assert.Equalf(t, &test.dom, book, "case (%d)", i) {
			t.Fatalf("case(%d) failed", i)
		}
	}
}

func TestDOMEdit(t *testing.T) {
	tests := []struct {
		ctx      CommitedTransaction
		deltaNil bool
		dom      DOMBook
	}{
		{ //0 add
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{New: &NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("100"),
					Price:     proto.NewPriceMust("250"),
					OwnerID:   123, ExtID: 567,
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("100"),
					OrderStatus: OrderStatus_ToBook,
				},
				TxList: nil,
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("100")},
				},
			},
		},
		{ //1 edit bad id
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{Edit: &EditOrder{
					NewAmount: NewAmountStringMust("13"),
					NewPrice:  proto.NewPriceMust("777"),
				}}},
				OrderFinalState: OrderState{OrderStatus: OrderStatus_IgnoredInvalidIdentification},
				TxList:          nil,
			}},
			deltaNil: true,
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("100")},
				},
			},
		},
		{ //2 edit bad id
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{Edit: &EditOrder{
					OwnerID: 1234, ExtID: 567,
					NewAmount: NewAmountStringMust("13"),
					NewPrice:  proto.NewPriceMust("777"),
				}}},
				OrderFinalState: OrderState{OrderStatus: OrderStatus_IgnoredIdentificationNotFound},
				TxList:          nil,
			}},
			deltaNil: true,
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("100")},
				},
			},
		},
		{ //3 edit bad id
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{Edit: &EditOrder{
					OwnerID: 123, ExtID: 5678,
					NewAmount: NewAmountStringMust("13"),
					NewPrice:  proto.NewPriceMust("777"),
				}}},
				OrderFinalState: OrderState{OrderStatus: OrderStatus_IgnoredIdentificationNotFound},
				TxList:          nil,
			}},
			deltaNil: true,
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("100")},
				},
			},
		},
		{ //4 edit amount
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{Edit: &EditOrder{
					OwnerID: 123, ExtID: 567,
					NewAmount: NewAmountStringMust("13"),
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("13"),
					OrderStatus: OrderStatus_ChangedAmount,
				},
				TxList: []TxDeal{{Maker: OrderChange{
					MarketSide:   MarketSide_Maker,
					AmountChange: NewAmountStringMust("100"),
					OrderStatus:  OrderStatus_ChangedAmount,
					DealPrice:    proto.NewPriceMust("250"),
					Order: NewOrder{
						Type:      Type_Limit,
						Operation: Operation_Buy,
						Amount:    NewAmountStringMust("13"),
						Price:     proto.NewPriceMust("250"),
						OwnerID:   123, ExtID: 567,
					},
				}}},
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("13")},
				},
			},
		},
		{ //5 edit price
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{Edit: &EditOrder{
					OwnerID: 123, ExtID: 567,
					NewPrice: proto.NewPriceMust("777"),
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("13"),
					OrderStatus: OrderStatus_ToBook,
				},
				TxList: []TxDeal{
					{Maker: OrderChange{
						MarketSide:  MarketSide_Maker,
						OrderStatus: OrderStatus_Cancel,
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("13"),
							Price:     proto.NewPriceMust("250"),
							OwnerID:   123, ExtID: 567,
						},
					}},
					{Maker: OrderChange{
						MarketSide:   MarketSide_Maker,
						AmountChange: NewAmountStringMust("13"),
						OrderStatus:  OrderStatus_ChangedPrice,
						DealPrice:    proto.NewPriceMust("250"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("13"),
							Price:     proto.NewPriceMust("777"),
							OwnerID:   123, ExtID: 567,
						},
					}},
				},
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("777"), Amount: NewAmountStringMust("13")},
				},
			},
		},
		{ //6 add opposite order
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{New: &NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Sell,
					Amount:    NewAmountStringMust("100"),
					Price:     proto.NewPriceMust("800"),
					OwnerID:   345, ExtID: 789,
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("100"),
					OrderStatus: OrderStatus_ToBook,
				},
				TxList: nil,
			}},
			dom: DOMBook{
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("800"), Amount: NewAmountStringMust("100")},
				},
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("777"), Amount: NewAmountStringMust("13")},
				},
			},
		},
		{ //7 move to deal :)
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{Edit: &EditOrder{
					OwnerID: 123, ExtID: 567,
					NewPrice: proto.NewPriceMust("900"),
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("0"),
					OrderStatus: OrderStatus_Closed,
				},
				TxList: []TxDeal{
					{Maker: OrderChange{
						MarketSide:  MarketSide_Maker,
						OrderStatus: OrderStatus_Cancel,
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("13"),
							Price:     proto.NewPriceMust("777"),
							OwnerID:   123, ExtID: 567,
						},
					}},
					{Maker: OrderChange{
						MarketSide:   MarketSide_Maker,
						AmountChange: NewAmountStringMust("13"),
						OrderStatus:  OrderStatus_ChangedPrice,
						DealPrice:    proto.NewPriceMust("777"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("13"),
							Price:     proto.NewPriceMust("900"),
							OwnerID:   123, ExtID: 567,
						},
					}},
					{
						Maker: OrderChange{
							MarketSide:   MarketSide_Maker,
							AmountChange: NewAmountStringMust("13"),
							OrderStatus:  OrderStatus_ClosedPartial,
							DealPrice:    proto.NewPriceMust("800"),
							Order: NewOrder{
								Type:      Type_Limit,
								Operation: Operation_Sell,
								Amount:    NewAmountStringMust("87"),
								Price:     proto.NewPriceMust("800"),
								OwnerID:   345, ExtID: 789,
							},
						},
						Taker: OrderChange{
							MarketSide:   MarketSide_Taker,
							AmountChange: NewAmountStringMust("13"),
							OrderStatus:  OrderStatus_Closed,
							DealPrice:    proto.NewPriceMust("800"),
							Order: NewOrder{
								Type:      Type_Limit,
								Operation: Operation_Buy,
								Amount:    NewAmountStringMust("0"),
								Price:     proto.NewPriceMust("900"),
								OwnerID:   123, ExtID: 567,
							},
						},
					},
				},
			}},
			dom: DOMBook{
				Bid: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("800"), Amount: NewAmountStringMust("87")},
				},
				Ask: nil,
			},
		},
	}

	service := &Engine{
		bookInt: initIntBook(),
	}

	//yep. its bad design.
	for i, test := range tests {
		delta := NewDeltaLog(&test.ctx) // вычисляем дельту
		if delta == nil {
			assert.True(t, test.deltaNil)
			continue
		}

		service.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево
		book := service.bookInt.GetDOMBook(-1)

		if test.dom.Ask == nil {
			test.dom.Ask = []DOMLine{}
		}
		if test.dom.Bid == nil {
			test.dom.Bid = []DOMLine{}
		}

		if !assert.Equalf(t, &test.dom, book, "case (%d)", i) {
			t.Fatalf("case(%d) failed", i)
		}
	}
}

func TestDOMDelete(t *testing.T) {
	tests := []struct {
		ctx      CommitedTransaction
		deltaNil bool
		dom      DOMBook
	}{
		{ //0 add
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{New: &NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("100"),
					Price:     proto.NewPriceMust("250"),
					OwnerID:   123, ExtID: 1,
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("100"),
					OrderStatus: OrderStatus_ToBook,
				},
				TxList: nil,
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("100")},
				},
			},
		},
		{ //1 add
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{New: &NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("200"),
					Price:     proto.NewPriceMust("250"),
					OwnerID:   123, ExtID: 2,
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("200"),
					OrderStatus: OrderStatus_ToBook,
				},
				TxList: nil,
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 2, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("300")},
				},
			},
		},
		{ //2 add
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{New: &NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("300"),
					Price:     proto.NewPriceMust("250"),
					OwnerID:   123, ExtID: 3,
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("300"),
					OrderStatus: OrderStatus_ToBook,
				},
				TxList: nil,
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 3, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("600")},
				},
			},
		},
		{ //3 add
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{New: &NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("100"),
					Price:     proto.NewPriceMust("350"),
					OwnerID:   234, ExtID: 1,
				}}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("100"),
					OrderStatus: OrderStatus_ToBook,
				},
				TxList: nil,
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 3, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("600")},
					{Count: 1, Price: proto.NewPriceMust("350"), Amount: NewAmountStringMust("100")},
				},
			},
		},
		{ //4 delete one
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{
					Delete: &DeleteOrder{OwnerID: 123, ExtID: 1},
				}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("100"),
					OrderStatus: OrderStatus_Cancel,
				},
				TxList: []TxDeal{{
					Maker: OrderChange{
						MarketSide:   MarketSide_Maker,
						AmountChange: NewAmountStringMust("0"),
						OrderStatus:  OrderStatus_Cancel,
						DealPrice:    proto.NewPriceMust("0"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("100"),
							Price:     proto.NewPriceMust("250"),
							OwnerID:   123, ExtID: 1,
						},
					},
				}},
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 2, Price: proto.NewPriceMust("250"), Amount: NewAmountStringMust("500")},
					{Count: 1, Price: proto.NewPriceMust("350"), Amount: NewAmountStringMust("100")},
				},
			},
		},
		{ //5 delete multi
			ctx: CommitedTransaction{Transaction: Transaction{
				Command: Command{SubCommands: SubCommands{
					Delete: &DeleteOrder{OwnerID: 123},
				}},
				OrderFinalState: OrderState{
					AmountFinal: NewAmountStringMust("0"),
					OrderStatus: OrderStatus_Cancel,
				},
				TxList: []TxDeal{{
					Maker: OrderChange{
						MarketSide:   MarketSide_Maker,
						AmountChange: NewAmountStringMust("0"),
						OrderStatus:  OrderStatus_Cancel,
						DealPrice:    proto.NewPriceMust("0"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("200"),
							Price:     proto.NewPriceMust("250"),
							OwnerID:   123, ExtID: 2,
						},
					},
				}, {
					Maker: OrderChange{
						MarketSide:   MarketSide_Maker,
						AmountChange: NewAmountStringMust("0"),
						OrderStatus:  OrderStatus_Cancel,
						DealPrice:    proto.NewPriceMust("0"),
						Order: NewOrder{
							Type:      Type_Limit,
							Operation: Operation_Buy,
							Amount:    NewAmountStringMust("300"),
							Price:     proto.NewPriceMust("250"),
							OwnerID:   123, ExtID: 3,
						},
					},
				}},
			}},
			dom: DOMBook{
				Bid: nil,
				Ask: []DOMLine{
					{Count: 1, Price: proto.NewPriceMust("350"), Amount: NewAmountStringMust("100")},
				},
			},
		},
	}

	service := &Engine{
		bookInt: initIntBook(),
	}

	//yep. its bad design.
	for i, test := range tests {
		delta := NewDeltaLog(&test.ctx) // вычисляем дельту
		if delta == nil {
			assert.True(t, test.deltaNil)
			continue
		}
		service.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево
		book := service.bookInt.GetDOMBook(-1)

		if test.dom.Ask == nil {
			test.dom.Ask = []DOMLine{}
		}
		if test.dom.Bid == nil {
			test.dom.Bid = []DOMLine{}
		}

		if !assert.Equalf(t, &test.dom, book, "case (%d)", i) {
			t.Fatalf("case(%d) failed", i)
		}
	}
}

func BenchmarkDOMAdd(b *testing.B) {
	service := &Engine{
		bookInt: initIntBook(),
	}

	tx := &CommitedTransaction{Transaction: Transaction{
		Command: Command{SubCommands: SubCommands{New: &NewOrder{
			Type:      Type_Limit,
			Operation: Operation_Buy,
			Amount:    NewAmountStringMust("100"),
			Price:     proto.NewPriceMust("250"),
			OwnerID:   123, ExtID: 1,
		}}},
		OrderFinalState: OrderState{
			AmountFinal: NewAmountStringMust("100"),
			OrderStatus: OrderStatus_ToBook,
		},
		TxList: nil,
	}}

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		delta := NewDeltaLog(tx)             // вычисляем дельту
		service.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево
	}
}

func BenchmarkDOMEdit(b *testing.B) {
	service := &Engine{
		bookInt: initIntBook(),
	}

	delta := NewDeltaLog(&CommitedTransaction{Transaction: Transaction{
		Command: Command{SubCommands: SubCommands{New: &NewOrder{
			Type:      Type_Limit,
			Operation: Operation_Buy,
			Amount:    NewAmountStringMust("100"),
			Price:     proto.NewPriceMust("250"),
			OwnerID:   123, ExtID: 1,
		}}},
		OrderFinalState: OrderState{
			AmountFinal: NewAmountStringMust("100"),
			OrderStatus: OrderStatus_ToBook,
		},
		TxList: nil,
	}}) // вычисляем дельту
	service.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево

	tx := &CommitedTransaction{Transaction: Transaction{
		Command: Command{SubCommands: SubCommands{Edit: &EditOrder{
			OwnerID: 123, ExtID: 1,
			NewPrice: proto.NewPriceMust("777"),
		}}},
		OrderFinalState: OrderState{
			AmountFinal: NewAmountStringMust("13"),
			OrderStatus: OrderStatus_ToBook,
		},
		TxList: []TxDeal{
			{Maker: OrderChange{
				MarketSide:  MarketSide_Maker,
				OrderStatus: OrderStatus_Cancel,
				Order: NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("13"),
					Price:     proto.NewPriceMust("250"),
					OwnerID:   123, ExtID: 1,
				},
			}},
			{Maker: OrderChange{
				MarketSide:   MarketSide_Maker,
				AmountChange: NewAmountStringMust("13"),
				OrderStatus:  OrderStatus_ChangedPrice,
				DealPrice:    proto.NewPriceMust("250"),
				Order: NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("13"),
					Price:     proto.NewPriceMust("777"),
					OwnerID:   123, ExtID: 1,
				},
			}},
		},
	}}

	price := [2]Price{proto.NewPriceMust("777"), proto.NewPriceMust("250")}
	amount := [2]Amount{NewAmountStringMust("13"), NewAmountStringMust("234")}

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tx.Transaction.Command.SubCommands.Edit.NewPrice = price[i%2]
		tx.Transaction.OrderFinalState.AmountFinal = amount[i%2]
		tx.Transaction.TxList[0].Maker.Order.Amount = amount[i%2]
		tx.Transaction.TxList[0].Maker.Order.Price = price[(i+1)%2]
		tx.Transaction.TxList[1].Maker.Order.Amount = amount[(i+1)%2]
		tx.Transaction.TxList[1].Maker.Order.Price = price[i%2]
		tx.Transaction.TxList[1].Maker.AmountChange = amount[i%2]
		tx.Transaction.TxList[1].Maker.DealPrice = price[i%2]

		delta := NewDeltaLog(tx)             // вычисляем дельту
		service.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево
	}
}

func BenchmarkDOMAddDelete(b *testing.B) {
	service := &Engine{
		bookInt: initIntBook(),
	}

	txAdd := &CommitedTransaction{Transaction: Transaction{
		Command: Command{SubCommands: SubCommands{New: &NewOrder{
			Type:      Type_Limit,
			Operation: Operation_Buy,
			Amount:    NewAmountStringMust("100"),
			Price:     proto.NewPriceMust("250"),
			OwnerID:   123, ExtID: 1,
		}}},
		OrderFinalState: OrderState{
			AmountFinal: NewAmountStringMust("100"),
			OrderStatus: OrderStatus_ToBook,
		},
		TxList: nil,
	}}

	txDel := &CommitedTransaction{Transaction: Transaction{
		Command: Command{SubCommands: SubCommands{
			Delete: &DeleteOrder{OwnerID: 123, ExtID: 1},
		}},
		OrderFinalState: OrderState{
			AmountFinal: NewAmountStringMust("100"),
			OrderStatus: OrderStatus_Cancel,
		},
		TxList: []TxDeal{{
			Maker: OrderChange{
				MarketSide:   MarketSide_Maker,
				AmountChange: NewAmountStringMust("0"),
				OrderStatus:  OrderStatus_Cancel,
				DealPrice:    proto.NewPriceMust("0"),
				Order: NewOrder{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    NewAmountStringMust("100"),
					Price:     proto.NewPriceMust("250"),
					OwnerID:   123, ExtID: 1,
				},
			},
		}},
	}}

	tx := [2]*CommitedTransaction{txAdd, txDel}

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		delta := NewDeltaLog(tx[i%2])        // вычисляем дельту
		service.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево
	}
}

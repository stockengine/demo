package dom

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/stockengine/demo/internal/proto"
)

func (t *Engine) prometheusReporter() {
	t.wg.Add(1)

	go func() {
		ticker := time.NewTicker(time.Second * 1)
		defer ticker.Stop()
		defer t.wg.Done()
		for {
			select {
			case <-ticker.C:
				var askLeft, askRight, bidLeft, bidRight proto.Price
				t.bookInt.rwMux.RLock()
				commits := t.bookInt.CommitID
				askSize := t.bookInt.Ask.Size()
				bidSize := t.bookInt.Bid.Size()
				if askSize > 0 {
					askLeft = t.bookInt.Ask.Left().Key
					askRight = t.bookInt.Ask.Right().Key
				}
				if bidSize > 0 {
					bidLeft = t.bookInt.Bid.Left().Key
					bidRight = t.bookInt.Bid.Right().Key
				}
				t.bookInt.rwMux.RUnlock()

				t.promBookIntCommits.WithLabelValues().Set(float64(commits))
				t.promBookIntSize.WithLabelValues("ask").Set(float64(askSize))
				t.promBookIntSize.WithLabelValues("bid").Set(float64(bidSize))
				if askSize > 0 {
					t.promBookIntPrices.WithLabelValues("ask", "left").Set(askLeft.ToFloat64())
					t.promBookIntPrices.WithLabelValues("ask", "right").Set(askRight.ToFloat64())
				}
				if bidSize > 0 {
					t.promBookIntPrices.WithLabelValues("bid", "left").Set(bidLeft.ToFloat64())
					t.promBookIntPrices.WithLabelValues("bid", "right").Set(bidRight.ToFloat64())
				}
			case <-t.quit:
				return
			}
		}
	}()
}

func (t *Engine) promScanDelta(deltaLog *DeltaLog) {
	helperScanDelta(deltaLog.Bid, "bid", t.promDeltaLogCnt)
	helperScanDelta(deltaLog.Ask, "ask", t.promDeltaLogCnt)
}

//DRY
func helperScanDelta(dl []proto.DOMDeltaElem, side string, cnt prometheus.CounterVec) {
	for i := range dl {
		if dl[i].Type == proto.DOMDeltaType_Add {
			cnt.WithLabelValues(side, "add").Add(float64(dl[i].DOMOrdersLine.Count))
		} else {
			cnt.WithLabelValues(side, "sub").Add(float64(dl[i].DOMOrdersLine.Count))
		}
	}
}

package dom

import (
	"sync"

	"gitlab.com/stockengine/demo/internal/lib/logger"

	"gitlab.com/stockengine/demo/internal/lib/pricetree"

	"gitlab.com/stockengine/demo/internal/proto"
)

// Покупка и продажа - Бид ( Bid) и Аск (Ask)
// Ask – это цена предложения (покупки)
// Bid – это цена спроса (продажи)
type internalDOMBook struct {
	CommitID uint64
	Bid, Ask *pricetree.Tree // value -> internalDOMOrdersLine
	rwMux    sync.RWMutex    //чтение и запись не должны конфликтовать
}

// этот тип стоит за interface в дереве
type internalDOMOrdersLine struct {
	Count uint64
	//Price  proto.Price - it will be in tree key
	Amount proto.Amount
}

func (t *Engine) addToBook(ctx *proto.CommitedTransaction) {
	select {
	case t.ctxChan <- ctx: //good
	default:
		logger.Log.Debug("ctxChan is full :( waiting...")
		t.ctxChan <- ctx
	}
}

func (t *Engine) addToBookConsumer() {
	defer t.wg.Done()
L:
	for {
		select {
		case ctx := <-t.ctxChan:
			//start := time.Now() // вся обработка занимает микросекунды.... нет смысла выносить её на мониторинг - это будет дороже, чем сама обработка...
			delta := NewDeltaLog(ctx) // вычисляем дельту
			if delta == nil {         // кривая команда на редактирование и т.п.
				continue
			}
			t.promScanDelta(delta)         // выводим на мониторинг
			t.bookInt.updateFullDOM(delta) // обновляем внутренне полное дерево
			t.deltaSubscribers.SendEvent(&delta.DOMDelta)

		case <-t.quit:
			break L
		}
	}
}

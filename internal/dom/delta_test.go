package dom

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/stockengine/demo/internal/proto"
	. "gitlab.com/stockengine/demo/internal/proto"
)

// BenchmarkNewDeltaLog-8           1162284              1034 ns/op            2000 B/op         16 allocs/op

func BenchmarkNewDeltaLog(b *testing.B) {
	ctx := &CommitedTransaction{
		CommitID: 123,
		Transaction: Transaction{
			Command:         Command{SubCommands: SubCommands{New: &NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("200"), Price: proto.NewPriceMust("95")}}},
			OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("100"), OrderStatus: OrderStatus_ClosedPartial},
			TxList: []TxDeal{
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("130"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("190"), Price: proto.NewPriceMust("95")}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("130"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("130")}},
				},
				{
					Taker: OrderChange{
						MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("90"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("100"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("100"), Price: proto.NewPriceMust("95")}},
					Maker: OrderChange{
						MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("90"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("100"),
						Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("100")}},
				},
			},
		},
	}

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = NewDeltaLog(ctx)
	}
}

func TestDeltaLog_ApplyAndLog(t *testing.T) {
	tests := []struct {
		ctx CommitedTransaction
		log DeltaLog
	}{
		{
			ctx: CommitedTransaction{
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("200"), Price: proto.NewPriceMust("95")}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("200"), OrderStatus: OrderStatus_ToBook},
				}},
			log: DeltaLog{DOMDelta{
				Bid: []DOMDeltaElem{
					{
						DOMOrdersLine: DOMLine{
							Count:  1,
							Price:  proto.NewPriceMust("95"),
							Amount: NewAmountStringMust("200"),
						},
						Type: DOMDeltaType_Add,
					},
				},
				Ask: []DOMDeltaElem{},
			}},
		},
		{
			ctx: CommitedTransaction{
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("200"), Price: proto.NewPriceMust("95")}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("200"), OrderStatus: OrderStatus_ToBook},
				}},
			log: DeltaLog{DOMDelta{
				Bid: []DOMDeltaElem{},
				Ask: []DOMDeltaElem{
					{
						DOMOrdersLine: DOMLine{
							Count:  1,
							Price:  proto.NewPriceMust("95"),
							Amount: NewAmountStringMust("200"),
						},
						Type: DOMDeltaType_Add,
					},
				},
			}},
		},
		{
			ctx: CommitedTransaction{
				Transaction: Transaction{
					Command:         Command{SubCommands: SubCommands{New: &NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("200"), Price: proto.NewPriceMust("95")}}},
					OrderFinalState: OrderState{AmountFinal: NewAmountStringMust("100"), OrderStatus: OrderStatus_ClosedPartial},
					TxList: []TxDeal{
						{
							Taker: OrderChange{
								MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("130"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("190"), Price: proto.NewPriceMust("95")}},
							Maker: OrderChange{
								MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("10"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("130"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("130")}},
						},
						{
							Taker: OrderChange{
								MarketSide: MarketSide_Taker, AmountChange: NewAmountStringMust("90"), OrderStatus: OrderStatus_ClosedPartial, DealPrice: proto.NewPriceMust("100"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Sell, Amount: NewAmountStringMust("100"), Price: proto.NewPriceMust("95")}},
							Maker: OrderChange{
								MarketSide: MarketSide_Maker, AmountChange: NewAmountStringMust("90"), OrderStatus: OrderStatus_Closed, DealPrice: proto.NewPriceMust("100"),
								Order: NewOrder{Type: Type_Limit, Operation: Operation_Buy, Amount: NewAmountStringMust("0"), Price: proto.NewPriceMust("100")}},
						},
					},
				},
			},
			log: DeltaLog{DOMDelta{
				Bid: []DOMDeltaElem{
					{
						DOMOrdersLine: DOMLine{
							Count:  1,
							Price:  proto.NewPriceMust("95"),
							Amount: NewAmountStringMust("100"),
						},
						Type: DOMDeltaType_Add,
					},
				},
				Ask: []DOMDeltaElem{
					{
						DOMOrdersLine: DOMLine{
							Count:  1,
							Price:  proto.NewPriceMust("130"),
							Amount: NewAmountStringMust("10"),
						},
						Type: DOMDeltaType_Sub,
					},
					{
						DOMOrdersLine: DOMLine{
							Count:  1,
							Price:  proto.NewPriceMust("100"),
							Amount: NewAmountStringMust("90"),
						},
						Type: DOMDeltaType_Sub,
					},
				},
			}},
		},
	}

	for _, test := range tests {
		lg := NewDeltaLog(&test.ctx)
		assert.Equal(t, &test.log, lg)
	}
}

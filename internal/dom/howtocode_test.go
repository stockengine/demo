// +build howtocode

package dom

import (
	"testing"

	"gitlab.com/stockengine/demo/internal/matching"
	"gitlab.com/stockengine/demo/internal/proto"

	. "gitlab.com/stockengine/demo/internal/proto"
)

func getBenchEnv(total int) (service *Engine, ctx *CommitedTransaction) {
	service = &Engine{
		bookInt: initIntBook(),
	}

	//initial fillup
	const shift = 1000
	for i := 0; i < total; i++ {
		service.bookInt.Bid[Price{Price: uint64(i * shift)}] = internalDOMOrdersLine{
			Count:  123,
			Amount: matching.NewAmountStringMust("1234567890"),
		}
	}
	for i := 0; i < total; i++ {
		service.bookInt.Ask[Price{Price: uint64((i + total) * shift)}] = internalDOMOrdersLine{
			Count:  123,
			Amount: matching.NewAmountStringMust("1234567890"),
		}
	}

	ctx = &CommitedTransaction{
		CommitID: CommitID{CommitID: 666},
		Transaction: Transaction{
			Command: Command{
				ReqID: ReqID{},
				Order: Order{
					Type:      Type_Limit,
					Operation: Operation_Buy,
					Amount:    matching.NewAmountStringMust("100"),
					Price:     proto.NewPriceMust("250"),
					Identification: Identification{
						Owner: Owner{Owner: 123},
						ExtID: ExtID{ExtID: 987},
					},
				},
			},
			OrderFinalState: OrderState{
				Amount:      matching.NewAmountStringMust("100"),
				OrderStatus: OrderStatus_ToBook,
			},
			TxList: []TxDeal{},
		},
	}
	return
}

func BenchmarkAddToBook1e2(b *testing.B) {
	//yep, stupid init
	service, ctx := getBenchEnv(1e2)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		service.addToBookWorker(ctx)
	}
}
func BenchmarkAddToBook1e3(b *testing.B) {
	//yep, stupid init
	service, ctx := getBenchEnv(1e3)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		service.addToBookWorker(ctx)
	}
}
func BenchmarkAddToBook1e6(b *testing.B) {
	//yep, stupid init
	service, ctx := getBenchEnv(1e6)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		service.addToBookWorker(ctx)
	}
}

func BenchmarkPrepareMsg1e2(b *testing.B) {
	//yep, stupid init
	service, _ := getBenchEnv(100)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		service.prepareMsg()
	}
}
func BenchmarkPrepareMsg1e3(b *testing.B) {
	//yep, stupid init
	service, _ := getBenchEnv(1e3)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		service.prepareMsg()
	}
}
func BenchmarkPrepareMsg1e6(b *testing.B) {
	//yep, stupid init
	service, _ := getBenchEnv(1e6)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		service.prepareMsg()
	}
}

func BenchmarkMapCopy1e2(b *testing.B) {
	//yep, stupid init
	service, _ := getBenchEnv(100)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		dst := make([]internalDOMOrdersLine, 0, service.bookInt.Ask.Size())
		for it := service.bookInt.Ask.Iterator(); it.Next(); {
			dst = append(dst, it.Value().(internalDOMOrdersLine))
		}
	}
}
func BenchmarkMapCopy1e3(b *testing.B) {
	//yep, stupid init
	service, _ := getBenchEnv(1e3)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		dst := make([]internalDOMOrdersLine, 0, service.bookInt.Ask.Size())
		for it := service.bookInt.Ask.Iterator(); it.Next(); {
			dst = append(dst, it.Value().(internalDOMOrdersLine))
		}
	}
}
func BenchmarkMapCopy1e6(b *testing.B) {
	//yep, stupid init
	service, _ := getBenchEnv(1e6)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		dst := make([]internalDOMOrdersLine, 0, service.bookInt.Ask.Size())
		for it := service.bookInt.Ask.Iterator(); it.Next(); {
			dst = append(dst, it.Value().(internalDOMOrdersLine))
		}
	}
}

package dom

import (
	"fmt"

	"gitlab.com/stockengine/demo/internal/proto"
)

// Покупка и продажа - Бид ( Bid) и Аск (Ask)
// Ask – это цена предложения (покупки)
// Bid – это цена спроса (продажи)
func NewDeltaLogNew(book *proto.CommitedTransaction) (ret *DeltaLog) {
	//идём по массиву закоммиченных транзакций и формируем дельту стакана
	deltaMap := initDeltaMap()

	//maker - анализируем список сделок т.к. по другому мы и не узнаем что и где закрылось
	//taker - смотрим финальный статус ордера

	//taker
	switch book.Transaction.OrderFinalState.OrderStatus {
	case proto.OrderStatus_Closed,
		proto.OrderStatus_IgnoredDublicateId,
		proto.OrderStatus_IgnoredInvalidPrice,
		proto.OrderStatus_IgnoredInvalidAmount:
		//do nothing
	case proto.OrderStatus_ToBook,
		proto.OrderStatus_ClosedPartial:
		//add to DOM
		deltaMap[DeltaMapOperationIndex(book.Transaction.Command.SubCommands.New.Operation)][book.Transaction.Command.SubCommands.New.Price] = internalDeltaMapElem{
			Count:  1,
			Amount: book.Transaction.OrderFinalState.AmountFinal,
			Type:   proto.DOMDeltaType_Add,
		}
	case proto.OrderStatus_Changed:
		// такого статуса не должно быть (на 2019-10-31)
		fallthrough
	default:
		panic(fmt.Sprintf("OMFG!!!111 bad testing, bad project control... its necessary to rewrite code for stupid :( status[%s]", book.Transaction.OrderFinalState.OrderStatus))
	}

	//maker changes
	for _, v := range book.Transaction.TxList {
		order := v.Maker
		delta := deltaMap[DeltaMapOperationIndex(order.Order.Operation)][order.DealPrice]
		//analyze
		switch order.OrderStatus {
		case proto.OrderStatus_Closed:
			delta.Count++ // только в этих случаях ордер закрывается, в других он просто уменьшается
			fallthrough
		case proto.OrderStatus_ClosedPartial:
			delta.Type = proto.DOMDeltaType_Sub // maker ордера могут только "уменьшаться"
			proto.AmountAdd(&delta.Amount, &order.AmountChange)
		case proto.OrderStatus_Changed, //схренали это он changed ?
			proto.OrderStatus_IgnoredDublicateId,
			proto.OrderStatus_IgnoredInvalidPrice,
			proto.OrderStatus_IgnoredInvalidAmount,
			proto.OrderStatus_ToBook:
			fallthrough //impossible for order on Maker side
		default:
			panic(fmt.Sprintf("OMFG!!!111 bad testing, bad project control... its necessary to rewrite code for stupid :( status[%s]", order.OrderStatus))
		}
		//write it back
		deltaMap[DeltaMapOperationIndex(order.Order.Operation)][order.DealPrice] = delta
	}

	//формируем результат в формате массива (что б быстрее сериализовать и иметь гарантированный порядок элементов
	return initDeltaLog(book.CommitID, deltaMap)
}

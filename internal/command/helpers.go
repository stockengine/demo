package command

import (
	"encoding/binary"
	"net"
	"reflect"
	"sync/atomic"
	"time"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
	"go.uber.org/zap"
)

func BuildReqID(acceptCnt *uint64, localAddr, remoteAddr net.Addr, connTime time.Time) (proto.ReqID, error) {
	rTCPAddr, ok := localAddr.(*net.TCPAddr)
	if !ok {
		logger.Log.Error(
			"invalid local addr",
			zap.Reflect("LocalAddr", localAddr),
			zap.String("LocalAddrType", reflect.TypeOf(localAddr).String()),
		)
		return proto.ReqID{}, ErrorInvalidLocalAddr
	}

	lTCPAddr, ok := remoteAddr.(*net.TCPAddr)
	if !ok {
		logger.Log.Error(
			"invalid remote addr",
			zap.Reflect("LocalAddr", remoteAddr),
			zap.String("LocalAddrType", reflect.TypeOf(remoteAddr).String()),
		)
		return proto.ReqID{}, ErrorInvalidRemoteAddr
	}

	return proto.ReqID{
		IPLow:      binary.BigEndian.Uint32(rTCPAddr.IP),
		SrcDstPort: uint32(rTCPAddr.Port) | (uint32(lTCPAddr.Port) << 16),
		ConnectTS:  uint32(connTime.Unix()), // eeah.. we are old skull. meet at 2038
		AcceptCnt:  atomic.AddUint64(acceptCnt, 1),
	}, nil
}

package command

import "errors"

var (
	ErrorInvalidLocalAddr  = errors.New("invalid local addr")
	ErrorInvalidRemoteAddr = errors.New("invalid remote addr")
)

package command

import (
	"context"

	"github.com/fasthttp/websocket"
	"github.com/mailru/easyjson"
	"github.com/valyala/fasthttp"
	"gitlab.com/stockengine/demo/internal/auth"
	"gitlab.com/stockengine/demo/internal/lib/config"
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
	"go.uber.org/zap"
)

func initDOMEngine(domAddr string, ce *Engine) error {
	var ctx context.Context
	var err error
	ce.connDom, err = config.GRPCClientDial(domAddr, true)
	if err != nil {
		return err
	}

	ce.domClient = proto.NewDOMClient(ce.connDom)

	// ебучая копипаста :(
	if ce.methodSubscribeShortBook == nil {
		ctx, ce.cancelSubscribeShortBook = context.WithCancel(context.Background())
		methodSubscribeTop100, err := ce.domClient.SubscribeShortBook(ctx, &proto.EmptyMessage{})
		if logger.ErrorLogHelper(err, "SubscribeShortBook") {
			return err
		}
		ce.methodSubscribeShortBook = methodSubscribeTop100

		ce.wgMethod.Add(1)
		go func() {
			defer ce.wgMethod.Done()
			for {
				dom, err := ce.methodSubscribeShortBook.Recv()
				if logger.ErrorLogHelper(err, "methodSubscribeShortBook") {
					return
				}
				ce.lastTop100DOM.Store(dom)
			}
		}()
	}

	if ce.methodSubscribeDelta == nil {
		ctx, ce.cancelSubscribeDelta = context.WithCancel(context.Background())
		methodSubscribeDelta, err := ce.domClient.SubscribeDelta(ctx, &proto.EmptyMessage{})
		if logger.ErrorLogHelper(err, "SubscribeDelta") {
			return err
		}
		ce.methodSubscribeDelta = methodSubscribeDelta

		ce.wgMethod.Add(1)
		go func() {
			defer ce.wgMethod.Done()
			for {
				delta, err := ce.methodSubscribeDelta.Recv()
				if logger.ErrorLogHelper(err, "methodSubscribeDelta") {
					return
				}
				ce.wsHub.BroadcastDOMDelta(delta)
			}
		}()
	}

	return nil
}

func (e *Engine) FullDOM(ctx *fasthttp.RequestCtx) {
	e.DomResponse(ctx, *e.lastFullDOM.Load().(*proto.DOMBook))
}

func (e *Engine) Top100DOM(ctx *fasthttp.RequestCtx) {
	e.DomResponse(ctx, *e.lastTop100DOM.Load().(*proto.DOMBook))
}

func (e *Engine) DomResponse(ctx *fasthttp.RequestCtx, domBook easyjson.Marshaler) {
	if _, err := easyjson.MarshalToWriter(domBook, ctx); err != nil {
		auth.SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusInternalServerError)
		logger.Log.Error("error writing response", zap.Error(err),
			zap.String("local addr", ctx.LocalAddr().String()),
			zap.String("remote addr", ctx.RemoteAddr().String()),
			zap.String("headers", string(ctx.Request.Header.RawHeaders())),
		)
		return
	}
	ctx.Response.Header.SetCanonical([]byte(fasthttp.HeaderContentType), []byte("application/json"))
	ctx.Response.Header.SetCanonical([]byte(fasthttp.HeaderCacheControl), []byte("no-cache"))
	ctx.Response.SetStatusCode(fasthttp.StatusOK)
}

func (e *Engine) ServeWSDOM(ctx *fasthttp.RequestCtx) {
	reqID, err := BuildReqID(&e.acceptCntMatching, ctx.LocalAddr(), ctx.RemoteAddr(), ctx.ConnTime())
	if err != nil {
		logger.ErrorLogHelper(err, "ServeWSDOM")
		auth.SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusBadRequest)
		return
	}

	err = upgrader.Upgrade(ctx, func(conn *websocket.Conn) {
		e.WSSessions.WithLabelValues(string(ctx.RequestURI())).Inc()
		defer e.WSSessions.WithLabelValues(string(ctx.RequestURI())).Dec()

		client, err := NewClientDOM(conn, reqID)
		if err != nil {
			logger.ErrorLogHelper(err, "ServeWSOrder.NewClientOrder")
			err = conn.WriteMessage(websocket.TextMessage, []byte(err.Error()))
			if err != nil {
				logger.ErrorLogHelper(err, "ServeWSOrder.WriteError")
			}
			return
		}
		defers := e.wsHub.RegisterDOMClient(client)
		go client.WriteMessages()
		err = client.ReadMessages()
		if err != nil {
			if websocket.IsUnexpectedCloseError(
				err,
				websocket.CloseGoingAway,
				websocket.CloseAbnormalClosure,
				websocket.CloseNoStatusReceived,
			) {
				logger.ErrorLogHelper(err, "ServeWSOrder")
			}
		}
		for _, fn := range defers {
			fn()
		}
	})

	if err != nil {
		logger.ErrorLogHelper(err, "upgrader.register")
	}
}

package command

import (
	"context"
	"sync/atomic"
	"time"

	"github.com/fasthttp/websocket"
	"gitlab.com/stockengine/demo/internal/auth"
	"gitlab.com/stockengine/demo/internal/lib/config"

	"github.com/mailru/easyjson"
	"github.com/valyala/fasthttp"
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
	"go.uber.org/zap"
)

func initMatchingEngine(order, tx string, ce *Engine) (err error) {
	ce.StatusPrint()

	var ctx context.Context
	//connect to committed transactions
	ce.connStorage, err = config.GRPCClientDial(tx, true)
	if err != nil {
		return err
	}
	ce.storageClient = proto.NewStorageEngineClient(ce.connStorage)

	ctx, ce.cancelSubscribeTx = context.WithCancel(context.Background())
	ce.methodSubscribeTx, err = ce.storageClient.SubscribeTx(ctx, &proto.EmptyMessage{})
	if err != nil {
		return
	}

	//connect to order create cmd
	ce.connMatching, err = config.GRPCClientDial(order, true)
	if err != nil {
		return
	}
	ce.matchingClient = proto.NewMatchingEngineClient(ce.connMatching)

	ctx, ce.cancelProceed = context.WithCancel(context.Background())
	ce.methodProceed, err = ce.matchingClient.Proceed(ctx)
	if err != nil { // для единообразия
		return
	}

	upgrader.CheckOrigin = func(ctx *fasthttp.RequestCtx) bool { return true }
	ce.routeResponses()

	return
}

func (e *Engine) BuildCmd(body []byte, reqID proto.ReqID, ownerID uint64) (*proto.Command, error) {
	scmd := proto.SubCommands{}

	err := easyjson.Unmarshal(body, &scmd)
	if err != nil {
		return nil, err
	}

	switch {
	case scmd.New != nil:
		scmd.New.OwnerID = ownerID
	case scmd.Edit != nil:
		scmd.Edit.OwnerID = ownerID
	case scmd.Delete != nil:
		scmd.Delete.OwnerID = ownerID
	}

	cmd := &proto.Command{
		ReqID:       reqID,
		SubCommands: scmd,
	}
	return cmd, nil
}

func (e *Engine) wsReadCallback(msg []byte, reqID proto.ReqID, ownerID uint64) {
	cmd, err := e.BuildCmd(msg, reqID, ownerID)
	if err != nil {
		logger.ErrorLogHelper(err, "wsRead")
		return
	}
	if err := e.methodProceed.Send(cmd); err != nil {
		logger.ErrorLogHelper(err, "wsRead")
		return
	}
}

func (e *Engine) ServeWSOrder(ctx *fasthttp.RequestCtx, token *proto.AccessToken) {
	reqID, err := BuildReqID(&e.acceptCntMatching, ctx.LocalAddr(), ctx.RemoteAddr(), ctx.ConnTime())
	if err != nil {
		logger.ErrorLogHelper(err, "ServeWSOrder")
		auth.SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusBadRequest)
		return
	}

	err = upgrader.Upgrade(ctx, func(conn *websocket.Conn) {
		e.WSSessions.WithLabelValues(string(ctx.RequestURI())).Inc()
		defer e.WSSessions.WithLabelValues(string(ctx.RequestURI())).Dec()

		client, err := NewClientOrder(conn, reqID, token.Owner)
		if err != nil {
			logger.ErrorLogHelper(err, "ServeWSOrder.NewClientOrder")
			err = conn.WriteMessage(websocket.TextMessage, []byte(err.Error()))
			if err != nil {
				logger.ErrorLogHelper(err, "ServeWSOrder.WriteError")
			}
			return
		}
		defers := e.wsHub.RegisterOrderClient(client)
		go client.WriteMessages()
		err = client.ReadMessages(e.wsReadCallback)
		if err != nil {
			if websocket.IsUnexpectedCloseError(
				err,
				websocket.CloseGoingAway,
				websocket.CloseAbnormalClosure,
				websocket.CloseNoStatusReceived,
			) {
				logger.ErrorLogHelper(err, "ServeWSOrder")
			}
		}
		for _, fn := range defers {
			fn()
		}
	})

	if err != nil {
		logger.ErrorLogHelper(err, "upgrader.register")
	}
}

// parce request and build command
// send via bus command and subscribe to response
// wait till response
// convert response to command
//
func (e *Engine) HandleOrderOperation(ctx *fasthttp.RequestCtx, token *proto.AccessToken) {
	//ToDo: order pool
	reqID, err := BuildReqID(&e.acceptCntMatching, ctx.LocalAddr(), ctx.RemoteAddr(), ctx.ConnTime())
	if err != nil {
		logger.ErrorLogHelper(err, "HandleOrderOperation")
		auth.SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusInternalServerError)
		return
	}
	cmd, err := e.BuildCmd(ctx.PostBody(), reqID, token.Owner)
	if err != nil {
		logger.ErrorLogHelper(err, "HandleOrderOperation")
		auth.SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusBadRequest)
		return
	}

	ch, err := e.SendAndSubscribe(cmd)
	if err != nil {
		logger.ErrorLogHelper(err, "HandleOrderOperation")
		auth.SetErrorCtx(ctx, []byte("SendAndSubscribe err"), fasthttp.StatusUnprocessableEntity)
		return
	}
	if err := e.methodProceed.Send(cmd); err != nil {
		logger.ErrorLogHelper(err, "HandleOrderOperation")
		auth.SetErrorCtx(ctx, []byte("methodProceed.Send err"), fasthttp.StatusUnprocessableEntity)
		return
	}

	timer := time.NewTimer(5 * time.Second)
	defer timer.Stop()

	select {
	case tx := <-ch:
		if _, err := easyjson.MarshalToWriter(tx, ctx); err != nil {
			logger.ErrorLogHelper(err, "HandleOrderOperation")
			auth.SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.SetCanonical([]byte("Content-Type"), []byte("application/json"))
		ctx.Response.SetStatusCode(fasthttp.StatusOK)
		atomic.AddUint64(&e.responceCntMatching, 1)
	case <-timer.C:
		auth.SetErrorCtx(ctx, []byte("Timeout"), fasthttp.StatusNoContent)
		atomic.AddUint64(&e.timeoutCntMatching, 1)
	}
}

func (e *Engine) SendAndSubscribe(command *proto.Command) (<-chan *proto.CommitedTransaction, error) {
	e.rwMux.Lock()
	defer e.rwMux.Unlock()

	_, ok := e.subscribers[command.ReqID]
	if ok {
		return nil, ErrorAlreadyExists(command.ReqID)
	}
	rc := make(chan *proto.CommitedTransaction, 1)
	e.subscribers[command.ReqID] = rc
	return rc, nil
}

func (e *Engine) handleResponse(tx *proto.CommitedTransaction) {
	e.rwMux.Lock()
	defer e.rwMux.Unlock()
	// определяем куда направить ответ
	var chOut chan *proto.CommitedTransaction
	closeChan := false
	if client, ok := e.wsHub.wsOrderClients[tx.Transaction.Command.ReqID]; ok {
		// отвечаем в сокет
		chOut = client.GetSendCh()
	} else if ch, ok := e.subscribers[tx.Transaction.Command.ReqID]; ok {
		// отвечаем на хттп запрос
		chOut = ch
		closeChan = true
	} else {
		logger.Log.Debug("error: no subscriber found", zap.Reflect("transaction", tx))
		return
	}

	select {
	case chOut <- tx:
	default:
		logger.Log.Warn("LOCK DETECTED, performance degraded: chOut for transaction is full :( waiting")
		chOut <- tx
	}

	if closeChan {
		close(chOut)
		delete(e.subscribers, tx.Transaction.Command.ReqID)
	}
}

func helperLoadAndDelta(cnt *uint64, old uint64) (delta, new uint64) {
	new = atomic.LoadUint64(cnt)
	delta = new - old
	return
}

func (e *Engine) StatusPrint() {
	go func() {
		ticker := time.NewTicker(time.Second)
		var acc, accDelta, resp, respDelta, timeout, timeDelta uint64
		for range ticker.C {
			accDelta, acc = helperLoadAndDelta(&e.acceptCntMatching, acc)
			respDelta, resp = helperLoadAndDelta(&e.responceCntMatching, resp)
			timeDelta, timeout = helperLoadAndDelta(&e.timeoutCntMatching, timeout)

			//skip dummy printing in case of no changes
			if accDelta == 0 || respDelta == 0 || timeDelta == 0 {
				continue
			}

			logger.Log.Debug("performance monitoring",
				zap.Uint64("accepted ps", accDelta),
				zap.Uint64("responses ps", respDelta),
				zap.Uint64("timeouts ps", timeDelta),
				zap.Uint64("total accepted", acc),
				zap.Uint64("total responses", resp),
				zap.Uint64("total timeouts", timeout),
			)
		}
	}()
}

func (e *Engine) routeResponses() {
	e.wgMethod.Add(1)
	go func() {
		defer e.wgMethod.Done()
		for {
			tx, err := e.methodSubscribeTx.Recv()
			if logger.ErrorLogHelper(err, "methodSubscribeTx") {
				return
			}
			e.handleResponse(tx)
		}
	}()
}

package command

import (
	"time"

	"github.com/fasthttp/websocket"
	"github.com/mailru/easyjson"
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 1024 * 4
)

const (
	TypeClientOrder = iota
	TypeClientDOM
)

var upgrader = websocket.FastHTTPUpgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type (
	WSClient interface {
		ReadMessages(...ReadCallback) error
		WriteMessages()
		SendWriteClose() error
		ReqID() proto.ReqID
		Conn() *websocket.Conn
		WriteMessage(v easyjson.Marshaler) error
		//closeWriteMessages(t *time.Ticker, wg *sync.WaitGroup)
		CloseSendCh()
		setConnDefaults() error
		ping() error
		setHub(h *WSHub)
	}

	WSClientOrder interface {
		WSClient
		GetSendCh() chan *proto.CommitedTransaction
	}

	WSClientDOM interface {
		WSClient
		GetSendCh() chan *proto.DOMDelta
	}

	Client struct {
		conn    *websocket.Conn
		reqID   proto.ReqID
		ownerID uint64
		hub     *WSHub
	}

	ClientOrder struct {
		Client
		send chan *proto.CommitedTransaction
	}

	ClientDOM struct {
		Client
		send chan *proto.DOMDelta
	}

	DefersWS     []func()
	ReadCallback func(msg []byte, reqID proto.ReqID, ownerID uint64)
)

func NewClientOrder(conn *websocket.Conn, reqID proto.ReqID, ownerID uint64) (WSClientOrder, error) {
	c := &ClientOrder{
		send: make(chan *proto.CommitedTransaction, 1000),
		Client: Client{
			conn:    conn,
			reqID:   reqID,
			ownerID: ownerID,
		},
	}
	err := c.setConnDefaults()
	if err != nil {
		return nil, err
	}
	return c, nil
}

func NewClientDOM(conn *websocket.Conn, reqID proto.ReqID) (WSClientDOM, error) {
	c := &ClientDOM{
		send: make(chan *proto.DOMDelta, 1000),
		Client: Client{
			conn:  conn,
			reqID: reqID,
		},
	}
	err := c.setConnDefaults()
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Client) setConnDefaults() error {
	c.conn.SetReadLimit(maxMessageSize)
	err := c.conn.SetReadDeadline(time.Now().Add(pongWait))
	if err != nil {
		return err
	}
	c.conn.SetPongHandler(func(string) error {
		return c.conn.SetReadDeadline(time.Now().Add(pongWait))
	})
	return nil
}

func (c *Client) setHub(h *WSHub) {
	c.hub = h
}

func (c *Client) ReadMessages(...ReadCallback) error {
	return nil
}

func (c *Client) WriteMessages() {
}

func (c *Client) SendWriteClose() error {
	err := c.conn.WriteMessage(websocket.CloseMessage, []byte{})
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) WriteMessage(v easyjson.Marshaler) error {
	_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))

	w, err := c.conn.NextWriter(websocket.TextMessage)
	if err != nil {
		return err
	}

	if _, err := easyjson.MarshalToWriter(v, w); err != nil {
		logger.ErrorLogHelper(err, "writeMessages.MarshalToWriter")
		return err
	}

	if err := w.Close(); err != nil {
		return err
	}
	return nil
}

func (c *Client) ping() error {
	err := c.conn.SetWriteDeadline(time.Now().Add(writeWait))
	if err != nil {
		return err
	}
	return c.conn.WriteMessage(websocket.PingMessage, nil)
}

func (c *Client) ReqID() proto.ReqID {
	return c.reqID
}

func (c *Client) CloseSendCh() {

}

func (c *Client) Conn() *websocket.Conn {
	return c.conn
}

func (cd *ClientDOM) ReadMessages(...ReadCallback) error {
	for {
		// прорсто читаем в пустоту
		_, _, err := cd.conn.ReadMessage()
		if err != nil {
			return err
		}
		globalWSOperations.WithLabelValues("in", "DOMReadMessages").Inc()
	}
}

func (cd *ClientDOM) WriteMessages() {
	cd.hub.wg.Add(1)
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		defer func() {
			ticker.Stop()
			err := cd.conn.Close()
			if err != nil {
				logger.ErrorLogHelper(err, "writeMessages.defer.conn.Close")
			}
			cd.hub.wg.Done()
		}()
	}()
	for {
		select {
		case tx, ok := <-cd.send:
			if !ok {
				logger.Log.Debug("writeMessages cd.send closed")
				return
			}
			if cd.conn == nil {
				logger.Log.Debug("writeMessages cd.conn is nil")
				return
			}
			err := cd.WriteMessage(tx)
			if err != nil {
				logger.ErrorLogHelper(err, "WriteMessages")
				return
			}
			globalWSOperations.WithLabelValues("out", "DOMWriteMessages").Inc()
		case <-ticker.C:
			err := cd.ping()
			if err != nil {
				logger.ErrorLogHelper(err, "WriteMessages")
				return
			}
		}
	}
}

func (cd *ClientDOM) CloseSendCh() {
	close(cd.send)
}

func (cd *ClientDOM) GetSendCh() chan *proto.DOMDelta {
	return cd.send
}

func (co *ClientOrder) GetSendCh() chan *proto.CommitedTransaction {
	return co.send
}

func (co *ClientOrder) ReadMessages(cb ...ReadCallback) error {
	for {
		_, message, err := co.conn.ReadMessage()
		if err != nil {
			return err
		}
		globalWSOperations.WithLabelValues("in", "OrderReadMessages").Inc()
		for _, c := range cb {
			c(message, co.reqID, co.ownerID)
		}
	}
}

func (co *ClientOrder) WriteMessages() {
	co.hub.wg.Add(1)
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		err := co.conn.Close()
		if err != nil {
			logger.ErrorLogHelper(err, "writeMessages.defer.conn.Close")
		}
		co.hub.wg.Done()
	}()
	for {
		select {
		case tx, ok := <-co.send:
			if !ok {
				logger.Log.Debug("writeMessages co.send closed")
				return
			}
			if co.conn == nil {
				logger.Log.Debug("writeMessages co.conn is nil")
				return
			}
			err := co.WriteMessage(tx)
			if err != nil {
				logger.ErrorLogHelper(err, "WriteMessages")
				return
			}
			globalWSOperations.WithLabelValues("out", "OrderWriteMessages").Inc()
		case <-ticker.C:
			err := co.ping()
			if err != nil {
				logger.ErrorLogHelper(err, "WriteMessages")
				return
			}
		}
	}
}

func (co *ClientOrder) CloseSendCh() {
	close(co.send)
}

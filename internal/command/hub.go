package command

import (
	"sync"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
)

type (
	WSHub struct {
		rwDOMMux       sync.RWMutex
		rwOrderMux     sync.RWMutex
		wsOrderClients map[proto.ReqID]WSClientOrder
		wsDOMClients   map[proto.ReqID]WSClientDOM
		wg             sync.WaitGroup
	}
)

func NewWSHub() *WSHub {
	return &WSHub{
		wsOrderClients: make(map[proto.ReqID]WSClientOrder),
		wsDOMClients:   make(map[proto.ReqID]WSClientDOM),
	}
}

func (h *WSHub) register(client WSClient, clientType uint) DefersWS {
	defers := DefersWS{
		func() {
			// не придумал как лучше избавиться от пасты :(
			if clientType == TypeClientDOM {
				h.rwDOMMux.Lock()
				if _, ok := h.wsDOMClients[client.ReqID()]; ok {
					delete(h.wsDOMClients, client.ReqID())
					client.CloseSendCh()
				}
				h.rwDOMMux.Unlock()
			} else {
				h.rwOrderMux.Lock()
				if _, ok := h.wsOrderClients[client.ReqID()]; ok {
					delete(h.wsOrderClients, client.ReqID())
					client.CloseSendCh()
				}
				h.rwOrderMux.Unlock()
			}
		},
		func() {
			err := client.Conn().Close()
			if err != nil {
				logger.ErrorLogHelper(err, "register")
			}
		},
	}
	if clientType == TypeClientDOM {
		h.rwDOMMux.Lock()
		h.wsDOMClients[client.ReqID()] = client.(WSClientDOM)
		h.rwDOMMux.Unlock()
	} else {
		h.rwOrderMux.Lock()
		h.wsOrderClients[client.ReqID()] = client.(WSClientOrder)
		h.rwOrderMux.Unlock()
	}
	client.setHub(h)
	return defers
}

func (h *WSHub) RegisterDOMClient(client WSClient) DefersWS {
	return h.register(client, TypeClientDOM)
}

func (h *WSHub) RegisterOrderClient(client WSClient) DefersWS {
	return h.register(client, TypeClientOrder)
}

func (h *WSHub) BroadcastDOMDelta(delta *proto.DOMDelta) {
	for _, c := range h.wsDOMClients {
		c.GetSendCh() <- delta
	}
}

func (h *WSHub) Close() {
	logger.Log.Debug("closing all websocket DOM clients")
	for _, c := range h.wsDOMClients {
		err := c.SendWriteClose()
		if logger.ErrorLogHelper(err, "CloseMatching.c.SendWriteClose") {
			return
		}
	}

	logger.Log.Debug("closing all websocket Order clients")
	for _, c := range h.wsOrderClients {
		err := c.SendWriteClose()
		if logger.ErrorLogHelper(err, "CloseMatching.c.SendWriteClose") {
			return
		}
	}
	h.wg.Wait()
}

package command

import (
	"context"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	prometheus2 "github.com/prometheus/client_golang/prometheus"

	"gitlab.com/stockengine/demo/internal/lib/prometheus"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type Engine struct {
	// matching & storage
	acceptCntMatching   uint64
	responceCntMatching uint64
	timeoutCntMatching  uint64

	connStorage, connMatching, connDom *grpc.ClientConn

	matchingClient proto.MatchingEngineClient
	storageClient  proto.StorageEngineClient
	domClient      proto.DOMClient

	methodProceed            proto.MatchingEngine_ProceedClient
	methodSubscribeTx        proto.StorageEngine_SubscribeTxClient
	methodSubscribeShortBook proto.DOM_SubscribeShortBookClient
	methodSubscribeDelta     proto.DOM_SubscribeDeltaClient

	cancelProceed,
	cancelSubscribeTx,
	cancelSubscribeShortBook,
	cancelSubscribeDelta context.CancelFunc
	subscribers routerMap

	// DOM
	lastFullDOM,
	lastTop100DOM atomic.Value //*proto.DOMBook

	// websockets
	wsHub *WSHub

	rwMux    sync.RWMutex
	wgMethod sync.WaitGroup

	//metrics
	prom         *prometheus.Prometheus
	HTTPStatus   prometheus2.HistogramVec
	WSSessions   prometheus2.GaugeVec
	WSOperations prometheus2.CounterVec
}

//так сделал что б во все коннекты не пробресывать +1 поле в 8 байт - указатель на счётчик
var globalWSOperations prometheus2.CounterVec

type routerMap map[proto.ReqID]chan *proto.CommitedTransaction

type ErrorAlreadyExists proto.ReqID

func (t ErrorAlreadyExists) Error() string {
	return fmt.Sprintf("req[%v:%v:%v:%v] already exists", t.IPLow, t.SrcDstPort, t.ConnectTS, t.AcceptCnt)
}

func NewService(order, tx, dom, prometheusBind string) (ce *Engine, err error) {
	prom, err := prometheus.New(prometheusBind)
	if err != nil {
		return nil, err
	}

	ce = &Engine{
		subscribers: make(routerMap),
		wsHub:       NewWSHub(),
		HTTPStatus: prom.NewHistogramVec("http", "request processing status and duration",
			[]string{"method", "operation", "status"},
			prometheus.DefBucketsLowLat,
		),
		WSSessions: prom.NewGaugeVec("ws", "number of active web sockets", []string{"operation"}),
		WSOperations: prom.NewCounterVec("WSOperations", "WS Operations counters",
			[]string{"direction", "operation"}),
		prom: prom,
	}

	globalWSOperations = ce.WSOperations

	err = initMatchingEngine(order, tx, ce)
	if err != nil {
		return
	}

	err = initDOMEngine(dom, ce)
	if err != nil {
		return
	}

	return
}

func (e *Engine) Stop() {
	//ToDo: надо бы закрывать все WS коннекты. пока HiJacked коннекты не закрываются.. блин, они закрываются... почему же тогда подвисает закрытие?
	logger.Log.Debug("Shutdown Engine...")
	start := time.Now()

	//отключаемся от методов
	e.cancelProceed()
	e.cancelSubscribeTx()
	e.cancelSubscribeShortBook()
	e.cancelSubscribeDelta()
	e.wsHub.Close()
	e.wgMethod.Wait()

	//закрываем коннекты, что б не ждать keepalive probe
	logger.ErrorLogHelper(e.connStorage.Close(), "e.connStorage.Stop()")
	logger.ErrorLogHelper(e.connDom.Close(), "e.connDom.Stop()")
	logger.ErrorLogHelper(e.connMatching.Close(), "e.connMatching.Stop()")

	logger.ErrorLogHelper(e.prom.Close(), "prom.Close()")

	elapsed := time.Since(start)
	logger.Log.Debug("...done", zap.Duration("elapsed", elapsed))
}

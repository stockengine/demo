package command

import (
	"context"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"math"
	"net"
	"net/http"
	"testing"
	"time"

	"github.com/mailru/easyjson"
	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
	"gitlab.com/stockengine/demo/internal/proto"
)

/*
BenchmarkRouter1e1-8      973218              1182 ns/op
BenchmarkRouter1e3-8     1000000              1173 ns/op
BenchmarkRouter1e6-8     1000000              1302 ns/op

*/

func helperRouterTest(b *testing.B, n int) {
	e := Engine{
		subscribers: make(routerMap),
		wsHub:       NewWSHub(),
	}
	for i := 0; i < n; i++ {
		_, err := e.SendAndSubscribe(&proto.Command{
			ReqID: proto.ReqID{
				IPLow:      0,
				SrcDstPort: 0,
				ConnectTS:  uint32(i + 1),
				AcceptCnt:  0,
			},
		})
		assert.NoError(b, err)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rid := proto.ReqID{
			IPLow:      uint32(i / math.MaxUint32),
			SrcDstPort: uint32(i % math.MaxUint32),
			ConnectTS:  0,
			AcceptCnt:  0,
		}
		ch, err := e.SendAndSubscribe(&proto.Command{
			ReqID: rid,
		})
		if !assert.NoError(b, err) {
			return
		}
		e.handleResponse(&proto.CommitedTransaction{
			Transaction: proto.Transaction{
				Command: proto.Command{
					ReqID: rid,
				},
			},
		})
		<-ch //trash message
	}
}

func BenchmarkRouter1e1(b *testing.B) {
	helperRouterTest(b, 1e1)
}
func BenchmarkRouter1e3(b *testing.B) {
	helperRouterTest(b, 1e3)
}

// serve serves http request using provided fasthttp handler
func serve(t *testing.T, handler fasthttp.RequestHandler, req *http.Request) (*http.Response, error) {
	ln := fasthttputil.NewInmemoryListener()
	defer func() {
		err := ln.Close()
		assert.NoError(t, err)
	}()

	go func() {
		err := fasthttp.Serve(ln, handler)
		if err != nil {
			panic(fmt.Errorf("failed to serve: %v", err))
		}
	}()

	client := http.Client{
		Transport: &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return ln.Dial()
			},
		},
	}

	return client.Do(req)
}

func serveRequest(t *testing.T, handler fasthttp.RequestHandler) (*http.Response, error) {
	r, err := http.NewRequest("GET", "http://test/", nil)
	if err != nil {
		return nil, err
	}

	res, err := serve(t, handler, r)
	if err != nil {
		return nil, err
	}

	return res, nil
}

type myAddr struct{}

func (a *myAddr) Network() string { return "tcp" }

func (a *myAddr) String() string {
	return ""
}

func TestBuildCmd(t *testing.T) {
	cnt := uint64(0)
	localAddr := net.TCPAddr{IP: net.IP{0, 0, 0, 0}, Port: 0, Zone: ""}
	remoteAddr := net.TCPAddr{IP: net.IP{0, 0, 0, 0}, Port: 0, Zone: ""}
	connTime := time.Now()
	_, err := BuildReqID(&cnt, &myAddr{}, &remoteAddr, connTime)

	assert.Error(t, err, "should be error")
	assert.Equal(t, err, ErrorInvalidLocalAddr, "should be ErrorInvalidLocalAddr")

	_, err = BuildReqID(&cnt, &localAddr, &myAddr{}, connTime)

	assert.Error(t, err, "should be error")
	assert.Equal(t, err, ErrorInvalidRemoteAddr, "should be ErrorInvalidRemoteAddr")

	reqID, err := BuildReqID(&cnt, &localAddr, &remoteAddr, connTime)

	reqIDAwaited := proto.ReqID{
		IPLow:      binary.BigEndian.Uint32(remoteAddr.IP),
		SrcDstPort: uint32(remoteAddr.Port) | (uint32(localAddr.Port) << 16),
		ConnectTS:  uint32(connTime.Unix()),
		AcceptCnt:  1,
	}

	assert.NoError(t, err, "should be no error")
	assert.Equal(t, reqID, reqIDAwaited, "wrong proto.ReqID")

	body := []byte(`{"New":{"Type":0,"Operation":0,"Amount":100,"Price":0.00000100,"OwnerID":123,"ExtID":1}}`)
	e := Engine{}
	cmd, err := e.BuildCmd(body, reqID, 123)
	assert.NoError(t, err)

	scmd := proto.SubCommands{}
	err = easyjson.Unmarshal(body, &scmd)
	cmdAwaited := &proto.Command{
		ReqID:       reqID,
		SubCommands: scmd,
	}
	assert.NoError(t, err)
	assert.Equal(t, cmd, cmdAwaited)
}

func validateDomResponse(t *testing.T, res *http.Response, domBook easyjson.Marshaler) {
	domJsonResp, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)

	domJson, err := easyjson.Marshal(domBook)
	assert.Equal(t, domJsonResp, domJson)

	assert.Equal(t, res.Header[fasthttp.HeaderContentType][0], "application/json")
	assert.Equal(t, res.Header[fasthttp.HeaderCacheControl][0], "no-cache")
	assert.Equal(t, res.StatusCode, fasthttp.StatusOK)
}

func makeDOMBook() proto.DOMBook {
	bidAsk := []proto.DOMLine{{
		Count:  1,
		Price:  proto.Price{Price: 1234},
		Amount: proto.Amount{Byte0: 1, Byte1: 1, Byte2: 1, Byte3: 1, Byte4: 1, Byte5: 1},
	}}

	return proto.DOMBook{
		CommitID: 1234,
		Bid:      bidAsk,
		Ask:      bidAsk,
	}
}

func TestDomResponse(t *testing.T) {
	e := Engine{}
	domBook := makeDOMBook()

	handler := func(ctx *fasthttp.RequestCtx) {
		e.DomResponse(ctx, domBook)
	}

	res, err := serveRequest(t, handler)
	assert.NoError(t, err)
	validateDomResponse(t, res, domBook)
}

func TestRESTCommands(t *testing.T) {
	tests := []struct {
		obj  easyjson.Marshaler
		json string
	}{
		{obj: &proto.SubCommands{
			New: &proto.NewOrder{
				Type:      proto.Type_Limit,
				Operation: proto.Operation_Buy,
				Amount:    proto.NewAmountStringMust("9876543210012345678901234567890123456789"),
				Price:     proto.NewPriceMust("1234567890.0987654321"),
				OwnerID:   123456,
				ExtID:     3456789,
			}},
			json: `{"New":{"Type":"Limit","Operation":"Buy","Amount":9876543210.01234567890123456789,"Price":1234567890.0987654321,"OwnerID":123456,"ExtID":3456789}}`,
		},
		{
			obj: &proto.SubCommands{
				Delete: &proto.DeleteOrder{
					OwnerID: 123456,
					ExtID:   456789,
				}},
			json: `{"Delete":{"OwnerID":123456,"ExtID":456789}}`,
		},
		{
			obj: &proto.SubCommands{
				Edit: &proto.EditOrder{
					OwnerID:   123456,
					ExtID:     456789,
					NewAmount: proto.NewAmountStringMust("9876543210012345678901234567890123456789"),
					NewPrice:  proto.NewPriceMust("1234567890.0987654321"),
				}},
			json: `{"Edit":{"OwnerID":123456,"ExtID":456789,"NewAmount":9876543210.01234567890123456789,"NewPrice":1234567890.0987654321}}`,
		},
	}

	for i, test := range tests {
		b, e := easyjson.Marshal(test.obj)
		assert.NoError(t, e, i)
		assert.Equal(t, test.json, string(b))
	}
}

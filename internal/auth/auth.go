package auth

import (
	"crypto"
	"crypto/rand"
	"errors"
	"math"
	"reflect"
	"runtime"

	gogoproto "github.com/gogo/protobuf/proto"
	"github.com/mailru/easyjson"
	"github.com/o1egl/paseto"

	"gitlab.com/stockengine/demo/internal/proto"
	"golang.org/x/crypto/argon2"
)

const saltLen = 16
const argon2Time = 1
const argon2Mem = 64 * 1024
const argon2KeyLen = 32

func hashPass(pass []byte) (hash, salt []byte, alg proto.AuthAlg, err error) {
	salt = make([]byte, saltLen)
	_, err = rand.Read(salt)
	if err != nil {
		return
	}
	threads := runtime.GOMAXPROCS(0)
	if threads > math.MaxUint8 {
		threads = math.MaxUint8
	}
	alg.Alg = "argon2.IDKey"
	alg.Iter = argon2Time
	alg.Mem = argon2Mem
	alg.Par = int64(threads)
	alg.Ver = argon2.Version
	alg.Len = argon2KeyLen
	hash = hashPassWrapper(pass, salt, alg)
	return
}

func hashPassWrapper(pass []byte, salt []byte, alg proto.AuthAlg) (hash []byte) {
	hash = argon2.IDKey(pass, salt, uint32(alg.Iter), uint32(alg.Mem), uint8(alg.Par), uint32(alg.Len))
	return
}

type encodeDataJSONInterface interface {
	easyjson.Marshaler
	easyjson.Unmarshaler
}
type encodeDataProtoInterface interface {
	gogoproto.Marshaler
	gogoproto.Unmarshaler
}
type encodeDataInterface interface {
	encodeDataJSONInterface
	encodeDataProtoInterface
}

func encodeHelper(token encodeDataInterface, key crypto.PrivateKey, footerEncode bool) (ret string, err error) {
	pst := paseto.NewV2()
	payload, err := token.Marshal() // it will be ecnrypted. use the same as footer
	if err != nil {
		return
	}
	var footer []byte
	if footerEncode {
		footer, err = easyjson.Marshal(token) // js must be able to read token, so use JSON
		if err != nil {
			return
		}
	}

	ret, err = pst.Sign(key, payload, footer)
	return
}

//nolint:interfacer,unparam
func decodeHelper(token encodeDataInterface, key crypto.PublicKey, data string, footerDecode bool) (err error) {
	//загрущаем и верифицируем данные
	pst := paseto.NewV2()
	var payload, footer []byte
	if err = pst.Verify(data, key, &payload, &footer); err != nil {
		return
	}

	// декодируем реальную структуру токенов
	if err = token.Unmarshal(payload); err != nil {
		return
	}
	if footerDecode {
		cpy := reflect.New(reflect.TypeOf(token).Elem()).Interface().(encodeDataJSONInterface) //мы и так на входе имеем указатель
		if err = easyjson.Unmarshal(footer, cpy); err != nil {
			return
		}
		//сравниваем (это избыточно, paseto должно гарантировать защиту)
		if !reflect.DeepEqual(token, cpy) {
			return errors.New("token payload and footer arent equal")
		}
	}

	return nil
}

func encryptHelper(token gogoproto.Marshaler, secretkey []byte) (ret string, err error) {
	var buf []byte
	buf, err = token.Marshal()
	if err != nil {
		return
	}

	pst := paseto.NewV2()
	return pst.Encrypt(secretkey, buf, nil)
}

func decryptHelper(token gogoproto.Unmarshaler, secretkey []byte, data string) (err error) {
	var buf []byte
	pst := paseto.NewV2()
	if err = pst.Decrypt(data, secretkey, &buf, nil); err != nil {
		return
	}

	return token.Unmarshal(buf)
}

package auth

import (
	"time"

	"github.com/mailru/easyjson"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"github.com/valyala/fasthttp"
)

const googleRecaptchaURL = "https://www.google.com/recaptcha/api/siteverify"

// https://developers.google.com/recaptcha/docs/verify
// https://developers.google.com/recaptcha/docs/v3

//easyjson:json
type RecaptchaResp struct {
	Success     bool      `json:"success"`      // whether this request was a valid reCAPTCHA token for your site
	Score       float64   `json:"score"`        // the score for this request (0.0 - 1.0)
	Action      string    `json:"action"`       // the action name for this request (important to verify)
	ChallengeTS time.Time `json:"challenge_ts"` // timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
	Hostname    string    `json:"hostname"`     // the hostname of the site where the reCAPTCHA was solved
	ErrorCodes  []string  `json:"error-codes"`  // optional
}

type RecaptchaChecker struct {
	token      string
	uri        *fasthttp.URI
	httpClient fasthttp.Client
}

func NewRecaptchaChecker(token string) (*RecaptchaChecker, error) {
	uri := fasthttp.AcquireURI()
	uri.Update(googleRecaptchaURL)
	return &RecaptchaChecker{token: token, uri: uri}, nil
}

func (t *RecaptchaChecker) Validate(response, ip string) (ret RecaptchaResp) {
	args := fasthttp.AcquireArgs()
	defer fasthttp.ReleaseArgs(args)
	t.uri.QueryArgs().CopyTo(args)

	args.Set("secret", t.token)
	args.Set("response", response)
	if ip != "" {
		args.Set("remoteip", ip)
	}

	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	req.Header.SetMethod(fasthttp.MethodPost)
	args.QueryString()
	t.uri.CopyTo(req.URI())
	args.CopyTo(req.URI().QueryArgs())

	err := t.httpClient.DoTimeout(req, resp, time.Second)
	if err != nil {
		logger.Log.Error("cant check recaptcha", zap.Error(err), zap.String("request", req.String()), zap.String("response", resp.String()))
		return
	}

	if err = easyjson.Unmarshal(resp.Body(), &ret); err != nil {
		logger.Log.Error("cant check recaptcha. unmarshall error", zap.Error(err), zap.String("request", req.String()), zap.String("response", resp.String()))
		return
	}
	return
}

package auth

import (
	"context"
	"crypto/ed25519"
	"encoding/hex"
	"fmt"
	"net"
	"strings"
	"testing"
	"time"

	"github.com/jackc/pgmock"
	"github.com/jackc/pgproto3/v2"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/stockengine/demo/internal/proto"
)

func helperHexMust(data string) []byte {
	ret, err := hex.DecodeString(data)
	if err != nil {
		panic(err)
	}
	return ret
}

func TestGrpcMethods(t *testing.T) {
	script := &pgmock.Script{
		Steps: pgmock.AcceptUnauthenticatedConnRequestSteps(),
	}

	// вызов хранимки для создания записи пользователя
	/*
		F {"Type":"Parse","Name":"lrupsc_1_0","Query":"select auth.new_auth_user2($1, $2, $3, $4, $5, $6, $7)","ParameterOIDs":null}
		F {"Type":"Describe","ObjectType":"S","Name":"lrupsc_1_0"}
		F {"Type":"Sync"}
		B {"Type":"ParseComplete"}
		B {"Type":"ParameterDescription","ParameterOIDs":[25,17,25,17,17,17,16]}
		B {"Type":"RowDescription","Fields":[{"Name":"new_auth_user2","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":23,"DataTypeSize":4,"TypeModifier":-1,"Format":0}]}
		B {"Type":"ReadyForQuery","TxStatus":"I"}
	*/
	const callPrefix = "lrupsc_2_"
	script.Steps = append(script.Steps,
		pgmock.ExpectMessage(&pgproto3.Parse{Name: callPrefix + "0", Query: "select auth.new_auth_user2($1, $2, $3, $4, $5, $6, $7)"}),
		pgmock.ExpectMessage(&pgproto3.Describe{Name: callPrefix + "0", ObjectType: 'S'}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25, 17, 25, 17, 17, 17, 16}}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{
				Name:                 []byte("new_auth_user2"),
				TableOID:             0,
				TableAttributeNumber: 0,
				DataTypeOID:          23,
				DataTypeSize:         4,
				TypeModifier:         -1,
				Format:               0,
			}}}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_0","ParameterFormatCodes":[0,1,0,1,1,1,1],"Parameters":[{"text":"test@test.com"},{"binary":"fdaf87d989c33c1b800f4061c3a68542"},{"text":"DemoRole"},{"binary":"3842daf1f4cab76a4b49d45926808732de011a8f31a4a0e57fea64c848867a43"},{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},{"binary":"54ddbcaec65a9cf71dcdd4fd3c0e47cb"},{"binary":"01"}],"ResultFormatCodes":[1]}
			F {"Type":"Describe","ObjectType":"P","Name":""}
			F {"Type":"Execute","Portal":"","MaxRows":0}
			F {"Type":"Sync"}
			B {"Type":"BindComplete"}
			B {"Type":"RowDescription","Fields":[{"Name":"new_auth_user2","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":23,"DataTypeSize":4,"TypeModifier":-1,"Format":1}]}
			B {"Type":"DataRow","Values":[{"binary":"00000001"}]}
			B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
			B {"Type":"ReadyForQuery","TxStatus":"I"}

		*/

		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), //в параметрах запроса уходит рандомная строка, бесполезно скриптовать
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{Portal: "", MaxRows: 0}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{{Name: []byte("new_auth_user2"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 23, DataTypeSize: 4, TypeModifier: -1, Format: 1}}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{helperHexMust("00000001")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
	)

	//ауентификация пользователя
	/*
			F {"Type":"Parse","Name":"lrupsc_1_1","Query":"select owner, pass_hash, pass_alg, pass_salt from auth.users where login=$1","ParameterOIDs":null}
			F {"Type":"Describe","ObjectType":"S","Name":"lrupsc_1_1"}
			F {"Type":"Sync"}
			B {"Type":"ParseComplete"}
			B {"Type":"ParameterDescription","ParameterOIDs":[25]}
			B {"Type":"RowDescription","Fields":[{"Name":"owner","TableOID":16398,"TableAttributeNumber":2,"DataTypeOID":20,"DataTypeSize":8,"TypeModifier":-1,"Format":0},
		{"Name":"pass_hash","TableOID":16398,"TableAttributeNumber":4,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":0},
		{"Name":"pass_alg","TableOID":16398,"TableAttributeNumber":5,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":0},
		{"Name":"pass_salt","TableOID":16398,"TableAttributeNumber":6,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":0}]}
			B {"Type":"ReadyForQuery","TxStatus":"I"}
	*/
	script.Steps = append(script.Steps,
		pgmock.ExpectMessage(&pgproto3.Parse{Name: callPrefix + "1", Query: "select owner, pass_hash, pass_alg, pass_salt from auth.users where login=$1"}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'S', Name: callPrefix + "1"}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25}}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("owner"), TableOID: 16398, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 0},
			{Name: []byte("pass_hash"), TableOID: 16398, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 0},
			{Name: []byte("pass_alg"), TableOID: 16398, TableAttributeNumber: 5, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 0},
			{Name: []byte("pass_salt"), TableOID: 16398, TableAttributeNumber: 6, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 0}}}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		/*
				F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_1","ParameterFormatCodes":[0],"Parameters":[{"text":"test@test.com"}],"ResultFormatCodes":[1,1,1,1]}
				F {"Type":"Describe","ObjectType":"P","Name":""}
				F {"Type":"Execute","Portal":"","MaxRows":0}
				F {"Type":"Sync"}
				B {"Type":"BindComplete"}
				B {"Type":"RowDescription","Fields":[{"Name":"owner","TableOID":16398,"TableAttributeNumber":2,"DataTypeOID":20,"DataTypeSize":8,"TypeModifier":-1,"Format":1},
			{"Name":"pass_hash","TableOID":16398,"TableAttributeNumber":4,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},
			{"Name":"pass_alg","TableOID":16398,"TableAttributeNumber":5,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},
			{"Name":"pass_salt","TableOID":16398,"TableAttributeNumber":6,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1}]}
				B {"Type":"DataRow","Values":[
			{"binary":"567fc6f3ea1a6e72"},
			{"binary":"3842daf1f4cab76a4b49d45926808732de011a8f31a4a0e57fea64c848867a43"},
			{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},
			{"binary":"54ddbcaec65a9cf71dcdd4fd3c0e47cb"}]}
				B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
				B {"Type":"ReadyForQuery","TxStatus":"I"}

		*/
		pgmock.ExpectMessage(&pgproto3.Bind{
			DestinationPortal:    "",
			PreparedStatement:    callPrefix + "1",
			ParameterFormatCodes: []int16{0},
			Parameters:           [][]byte{[]byte("test@test.com")},
			ResultFormatCodes:    []int16{1, 1, 1, 1},
		}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("owner"), TableOID: 16422, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_hash"), TableOID: 16422, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_alg"), TableOID: 16422, TableAttributeNumber: 5, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_salt"), TableOID: 16422, TableAttributeNumber: 6, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1}}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{
			helperHexMust("567fc6f3ea1a6e72"),
			helperHexMust("3842daf1f4cab76a4b49d45926808732de011a8f31a4a0e57fea64c848867a43"),
			helperHexMust("0a0c6172676f6e322e49444b6579101318808004200128083020"),
			helperHexMust("54ddbcaec65a9cf71dcdd4fd3c0e47cb")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		/*
				F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_1","ParameterFormatCodes":[0],"Parameters":[{"text":"test@test.com"}],"ResultFormatCodes":[1,1,1,1]}
				F {"Type":"Describe","ObjectType":"P","Name":""}
				F {"Type":"Execute","Portal":"","MaxRows":0}
				F {"Type":"Sync"}
				B {"Type":"BindComplete"}
				B {"Type":"RowDescription","Fields":[
			{"Name":"owner","TableOID":16398,"TableAttributeNumber":2,"DataTypeOID":20,"DataTypeSize":8,"TypeModifier":-1,"Format":1},
			{"Name":"pass_hash","TableOID":16398,"TableAttributeNumber":4,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},
			{"Name":"pass_alg","TableOID":16398,"TableAttributeNumber":5,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},
			{"Name":"pass_salt","TableOID":16398,"TableAttributeNumber":6,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1}]}
				B {"Type":"DataRow","Values":[
			{"binary":"567fc6f3ea1a6e72"},
			{"binary":"3842daf1f4cab76a4b49d45926808732de011a8f31a4a0e57fea64c848867a43"},
			{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},
			{"binary":"54ddbcaec65a9cf71dcdd4fd3c0e47cb"}]}
				B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
				B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectMessage(&pgproto3.Bind{
			DestinationPortal:    "",
			PreparedStatement:    callPrefix + "1",
			ParameterFormatCodes: []int16{0},
			Parameters:           [][]byte{[]byte("test@test.com")},
			ResultFormatCodes:    []int16{1, 1, 1, 1},
		}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("owner"), TableOID: 16398, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_hash"), TableOID: 16398, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_alg"), TableOID: 16398, TableAttributeNumber: 5, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_salt"), TableOID: 16398, TableAttributeNumber: 6, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1}}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{
			helperHexMust("567fc6f3ea1a6e72"),
			helperHexMust("3842daf1f4cab76a4b49d45926808732de011a8f31a4a0e57fea64c848867a43"),
			helperHexMust("0a0c6172676f6e322e49444b6579101318808004200128083020"),
			helperHexMust("54ddbcaec65a9cf71dcdd4fd3c0e47cb")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
	)
	//
	////проверка и получение токенов
	///*
	//	F {"Type":"Parse","Name":"lrupsc_1_2","Query":"select auth.new_token($1, $2, $3, $4)","ParameterOIDs":null}
	//	F {"Type":"Describe","ObjectType":"S","Name":"lrupsc_1_2"}
	//	F {"Type":"Sync"}
	//	B {"Type":"ParseComplete"}
	//	B {"Type":"ParameterDescription","ParameterOIDs":[25,1114,1114,25]}
	//	B {"Type":"RowDescription","Fields":[{"Name":"new_token","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":2278,"DataTypeSize":4,"TypeModifier":-1,"Format":0}]}
	//	B {"Type":"ReadyForQuery","TxStatus":"I"}
	//
	//*/
	script.Steps = append(script.Steps,
		pgmock.ExpectMessage(&pgproto3.Parse{Name: callPrefix + "2", Query: "select auth.new_token($1, $2, $3, $4)"}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'S', Name: callPrefix + "2"}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25, 1114, 1114, 25}}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_token"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 2278, DataTypeSize: 4, TypeModifier: -1, Format: 0},
		}}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_2","ParameterFormatCodes":[0,1,1,0],"Parameters":[{"text":"test@test.com"},{"binary":"0002435fa84d1780"},{"binary":"000242d2d7697780"},{"text":"v2.public.Cg10ZXN0QHRlc3QuY29tEL7AlfMFGL7L8PIFIPLc6dC-3vG_VigBMAAEXOF9tMndczevzNKtz7TlEuEkQ-Xsm3BQu0R3SIGhoK1We9ox4fTpWZTGpi4MYoYKAG8M5y8kP99Pur9cQygC.eyJMb2dpbiI6InRlc3RAdGVzdC5jb20iLCJFeHBpcmF0aW9uIjoxNTgzNzAyMDc4LCJJc3N1ZWRBdCI6MTU4MzA5NzI3OCwiT3duZXIiOjYyMzI5MTkxNjAyMTEwMDkxMzgsIlJvbGUiOiJEZW1vUm9sZSIsIkdlbmVyYXRpb24iOjB9"}],"ResultFormatCodes":[0]}
			F {"Type":"Describe","ObjectType":"P","Name":""}
			F {"Type":"Execute","Portal":"","MaxRows":0}
			F {"Type":"Sync"}
			B {"Type":"BindComplete"}
			B {"Type":"RowDescription","Fields":[{"Name":"new_token","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":2278,"DataTypeSize":4,"TypeModifier":-1,"Format":0}]}
			B {"Type":"DataRow","Values":[{"text":""}]}
			B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
			B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), // рандомный ключ то
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_token"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 2278, DataTypeSize: 4, TypeModifier: -1, Format: 0},
		}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{[]byte("")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
	)

	//refresh token
	/*
		F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_0","ParameterFormatCodes":[0,1,0,1,1,1,1],"Parameters":[{"text":"test@test.com"},{"binary":"fdaf87d989c33c1b800f4061c3a68542"},{"text":"DemoRole"},{"binary":"f1c8610fe524a5ce845e85eb783f392ba0be0e9a019fdc3a346d8ba71605e7ab"},{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},{"binary":"1426a89fe1132039eb6d80a02c09b39b"},{"binary":"01"}],"ResultFormatCodes":[1]}
		F {"Type":"Describe","ObjectType":"P","Name":""}
		F {"Type":"Execute","Portal":"","MaxRows":0}
		F {"Type":"Sync"}
		B {"Type":"BindComplete"}
		B {"Type":"RowDescription","Fields":[{"Name":"new_auth_user2","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":23,"DataTypeSize":4,"TypeModifier":-1,"Format":1}]}
		B {"Type":"DataRow","Values":[{"binary":"00000002"}]}
		B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
		B {"Type":"ReadyForQuery","TxStatus":"I"}
	*/
	script.Steps = append(script.Steps,
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), // рандомный ключ то
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_auth_user2"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 23, DataTypeSize: 4, TypeModifier: -1, Format: 1},
		}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{helperHexMust("00000002")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		/*
				F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_1","ParameterFormatCodes":[0],"Parameters":[{"text":"test@test.com"}],"ResultFormatCodes":[1,1,1,1]}
				F {"Type":"Describe","ObjectType":"P","Name":""}
				F {"Type":"Execute","Portal":"","MaxRows":0}
				F {"Type":"Sync"}
				B {"Type":"BindComplete"}
				B {"Type":"RowDescription","Fields":[
			{"Name":"owner","TableOID":16398,"TableAttributeNumber":2,"DataTypeOID":20,"DataTypeSize":8,"TypeModifier":-1,"Format":1},
			{"Name":"pass_hash","TableOID":16398,"TableAttributeNumber":4,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},
			{"Name":"pass_alg","TableOID":16398,"TableAttributeNumber":5,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},
			{"Name":"pass_salt","TableOID":16398,"TableAttributeNumber":6,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1}]}
				B {"Type":"DataRow","Values":[
			{"binary":"567fc6f3ea1a6e72"},
			{"binary":"f1c8610fe524a5ce845e85eb783f392ba0be0e9a019fdc3a346d8ba71605e7ab"},
			{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},
			{"binary":"1426a89fe1132039eb6d80a02c09b39b"}]}
				B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
				B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectMessage(&pgproto3.Bind{
			DestinationPortal:    "",
			PreparedStatement:    callPrefix + "1",
			ParameterFormatCodes: []int16{0},
			Parameters:           [][]byte{[]byte("test@test.com")},
			ResultFormatCodes:    []int16{1, 1, 1, 1},
		}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("owner"), TableOID: 16398, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_hash"), TableOID: 16398, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_alg"), TableOID: 16398, TableAttributeNumber: 5, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_salt"), TableOID: 16398, TableAttributeNumber: 6, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1}}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{
			helperHexMust("567fc6f3ea1a6e72"),
			helperHexMust("f1c8610fe524a5ce845e85eb783f392ba0be0e9a019fdc3a346d8ba71605e7ab"),
			helperHexMust("0a0c6172676f6e322e49444b6579101318808004200128083020"),
			helperHexMust("1426a89fe1132039eb6d80a02c09b39b")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		/*
			F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_1","ParameterFormatCodes":[0],"Parameters":[{"text":"test@test.com"}],"ResultFormatCodes":[1,1,1,1]}
			F {"Type":"Describe","ObjectType":"P","Name":""}
			F {"Type":"Execute","Portal":"","MaxRows":0}
			F {"Type":"Sync"}
			B {"Type":"BindComplete"}
			B {"Type":"RowDescription","Fields":[{"Name":"owner","TableOID":16398,"TableAttributeNumber":2,"DataTypeOID":20,"DataTypeSize":8,"TypeModifier":-1,"Format":1},{"Name":"pass_hash","TableOID":16398,"TableAttributeNumber":4,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},{"Name":"pass_alg","TableOID":16398,"TableAttributeNumber":5,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1},{"Name":"pass_salt","TableOID":16398,"TableAttributeNumber":6,"DataTypeOID":17,"DataTypeSize":-1,"TypeModifier":-1,"Format":1}]}
			B {"Type":"DataRow","Values":[{"binary":"567fc6f3ea1a6e72"},{"binary":"f1c8610fe524a5ce845e85eb783f392ba0be0e9a019fdc3a346d8ba71605e7ab"},{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},{"binary":"1426a89fe1132039eb6d80a02c09b39b"}]}
			B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
			B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectMessage(&pgproto3.Bind{
			DestinationPortal:    "",
			PreparedStatement:    callPrefix + "1",
			ParameterFormatCodes: []int16{0},
			Parameters:           [][]byte{[]byte("test@test.com")},
			ResultFormatCodes:    []int16{1, 1, 1, 1},
		}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("owner"), TableOID: 16398, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_hash"), TableOID: 16398, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_alg"), TableOID: 16398, TableAttributeNumber: 5, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_salt"), TableOID: 16398, TableAttributeNumber: 6, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1}}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{
			helperHexMust("567fc6f3ea1a6e72"),
			helperHexMust("f1c8610fe524a5ce845e85eb783f392ba0be0e9a019fdc3a346d8ba71605e7ab"),
			helperHexMust("0a0c6172676f6e322e49444b6579101318808004200128083020"),
			helperHexMust("1426a89fe1132039eb6d80a02c09b39b")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_2","ParameterFormatCodes":[0,1,1,0],"Parameters":[{"text":"test@test.com"},{"binary":"0002435fa84d1780"},{"binary":"000242d2d7697780"},{"text":"v2.public.Cg10ZXN0QHRlc3QuY29tEL7AlfMFGL7L8PIFIPLc6dC-3vG_VigBMAAEXOF9tMndczevzNKtz7TlEuEkQ-Xsm3BQu0R3SIGhoK1We9ox4fTpWZTGpi4MYoYKAG8M5y8kP99Pur9cQygC.eyJMb2dpbiI6InRlc3RAdGVzdC5jb20iLCJFeHBpcmF0aW9uIjoxNTgzNzAyMDc4LCJJc3N1ZWRBdCI6MTU4MzA5NzI3OCwiT3duZXIiOjYyMzI5MTkxNjAyMTEwMDkxMzgsIlJvbGUiOiJEZW1vUm9sZSIsIkdlbmVyYXRpb24iOjB9"}],"ResultFormatCodes":[0]}
			F {"Type":"Describe","ObjectType":"P","Name":""}
			F {"Type":"Execute","Portal":"","MaxRows":0}
			F {"Type":"Sync"}
			B {"Type":"BindComplete"}
			B {"Type":"RowDescription","Fields":[{"Name":"new_token","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":2278,"DataTypeSize":4,"TypeModifier":-1,"Format":0}]}
			B {"Type":"DataRow","Values":[{"text":""}]}
			B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
			B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), // рандомный ключ то
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_token"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 2278, DataTypeSize: 4, TypeModifier: -1, Format: 1},
		}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{[]byte("")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		/*
			F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_0","ParameterFormatCodes":[0,1,0,1,1,1,1],"Parameters":[{"text":"test@test.com"},{"binary":"fdaf87d989c33c1b800f4061c3a68542"},{"text":"DemoRole"},{"binary":"d5a17e4db5d544ac51f42fbaa9d20e3af212b982f29e428f9db65b36c72b5daa"},{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},{"binary":"ea51d32a50c2dd9dcbed52fdddb644c6"},{"binary":"00"}],"ResultFormatCodes":[1]}
			F {"Type":"Describe","ObjectType":"P","Name":""}
			F {"Type":"Execute","Portal":"","MaxRows":0}
			F {"Type":"Sync"}
			B {"Type":"BindComplete"}
			B {"Type":"RowDescription","Fields":[{"Name":"new_auth_user2","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":23,"DataTypeSize":4,"TypeModifier":-1,"Format":1}]}
			B {"Type":"DataRow","Values":[{"binary":"00000003"}]}
			B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
			B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), // рандомный ключ то
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_auth_user2"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 23, DataTypeSize: 4, TypeModifier: -1, Format: 1},
		}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{helperHexMust("00000003")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_0","ParameterFormatCodes":[0,1,0,1,1,1,1],"Parameters":[{"text":"test2@test.com"},{"binary":"fdaf87d989c33c1b800f4061c3a68542"},{"text":"DemoRole"},{"binary":"1d1acc2d28310c8e3b081ff8206ba93d16ed28f35ad3acbc2e979186752e024a"},{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},{"binary":"fca66b26e2818a7ff536f7da13becbb6"},{"binary":"00"}],"ResultFormatCodes":[1]}
			F {"Type":"Describe","ObjectType":"P","Name":""}
			F {"Type":"Execute","Portal":"","MaxRows":0}
			F {"Type":"Sync"}
			B {"Type":"BindComplete"}
			B {"Type":"RowDescription","Fields":[{"Name":"new_auth_user2","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":23,"DataTypeSize":4,"TypeModifier":-1,"Format":1}]}
			B {"Type":"DataRow","Values":[{"binary":"00000001"}]}
			B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
			B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), // рандомный ключ то
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_auth_user2"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 23, DataTypeSize: 4, TypeModifier: -1, Format: 1},
		}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{helperHexMust("00000001")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			register user (already registred)
				F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_0","ParameterFormatCodes":[0,1,0,1,1,1,1],"Parameters":[{"text":"test2@test.com"},{"binary":"fdaf87d989c33c1b800f4061c3a68542"},{"text":"DemoRole"},{"binary":"f0d3f07b0d322c96e074b9aa2091b359f62e2f8f5f274e7485bff3d0af29ad89"},{"binary":"0a0c6172676f6e322e49444b6579101318808004200128083020"},{"binary":"9ebfabf2712bd03639c4149d9323f46f"},{"binary":"00"}],"ResultFormatCodes":[1]}
				F {"Type":"Describe","ObjectType":"P","Name":""}
				F {"Type":"Execute","Portal":"","MaxRows":0}
				F {"Type":"Sync"}
				B {"Type":"BindComplete"}
				B {"Type":"RowDescription","Fields":[{"Name":"new_auth_user2","TableOID":0,"TableAttributeNumber":0,"DataTypeOID":23,"DataTypeSize":4,"TypeModifier":-1,"Format":1}]}
				B {"Type":"DataRow","Values":[{"binary":"00000003"}]}
				B {"Type":"CommandComplete","CommandTag":"SELECT 1"}
				B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), // рандомный ключ то
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_auth_user2"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 23, DataTypeSize: 4, TypeModifier: -1, Format: 1},
		}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{helperHexMust("00000003")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			отправка ссылки на восстановление пароля
		*/
		pgmock.ExpectMessage(&pgproto3.Parse{Name: callPrefix + "3", Query: "SELECT pass_hash, owner FROM auth.users WHERE login=$1"}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'S', Name: callPrefix + "3"}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25}}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("pass_hash"), TableOID: 16398, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 0},
			{Name: []byte("owner"), TableOID: 16398, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 0}}}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		pgmock.ExpectMessage(&pgproto3.Bind{
			DestinationPortal:    "",
			PreparedStatement:    callPrefix + "3",
			ParameterFormatCodes: []int16{0},
			Parameters:           [][]byte{[]byte("test2@test.com")},
			ResultFormatCodes:    []int16{1, 1},
		}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("pass_hash"), TableOID: 16422, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("owner"), TableOID: 16422, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 1}}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{
			helperHexMust("3842daf1f4cab76a4b49d45926808732de011a8f31a4a0e57fea64c848867a43"),
			helperHexMust("567fc6f3ea1a6e72")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			refresh token

				F {"Type":"Parse","Name":"lrupsc_1_3","Query":"UPDATE auth.tokens SET token=$1, issuedat=$2, generation=$7 WHERE token=$3 AND login=$4 AND token is distinct from $1 AND expiration=$5 AND issuedat=$6 AND generation=$8","ParameterOIDs":null}
				F {"Type":"Describe","ObjectType":"S","Name":"lrupsc_1_3"}
				F {"Type":"Sync"}
				B {"Type":"ParseComplete"}
				B {"Type":"ParameterDescription","ParameterOIDs":[25,1114,25,25,1114,1114,20,20]}
				B {"Type":"NoData"}
				B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectMessage(&pgproto3.Parse{Name: callPrefix + "4", Query: "UPDATE auth.tokens SET token=$1, issuedat=$2, generation=$7 WHERE token=$3 AND login=$4 AND token is distinct from $1 AND expiration=$5 AND issuedat=$6 AND generation=$8"}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'S', Name: callPrefix + "4"}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25, 1114, 25, 25, 1114, 1114, 20, 20}}),
		pgmock.SendMessage(&pgproto3.NoData{}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		/*
			F {"Type":"Bind","DestinationPortal":"","PreparedStatement":"lrupsc_1_3","ParameterFormatCodes":[0,1,0,0,1,1,1,1],"Parameters":[{"text":"v2.public.Cg50ZXN0MkB0ZXN0LmNvbRC-wJXzBRi-y_DyBSCnv7PvtfCTw0QoATABOMiTzI_vB8tWzacJj_h21QKznAhRzF7-flL05oBeHtQAEorg0ZApqm2WMVmxEidZ2fI-ra7AjziG9HiYHsBIBQ.eyJMb2dpbiI6InRlc3QyQHRlc3QuY29tIiwiRXhwaXJhdGlvbiI6MTU4MzcwMjA3OCwiSXNzdWVkQXQiOjE1ODMwOTcyNzgsIk93bmVyIjo0OTM3NzIxNDY3MDkzNDQyNDcxLCJSb2xlIjoiRGVtb1JvbGUiLCJHZW5lcmF0aW9uIjoxfQ"},{"binary":"000242d2d7697780"},{"text":"v2.public.Cg50ZXN0MkB0ZXN0LmNvbRC-wJXzBRi-y_DyBSCnv7PvtfCTw0QoATAAMMsQ6SzMJ7rWj9l3I925UbO1hccTtBGyhlixT16qKiH8m_taKGbHPILiuhrzymSuo0hHFeHf7-Zb-VzqUZB9Dg.eyJMb2dpbiI6InRlc3QyQHRlc3QuY29tIiwiRXhwaXJhdGlvbiI6MTU4MzcwMjA3OCwiSXNzdWVkQXQiOjE1ODMwOTcyNzgsIk93bmVyIjo0OTM3NzIxNDY3MDkzNDQyNDcxLCJSb2xlIjoiRGVtb1JvbGUiLCJHZW5lcmF0aW9uIjowfQ"},{"text":"test2@test.com"},{"binary":"0002435fa84d1780"},{"binary":"000242d2d7697780"},{"binary":"0000000000000001"},{"binary":"0000000000000000"}],"ResultFormatCodes":[]}
			F {"Type":"Describe","ObjectType":"P","Name":""}
			F {"Type":"Execute","Portal":"","MaxRows":0}
			F {"Type":"Sync"}
			B {"Type":"BindComplete"}
			B {"Type":"NoData"}
			B {"Type":"CommandComplete","CommandTag":"UPDATE 1"}
			B {"Type":"ReadyForQuery","TxStatus":"I"}
		*/
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.NoData{}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("UPDATE 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),

		pgmock.ExpectMessage(&pgproto3.Terminate{}))

	ln, err := net.Listen("tcp", "127.0.0.1:")
	require.NoError(t, err)
	defer func() {
		assert.NoError(t, ln.Close())
	}()

	serverErrChan := make(chan error, 1)
	serverReadyChan := make(chan struct{})
	pgTimeout = time.Second * 10 //test may be slow...
	go func() {
		defer close(serverErrChan)
		close(serverReadyChan)
		conn, err := ln.Accept()
		if err != nil {
			t.Log(err)
			serverErrChan <- err
			return
		}
		defer func() {
			assert.NoError(t, conn.Close())
		}()

		err = conn.SetDeadline(time.Now().Add(pgTimeout))
		if err != nil {
			t.Log(err)
			serverErrChan <- err
			return
		}

		err = script.Run(pgproto3.NewBackend(pgproto3.NewChunkReader(conn), conn))
		if err != nil {
			t.Log(err)
			serverErrChan <- err
			return
		}
	}()

	<-serverReadyChan // wait till server goroutine starts
	/*
				   что б через ./pgmockproxy снять трассу запросов в БД, надо
					отключить вход по логину и паролю, а именно:
		находим где постгрес хранит в FS файлик pg_hba.conf
			sudo find / -name pg_hba.conf
		в pg_hba.conf добавить/ отредактировать строку вида
			host all all all trust

		запускаем через make postgres в докере и оно работает

	*/
	parts := strings.Split(ln.Addr().String(), ":")
	host := parts[0]
	port := parts[1]
	connStr := fmt.Sprintf("sslmode=disable host=%s port=%s database=stockengine", host, port)
	//connStr := "sslmode=disable host=localhost port=15432 database=stockengine user=auth password=auth_pass"

	b, err := hex.DecodeString("b4cbfb43df4ce210727d953e4a713307fa19bb7d9f85041438d9e11b942a37741eb9dbbbbc047c03fd70604e0071f0987e16b28b757225c11f00415d0e20b1a2")
	require.NoError(t, err)
	privateKey := ed25519.PrivateKey(b)

	b, err = hex.DecodeString("1eb9dbbbbc047c03fd70604e0071f0987e16b28b757225c11f00415d0e20b1a2")
	require.NoError(t, err)
	publicKey := ed25519.PublicKey(b)

	eng, err := NewEngine(connStr, "127.0.0.1:", "127.0.0.1:", Keys{
		Auth: Key{
			PrivateKey: privateKey,
			PublicKey:  publicKey,
		},
		Access: Key{
			PrivateKey: privateKey,
			PublicKey:  publicKey,
		},
	},
		[]byte{0xfd, 0xaf, 0x87, 0xd9, 0x89, 0xc3, 0x3c, 0x1b, 0x80, 0xf, 0x40, 0x61, 0xc3, 0xa6, 0x85, 0x42},
		"127.0.0.10:/metrics",
		[]string{"https://demo.stockengine.tech"},
		"asd",
		"se.notify:7111",
		"12345678901234567890123456789012",
	)
	require.NoError(t, err)

	//t.Log(conn.Stat().AcquireCount(), conn.Stat().AcquiredConns(), conn.Stat().AcquireDuration(), conn.Stat().IdleConns(), conn.Stat().TotalConns())

	//t.Log(conn.Stat().AcquireCount(), conn.Stat().AcquiredConns(), conn.Stat().AcquireDuration(), conn.Stat().IdleConns(), conn.Stat().TotalConns())

	/*
		тест кейсы
		- принудительное создание пользователя (несуществующего)
		- генерация токена (с некорректный паролем)
		- генерация токена (с корректным паролем)
		- принудительное создание пользователя (существующего, по сути перезапись пароля)
		- генерация токена
		- самостоятельная регистрация пользователдя (существующего, по сути ошибка)
		- саморегистрация пользователя (несуществующего)
		- разные проверки токенов
	*/

	var oldPass = "test password"
	var newPAss = "test password 2"
	var newUser = "test2@test.com"
	var lp = &proto.LoginPass{
		Login:    "test@test.com",
		Password: oldPass,
	}

	m, err := eng.CreateUpdatePassword(context.Background(), lp)
	assert.NoError(t, err)
	assert.NotNil(t, m)

	var newLp = &proto.LoginPass{
		Login:    lp.Login,
		Password: newPAss,
	}
	at, err := eng.GetToken(context.Background(), newLp)
	assert.Error(t, err)
	assert.Nil(t, at)

	at, err = eng.GetToken(context.Background(), lp)
	assert.NoError(t, err)
	require.NotNil(t, at)

	//перезапись пароля
	m, err = eng.CreateUpdatePassword(context.Background(), newLp)
	assert.NoError(t, err)
	assert.NotNil(t, m)

	//старый не подходит
	at, err = eng.GetToken(context.Background(), lp)
	assert.Error(t, err)
	assert.Nil(t, at)

	// новый подходит
	at, err = eng.GetToken(context.Background(), newLp)
	assert.NoError(t, err)
	require.NotNil(t, at)
	assert.NotEmpty(t, at.Auth)

	//
	lp2, rs, err := eng.registerUser(context.Background(), &proto.RegisterRequest{Login: lp.Login})
	assert.NoError(t, err)
	require.NotNil(t, lp2)
	assert.NotEmpty(t, lp2.Login)
	assert.Empty(t, lp2.Password)
	assert.Equal(t, proto.RegisterStatus_Exists, rs)

	//пароль то рандомный,мне смогу угадать эмуляцию ответа бд
	lp2, rs, err = eng.registerUser(context.Background(), &proto.RegisterRequest{Login: newUser})
	assert.NoError(t, err)
	require.NotNil(t, lp2)
	assert.Equal(t, lp2.Login, newUser)
	assert.NotEmpty(t, lp2.Password)
	assert.Equal(t, proto.RegisterStatus_Added, rs)

	//отправляет ссылку для сброса пароля
	rr, err := eng.RegisterUser(context.Background(), &proto.RegisterRequest{Login: newUser})
	assert.NoError(t, err)
	require.NotNil(t, rr)
	assert.Equal(t, proto.RegisterStatus_Exists, rr.Status)
	assert.Empty(t, rr.Auth)

	// разные проверки токенов
	var invalidToken = "v2.public.Cg10ZXN0QHRlc3QuY29tEIvQte8FGIvbkO8FINnX_pHM1IK7figB7zQjUXiqW5gD1mc68Vw9911FY_lvlGrZy4vvlnI04DnkzQW54jMg1YdAOKFh6xo4sNtn8VphECrM8n9eZJYTBQ.eyJMb2dpbiI6InRlc3RAdGVzdC5jb20iLCJFeHBpcmF0aW9uIjoxNTc1ODM5NzU1LCJJc3N1ZWRBdCI6MTU3NTIzNDk1NSwiT3duZXIiOjkxMTI0ODI1OTg3ODA2NDQzMTMsIlJvbGUiOjB9"

	atd, err := eng.DecodeAuthToken(context.Background(), &proto.AuthTokenCoded{Auth: invalidToken})
	assert.Error(t, err)
	require.Nil(t, atd)

	atd, err = eng.DecodeAuthToken(context.Background(), at)
	assert.NoError(t, err)
	require.NotNil(t, atd)

	act, err := eng.RefreshToken(context.Background(), &proto.AuthTokenCoded{})
	assert.Error(t, err)
	require.Nil(t, act)

	act, err = eng.RefreshToken(context.Background(), at)
	assert.NoError(t, err)
	require.NotNil(t, act)

	atd, err = eng.DecodeAuthToken(context.Background(), &proto.AuthTokenCoded{Auth: act.Access})
	assert.Error(t, err)
	require.Nil(t, atd)

	atd, err = eng.DecodeAuthToken(context.Background(), &proto.AuthTokenCoded{Auth: act.Auth})
	assert.NoError(t, err)
	require.NotNil(t, atd)

	var invalidAccessToken = "v2.public.CNnX_pHM1IK7fhDJhJHvBRidgpHvBSABrPS10_3kRzO95rfGzOaq6dC2u622pZdRHVipFu3xxbyHC9g5NNYTe1DoSfQN1-cmn8CrioEP55iTDlqB3_EyCw"

	actd, err := eng.DecodeAccessToken(context.Background(), &proto.AccessTokenCoded{Access: invalidAccessToken})
	assert.Error(t, err)
	require.Nil(t, actd)

	actd, err = eng.DecodeAccessToken(context.Background(), &proto.AccessTokenCoded{Access: act.Access})
	assert.NoError(t, err)
	require.NotNil(t, actd)

	em, err := eng.CheckAccessToken(context.Background(), &proto.AccessTokenCoded{Access: act.Access})
	assert.NoError(t, err)
	require.NotNil(t, em)

	eng.Stop()

	assert.NoError(t, <-serverErrChan, "postgres server errors")
}

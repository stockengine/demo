// +build howtocode

package auth

import (
	"testing"

	"gitlab.com/stockengine/demo/internal/proto"
)

/*
тест размера структур (сериализованных) с учётом типа кодирования в протобаф

с пустым значением
    sizes_test.go:23: class(*proto.AccessToken), size(8)
    sizes_test.go:23: class(*proto.AccessTokenEmbed), size(4)
    sizes_test.go:23: class(*proto.AccessTokenEmbed2), size(4)
    sizes_test.go:23: class(*proto.AccessTokenRaw), size(2)

со значением 12345678901234567890 во всех полях
    sizes_test.go:40: class(*proto.AccessToken), size(35)
    sizes_test.go:40: class(*proto.AccessTokenEmbed), size(13)
    sizes_test.go:40: class(*proto.AccessTokenEmbed2), size(13)
    sizes_test.go:40: class(*proto.AccessTokenRaw), size(11)

*/

func TestSize(t *testing.T) {
	const uint64Val = 12345678901234567890
	tests := []interface {
		Marshal() (dAtA []byte, err error)
	}{
		&proto.AccessToken{
			Owner:      uint64Val,
			Expiration: uint64Val,
			IssuedAt:   uint64Val,
			Role:       1,
		},
		&proto.AccessTokenEmbed{Owner: proto.Owner{Owner: uint64Val}},
		&proto.AccessTokenEmbed2{Owner: proto.Owner{Owner: uint64Val}},
		&proto.AccessTokenRaw{Owner: uint64Val},
	}
	for _, test := range tests {
		buf, err := test.Marshal()
		if err != nil {
			t.Error(err)
		}
		t.Logf("class(%T), size(%d)", test, len(buf))
	}
	t.Fail()
}

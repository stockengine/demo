package auth

import (
	"bytes"
	"context"
	"crypto"
	"errors"
	"fmt"
	"regexp"
	"time"

	"github.com/valyala/fasthttp"

	"github.com/sethvargo/go-password/password"

	"gitlab.com/stockengine/demo/internal/lib/prometheus"

	"github.com/jackc/pgconn"

	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
)

var pgTimeout = time.Second * 1

//const tokenVersion = 0x1
const authExpiration = time.Hour * 24 * 7
const accessExpiration = time.Minute * 5

const accessFooter = true
const authFooter = true

//ToDo: make errors via global vars

func (t *Engine) CreateUpdatePassword(ctx context.Context, req *proto.LoginPass) (ret *proto.EmptyMessage, err error) {
	start := time.Now()
	var status proto.RegisterStatus
	status, err = t.createUpdatePassword(ctx, req, true)
	var lb string
	if err != nil {
		lb = prometheus.Error2Label(err)
	} else {
		lb = status.String()
		ret = &proto.EmptyMessage{}
	}
	t.methodStatus.WithLabelValues("CreateUpdatePassword", lb).Observe(time.Since(start).Seconds())
	return
}

//DRY
func generateDBpasswd(pwd string) (passHash, passSlt, serAlg []byte, err error) {
	passHash, passSlt, passAlg, err := hashPass([]byte(pwd))
	if err != nil {
		return
	}
	serAlg, err = passAlg.Marshal()
	return
}

func (t *Engine) createUpdatePassword(ctx context.Context, req *proto.LoginPass, force bool) (proto.RegisterStatus, error) {
	passHash, passSlt, serAlg, err := generateDBpasswd(req.Password)
	if err != nil {
		logger.Log.Error("generateDBpasswd", zap.Error(err))
		return proto.RegisterStatus_unknown, err
	}

	ctx, cancel := context.WithTimeout(ctx, pgTimeout)
	defer cancel()
	var status int
	qr := t.pgConn.QueryRow(ctx, "select auth.new_auth_user2($1, $2, $3, $4, $5, $6, $7)",
		req.Login,
		t.xteaKey[:],
		proto.AuthRole_DemoRole.String(),
		passHash,
		serAlg,
		passSlt,
		force)
	err = qr.Scan(&status)
	if err != nil {
		//dont thrupass it because can contain secrets
		return proto.RegisterStatus_unknown, err //errors.New("sql error")
	}

	return proto.RegisterStatus(status), nil
}

func (t *Engine) GetToken(ctx context.Context, req *proto.LoginPass) (ret *proto.AuthTokenCoded, err error) {
	start := time.Now()
	ret, err = t.getToken(ctx, req)
	t.methodStatus.WithLabelValues("GetToken", prometheus.Error2Label(err)).Observe(time.Since(start).Seconds())
	return
}

func (t *Engine) getToken(ctx context.Context, req *proto.LoginPass) (ret *proto.AuthTokenCoded, err error) {
	defer func() {
		if err != nil {
			logger.Log.Error("GetAuthToken", zap.Error(err))
		}
	}()
	//прочитать из БД по логину-паролю хеш и данные
	//сгенерировать и проверить на совпадение хешей
	//записать новый токен, удалив старый
	ctx, cancel := context.WithTimeout(ctx, pgTimeout)
	defer cancel()
	var owner uint64
	var passHash, passAlg, passSalt []byte
	if err = t.pgConn.QueryRow(ctx, "select owner, pass_hash, pass_alg, pass_salt from auth.users where login=$1",
		req.Login).Scan(&owner, &passHash, &passAlg, &passSalt); err != nil {
		return
	}

	//check password
	var alg proto.AuthAlg
	if err = alg.Unmarshal(passAlg); err != nil {
		return
	}
	realHash := hashPassWrapper([]byte(req.Password), passSalt, alg)
	if !bytes.Equal(realHash, passHash) {
		err = errors.New("password mismatch")
		return
	}
	//generate token
	now := helperStripNS()
	exp := now.Add(authExpiration) // 7 days
	token := &proto.AuthToken{
		Login:      req.Login,
		Expiration: uint64(exp.Unix()),
		IssuedAt:   uint64(now.Unix()),
		Owner:      owner,
		Role:       proto.AuthRole_DemoRole,
	}

	var encoded string
	encoded, err = encodeHelper(token, t.keys.Auth.PrivateKey, true)
	if err != nil {
		return
	}

	//write to db and overwrite old if necessary
	ctx2, cancel2 := context.WithTimeout(ctx, pgTimeout)
	defer cancel2()
	var ct pgconn.CommandTag
	ct, err = t.pgConn.Exec(ctx2, "select auth.new_token($1, $2, $3, $4)",
		req.Login,
		exp,
		now,
		encoded)

	if err != nil {
		return
	}
	if ct.RowsAffected() != 1 {
		err = errors.New("rows affected wrong counter")
		return
	}

	ret = &proto.AuthTokenCoded{Auth: encoded}

	return
}

func helperStripNS() time.Time {
	return time.Unix(time.Now().Unix(), 0) //strip ns
}

//DRY
func helperCheckAccessToken(coded *proto.AccessTokenCoded, publicKey crypto.PublicKey) (ret *proto.AccessToken, err error) {
	token := &proto.AccessToken{}
	if err = decodeHelper(token, publicKey, coded.Access, accessFooter); err != nil {
		return
	}
	nowTime := helperStripNS()
	now := uint64(nowTime.Unix())
	tokenValid := token.IssuedAt <= now && token.Expiration > now
	if !tokenValid {
		err = errors.New("token invalid")
		return
	}
	ret = token
	return
}

func (t *Engine) CheckAccessToken(ctx context.Context, req *proto.AccessTokenCoded) (ret *proto.EmptyMessage, err error) {
	start := time.Now()
	_, err = helperCheckAccessToken(req, t.keys.Access.PublicKey)
	t.methodStatus.WithLabelValues("CheckAccessToken", prometheus.Error2Label(err)).Observe(time.Since(start).Seconds())

	if err != nil {
		return
	}
	ret = &proto.EmptyMessage{}
	return
}

// декодировать токен,
// валидировать токен,
// сгенерировать новые токены,
// обновить токен в бд
// если токен обновился в бд, то выдать новые токены
func (t *Engine) RefreshToken(ctx context.Context, req *proto.AuthTokenCoded) (ret *proto.RefreshTokenCoded, err error) {
	start := time.Now()
	ret, err = t.refreshToken(ctx, req)
	t.methodStatus.WithLabelValues("RefreshToken", prometheus.Error2Label(err)).Observe(time.Since(start).Seconds())
	return
}
func (t *Engine) refreshToken(ctx context.Context, req *proto.AuthTokenCoded) (ret *proto.RefreshTokenCoded, err error) {
	token := &proto.AuthToken{}

	//decode
	if err = decodeHelper(token, t.keys.Auth.PublicKey, req.Auth, authFooter); err != nil {
		return
	}

	//validate
	nowTime := helperStripNS()
	now := uint64(nowTime.Unix())
	tokenValid := token.IssuedAt <= now && token.Expiration > now
	if !tokenValid {
		err = errors.New("token invalid")
		return
	}

	//generate new tokens
	newToken := *token
	newToken.IssuedAt = now
	newToken.Generation++
	var encodedAuth string
	if encodedAuth, err = encodeHelper(&newToken, t.keys.Auth.PrivateKey, authFooter); err != nil {
		return
	}

	newAccess := &proto.AccessToken{
		Owner:      newToken.Owner,
		Expiration: uint64(nowTime.Add(accessExpiration).Unix()),
		IssuedAt:   now,
		Role:       newToken.Role,
	}
	var encodedAccess string
	if encodedAccess, err = encodeHelper(newAccess, t.keys.Access.PrivateKey, accessFooter); err != nil {
		return
	}

	//update in db and check for uniq and exists
	ctx2, cancel2 := context.WithTimeout(ctx, pgTimeout)
	defer cancel2()
	var ct pgconn.CommandTag
	// update auth.tokens set token='v2.public.Cg10ZXN0QHRlc3QuY29tEOLXwO8FGOLim-8FINOlm-G-qpnzBygBqBQmx2Fzolwh7d2AF9doMPyWt3ppw6zpbMiDw453QbP7VXCuKYVXphLanqIS7tUj0RBkziWkEPCJxURF01vvCw', login='asd', issuedat='2020-10-10'
	// where login='test@test.com' AND token is distinct from 'v2.public.Cg10ZXN0QHRlc3QuY29tEOLXwO8FGOLim-8FINOlm-G-qpnzBygBqBQmx2Fzolwh7d2AF9doMPyWt3ppw6zpbMiDw453QbP7VXCuKYVXphLanqIS7tUj0RBkziWkEPCJxURF01vvC'
	// returning token, login, expiration, issuedat;
	ct, err = t.pgConn.Exec(ctx2, "UPDATE auth.tokens SET token=$1, issuedat=$2, generation=$7 WHERE token=$3 AND login=$4 AND token is distinct from $1 AND expiration=$5 AND issuedat=$6 AND generation=$8",
		encodedAuth,
		nowTime,
		req.Auth,
		newToken.Login,
		time.Unix(int64(token.Expiration), 0),
		time.Unix(int64(token.IssuedAt), 0),
		newToken.Generation,
		token.Generation,
	)

	if err != nil {
		return
	}
	rows := ct.RowsAffected()
	if rows != 1 {
		err = errors.New("token mismatch with stored")
		return
	}

	//и в БД токен обновили (атомарно)
	ret = &proto.RefreshTokenCoded{
		Access: encodedAccess,
		Auth:   encodedAuth,
	}

	return
}

func (t *Engine) DecodeAuthToken(ctx context.Context, req *proto.AuthTokenCoded) (ret *proto.AuthToken, err error) {
	ret = &proto.AuthToken{}

	start := time.Now()
	err = decodeHelper(ret, t.keys.Auth.PublicKey, req.Auth, authFooter)
	t.methodStatus.WithLabelValues("DecodeAuthToken", prometheus.Error2Label(err)).Observe(time.Since(start).Seconds())

	if err != nil {
		ret = nil
	}
	return
}

func (t *Engine) DecodeAccessToken(ctx context.Context, req *proto.AccessTokenCoded) (ret *proto.AccessToken, err error) {
	ret = &proto.AccessToken{}

	start := time.Now()
	err = decodeHelper(ret, t.keys.Access.PublicKey, req.Access, accessFooter)
	t.methodStatus.WithLabelValues("DecodeAccessToken", prometheus.Error2Label(err)).Observe(time.Since(start).Seconds())

	if err != nil {
		ret = nil
	}
	return
}

func (t *Engine) RegisterUser(ctx context.Context, req *proto.RegisterRequest) (ret *proto.RegisterResult, err error) {
	start := time.Now()
	defer func() {
		t.methodStatus.WithLabelValues("RegisterUser", prometheus.Error2Label(err)).Observe(time.Since(start).Seconds())
	}()
	var lp *proto.LoginPass
	ret = &proto.RegisterResult{}

	if lp, ret.Status, err = t.registerUser(ctx, req); err != nil {
		return
	}

	switch ret.Status {
	case proto.RegisterStatus_Added: //send password via email and create session
		t.sendPassword(lp)
		var tk *proto.AuthTokenCoded
		tk, err = t.GetToken(ctx, lp)
		if err == nil && tk != nil {
			ret.Auth = tk.Auth
		}
	case proto.RegisterStatus_Exists:
		//send password reset link via email
		t.sendResetLink(lp)
	default:
		err = fmt.Errorf("unsupported status -> %s", ret.Status.String())
	}

	return
}

var validEmailRegEx = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func passwordHelper() (string, error) {
	return password.Generate(10, 4, 0, true, false)
}

func (t *Engine) registerUser(ctx context.Context, req *proto.RegisterRequest) (resp *proto.LoginPass, status proto.RegisterStatus, err error) {
	email := req.Login
	resp = &proto.LoginPass{}
	resp.Login = email
	resp.Password, err = passwordHelper()
	if err != nil {
		logger.Log.Error("cant generate password", zap.String("email", email), zap.Error(err))
		return
	}
	if validEmailRegEx.MatchString(email) {
		//logger.Log.Debug("validate email", zap.String("email", email), zap.Bool("correct", true))
	} else {
		logger.Log.Warn("validate email", zap.String("email", email), zap.Bool("correct", false))
	}

	status, err = t.createUpdatePassword(ctx, resp, false)

	switch {
	case err != nil:
		logger.Log.Error("cant create user", zap.String("email", email), zap.Error(err))
		resp = nil
	case status == proto.RegisterStatus_Added:
		logger.Log.Debug("register user", zap.String("email", email), zap.String("pass", resp.Password), zap.String("status", status.String()))
	case status == proto.RegisterStatus_Exists:
		logger.Log.Debug("register user", zap.String("email", email), zap.String("status", status.String()))
		resp.Password = ""
	default:
		logger.Log.Error("cant create user", zap.String("email", email), zap.String("status", status.String()))
		err = fmt.Errorf("unexpected status(%s)", status.String())
		resp = nil
	}

	return
}

func (t *Engine) sendPassword(lp *proto.LoginPass) {
	_, err := t.notify.SendPassword(context.Background(), lp)
	logger.Log.Info("sendPassword", zap.String("login", lp.Login), zap.String("pass", lp.Password), zap.Error(err))
}

//цель - сгенерировать одноразовую ссылку по переходу по которой будет сгенерирован и отправлен новый временный пароль, как при регистрации
//ссылка включает хеш старого пароля (для одноразового срабатывания), логин и дату генерации ссылки
//ссылка зашифрована и закодирована в base64
func (t *Engine) sendResetLink(lp *proto.LoginPass) {
	//берём из БД хеш пароля и ид пользователя
	ctx, cancel := context.WithTimeout(context.Background(), pgTimeout)
	defer cancel()
	var link proto.AuthResetPasswordLink
	link.CreatedAt = time.Now().Unix()
	if err := t.pgConn.QueryRow(ctx, "SELECT pass_hash, owner FROM auth.users WHERE login=$1", lp.Login).Scan(&link.PassHash, &link.OwnerID); err != nil {
		logger.Log.Error("pgConn select pass_hash, owner", zap.Error(err), zap.String("login", lp.Login))
		return
	}

	//генерируем зашифрованные параметры
	cryptedLink, err := encryptHelper(&link, t.resetPasswordKey)
	if err != nil {
		logger.Log.Error("encryptHelper", zap.Error(err), zap.String("login", lp.Login), zap.Reflect("link", link))
		return
	}
	//отправляем
	lrp := &proto.LoginResetPassword{
		Login:            lp.Login,
		ResetPasswordURL: urlParam + "=" + cryptedLink,
	}
	_, err = t.notify.SendResetPassword(context.Background(), lrp)
	logger.Log.Info("sendResetLink", zap.Reflect("lrp", lrp), zap.Error(err))
}

const urlParam = "reset"
const maxAge = time.Hour * 1

func (t *Engine) resetPasswordLink(u *fasthttp.URI) (rr *proto.RegisterResult, err error) {
	data := string(u.QueryArgs().Peek(urlParam))

	var link proto.AuthResetPasswordLink
	err = decryptHelper(&link, t.resetPasswordKey, data)
	if err != nil {
		logger.Log.Info("resetURL", zap.Error(err))
		return
	}

	age := time.Since(time.Unix(link.CreatedAt, 0))
	if age > maxAge {
		err = errors.New("token expired. try again")
		logger.Log.Info("token expired", zap.Duration("age", age), zap.Uint64("owner", link.OwnerID))
	}

	//check owner & hash and update password
	ctx, cancel := context.WithTimeout(context.Background(), pgTimeout)
	defer cancel()
	var lp = &proto.LoginPass{}
	if err = t.pgConn.QueryRow(ctx, "SELECT login FROM auth.users WHERE owner=$1 AND pass_hash=$2", link.OwnerID, link.PassHash).Scan(&lp.Login); err != nil {
		logger.Log.Error("pgConn check pass_hash, owner", zap.Error(err), zap.Uint64("owner", link.OwnerID))
		return
	}

	lp.Password, err = passwordHelper()
	if err != nil {
		logger.Log.Error("cant generate password", zap.String("login", lp.Login), zap.Error(err))
		return
	}

	rr = &proto.RegisterResult{}
	rr.Status, err = t.createUpdatePassword(ctx, lp, true)

	switch {
	case err != nil:
		logger.Log.Error("cant update password", zap.String("email", lp.Login), zap.Error(err))
	case rr.Status == proto.RegisterStatus_Reset:
		//create auth
		t.sendPassword(lp)
		var tk *proto.AuthTokenCoded
		tk, err = t.GetToken(ctx, lp)
		if err == nil && tk != nil {
			rr.Auth = tk.Auth
		}
	default:
		logger.Log.Error("unexpected status", zap.String("email", lp.Login), zap.String("status", rr.Status.String()))
		err = fmt.Errorf("unexpected status(%s)", rr.Status.String())
	}

	return
}

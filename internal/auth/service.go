package auth

import (
	"context"
	"crypto/ed25519"
	"errors"
	"net"
	"sync"
	"time"

	prometheus2 "github.com/prometheus/client_golang/prometheus"

	"gitlab.com/stockengine/demo/internal/lib/prometheus"

	"google.golang.org/grpc"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/stockengine/demo/internal/proto"

	"github.com/valyala/fasthttp"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp/reuseport"
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"
)

const XteaSize = 16

type Engine struct {
	//proto.UnimplementedAuthServer

	//grpc
	lis net.Listener
	srv *grpc.Server

	//http
	ln      net.Listener
	srvHTTP *fasthttp.Server

	pgConn  *pgxpool.Pool
	wg      sync.WaitGroup
	xteaKey [XteaSize]byte

	keys Keys

	prom           *prometheus.Prometheus
	httpStatus     prometheus2.HistogramVec
	methodStatus   prometheus2.HistogramVec
	refreshTokens  prometheus2.GaugeVec
	registredUsers prometheus2.GaugeVec

	quit chan struct{}

	recaptcha *RecaptchaChecker

	notify     proto.NotifyEngineClient
	notifyConn *grpc.ClientConn

	resetPasswordKey []byte //32bytes
}

type Keys struct {
	Auth   Key
	Access Key
}

type Key struct {
	//Valid  time.Time
	//Expire time.Time
	PrivateKey ed25519.PrivateKey
	PublicKey  ed25519.PublicKey
}

func pgConnHelper(pgconn string) (*pgxpool.Pool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
	defer cancel()

	conn, err := pgxpool.Connect(ctx, pgconn) //pgxpool.ConnectConfig(ctx, cc)

	if err != nil {
		return nil, err
	}
	return conn, nil
}

func NewEngine(pgconn string, listen string, bind string, keys Keys, xteaKey []byte, prometheusBind string, allowedOrigins []string, recaptcha string, notify string, resetPasswordKey string) (*Engine, error) {
	prom, err := prometheus.New(prometheusBind)
	if err != nil {
		return nil, err
	}
	//метрики...
	// длительность обработки запросов c ключами:
	//   метод (логин, рефреш, access и т.п.),
	//   статус обработки (успешно или код ошибки)
	httpStatus := prom.NewHistogramVec("http", "request processing status and duration",
		[]string{"method", "operation", "status"},
		prometheus.DefBucketsHiLat)
	grpcStatus := prom.NewHistogramVec("methods", "request processing status and duration",
		[]string{"operation", "status"},
		prometheus.DefBucketsHiLat)
	// количество активных рефреш токенов:
	//   роль пользователя
	refreshTokens := prom.NewGaugeVec("tokens_count", "number of active tokens", []string{"type"})
	// количество зарегистрированных пользователей
	//   роль пользователя
	registredUsers := prom.NewGaugeVec("registred_users_count", "number of registred users", []string{"role"})

	conn, err := pgConnHelper(pgconn)
	if err != nil {
		return nil, err
	}
	if len(xteaKey) != XteaSize {
		return nil, errors.New("xtea wring len")
	}

	rc, err := NewRecaptchaChecker(recaptcha)
	if err != nil {
		return nil, err
	}
	ret := &Engine{
		pgConn:           conn,
		keys:             keys,
		prom:             prom,
		httpStatus:       httpStatus,
		methodStatus:     grpcStatus,
		refreshTokens:    refreshTokens,
		registredUsers:   registredUsers,
		quit:             make(chan struct{}),
		recaptcha:        rc,
		resetPasswordKey: []byte(resetPasswordKey),
	}
	ret.prometheusReporter()

	ret.notifyConn, err = config.GRPCClientDial(notify, false)
	if err != nil {
		return nil, err
	}
	ret.notify = proto.NewNotifyEngineClient(ret.notifyConn)

	copy(ret.xteaKey[:], xteaKey[:XteaSize])

	authMiddleware, err := NewFastHTTPMiddleware(FastHTTPAuthMiddlewareOptions{
		PublicKey:      keys.Access.PublicKey,
		AllowedOrigins: allowedOrigins,
	})
	if err != nil {
		return nil, err
	}

	r := router.New()
	rgAuth := r.Group("/auth")

	rgAuth.POST("/login", ret.fastHTTPLogin)
	rgAuth.POST("/register", ret.fastHTTPRegister)
	rgAuth.POST("/refresh", ret.fastHTTPRefresh)
	rgAuth.GET("/reset", ret.fastHTTPReset)
	rgAuth.GET("/check", authMiddleware.HandlerByRole(ret.fastHTTPCheck, proto.AuthRole_DemoRole))
	rgAuth.GET("/show", authMiddleware.HandlerWithAccessToken(ret.fastHTTPCheckToken))

	ln, err := reuseport.Listen("tcp4", listen)
	if err != nil {
		logger.Log.Error("reuseport listener", zap.Error(err))
		return nil, err
	}
	ret.ln = ln
	ret.srvHTTP = &fasthttp.Server{
		Handler:            prometheus.MetricsWrapper(authMiddleware.HandlerOnlyCors(r.Handler), ret.httpStatus),
		Name:               "stockengine auth server",
		ReadTimeout:        time.Second,
		WriteTimeout:       time.Second,
		MaxRequestBodySize: 4096,
		Logger:             &Logger{},
	}

	ret.wg.Add(1)
	go func(t *Engine) {
		defer t.wg.Done()
		logger.Log.Info("service started")
		if err := t.srvHTTP.Serve(ln); err != nil {
			logger.Log.Error("ListenAndServe", zap.Error(err))
		}
	}(ret)

	ret.lis, ret.srv, err = config.CreateGRPCListen(bind)
	if err != nil {
		return nil, err
	}

	proto.RegisterAuthEngineServer(ret.srv, ret)

	ret.wg.Add(1)
	go func() {
		defer ret.wg.Done()
		err := ret.srv.Serve(ret.lis)
		logger.ErrorLogHelper(err, "Serve")
	}()

	return ret, nil
}

func (t *Engine) Stop() {
	close(t.quit)
	if err := t.srvHTTP.Shutdown(); err != nil {
		logger.Log.Error("Shutdown", zap.Error(err))
	}
	t.srv.GracefulStop()
	t.wg.Wait()
	if err := t.prom.Close(); err != nil {
		logger.Log.Error("cant close prometheus", zap.Error(err))
	}
	t.pgConn.Close()
}

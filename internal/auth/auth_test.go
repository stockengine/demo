package auth

import (
	"math/rand"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"

	"gitlab.com/stockengine/demo/internal/proto"
)

func TestHashPass(t *testing.T) {

	pass := []byte("my perfect password")
	salt := []byte("1234567890123456")
	target := struct {
		hash []byte
		alg  proto.AuthAlg
		salt []byte
	}{
		hash: []byte{0xfd, 0xaf, 0x87, 0xd9, 0x89, 0xc3, 0x3c, 0x1b, 0x80, 0xf, 0x40, 0x61, 0xc3, 0xa6, 0x85, 0x42, 0x5e, 0x26, 0x57, 0xaa, 0x96, 0x29, 0xc1, 0x7a, 0xb, 0xa, 0x9e, 0x43, 0xd5, 0xbd, 0x82, 0xd7},
		alg: proto.AuthAlg{
			Alg:  "argon2.IDKey",
			Ver:  19,
			Mem:  65536,
			Iter: 1,
			Par:  8,
			Len:  32,
		},
	}
	h := hashPassWrapper(pass, salt, target.alg)
	//t.Logf("hash[%#v] salt[%0X]", h, target.salt)
	assert.Equal(t, target.hash, h)
	//t.Fail()
}

func BenchmarkHashPass(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pass := strconv.Itoa(i)
		h, s, a, e := hashPass([]byte(pass))
		_, _, _ = h, s, a
		require.NoError(b, e)
	}
}

func TestEncryptDecryptHelper(t *testing.T) {
	data := &proto.AuthResetPasswordLink{
		OwnerID:   123,
		CreatedAt: 1234567890,
		PassHash:  []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0},
	}
	var secretKey32byte = []byte("yohoho and a bottle of rum!!!111")
	crypted, err := encryptHelper(data, secretKey32byte)
	require.NoError(t, err)
	assert.NotEmpty(t, crypted)

	//t.Logf("crypted(%d) %+v", len(crypted), crypted)

	data2 := &proto.AuthResetPasswordLink{}
	err = decryptHelper(data2, secretKey32byte, crypted)
	require.NoError(t, err)

	assert.Equal(t, data, data2)

	//check corruption
	b64 := []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_")
	for i := 0; i < 100; i++ {
		bytesBlock := []byte(crypted)
		rLoop := rand.Intn(20) + 1
		for j := 0; j < rLoop; j++ {
			pos := rand.Intn(len(bytesBlock)-20) + 20
			bytesBlock[pos] = b64[rand.Intn(len(b64))]
		}
		err = decryptHelper(data2, secretKey32byte, string(bytesBlock))
		assert.Error(t, err)
		//if err != nil {
		//	t.Logf("err -> %+v", err)
		//}
	}
}

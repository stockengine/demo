package auth

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRecaptchaChecker_Validate(t *testing.T) {
	rc, err := NewRecaptchaChecker("asdasd")
	require.NoError(t, err)
	require.NotNil(t, rc)

	rt := rc.Validate("asdsdfdfgrtydfgsdf", "")
	assert.False(t, rt.Success)

	rt = rc.Validate("asdsdfdfgrtydfgsdf", "127.0.0.1")
	assert.False(t, rt.Success)

}

Аутентификация. Системный ландшифт такой получается:
- ваулт с приватным ключем для токенов
- etcd v3 для храненения логинов и хешей паролей (argon2)
- демон аутентификации (по логину и паролю выдаёт рефреш токен paseto + контролирует брутфорс и прочие забавы)
- демон доступа (по рефреш токену выдаёт токен доступа paseto + контролирует утечки рефреш токена + редирект на аутентификацию при просрочке)
- библиотека/ мидлваре для rest api гетвея проверяющая токен доступа и делающая редирект на рефреш при просрочке

Задачи:
 - забрать из ваулта приватный ключ и передать в демона аутентификации
 - сделать демона аутентификации принимающего запросы по http и генерирующего рефреш токен
   - http basic метод аутентификации, в ответ токен зашифрованный приватным ключем. схема - paseto
   - grpc методы для создания/ удаления/ смены пароля у пользователей, хеш пароля argon2, хранение в etcd v3
   - http метод генерации токена доступа по рефреш токену, а так же контролирующего попытки использования рефреш токена
 - библиотека проверки токена доступа, с чтением публичного ключа из etcd v3
 - для секрета надо обеспечить версионирование - а то при смене ключа не получится работать с ранее выданными токенами

Схема работы с токенами:   
- в ответ на логин/пароль возвращаем 2 токена: auth и access. auth - одноразовый токен, существующий в одном и только одном экземпляре, время жизни большое (60 дней). Access - многоразовый токен , без контроля уникальности, живет немного (30 секунд)
  - логин пароль отправляем в post json вида {login:"test@test.com", password:"password"} + CORS и т.п.
  - обрабатываем только если https 
- auth сохраняется в localstorage и используется только при просрочке access токена
  - отправляем и принимаем auth при запросах refresh в заголовке Authorization: Bearer <token>
  - при ошибке отвечаем http 401 WWW-Authenticate: Bearer realm="stockengine",error="invalid_token",error_description="The access token expired"
- access отправляется в каждом запросе к методам апи, в бд не сохраняется, когда становится просроченным, то софт (js или другой клиент) делают запрос к /refresh обработчику и по auth токену получает новый access токен и новый auth токен. При этом старый auth токен инвалидируется
  - отправляем и принимаем access при запросах к методам api в заголовке x-access-token: <token>
  - при ошибке отвечаем http 401 WWW-Authenticate: Bearer realm="stockengine",error="invalid_token",error_description="The access token expired"

Схема работы саморегистрации:
- полученный из фронт email проверяется на наличие в БД
  - если уже есть, то
    - в лог пишется предупреждение
    - в телеметрию выдаётся ошибка об уже существующем пользователе
    - на email отправляется письмо со ссылкой для сброса пароля
    - по api выдаётся код об уже существующем пользователе
  - если отсутствует, то
    - в лог записывается новый пароль
    - в телеметрию выдаётся регистрация пользователя
    - на email отправляется письмо с новым паролем
    - по api выдаётся код об успешной регистрации и генерируется refresh токен
- клик по ссылке сброса пароля
  - проверка подписи (poseto token)
  - генерация и установка пароля
  - в лог новый пароль
  - по api выдаётся код об успешной регистрации и генерируется refresh токен

MiddleWare. Пример использования
	
	type Config struct {
		AccessKey          []byte `required:"true" desc:"access public key" default:"/run/secrets/auth_access_public_key" fromfile:""`
	}

	cfg := Config{}
	if !config.LoadConfig(&cfg) {...}
	
	authMiddleware, err := NewFastHTTPMiddleware(FastHTTPAuthMiddlewareOptions{
		PublicKey: []byte(cfg.AccessKey),
	})
	if err != nil {...}
	
	r := router.New()

	r.POST("/auth", ret.fastHTTPAuth)
	r.POST("/refresh", ret.fastHTTPRefresh)
	r.GET("/check", authMiddleware.HandlerByRole(ret.fastHTTPCheck, proto.AuthRole_DemoRole))
	r.GET("/show", authMiddleware.HandlerWithAccessToken(ret.fastHTTPCheckToken))

	ret.srvHTTP = &fasthttp.Server{
		Handler:            authMiddleware.HandlerOnlyCors(r.Handler),
	}


сборник идей:
 - https://habr.com/ru/company/Voximplant/blog/323160/
 - https://gist.github.com/zmts/802dc9c3510d79fd40f9dc38a12bccfc
 - https://toster.ru/q/496786
 - https://habr.com/ru/company/dataart/blog/262817/

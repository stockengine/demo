package auth

import (
	"context"
	"fmt"
	"strings"

	"github.com/mailru/easyjson"

	"github.com/valyala/fasthttp"
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"
	"go.uber.org/zap"
)

const bearerPrefix = "Bearer"
const HeaderAccessToken = "X-Access-Token"

func setValidLogin(ctx *fasthttp.RequestCtx, auth string, marshaler easyjson.Marshaler) {
	ctx.Response.Header.Set(fasthttp.HeaderAuthorization, bearerPrefix+" "+auth)
	ctx.Response.Header.Set(fasthttp.HeaderCacheControl, "no-store")
	ctx.Response.Header.Set(fasthttp.HeaderPragma, "no-cache")
	marshallToCtx(ctx, marshaler, fasthttp.StatusOK)
}

func marshallToCtx(ctx *fasthttp.RequestCtx, marshaler easyjson.Marshaler, code int) {
	_, err := easyjson.MarshalToWriter(marshaler, ctx)
	if err != nil {
		logger.Log.Error("marshall", zap.Error(err), zap.String("addr", ctx.RemoteIP().String()), zap.String("headers", string(ctx.Request.Header.RawHeaders())))
		SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusUnprocessableEntity)
		return
	}
	ctx.SetStatusCode(code)
}

func (t *Engine) fastHTTPLogin(ctx *fasthttp.RequestCtx) {
	//logger.Log.Debug("new auth req", zap.String("addr", ctx.RemoteIP().String()))
	body := ctx.PostBody()
	req := proto.LoginPass{}

	if err := easyjson.Unmarshal(body, &req); err != nil {
		logger.Log.Error("cant unmarshall", zap.Error(err), zap.String("addr", ctx.RemoteIP().String()), zap.String("body", string(body)))
		SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusBadRequest)
		return
	}
	var ret *proto.AuthTokenCoded
	var err error
	ret, err = t.GetToken(context.Background(), &req)
	if err != nil {
		logger.Log.Error("cant GetToken", zap.Error(err), zap.String("addr", ctx.RemoteIP().String()), zap.String("body", string(body)), zap.String("headers", string(ctx.Request.Header.RawHeaders())))
		ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, "Bearer realm=\"stockengine\",error=\"invalid login password\",error_description=\"Login and password mismatch\"")
		SetErrorCtx(ctx, []byte("invalid token"), fasthttp.StatusUnauthorized)
		return
	}
	//logger.Log.Debug("auth done", zap.String("addr", ctx.RemoteIP().String()), zap.Reflect("auth", req), zap.String("token", ret.Auth))
	setValidLogin(ctx, ret.Auth, ret)
}

func (t *Engine) fastHTTPRefresh(ctx *fasthttp.RequestCtx) {
	//logger.Log.Debug("new auth refresh", zap.String("addr", ctx.RemoteIP().String()))
	bearer := ctx.Request.Header.Peek(fasthttp.HeaderAuthorization)
	hasPrefix := strings.HasPrefix(string(bearer), bearerPrefix)
	if !hasPrefix || len(bearer) < len(bearerPrefix)+1 {
		logger.Log.Error("cant GetToken", zap.String("addr", ctx.RemoteIP().String()), zap.String("headers", string(ctx.Request.Header.RawHeaders())))
		ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, "Bearer realm=\"stockengine\",error=\"invalid_token\",error_description=\"Bad token\"")
		SetErrorCtx(ctx, []byte("invalid token"), fasthttp.StatusUnauthorized)
		return
	}
	token := bearer[len(bearerPrefix)+1:]
	req := proto.AuthTokenCoded{Auth: string(token)}
	refresh, err := t.RefreshToken(context.Background(), &req)
	if err != nil {
		logger.Log.Error("cant RefreshToken", zap.Error(err), zap.String("addr", ctx.RemoteIP().String()), zap.String("headers", string(ctx.Request.Header.RawHeaders())))
		ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, "Bearer realm=\"stockengine\",error=\"invalid_token\",error_description=\"Bad token\"")
		SetErrorCtx(ctx, []byte("invalid token"), fasthttp.StatusUnauthorized)
		return
	}
	ctx.Response.Header.Set(fasthttp.HeaderAuthorization, bearerPrefix+" "+refresh.Auth)
	ctx.Response.Header.Set(HeaderAccessToken, refresh.Access)
	ctx.Response.Header.Set(fasthttp.HeaderCacheControl, "no-store")
	ctx.Response.Header.Set(fasthttp.HeaderPragma, "no-cache")
	marshallToCtx(ctx, refresh, fasthttp.StatusOK)
}

func (t *Engine) fastHTTPCheck(ctx *fasthttp.RequestCtx) {
	ctx.Response.SetBodyString("access granted")
	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (t *Engine) fastHTTPCheckToken(ctx *fasthttp.RequestCtx, token *proto.AccessToken) {
	if _, err := easyjson.MarshalToWriter(token, ctx); err != nil {
		ctx.SetStatusCode(fasthttp.StatusUnprocessableEntity)
		return
	}
	ctx.SetStatusCode(fasthttp.StatusOK)
}

type Logger struct{}

// Printf must have the same semantics as log.Printf.
func (t *Logger) Printf(format string, args ...interface{}) {
	logger.Log.WithOptions(zap.AddCallerSkip(1)).Sugar().Debugf(format, args)
}

func (t *Engine) fastHTTPRegister(ctx *fasthttp.RequestCtx) {
	body := ctx.PostBody()
	req := proto.RegisterRequest{}

	if err := easyjson.Unmarshal(body, &req); err != nil {
		logger.Log.Error("cant unmarshall", zap.Error(err), zap.String("addr", ctx.RemoteIP().String()), zap.String("body", string(body)))
		SetErrorCtx(ctx, []byte(err.Error()), fasthttp.StatusBadRequest)
		return
	}

	cv := t.recaptcha.Validate(req.ReCaptchaToken, ctx.RemoteIP().String())
	logger.Log.Info("recaptcha", zap.Reflect("resp", cv))

	rr, err := t.RegisterUser(context.Background(), &req)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		_, _ = ctx.WriteString(err.Error())
		return
	}
	switch rr.Status {
	case proto.RegisterStatus_Added:
		setValidLogin(ctx, rr.Auth, rr)
	default:
		ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, fmt.Sprintf("Bearer realm=\"stockengine\",error=\"%s\"", rr.Status.String()))
		marshallToCtx(ctx, rr, fasthttp.StatusUnauthorized)
	}
}

func (t *Engine) fastHTTPReset(ctx *fasthttp.RequestCtx) {
	rr, err := t.resetPasswordLink(ctx.URI())
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusUnauthorized)
		_, _ = ctx.WriteString(err.Error())
		return
	}
	switch rr.Status {
	case proto.RegisterStatus_Reset:
		setValidLogin(ctx, rr.Auth, rr)
	default:
		ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, fmt.Sprintf("Bearer realm=\"stockengine\",error=\"%s\"", rr.Status.String()))
		marshallToCtx(ctx, rr, fasthttp.StatusUnauthorized)
	}
}

package auth

import (
	"crypto/ed25519"
	"encoding/hex"
	"fmt"
	"net"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/mailru/easyjson"
	"gitlab.com/stockengine/demo/internal/proto"

	"github.com/valyala/fasthttp"

	"github.com/jackc/pgmock"
	"github.com/jackc/pgproto3/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const testURI = "https://" + testDomain
const testDomain = "demo.stockengine.tech"

func TestFastHttpAuth(t *testing.T) {

	script := &pgmock.Script{
		Steps: pgmock.AcceptUnauthenticatedConnRequestSteps(),
	}
	//ауентификация пользователя
	var owner = "07e66553ec26d2d3"
	script.Steps = append(script.Steps,
		pgmock.ExpectMessage(&pgproto3.Parse{Name: "lrupsc_1_0", Query: "select owner, pass_hash, pass_alg, pass_salt from auth.users where login=$1"}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'S', Name: "lrupsc_1_0"}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25}}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("owner"), TableOID: 16422, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 0},
			{Name: []byte("pass_hash"), TableOID: 16422, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 0},
			{Name: []byte("pass_alg"), TableOID: 16422, TableAttributeNumber: 5, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 0},
			{Name: []byte("pass_salt"), TableOID: 16422, TableAttributeNumber: 6, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 0}}}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		pgmock.ExpectMessage(&pgproto3.Bind{
			DestinationPortal:    "",
			PreparedStatement:    "lrupsc_1_0",
			ParameterFormatCodes: []int16{0},
			Parameters:           [][]byte{[]byte("test@test.com")},
			ResultFormatCodes:    []int16{1, 1, 1, 1},
		}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("owner"), TableOID: 16422, TableAttributeNumber: 2, DataTypeOID: 20, DataTypeSize: 8, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_hash"), TableOID: 16422, TableAttributeNumber: 4, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_alg"), TableOID: 16422, TableAttributeNumber: 5, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1},
			{Name: []byte("pass_salt"), TableOID: 16422, TableAttributeNumber: 6, DataTypeOID: 17, DataTypeSize: -1, TypeModifier: -1, Format: 1}}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{
			helperHexMust(owner),
			helperHexMust("2416ba17586998fc3982617506829622612ef22f015e4dd347b042036e28ab30"),
			helperHexMust("0a0c6172676f6e322e49444b6579101318808004200128083020"),
			helperHexMust("1309c1ab77247ae6381faf6fc74bf6e3")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
	)

	//проверка и получение токенов
	script.Steps = append(script.Steps,
		pgmock.ExpectMessage(&pgproto3.Parse{Name: "lrupsc_1_1", Query: "select auth.new_token($1, $2, $3, $4)"}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'S', Name: "lrupsc_1_1"}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25, 1114, 1114, 25}}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_token"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 2278, DataTypeSize: 4, TypeModifier: -1, Format: 0},
		}}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}), // рандомный ключ то
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.RowDescription{Fields: []pgproto3.FieldDescription{
			{Name: []byte("new_token"), TableOID: 0, TableAttributeNumber: 0, DataTypeOID: 2278, DataTypeSize: 4, TypeModifier: -1, Format: 0},
		}}),
		pgmock.SendMessage(&pgproto3.DataRow{Values: [][]byte{[]byte("")}}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("SELECT 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
	)

	//рефреш токенов
	script.Steps = append(script.Steps,
		pgmock.ExpectMessage(&pgproto3.Parse{Name: "lrupsc_1_2", Query: "UPDATE auth.tokens SET token=$1, issuedat=$2, generation=$7 WHERE token=$3 AND login=$4 AND token is distinct from $1 AND expiration=$5 AND issuedat=$6 AND generation=$8"}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'S', Name: "lrupsc_1_2"}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.ParseComplete{}),
		pgmock.SendMessage(&pgproto3.ParameterDescription{ParameterOIDs: []uint32{25, 1114, 25, 25, 1114, 1114, 20, 20}}),
		pgmock.SendMessage(&pgproto3.NoData{}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
		pgmock.ExpectAnyMessage(&pgproto3.Bind{}),
		pgmock.ExpectMessage(&pgproto3.Describe{ObjectType: 'P', Name: ""}),
		pgmock.ExpectMessage(&pgproto3.Execute{}),
		pgmock.ExpectMessage(&pgproto3.Sync{}),
		pgmock.SendMessage(&pgproto3.BindComplete{}),
		pgmock.SendMessage(&pgproto3.NoData{}),
		pgmock.SendMessage(&pgproto3.CommandComplete{CommandTag: []byte("UPDATE 1")}),
		pgmock.SendMessage(&pgproto3.ReadyForQuery{TxStatus: 'I'}),
	)

	script.Steps = append(script.Steps, pgmock.ExpectMessage(&pgproto3.Terminate{}))

	ln, err := net.Listen("tcp", "127.0.0.1:")
	require.NoError(t, err)
	defer func() {
		assert.NoError(t, ln.Close())
	}()

	serverErrChan := make(chan error, 1)
	serverReadyChan := make(chan struct{})
	pgTimeout = time.Second * 10 //test may be slow...
	go func() {
		defer close(serverErrChan)
		close(serverReadyChan)
		conn, err := ln.Accept()
		if err != nil {
			t.Log(err)
			serverErrChan <- err
			return
		}
		defer func() {
			assert.NoError(t, conn.Close())
		}()

		err = conn.SetDeadline(time.Now().Add(pgTimeout))
		if err != nil {
			t.Log(err)
			serverErrChan <- err
			return
		}

		err = script.Run(pgproto3.NewBackend(pgproto3.NewChunkReader(conn), conn))
		if err != nil {
			t.Log(err)
			serverErrChan <- err
			return
		}
	}()

	<-serverReadyChan // wait till server goroutine starts

	parts := strings.Split(ln.Addr().String(), ":")
	host := parts[0]
	port := parts[1]
	connStr := fmt.Sprintf("sslmode=disable host=%s port=%s database=stockengine pool_max_conns=1", host, port)
	//connStr := "sslmode=disable host=localhost port=15432 database=stockengine user=auth password=auth_pass"

	b, err := hex.DecodeString("b4cbfb43df4ce210727d953e4a713307fa19bb7d9f85041438d9e11b942a37741eb9dbbbbc047c03fd70604e0071f0987e16b28b757225c11f00415d0e20b1a2")
	require.NoError(t, err)
	privateKey := ed25519.PrivateKey(b)

	b, err = hex.DecodeString("1eb9dbbbbc047c03fd70604e0071f0987e16b28b757225c11f00415d0e20b1a2")
	require.NoError(t, err)
	publicKey := ed25519.PublicKey(b)

	eng, err := NewEngine(connStr, "127.0.0.1:", "127.0.0.1:", Keys{
		Auth: Key{
			PrivateKey: privateKey,
			PublicKey:  publicKey,
		},
		Access: Key{
			PrivateKey: privateKey,
			PublicKey:  publicKey,
		},
	},
		[]byte{0xfd, 0xaf, 0x87, 0xd9, 0x89, 0xc3, 0x3c, 0x1b, 0x80, 0xf, 0x40, 0x61, 0xc3, 0xa6, 0x85, 0x42},
		"127.0.0.10:/metrics",
		[]string{testURI},
		"asd",
		"se.notify:7111",
		"12345678901234567890123456789012",
	)
	require.NoError(t, err)
	require.NotNil(t, eng)

	//conn := eng.pgConn
	//t.Log(conn.Stat().AcquireCount(), conn.Stat().AcquiredConns(), conn.Stat().AcquireDuration(), conn.Stat().IdleConns(), conn.Stat().TotalConns())

	var headerAuth []byte

	{ //preflight
		/*
			curl -v http://127.0.0.1:8080/auth/login
			-H 'Host: api.stockengine.tech'
			-X OPTIONS
			-H 'authority: api.stockengine.tech'
			-H 'pragma: no-cache'
			-H 'cache-control: no-cache'
			-H 'access-control-request-method: POST'
			-H 'origin: https://demo.stockengine.tech'
			-H 'sec-fetch-dest: empty'
			-H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.100 Chrome/80.0.3987.100 Safari/537.36'
			-H 'dnt: 1'
			-H 'access-control-request-headers: content-type'
			-H 'accept: *\/*'
			-H 'sec-fetch-site: cross-site'
			-H 'sec-fetch-mode: cors'
			-H 'referer: http://loc.stockengine.tech:8080/'
			-H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
			--compressed
		*/
		req := fasthttp.AcquireRequest()
		req.SetRequestURI("/auth/login") // task URI
		req.SetHost(eng.ln.Addr().String())
		req.Header.SetMethod(fasthttp.MethodOptions)
		req.Header.Set("authority", testDomain)
		req.Header.Set(fasthttp.HeaderPragma, "no-cache")
		req.Header.Set(fasthttp.HeaderAccessControlRequestMethod, fasthttp.MethodPost)
		req.Header.Set(fasthttp.HeaderOrigin, testURI)
		req.Header.Set("sec-fetch-dest", "empty")
		req.Header.SetUserAgentBytes([]byte("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.100 Chrome/80.0.3987.100 Safari/537.36"))
		req.Header.Set("dnt", "1")
		req.Header.Set(fasthttp.HeaderAccessControlRequestHeaders, fasthttp.HeaderContentType)
		req.Header.Set(fasthttp.HeaderAccept, "*/*")
		req.Header.Set("sec-fetch-site", "cross-site")
		req.Header.Set("sec-fetch-mode", "cors")

		resp := fasthttp.AcquireResponse()

		require.NoError(t, serve(req, resp))
		//t.Log(req.String())
		//t.Log(resp.String())

		assert.Equal(t, fasthttp.StatusOK, resp.Header.StatusCode())
		assert.Equal(t, testURI, string(resp.Header.Peek(fasthttp.HeaderAccessControlAllowOrigin)))
		assert.Equal(t, fasthttp.MethodPost, string(resp.Header.Peek(fasthttp.HeaderAccessControlAllowMethods)))
		assert.Equal(t, fasthttp.HeaderContentType, string(resp.Header.Peek(fasthttp.HeaderAccessControlAllowHeaders)))
		assert.Equal(t, "true", string(resp.Header.Peek(fasthttp.HeaderAccessControlAllowCredentials)))

		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}

	{ //login
		req := fasthttp.AcquireRequest()
		req.SetRequestURI("/auth/login") // task URI
		req.SetHost(eng.ln.Addr().String())
		req.Header.Set(fasthttp.HeaderOrigin, testURI)
		req.Header.SetMethod("POST")
		//req.Header.Set("Authorization", "Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMTMxNDg2ODYwMjAwMDAxNyIsImFjY2Vzc190b2tlbiI6IkVBQUduN3QySWVCd0JBT1BvRVFPbGRBb0VLYTZVaWVKRlpBeDJzdnJ3V0lkTm40OFhscGR5MnhPVjdQbmtqTHc5bXhqNmpxSHZkaE9qdjhLNGtMdkRnRjdyRHZaQ2I0TmMwNXZHQlFUQkgxa3ozN3FMRXVVemdaQk9QMDNYWkFnR3dXcDE5bFhROEVpWkJYaW1aQVpCTzNYVVNwdno5Nm9GQmVmT3VUQnFnZmlMQVpEWkQifQ.L9WfIAS67TYESg4k-wanTV0gOJGEPEPG2ixIgPrCb68")
		req.Header.SetContentType("application/json")
		req.SetBodyString("{\"Login\":\"test@test.com\",\"Password\":\"test password\"}")

		resp := fasthttp.AcquireResponse()

		require.NoError(t, serve(req, resp))
		//t.Log(req.String())
		//t.Log(resp.String())

		assert.NotEmpty(t, resp.Header.Peek(fasthttp.HeaderAccessControlAllowOrigin))
		assert.NotEmpty(t, resp.Header.Peek(fasthttp.HeaderAccessControlExposeHeaders))
		assert.Equal(t, "Authorization", string(resp.Header.Peek(fasthttp.HeaderAccessControlExposeHeaders)))
		assert.Equal(t, "true", string(resp.Header.Peek(fasthttp.HeaderAccessControlAllowCredentials)))
		assert.NotEmpty(t, resp.Header.Peek(fasthttp.HeaderAuthorization))
		assert.Equal(t, "no-store", string(resp.Header.Peek(fasthttp.HeaderCacheControl)))
		assert.Equal(t, "no-cache", string(resp.Header.Peek(fasthttp.HeaderPragma)))

		headerAuth = make([]byte, len(resp.Header.Peek(fasthttp.HeaderAuthorization)))
		copy(headerAuth, resp.Header.Peek(fasthttp.HeaderAuthorization))

		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}

	//v2.public.Cg10ZXN0QHRlc3QuY29tEOeY0O8FGOejq-8FINOlm-G-qpnzBygBMAAWdLLDZ917z9NXafUDLUWbrq21RDNqJw6yC6o0afkRIMoylhXrGKbpY2DQSUmZdEbzshLAKRSP_35w8W16n7EC.eyJMb2dpbiI6InRlc3RAdGVzdC5jb20iLCJFeHBpcmF0aW9uIjoxNTc2Mjc1MDQ3LCJJc3N1ZWRBdCI6MTU3NTY3MDI0NywiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0
	//v2.public.Cg10ZXN0QHRlc3QuY29tEMyT0O8FGMyeq-8FINOlm-G-qpnzBygBMAAYQWMeAQUg7nuayZk-yTEdX61doXjTvQM6TE2vK4t9I7WeFYYexMWuFExCV0Ho5OKjeLmMKjWxos3-RjBeMSYM.eyJMb2dpbiI6InRlc3RAdGVzdC5jb20iLCJFeHBpcmF0aW9uIjoxNTc2Mjc0MzgwLCJJc3N1ZWRBdCI6MTU3NTY2OTU4MCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0

	var refreshTokens proto.RefreshTokenCoded

	{ //refresh
		req := fasthttp.AcquireRequest()
		req.SetRequestURI("/auth/refresh") // task URI
		req.SetHost(eng.ln.Addr().String())
		req.Header.Set(fasthttp.HeaderOrigin, testURI)
		req.Header.SetMethod("POST")
		req.Header.Set("Authorization", string(headerAuth))
		req.Header.SetContentType("application/json")

		resp := fasthttp.AcquireResponse()

		require.NoError(t, serve(req, resp))
		//t.Log(req.String())
		//t.Log(resp.String())

		assert.NotEmpty(t, resp.Header.Peek(fasthttp.HeaderAccessControlAllowOrigin))
		assert.NotEmpty(t, resp.Header.Peek(fasthttp.HeaderAccessControlExposeHeaders))
		assert.Equal(t, resp.Header.Peek(fasthttp.HeaderAccessControlExposeHeaders), []byte("Authorization"))
		assert.Equal(t, resp.Header.Peek(fasthttp.HeaderAccessControlAllowCredentials), []byte("true"))
		assert.NotEmpty(t, resp.Header.Peek(fasthttp.HeaderAuthorization))
		assert.Equal(t, resp.Header.Peek(fasthttp.HeaderCacheControl), []byte("no-store"))
		assert.Equal(t, resp.Header.Peek(fasthttp.HeaderPragma), []byte("no-cache"))

		refreshTokens = proto.RefreshTokenCoded{}
		assert.NoError(t, easyjson.Unmarshal(resp.Body(), &refreshTokens))
		assert.NotEmpty(t, refreshTokens.Access)
		assert.NotEmpty(t, refreshTokens.Auth)

		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}

	{ //check access token (bad request)
		req := fasthttp.AcquireRequest()
		req.SetRequestURI("/auth/check") // task URI
		req.SetHost(eng.ln.Addr().String())
		req.Header.Set(fasthttp.HeaderOrigin, testDomain)
		req.Header.SetMethod("GET")
		req.Header.Set("x-access-token", refreshTokens.Auth)

		resp := fasthttp.AcquireResponse()

		require.NoError(t, serve(req, resp))
		//t.Log(req.String())
		//t.Log(resp.String())

		assert.Equal(t, fasthttp.StatusUnauthorized, resp.StatusCode())

		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}

	{ //check access token (good request)
		req := fasthttp.AcquireRequest()
		req.SetRequestURI("/auth/show") // task URI
		req.SetHost(eng.ln.Addr().String())
		req.Header.Set(fasthttp.HeaderOrigin, testDomain)
		req.Header.SetMethod("GET")
		req.Header.Set("x-access-token", refreshTokens.Access)

		resp := fasthttp.AcquireResponse()

		require.NoError(t, serve(req, resp))
		//t.Log(req.String())
		//t.Log(resp.String())

		assert.Equal(t, fasthttp.StatusOK, resp.StatusCode())

		token := proto.AccessToken{}
		uintOwner, err := strconv.ParseUint(string(owner), 16, 64)

		require.NoError(t, err)
		assert.NoError(t, easyjson.Unmarshal(resp.Body(), &token))
		assert.Equal(t, uintOwner, token.Owner)
		assert.Equal(t, proto.AuthRole_DemoRole, token.Role)

		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}
	eng.Stop()
	assert.NoError(t, <-serverErrChan, "postgres server errors")
}

func serve(req *fasthttp.Request, res *fasthttp.Response) error {
	client := fasthttp.Client{}
	req.SetConnectionClose() // to prevent keep-alive connections
	return client.Do(req, res)
}

package auth

import (
	"crypto/ed25519"
	"errors"
	"fmt"

	"go.uber.org/zap"

	"github.com/lab259/cors"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"gitlab.com/stockengine/demo/internal/proto"

	"github.com/valyala/fasthttp"
)

type FastHTTPAuthMiddlewareOptions struct {
	PublicKey      []byte
	AllowedOrigins []string
}

type FastHTTPAccessHandler func(ctx *fasthttp.RequestCtx, token *proto.AccessToken)

type FastHTTPMiddleware struct {
	publicKey ed25519.PublicKey
	cors      *cors.Cors
}

func NewFastHTTPMiddleware(opts FastHTTPAuthMiddlewareOptions) (ret FastHTTPMiddleware, err error) {
	if len(opts.PublicKey) != ed25519.PublicKeySize {
		err = errors.New("wrong public key size")
		return
	}
	ret.publicKey = opts.PublicKey
	ret.cors = cors.New(cors.Options{
		AllowedOrigins:     opts.AllowedOrigins,
		AllowedMethods:     []string{fasthttp.MethodPost, fasthttp.MethodGet},
		AllowedHeaders:     []string{fasthttp.HeaderAuthorization, fasthttp.HeaderContentType, HeaderAccessToken},
		ExposedHeaders:     []string{fasthttp.HeaderAuthorization},
		MaxAge:             60 * 60,
		AllowCredentials:   true,
		OptionsPassthrough: false,
		Debug:              false,
	})

	//ret.cors.Log = &ret //ToDO: uncomment in case of selectable logging level
	return
}

//stupid cors logs wrapper
func (t *FastHTTPMiddleware) Printf(f string, d ...interface{}) {
	str := fmt.Sprintf(f, d...)
	logger.Log.Debug("cors", zap.String("message", str))
}

func (t *FastHTTPMiddleware) ExtractAccessToken(ctx *fasthttp.RequestCtx) (at *proto.AccessToken, err error) {
	bearer := ctx.Request.Header.Peek(HeaderAccessToken)
	if len(bearer) == 0 {
		//пишем только в режиме дебаг т.к. в продакшн шквал невалидных запросов не должен приводить к росту нагрузки на систему
		logger.Log.Debug("cant GetToken", zap.String("addr", ctx.RemoteIP().String()), zap.String("headers", string(ctx.Request.Header.RawHeaders())))
		ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, "Bearer realm=\"stockengine\",error=\"invalid_token\",error_description=\"Bad token\"")
		SetErrorCtx(ctx, []byte("invalid token"), fasthttp.StatusUnauthorized)
		err = errors.New("no token")
		return
	}
	req := proto.AccessTokenCoded{Access: string(bearer)}
	ctx.Response.Header.Set(fasthttp.HeaderCacheControl, "no-store")
	ctx.Response.Header.Set(fasthttp.HeaderPragma, "no-cache")

	at, err = helperCheckAccessToken(&req, t.publicKey)
	if err != nil {
		logger.Log.Debug("bad access token", zap.String("addr", ctx.RemoteIP().String()), zap.String("headers", string(ctx.Request.Header.RawHeaders())))
		ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, "Bearer realm=\"stockengine\",error=\"invalid_token\",error_description=\"Bad token\"")
		SetErrorCtx(ctx, []byte("invalid token"), fasthttp.StatusUnauthorized)
		return
	}
	return
}

func (t *FastHTTPMiddleware) HandlerWithAccessToken(h FastHTTPAccessHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		if at, err := t.ExtractAccessToken(ctx); err == nil {
			h(ctx, at)
		} else {
			ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, "Bearer realm=\"stockengine\",error=\"no token\",error_description=\"there is no acces token\"")
			SetErrorCtx(ctx, []byte("invalid token"), fasthttp.StatusUnauthorized)
		}
	}
}

func (t *FastHTTPMiddleware) HandlerByRole(h fasthttp.RequestHandler, role proto.AuthRole) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		if at, err := t.ExtractAccessToken(ctx); err == nil {
			if role == at.Role {
				h(ctx)
			} else {
				logger.Log.Debug("wrong role", zap.String("addr", ctx.RemoteIP().String()), zap.String("headers", string(ctx.Request.Header.RawHeaders())))
				ctx.Response.Header.Set(fasthttp.HeaderWWWAuthenticate, "Bearer realm=\"stockengine\",error=\"insufficient permissions\",error_description=\"wrong role\"")
				SetErrorCtx(ctx, []byte("invalid token"), fasthttp.StatusUnauthorized)
			}
		}
	}
}

func (t *FastHTTPMiddleware) HandlerOnlyCors(h fasthttp.RequestHandler) fasthttp.RequestHandler {
	return t.cors.Handler(h)
}

func (t *FastHTTPMiddleware) HandlerSetCors() fasthttp.RequestHandler {
	return t.cors.Handler(func(ctx *fasthttp.RequestCtx) {
		ctx.SetStatusCode(fasthttp.StatusOK)
	})
}

//helper to avoid headers reset in case of ctx.Error
func SetErrorCtx(ctx *fasthttp.RequestCtx, errStr []byte, status int) {
	ctx.SetBody(errStr)
	ctx.SetStatusCode(status)
}

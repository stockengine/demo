package auth

import (
	"context"
	"time"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"
)

func (t *Engine) prometheusReporter() {
	t.wg.Add(1)
	go func() {
		defer t.wg.Done()
		ticker := time.NewTicker(time.Second * 10)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				t.prometheusReportUsers()
				t.prometheusReportRefresh()
				t.prometheusReportAccess()
			case <-t.quit:
				return
			}
		}
	}()
}

func (t *Engine) prometheusReportUsers() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	rows, err := t.pgConn.Query(ctx, "SELECT role, count(*) as cnt from auth.users group by role")
	if rows != nil {
		defer rows.Close()
	}
	if err != nil {
		logger.Log.Error("cant read auth.users count", zap.Error(err))
		return
	}
	for rows.Next() {
		var role string
		var cnt int
		if err := rows.Scan(&role, &cnt); err != nil {
			logger.Log.Error("cant read auth.users count", zap.Error(err))
			break
		}
		t.registredUsers.WithLabelValues(role).Set(float64(cnt))
	}
}
func (t *Engine) prometheusReportRefresh() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	var cnt int
	now := time.Now()
	err := t.pgConn.QueryRow(ctx, "SELECT count(*) as cnt from auth.tokens where issuedat <= $1 AND expiration > $1", now).Scan(&cnt)

	if err != nil {
		logger.Log.Error("cant read auth.tokens count", zap.Error(err))
		return
	}
	t.refreshTokens.WithLabelValues("refresh").Set(float64(cnt))
}
func (t *Engine) prometheusReportAccess() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	var cnt int
	now := time.Now()
	accessExpire := now.Add(-accessExpiration)
	err := t.pgConn.QueryRow(ctx, "SELECT count(*) as cnt from auth.tokens where issuedat <= $1 AND expiration > $1 AND issuedat > $2", now, accessExpire).Scan(&cnt)

	if err != nil {
		logger.Log.Error("cant read auth.tokens count", zap.Error(err))
		return
	}
	t.refreshTokens.WithLabelValues("access").Set(float64(cnt))
}

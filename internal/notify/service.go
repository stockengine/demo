package notify

import (
	"context"
	"net"
	"time"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"github.com/mailjet/mailjet-apiv3-go"

	"google.golang.org/grpc"

	prometheus2 "github.com/prometheus/client_golang/prometheus"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/lib/prometheus"

	"gitlab.com/stockengine/demo/internal/proto"
)

type Engine struct {
	mailjetClient           *mailjet.Client
	passwordTemplateID      int
	resetPasswordTemplateID int
	promEmailStatus         prometheus2.HistogramVec
	lis                     net.Listener
	srv                     *grpc.Server
}

func NewEngine(bind, prometheusBind, publicKey, privateKey string, passwordTemplateID, resetPasswordTemplateID int) (ret *Engine, err error) {
	var prom *prometheus.Prometheus
	prom, err = prometheus.New(prometheusBind)
	if err != nil {
		return nil, err
	}
	//метрики...
	// длительность обработки запросов c ключами:
	//   метод (логин, рефреш, access и т.п.),
	//   статус обработки (успешно или код ошибки)
	emailStatus := prom.NewHistogramVec("email", "request processing status and duration",
		[]string{"type", "status"},
		prometheus.DefBucketsUltraHiLat)

	ret = &Engine{
		mailjetClient:           mailjet.NewMailjetClient(publicKey, privateKey),
		passwordTemplateID:      passwordTemplateID,
		resetPasswordTemplateID: resetPasswordTemplateID,
		promEmailStatus:         emailStatus,
	}

	ret.mailjetClient.Client().Timeout = time.Second * 5

	ret.lis, ret.srv, err = config.CreateGRPCListen(bind)
	if err != nil {
		return nil, err
	}

	proto.RegisterNotifyEngineServer(ret.srv, ret)
	go func() {
		err := ret.srv.Serve(ret.lis)
		logger.ErrorLogHelper(err, "Serve")
	}()
	logger.Log.Info("server ready", zap.String("addr", ret.lis.Addr().String()))
	return
}

func (t *Engine) Stop() {
	t.srv.GracefulStop()
}

func (t *Engine) SendPassword(ctx context.Context, lp *proto.LoginPass) (ret *proto.EmptyMessage, err error) {
	start := time.Now()
	err = t.sendMailJet(lp.Login, t.passwordTemplateID, map[string]interface{}{"password": lp.Password})
	elapsed := time.Since(start)
	t.promEmailStatus.WithLabelValues("send", prometheus.Error2Label(err)).Observe(elapsed.Seconds())
	ret = &proto.EmptyMessage{}
	return
}

func (t *Engine) SendResetPassword(ctx context.Context, lrp *proto.LoginResetPassword) (ret *proto.EmptyMessage, err error) {
	start := time.Now()
	err = t.sendMailJet(lrp.Login, t.resetPasswordTemplateID, map[string]interface{}{"resetlink": lrp.ResetPasswordURL})
	elapsed := time.Since(start)
	t.promEmailStatus.WithLabelValues("reset", prometheus.Error2Label(err)).Observe(elapsed.Seconds())
	ret = &proto.EmptyMessage{}
	return
}

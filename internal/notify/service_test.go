// +build HowToCode

package notify

import (
	"context"
	"testing"

	"gitlab.com/stockengine/demo/internal/proto"

	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/logger"
)

func TestEngine_SendPassword(t *testing.T) {
	logger.Log, _ = zap.NewDevelopment()
	defer logger.Log.Sync()
	m, _ := NewEngine("", "", "", "", 1274778, 1274778)
	_, _ = m.SendPassword(context.Background(), &proto.LoginPass{
		Login:    "georgy@stockengine.tech",
		Password: "tes1pad2",
	})
}

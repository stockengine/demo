package notify

import (
	"github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"
)

func (t *Engine) sendMailJet(email string, templateID int, params map[string]interface{}) (err error) {
	messagesInfo := []mailjet.InfoMessagesV31{
		{
			From: &mailjet.RecipientV31{
				Email: "notify@stockengine.tech",
				Name:  "notify",
			},
			To: &mailjet.RecipientsV31{
				mailjet.RecipientV31{
					Email: email,
				},
			},
			TemplateID:       templateID,
			TemplateLanguage: true,
			Subject:          "you have registered",
			Variables:        params,
		},
	}
	messages := mailjet.MessagesV31{Info: messagesInfo}
	var res *mailjet.ResultsV31
	res, err = t.mailjetClient.SendMailV31(&messages)
	if err != nil {
		logger.Log.Error("cant send email", zap.Error(err), zap.String("email", email), zap.Int("templateID", templateID), zap.Reflect("params", params))
		return
	}
	logger.Log.Info("email sended", zap.String("email", email), zap.Reflect("results", res), zap.Int("templateID", templateID), zap.Reflect("params", params))
	return
}

package migrate

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres" // we will update postgres
	_ "github.com/golang-migrate/migrate/v4/source/file"       // we will read migrations from files
	"github.com/jackc/pgx/v4"
)

type Engine struct {
	m *migrate.Migrate
	user,
	pass,
	endpoint string
	updatePass []UserPass
}

type UserPass struct {
	User string
	Pass string
}

// идея - прогнать скрипты, потом создать пароли для пользователей т.к. в скриптах пароли не получится безопасно хранить
func NewService(user, pass, endpoint, path string, updatePass ...UserPass) (*Engine, error) {
	m, err := migrate.New(
		fmt.Sprintf("file://%s", path),
		fmt.Sprintf("postgres://%s:%s@%s", user, pass, endpoint))
	if err != nil {
		return nil, err
	}
	return &Engine{
		m:          m,
		user:       user,
		pass:       pass,
		endpoint:   endpoint,
		updatePass: updatePass,
	}, nil
}

func (t *Engine) Do() error {
	logger.Log.Info("migrating UP")
	// Migrate all the way up ...
	switch err := t.m.Up(); err {
	case migrate.ErrNoChange: // на всякий случай продолжаем работать - для обновления паролей из секретов
		ver, dirty, _ := t.m.Version()
		logger.Log.Info("no change. db already up to date", zap.Uint("ver", ver), zap.Bool("dirty", dirty))
	case nil:
		ver, dirty, _ := t.m.Version()
		logger.Log.Info("migration success", zap.Uint("ver", ver), zap.Bool("dirty", dirty))
	default:
		return err
	}

	logger.Log.Info("updating passwords")
	if err := CreatePassword(t.user, t.pass, t.endpoint, t.updatePass); err != nil {
		return err
	}

	return errors.New("migration complete") // что бы wait завершился
}

func (t *Engine) Stop() {
	t.m.GracefulStop <- true
	se, de := t.m.Close()
	if se != nil {
		logger.Log.Error("migration source", zap.Error(se))
	}
	if de != nil {
		logger.Log.Error("database", zap.Error(de))
	}
}

func CreatePassword(user, pass, endpoint string, updatePass []UserPass) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	conn, err := pgx.Connect(ctx, fmt.Sprintf("postgres://%s:%s@%s", user, pass, endpoint))
	if err != nil {
		return err
	}
	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		if err := conn.Close(ctx); err != nil {
			logger.Log.Error("conn.Close", zap.Error(err))
		}
	}()

	for _, cp := range updatePass {
		u := quoteIdentifier(cp.User)
		p := "'" + quoteIdentifier(cp.Pass) + "'"
		sql := fmt.Sprintf("ALTER ROLE %s WITH PASSWORD %s;", u, p)
		//logger.Log.Debug(sql)
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		_, err := conn.Exec(ctx, sql) // https://github.com/jackc/pgx/issues/202
		if err != nil {
			return err
		}
	}
	return nil
}

func quoteIdentifier(s string) string {
	return strings.Replace(s, `"`, `""`, -1)
}

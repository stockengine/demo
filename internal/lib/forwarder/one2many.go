package forwarder

import (
	"sync"
)

/*
one2many:
- получает одно сообщение (синхронный вызов)
- перекладывает в канал
- сервисная рутина вычитывает канал и перекладывает в каналы получателей

так же определены функции подписки и отписки
подписка:
- атомарно добавляет ещё один канал-получатель
- возвращает его только для чтения

отписка:
- атомарно удаляет канал получатель

оптимизации. структуру оптимизирую под:
- работы с небольшим количеством получателей (10-100)
- частая отправка сообщений (10-100к в секунду)
- редкое изменение набора подписчиков (раз в минуту)

не применённые опиимизации:
- можно использовать "пустые" элементы массива, для экономии времени на вставку/ удаление и бинарную карту для быстрого поиска

*/

type OneToMany struct {
	inChan      chan interface{}
	subscribers []subscriber
	wg          sync.WaitGroup
	mx          sync.Mutex
	opts        OneToManyOptions
	//quit        chan struct{}
}

type subscriber struct {
	outChan chan interface{}
	cnt     SendDropCounter // число отправленных сообщений и число сброшенных (по причине заполнения буфера)
}

type OneToManyOptions struct {
	ChannelSize        int
	DropSubscribersMsg bool
	ReportLock         bool
}

func NewOneToMany(opts OneToManyOptions) *OneToMany {
	otm := &OneToMany{
		inChan: make(chan interface{}, opts.ChannelSize),
		opts:   opts,
		//quit:   make(chan struct{}),
	}

	otm.wg.Add(1)
	go func() {
		//runtime.LockOSThread() //on server that works better
		otm.msgTransactionForwarder()
		otm.wg.Done()
		//runtime.UnlockOSThread()
	}()
	return otm
}

func (t *OneToMany) Close() {
	//close(t.quit)
	close(t.inChan)
	t.wg.Wait()

	t.mx.Lock()
	for i := range t.subscribers {
		close(t.subscribers[i].outChan)
	}
	t.subscribers = t.subscribers[:0]
	t.mx.Unlock()
}

func (t *OneToMany) SendEvent(d interface{}) {
	_ = sendEvent(d, t.inChan, t.opts.ReportLock, false)
}

func (t *OneToMany) msgTransactionForwarder() {
	for msg := range t.inChan {
		t.msgTransactionForwarderWorker(msg)
	}
}

func (t *OneToMany) msgTransactionForwarderWorker(msg interface{}) {
	t.mx.Lock()
	for i := range t.subscribers {
		if t.subscribers[i].outChan == nil {
			continue
		}
		select {
		case t.subscribers[i].outChan <- msg:
			t.subscribers[i].cnt.SendPlus()
		default:
			switch {
			case t.opts.DropSubscribersMsg:
				t.subscribers[i].cnt.DropPlus()
			default:
				sendWithReport(msg, t.subscribers[i].outChan, "LOCK DETECTED, performance degraded: subscriberChan is full :( waiting", t.opts.ReportLock)
				t.subscribers[i].cnt.SendPlus()
			}
		}
	}
	t.mx.Unlock()
}

func (t *OneToMany) RemoveSubscriber(ch <-chan interface{}) {
	t.mx.Lock()
	for i := range t.subscribers {
		if t.subscribers[i].outChan == ch {
			close(t.subscribers[i].outChan)
			t.subscribers = append(t.subscribers[:i], t.subscribers[i+1:]...)
			break
		}
	}
	t.mx.Unlock()
}

func (t *OneToMany) NewSubscriber() <-chan interface{} {
	elem := subscriber{
		outChan: make(chan interface{}, t.opts.ChannelSize),
	}
	t.mx.Lock()
	t.subscribers = append(t.subscribers, elem)
	t.mx.Unlock()

	return elem.outChan
}

package forwarder

import (
	"sync/atomic"
)

// только ради DRY

type SendDropCounter struct {
	send, drop uint64
}

func (t *SendDropCounter) SendPlus() {
	atomic.AddUint64(&t.send, 1)
}
func (t *SendDropCounter) DropPlus() {
	atomic.AddUint64(&t.drop, 1)
}
func (t *SendDropCounter) Load() (send, drop uint64) {
	return atomic.LoadUint64(&t.send), atomic.LoadUint64(&t.drop)
}

package forwarder

/*
many2one:
- из множества потоков получает одно сообщение (синхронный вызов)
- перекладывает в канал
- сервисная рутина вычитывает канал и перекладывает в каналы получателей

по сути примитивная обёртка над каналами. сделана для единообразности с one2many

*/

type ManyToOne struct {
	inMsg chan interface{}
	opts  ManyToOneOptions
	cnt   SendDropCounter
}

type ManyToOneOptions struct {
	ChannelSize int
	DropMsg     bool
	ReportLock  bool
}

func NewManyToOne(opts ManyToOneOptions) *ManyToOne {
	ret := &ManyToOne{
		inMsg: make(chan interface{}, opts.ChannelSize),
		opts:  opts,
	}
	return ret
}

func (t *ManyToOne) Put(v interface{}) {
	switch sendEvent(v, t.inMsg, t.opts.ReportLock, t.opts.DropMsg) {
	case true:
		t.cnt.SendPlus()
	case false:
		t.cnt.DropPlus()
	}
}

func (t *ManyToOne) Get() <-chan interface{} {
	return t.inMsg
}

func (t *ManyToOne) Close() {
	close(t.inMsg)
}

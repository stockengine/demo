package forwarder

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewManyToOne(t *testing.T) {
	f := NewManyToOne(ManyToOneOptions{
		ChannelSize: 1,
		DropMsg:     true,
		ReportLock:  true,
	})

	const test = "test"
	send, drop := f.cnt.Load()
	assert.Equal(t, uint64(0), send)
	assert.Equal(t, uint64(0), drop)
	f.Put(test)
	send, drop = f.cnt.Load()
	assert.Equal(t, uint64(1), send)
	assert.Equal(t, uint64(0), drop)
	f.Put("test2")
	send, drop = f.cnt.Load()
	assert.Equal(t, uint64(1), send)
	assert.Equal(t, uint64(1), drop)
	v := <-f.Get()
	assert.Equal(t, test, v)
	assert.Equal(t, 0, len(f.inMsg))
}

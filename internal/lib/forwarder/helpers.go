package forwarder

import (
	"time"

	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/logger"
)

func sendWithReport(msg interface{}, ch chan interface{}, logMsg string, report bool) {
	var start time.Time
	if report {
		logger.Log.Warn(logMsg)
		start = time.Now()
	}
	ch <- msg
	if report {
		elapsed := time.Since(start)
		logger.Log.Warn(logMsg, zap.Duration("elapsed", elapsed))
	}
}

func sendEvent(d interface{}, ch chan interface{}, report, drop bool) bool {
	select {
	case ch <- d:
		return true
	default:
		if drop {
			return false
		}
		sendWithReport(d, ch, "LOCK DETECTED, performance degraded: inChan is full :( waiting", report)
		return true
	}
}

package forwarder

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

/*
тест с каналом интерфейсов и каналом int. int = old, interface = new
interface медленее в 2 раза, чем конкретный тип. но только в операции по подписке/ отписке, некритично

$ benchcmp bench_int.txt bench_interface.txt
benchmark                              old ns/op     new ns/op     delta
BenchmarkSubscribeUnsubscribe10-8      97.5          167           +71.28%
BenchmarkSubscribeUnsubscribe100-8     235           506           +115.32%
BenchmarkSend10-8                      141           161           +14.18%
BenchmarkSend100-8                     712           710           -0.28%

*/

func TestOneToMany_AllFunctions(t *testing.T) {
	const maxEvents = 5
	otm := NewOneToMany(OneToManyOptions{
		ChannelSize:        maxEvents,
		DropSubscribersMsg: true,
		ReportLock:         false, // true -> for manual control
	})
	//не должно зависнуть на блокировке

	for i := 0; i < maxEvents<<1; i++ {
		otm.SendEvent(i)
	}

	time.Sleep(time.Millisecond * 1) // wait till old messages will be trashed
	//добавляем подписчика и повторяем
	sub := otm.NewSubscriber()
	for i := 0; i < maxEvents<<1; i++ {
		otm.SendEvent(i)
	}

	time.Sleep(time.Millisecond * 1) // wait till messages will be forwarded
	assert.Equal(t, 1, len(otm.subscribers))
	assert.True(t, sub == otm.subscribers[0].outChan)
	send, drop := otm.subscribers[0].cnt.Load()
	assert.Equal(t, uint64(maxEvents), drop)
	assert.Equal(t, uint64(maxEvents), send)

	//удаляем подпичку
	otm.RemoveSubscriber(sub)
	assert.Equal(t, 0, len(otm.subscribers))

	//повторяем эксперимент, но только в этот раз со считыванием
	//добавляем подписчика и повторяем
	sub = otm.NewSubscriber()
	for i := 0; i < maxEvents; i++ {
		otm.SendEvent(i)
	}
	time.Sleep(time.Millisecond * 1) // wait till messages will be forwarded
	//удаляем подпичку
	otm.RemoveSubscriber(sub)

	var cnt int
	for range sub {
		cnt++
	}
	assert.Equal(t, maxEvents, cnt)

	otm.Close()
}

func helperPrepare(n int) *OneToMany {
	otm := NewOneToMany(OneToManyOptions{
		ChannelSize:        n,
		DropSubscribersMsg: true,
		ReportLock:         false, // true -> for manual control
	})

	//заполняем что б полный массив был, а то будет добавление и удаление только одного первого элемента
	for i := 0; i < n-1; i++ {
		_ = otm.NewSubscriber()
	}
	return otm
}

func helperSubscribeUnsubscribeN(b *testing.B, n int) {
	otm := helperPrepare(n)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sub := otm.NewSubscriber()
		otm.RemoveSubscriber(sub)
	}
	otm.Close()
}

func BenchmarkSubscribeUnsubscribe10(b *testing.B) {
	helperSubscribeUnsubscribeN(b, 10)
}
func BenchmarkSubscribeUnsubscribe100(b *testing.B) {
	helperSubscribeUnsubscribeN(b, 100)
}

func helperSendN(b *testing.B, n int) {
	otm := helperPrepare(n)
	msg := new(int)
	*msg = 666
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		otm.SendEvent(msg)
	}
	otm.Close()
}

func BenchmarkSend10(b *testing.B) {
	helperSendN(b, 10)
}
func BenchmarkSend100(b *testing.B) {
	helperSendN(b, 100)
}

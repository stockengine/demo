package config

import (
	"errors"
	"io/ioutil"
	"reflect"
)

const loadFromFileTag = "fromfile"

var ErrorCantLoadFileFromNonStringField = errors.New("cant load file from non string of []byte field")
var ErrorMustBePointerToStruct = errors.New("must be pointerr to struct")

func LoadSecrets(fname string) ([]byte, error) {
	return ioutil.ReadFile(fname)
}

func LoadFromFile(cfg interface{}) (secrets int, err error) {
	if reflect.TypeOf(cfg).Kind() != reflect.Ptr {
		return 0, ErrorMustBePointerToStruct
	}
	for i := 0; i < reflect.TypeOf(cfg).Elem().NumField(); i++ {
		fieldVal := reflect.ValueOf(cfg).Elem().Field(i)
		fieldType := reflect.TypeOf(cfg).Elem().Field(i)

		_, ok := fieldType.Tag.Lookup(loadFromFileTag)
		if !ok {
			continue
		}

		fname := fieldVal.String()
		fileVal, err := LoadSecrets(fname)
		if err != nil {
			return secrets, err
		}

		switch fieldType.Type.Kind() {
		case reflect.String: //good
			fieldVal.SetString(string(fileVal))
		case reflect.Slice:
			//we can fill only byte slice
			if fieldType.Type.Elem().Kind() != reflect.Uint8 {
				return secrets, ErrorCantLoadFileFromNonStringField
			}
			fieldVal.SetBytes(fileVal)
		default:
			return secrets, ErrorCantLoadFileFromNonStringField
		}
		secrets++
	}
	return secrets, nil
}

func LoadFromFile2(cfg interface{}) error {
	_, err := LoadFromFile(cfg)
	return err
}

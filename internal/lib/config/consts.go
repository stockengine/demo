package config

import "time"

const grpcTime = 10 * time.Second
const grpcTimeout = 25 * time.Second // two probes and one half
const grpcDialTimeout = 1 * time.Second

package config

import (
	"net"
	"time"

	"google.golang.org/grpc/keepalive"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"google.golang.org/grpc"
)

func CreateGRPCListen(hostPort string) (lis net.Listener, srv *grpc.Server, err error) {
	lis, err = net.Listen("tcp", hostPort)
	if err != nil {
		return
	}
	srv = grpc.NewServer(
		grpc.ConnectionTimeout(time.Second),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             grpcTime / 2,
			PermitWithoutStream: true,
		}),
		grpc.KeepaliveParams(keepalive.ServerParameters{
			Time:    grpcTime,
			Timeout: grpcTimeout,
		}),
	)
	log := logger.Log.WithOptions(zap.AddCallerSkip(1))
	log.Info("listen", zap.String("demand", hostPort), zap.String("addr", lis.Addr().String()))
	return
}

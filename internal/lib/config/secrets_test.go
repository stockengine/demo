package config

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadFromFile(t *testing.T) {
	type Config struct {
		User     string `required:"true" desc:"postgres file with user" default:"/run/secrets/postgres_user.txt" fromfile:""`
		Endpoint string `required:"true" desc:"postgres endpoint" default:"postgres:5432"`
	}

	f, err := ioutil.TempFile("", t.Name())
	assert.NoError(t, err)

	defer func() {
		assert.NoError(t, f.Close())
	}()

	const testData = string("testData")
	_, err = f.WriteString(testData)
	assert.NoError(t, err)

	test := &Config{User: f.Name()}
	assert.NoError(t, LoadFromFile2(test))
	assert.Equal(t, testData, test.User)
	assert.Equal(t, "", test.Endpoint)

	assert.Error(t, LoadFromFile2("asd"))

	type Config2 struct {
		User int `fromfile:""`
	}
	test2 := &Config2{User: 123}
	assert.Error(t, LoadFromFile2(test2))

	test = &Config{User: "unknownfile"}
	assert.Error(t, LoadFromFile2(test))

}

package config

import (
	"context"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

func GRPCClientDial(addr string, withBlock bool) (conn *grpc.ClientConn, err error) {
	var ctx context.Context
	var cancel context.CancelFunc
	ctx, cancel = context.WithTimeout(context.Background(), grpcDialTimeout) // отмена этого dial вроде бы как нафиг не нужна при таймауте в 1s, даже для досрочной остановки 1с можно подождать
	defer cancel()                                                           //to avoid leak
	opts := append([]grpc.DialOption{},
		grpc.WithInsecure(), grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                grpcTime,
			Timeout:             grpcTimeout,
			PermitWithoutStream: true,
		}))
	if withBlock {
		opts = append(opts, grpc.WithBlock())
	}
	conn, err = grpc.DialContext(ctx, addr,
		opts...,
	)
	log := logger.Log.WithOptions(zap.AddCallerSkip(1))
	if err != nil {
		log.Error("cant dial", zap.String("addr", addr), zap.Error(err))
		return
	}
	log.Debug("connected",
		zap.String("state", conn.GetState().String()),
		zap.String("target", conn.Target()),
	)
	return
}

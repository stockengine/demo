package config

import (
	"fmt"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"

	"go.uber.org/zap"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/stockengine/demo/internal/lib/logger"
)

// support tag to load values from file [fromfile:""]
// 	User       string `required:"true" desc:"postgres file with user" default:"/run/secrets/postgres_user.txt" fromfile:""`
func LoadConfig(cfg interface{}) bool {
	logConfig := zap.NewDevelopmentConfig()
	logConfig.DisableStacktrace = true
	log, err := logConfig.Build() //потом-потом на основе конфига можно будет логгер настраивать
	if err != nil {
		fmt.Printf("cant init logger -> %s\n", err)
		return false
	}
	logger.Log = log
	localLog := log.WithOptions(zap.AddCallerSkip(1))

	prefix := strings.ReplaceAll(path.Base(os.Args[0]), ".", "_")
	if err := envconfig.Process(prefix, cfg); err != nil || len(os.Args) > 1 {
		//localLog.Error("envconfig.Process", zap.Error(err), zap.Int("len(os.Args)", len(os.Args)))
		localLog.Warn("service is configured via ENV values, dont pass any cmd line params")
		err = envconfig.Usage(prefix, cfg)
		if err != nil {
			localLog.Error("cant print usage", zap.Error(err))
		}
		return false
	}

	// сначала печатаем конфиг, а только потом загружаем секреты что б в дебаг печать не вывести секреты
	localLog.Debug("config loaded", zap.Reflect("config", cfg))

	var secrets int
	if secrets, err = LoadFromFile(cfg); err != nil {
		localLog.Error("cant load files", zap.Error(err))
		return false
	}

	if secrets != 0 {
		localLog.Info("loaded secrets", zap.Int("secrets count", secrets))
	}

	return true
}

type WaitFunction func() error
type AfterWaitFunction func() error

func WaitTerm(functs []WaitFunction, afterFn []AfterWaitFunction) {
	log := logger.Log.WithOptions(zap.AddCallerSkip(1))
	//waiting till end
	c := make(chan os.Signal, 1+len(functs))

	// Passing no signals to Notify means that
	// all signals will be sent to the channel.
	signals := []os.Signal{syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT}
	log.Info(fmt.Sprintf("will wait until signal happens %v or error", signals))
	signal.Notify(c, signals...)

	for _, f := range functs {
		go func(fn func() error) {
			if err := fn(); err != nil {
				logger.Log.Debug("waitFunction", zap.Error(err))
				c <- syscall.SIGABRT
			}
		}(f)
	}

	// Block until any signal is received.
	sig := <-c
	log.Info("Got signal:", zap.String("signal", sig.String()))

	for _, fn := range afterFn {
		if err := fn(); err != nil {
			logger.Log.Error("AfterWaitFunction", zap.Error(err))
		}
	}
}

func WaitTermOld(fn ...WaitFunction) {
	WaitTerm(fn, []AfterWaitFunction{})
}

package subcommand

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/stockengine/demo/internal/proto"
)

func TestNewSubCommand(t *testing.T) {
	old := proto.SubCommands{
		New: &proto.NewOrder{
			Type: 123,
		},
		Delete: &proto.DeleteOrder{OwnerID: 123},
		Edit: &proto.EditOrder{
			NewPrice: proto.Price{Price: 123},
		},
	}
	cpy := NewSubCommand(old)
	old.New.Type = 234
	old.Edit.NewPrice.Price = 234
	old.Delete.OwnerID = 234

	assert.NotEqual(t, *old.New, *cpy.New)
	assert.NotEqual(t, *old.Edit, *cpy.Edit)
	assert.NotEqual(t, *old.Delete, *cpy.Delete)
}

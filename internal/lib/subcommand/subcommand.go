package subcommand

import "gitlab.com/stockengine/demo/internal/proto"

func NewSubCommand(old proto.SubCommands) (ret proto.SubCommands) {
	if old.New != nil {
		ret.New = new(proto.NewOrder)
		*ret.New = *old.New
	}
	if old.Delete != nil {
		ret.Delete = new(proto.DeleteOrder)
		*ret.Delete = *old.Delete
	}
	if old.Edit != nil {
		ret.Edit = new(proto.EditOrder)
		*ret.Edit = *old.Edit
	}
	return
}

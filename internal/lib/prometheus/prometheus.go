package prometheus

import (
	"os"
	"path"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

const nameSpace = "STOCKENGINE"

var subSystemName = strings.ToUpper(strings.ReplaceAll(path.Base(os.Args[0]), ".", "_"))

var DefBucketsLowLat = append(prometheus.LinearBuckets(0.001, 0.001, 20), prometheus.LinearBuckets(0.03, 0.01, 8)...)
var DefBucketsUltraLowLat = append(prometheus.LinearBuckets(0.00001, 0.00001, 20), prometheus.LinearBuckets(0.0003, 0.0001, 8)...)
var DefBucketsHiLat = append(prometheus.LinearBuckets(0.005, 0.005, 4), append(prometheus.LinearBuckets(0.03, 0.01, 8), prometheus.LinearBuckets(0.2, 0.1, 9)...)...)
var DefBucketsUltraHiLat = prometheus.LinearBuckets(0.1, 0.1, 10)

//https://prometheus.io/docs/practices/naming/#metric-names
func (t *Prometheus) NewCounterVec(name, help string, labelNames []string) prometheus.CounterVec {
	c := promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace:   nameSpace,
		Subsystem:   subSystemName,
		Name:        name,
		Help:        help,
		ConstLabels: nil,
	}, labelNames)
	t.collectors = append(t.collectors, c)
	return *c
}

func (t *Prometheus) NewGaugeVec(name, help string, labelNames []string) prometheus.GaugeVec {
	c := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace:   nameSpace,
		Subsystem:   subSystemName,
		Name:        name,
		Help:        help,
		ConstLabels: nil,
	}, labelNames)
	t.collectors = append(t.collectors, c)
	return *c
}
func (t *Prometheus) NewCounter(name, help string) prometheus.Counter {
	c := promauto.NewCounter(prometheus.CounterOpts{
		Namespace:   nameSpace,
		Subsystem:   subSystemName,
		Name:        name,
		Help:        help,
		ConstLabels: nil,
	})
	t.collectors = append(t.collectors, c)
	return c
}

func (t *Prometheus) NewGauge(name, help string) prometheus.Gauge {
	c := promauto.NewGauge(prometheus.GaugeOpts{
		Namespace:   nameSpace,
		Subsystem:   subSystemName,
		Name:        name,
		Help:        help,
		ConstLabels: nil,
	})
	t.collectors = append(t.collectors, c)
	return c
}

func (t *Prometheus) NewHistogramVec(name, help string, labelNames []string, buckets []float64) prometheus.HistogramVec {
	c := promauto.NewHistogramVec(prometheus.HistogramOpts{
		Namespace:   nameSpace,
		Subsystem:   subSystemName,
		Name:        name,
		Help:        help,
		ConstLabels: nil,
		Buckets:     buckets,
	}, labelNames)
	t.collectors = append(t.collectors, c)
	return *c
}

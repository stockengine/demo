package prometheus

import (
	"strconv"
	"time"

	prometheus2 "github.com/prometheus/client_golang/prometheus"
	"github.com/valyala/fasthttp"
)

func MetricsWrapper(fn func(ctx *fasthttp.RequestCtx), httpStatus prometheus2.HistogramVec) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		start := time.Now()
		fn(ctx)
		httpStatus.WithLabelValues(
			string(ctx.Method()), string(ctx.RequestURI()), strconv.Itoa(ctx.Response.StatusCode())).
			Observe(time.Since(start).Seconds())
	}
}

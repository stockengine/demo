package prometheus

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	p, err := New("127.0.0.10:7010/metrics")
	require.NoError(t, err)
	require.NotNil(t, p)

	cntr := p.NewCounterVec("test1", "", []string{"label1", "label2"})
	cntr.WithLabelValues("test1", "test2").Add(12.12)

	hstgrm := p.NewHistogramVec("test2", "", []string{"label3", "label4"}, []float64{1, 5, 10})
	hstgrm.WithLabelValues("test3", "test4").Observe(23.34)

	gg := p.NewGaugeVec("test3", "", []string{"label5", "label6"})
	gg.WithLabelValues("test5", "test6").Set(34.34)

	require.NoError(t, p.Close())
}

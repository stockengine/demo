package prometheus

func Error2Label(err error) string {
	if err == nil {
		return "ok"
	}
	return err.Error()
}

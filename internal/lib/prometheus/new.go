package prometheus

import (
	"context"
	"errors"
	"net"
	"net/http"
	"net/url"
	"time"

	prometheus2 "github.com/prometheus/client_golang/prometheus"

	prom "github.com/prometheus/client_golang/prometheus/promhttp"
)

type Prometheus struct {
	httpServer *http.Server
	netList    net.Listener
	err        chan error
	collectors []prometheus2.Collector
}

//создаём абстракции и запускаем веб-сервер. Метрики создаются отдельно
func New(urlStr string) (*Prometheus, error) {
	urlParsed, err := url.ParseRequestURI("http://" + urlStr)
	if err != nil {
		return nil, err
	}
	if urlParsed.Path == "" {
		return nil, errors.New("empty prometheus path")
	}

	ret := &Prometheus{
		err: make(chan error),
	}

	mux := http.NewServeMux()

	mux.Handle(urlParsed.Path, prom.Handler())

	ret.httpServer = &http.Server{
		Addr:              urlParsed.Host,
		Handler:           mux,
		ReadTimeout:       10 * time.Second,
		ReadHeaderTimeout: 10 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       60 * time.Second,
	}

	ret.netList, err = net.Listen("tcp", urlParsed.Host)
	if err != nil {
		return nil, err
	}

	go func(p *Prometheus) {
		p.err <- p.httpServer.Serve(p.netList)
	}(ret) //явный аргуент что б стек туда\ сюда не таскать

	return ret, nil
}

func (t *Prometheus) Close() error {
	select {
	case _, ok := <-t.err:
		if !ok {
			//already going shutdown
			return nil
		}
	default:
		//do nothing
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	err := t.httpServer.Shutdown(ctx)
	if err != nil {
		return err
	}

	for _, v := range t.collectors {
		prometheus2.DefaultRegisterer.Unregister(v)
	}

	defer close(t.err)

	switch err := <-t.err; err {
	case http.ErrServerClosed:
		//good
		return nil
	default:
		return err
	}
}

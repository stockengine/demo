package logger

import (
	"context"
	"io"
	"reflect"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
)

func ErrorLogHelper(err error, method string, fields ...zap.Field) (needBreake bool) {
	log := Log.WithOptions(zap.AddCallerSkip(1))
	needBreake, _ = ErrorLogHelper4(err, method, log, fields...)
	return
}

func ErrorLogHelper3(err error, method string, log *zap.Logger, fields ...zap.Field) (needBreake bool) {
	log = log.WithOptions(zap.AddCallerSkip(1))
	needBreake, _ = ErrorLogHelper4(err, method, log, fields...)
	return
}

func ErrorLogHelper2(err error, method string, fields ...zap.Field) (needBreake, isGood bool) {
	log := Log.WithOptions(zap.AddCallerSkip(1))
	return ErrorLogHelper4(err, method, log, fields...)
}

func ErrorLogHelper4(err error, method string, log *zap.Logger, fields ...zap.Field) (needBreake, isGood bool) {
	log = log.WithOptions(zap.AddCallerSkip(1))
	defer Sync()
	switch err {
	case nil:
		return false, true
	case io.EOF:
		fields = append(fields, zap.Error(err))
		log.Debug(method, fields...)
		return true, true
	default:
		if stat, ok := status.FromError(err); ok {
			switch stat.Code() {
			case codes.Canceled: // контекст отменили на нашей стороне
				fallthrough
			case codes.Unavailable: // так бывает когда другая сторона разрывает коннект
				fields = append(fields, zap.String("code", stat.Code().String()), zap.String("message", stat.Message()))
				log.Info(method, fields...)
			default:
				fields = append(fields, zap.String("codedesc", stat.Code().String()), zap.Error(err), zap.Uint32("code", uint32(stat.Code())))
				log.Error(method, fields...)
			}
		} else {
			fields = append(fields, zap.Error(err), zap.Reflect("err", err), zap.String("type", reflect.TypeOf(err).String()))
			log.Error(method, fields...)
		}
	}
	return true, false
}

func LogNewConnect(ctx context.Context, method string, fields ...zap.Field) (log *zap.Logger) {
	localLog := Log.WithOptions(zap.AddCallerSkip(1))
	p, ok := peer.FromContext(ctx)
	addr := "unknown"
	if !ok {
		localLog.Warn("cant get peer context :(", zap.Reflect("ctx", ctx))
	} else {
		addr = p.Addr.String()
	}
	fields = append(fields, zap.String("addr", addr))
	log = Log.With(fields...)

	localLog.Info(method, fields...)
	return
}

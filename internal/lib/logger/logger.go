package logger

import (
	"fmt"
	"os"

	"go.uber.org/zap"
)

var Log = zap.NewNop()

func Sync() {
	switch err := Log.Sync(); err.(type) {
	case nil:
	case *os.PathError: //zap tryes to sync /dev/stderr
	default:
		fmt.Printf("Log.Sync error [%T] -> %s\n", err, err)
	}
}

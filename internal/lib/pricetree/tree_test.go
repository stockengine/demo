package pricetree

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/stockengine/demo/internal/proto"
)

//noinspection GoUnusedGlobalVariable
var out interface{}

// стресс тест - проверяем упадёт или нет после пула для элементов дерева
func BenchmarkBookCol(b *testing.B) {
	bt := NewTree(false)
	//bl := &BookLineV2{}
	rnd := rand.New(rand.NewSource(666))
	ops := make(map[int32]int)
	priceCnt := make(map[Price]int)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		//b.StopTimer()
		op := rnd.Int31n(3)
		price := Price{Price: uint64(rnd.Int31n(200) * 100)}

		ops[op]++
		//b.StartTimer()

		switch op {
		case 0:
			bt.Put(price, nil)
			if _, ok := priceCnt[price]; !ok { // такого ключа пока нет, добавляем
				priceCnt[price]++
			} // else //уже есть, фактического добавления не будет
		case 1:
			out, _ = bt.Get(price)
		case 2:
			bt.Remove(price)
			delete(priceCnt, price)
		default:
			b.Fatal("OMFG!!!111")
		}
		//b.StopTimer()
		assert.Equal(b, len(priceCnt), bt.Size())
	}
	//b.StopTimer()
}

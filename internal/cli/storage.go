package cli

import (
	"container/ring"
	"context"
	"fmt"
	"io"
	"os"
	"reflect"
	"strconv"
	"text/tabwriter"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/proto"
	"google.golang.org/grpc"
)

type Storage struct {
	clientConn  *grpc.ClientConn
	serviceConn proto.StorageEngineClient
	//methodLoadTxConn  proto.StorageEngine_LoadTxClient //stateless call
	methodSubscribeTx proto.StorageEngine_SubscribeTxClient

	txRing *ring.Ring
}

const storageSubscribeHistory = 10

func NewStorage() *Storage {
	return &Storage{}
}

func (t *Storage) Description(method string) string {
	desc := map[string]string{
		"Connect": "connect to se.Storage via grpc and subscribe to tx",
		"Load":    "load and show all tx",
		"Show":    "show last tx (received by subscribe)",
	}
	return desc[method]
}

func (t *Storage) Connect(addr *Connect) {
	//connect to order create cmd
	if t.clientConn == nil {
		fmt.Print("grpc.Dial...")

		clientConn, err := config.GRPCClientDial(addr.Addr, true)
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		fmt.Println("done")
		t.clientConn = clientConn
	}
	if t.serviceConn == nil {
		fmt.Print("serviceConn...")
		t.serviceConn = proto.NewStorageEngineClient(t.clientConn)
		fmt.Println("done")
	}

	if t.methodSubscribeTx == nil {
		fmt.Print("methodSubscribeTx....")
		methodSubscribeTx, err := t.serviceConn.SubscribeTx(context.Background(), &proto.EmptyMessage{})
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		t.methodSubscribeTx = methodSubscribeTx
		t.txRing = ring.New(storageSubscribeHistory)
		go func() {
			//так грязно т.к. этот код ничего полезного не делает и его не жалко
		L:
			for {
				ct, err := t.methodSubscribeTx.Recv()
				switch err {
				case nil:
				//good
				case io.EOF:
					fmt.Println("methodSubscribeTx.Recv() = EOF")
					break L
				default:
					fmt.Println("methodSubscribeTx.Recv() err -> ", err)
					break L
				}
				t.txRing.Value = ct
				t.txRing = t.txRing.Next()
			}
		}()

		fmt.Println("done")
	}
}

func (t *Storage) Show() {
	if t.txRing == nil {
		fmt.Println("no data")
	} else {
		var i int
		t.txRing.Do(func(iface interface{}) {
			cx, ok := iface.(*proto.CommitedTransaction)
			var data string
			switch {
			case iface == nil:
				//do nothing
				return
			case !ok:
				data = "invalid struct ->" + reflect.TypeOf(iface).String()
			default:
				data = fmt.Sprintf("%+v", cx)
			}
			i++
			fmt.Printf("tx[%d] -> %s\n", i, data)
		})
		fmt.Printf("total %d tx\n", i)
	}
}

type LoadParams struct {
	Status string `cli:"comma separated list of statuses to be included in response: Closed,ToBook. use ! to exclude status from response"`
	Owner  string `cli:"comma separated list of owners to be included in response: 123,345. use ! to exclude status from response"`
	Limit  int    `cli:"0 - show all records, negative values - show first N, positive - last N"`
}

func (t *Storage) Load(filter *LoadParams) {
	if t.serviceConn == nil {
		fmt.Println("there is no connection")
	} else {
		if methodLoadTxConn, err := t.serviceConn.LoadTx(context.Background(), &proto.EmptyMessage{}); err != nil {
			fmt.Println("error -> ", err.Error())
		} else {
			statusFilter := NewFilter(filter.Status)
			ownerFilters := NewFilter(filter.Owner)

			var i int
			var w = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.AlignRight)
			var lb = NewLimitBuffer(filter.Limit)
		L:
			for {
				ct, err := methodLoadTxConn.Recv()
				switch err {
				case nil:
				//good
				case io.EOF:
					break L
				default:
					fmt.Println("methodLoadTxConn.Recv() error -> ", err)
					break L
				}

				//если фильтры заданы и при этом статус не в списке, то пропускаем строку
				if statusFilter.Skip(ct.Transaction.OrderFinalState.OrderStatus.String()) ||
					(ownerFilters.Skip(strconv.FormatUint(CommandGetOwnerID(&ct.Transaction.Command), 10))) {
					continue
				}
				var skipCnt int
				for _, tx := range ct.Transaction.TxList {
					if ownerFilters.Skip(strconv.FormatUint(tx.Maker.Order.OwnerID, 10)) &&
						ownerFilters.Skip(strconv.FormatUint(tx.Taker.Order.OwnerID, 10)) {
						skipCnt++
					}
				}
				if len(ct.Transaction.TxList) != 0 && skipCnt == len(ct.Transaction.TxList) {
					// если есть транзакции и ни одна из них не подходит под фильтр, то скипаем. показываем только если хоть одна
					continue
				}
				//ownerFilters.Skip(ct.)

				const linesPerPage = 30
				//хранилище прекрасно гигантские объёмы хранит и через grpc передаёт, а вот в буффере, текстом всё держать очень накладно
				// протестировано на 100500 записях
				if i != 0 && i%linesPerPage == 0 && filter.Limit == 0 {
					_, _ = lb.WriteTo(w)
					_ = w.Flush()
				}
				lb.ResetCurrent()
				if i%linesPerPage == 0 {
					//print header
					_, _ = fmt.Fprintf(lb, "\t\tCommand\t\t\t\t\t\tOriginal\t\t\t\tFinal\t\t\n")
					_, _ = fmt.Fprintf(lb, "###\tCommitID\tIPLow\tSrcDstPort\tConnectTS\tAcceptCnt\tType\tOperation\tAmount\tPrice\tOwner\tExtID\tAmount\tStatus\t\n")
				}
				i++
				switch {
				case ct.Transaction.Command.SubCommands.New != nil:
					_, _ = fmt.Fprintf(lb, "%d\t%d\t%x\t%x\t%d\t%d\t%s\t%s\t%s\t%s\t%d\t%d\t%s\t%s\t\n",
						i, ct.CommitID, ct.Transaction.Command.ReqID.IPLow, ct.Transaction.Command.ReqID.SrcDstPort, ct.Transaction.Command.ReqID.ConnectTS, ct.Transaction.Command.ReqID.AcceptCnt,
						ct.Transaction.Command.SubCommands.New.Type.String(), ct.Transaction.Command.SubCommands.New.Operation.String(), AmountWithSpaces(proto.AmountToString(&ct.Transaction.Command.SubCommands.New.Amount)),
						proto.PriceToString(ct.Transaction.Command.SubCommands.New.Price), ct.Transaction.Command.SubCommands.New.OwnerID, ct.Transaction.Command.SubCommands.New.ExtID,
						AmountWithSpaces(proto.AmountToString(&ct.Transaction.OrderFinalState.AmountFinal)), ct.Transaction.OrderFinalState.OrderStatus.String(),
					)
				case ct.Transaction.Command.SubCommands.Edit != nil:
					_, _ = fmt.Fprintf(lb, "%d\t%d\t%x\t%x\t%d\t%d\t%s\t%s\t%s\t%s\t%d\t%d\t%s\t%s\t\n",
						i, ct.CommitID, ct.Transaction.Command.ReqID.IPLow, ct.Transaction.Command.ReqID.SrcDstPort, ct.Transaction.Command.ReqID.ConnectTS, ct.Transaction.Command.ReqID.AcceptCnt,
						"", "Edit", AmountWithSpaces(proto.AmountToString(&ct.Transaction.Command.SubCommands.Edit.NewAmount)), proto.PriceToString(ct.Transaction.Command.SubCommands.Edit.NewPrice),
						ct.Transaction.Command.SubCommands.Edit.OwnerID, ct.Transaction.Command.SubCommands.Edit.ExtID,
						AmountWithSpaces(proto.AmountToString(&ct.Transaction.OrderFinalState.AmountFinal)), ct.Transaction.OrderFinalState.OrderStatus.String(),
					)
				case ct.Transaction.Command.SubCommands.Delete != nil:
					_, _ = fmt.Fprintf(lb, "%d\t%d\t%x\t%x\t%d\t%d\t%s\t%s\t%s\t%s\t%d\t%d\t%s\t%s\t\n",
						i, ct.CommitID, ct.Transaction.Command.ReqID.IPLow, ct.Transaction.Command.ReqID.SrcDstPort, ct.Transaction.Command.ReqID.ConnectTS, ct.Transaction.Command.ReqID.AcceptCnt,
						"", "Delete", "", "", ct.Transaction.Command.SubCommands.Delete.OwnerID, ct.Transaction.Command.SubCommands.Delete.ExtID,
						AmountWithSpaces(proto.AmountToString(&ct.Transaction.OrderFinalState.AmountFinal)), ct.Transaction.OrderFinalState.OrderStatus.String(),
					)
				default:
					_, _ = fmt.Fprintf(lb, "%d\t%d\t%x\t%x\t%d\t%d\t%s\t\t\t\t\t\t\t\t\n",
						i, ct.CommitID, ct.Transaction.Command.ReqID.IPLow, ct.Transaction.Command.ReqID.SrcDstPort, ct.Transaction.Command.ReqID.ConnectTS, ct.Transaction.Command.ReqID.AcceptCnt,
						"unsupported")
				}
				for i := range ct.Transaction.TxList {
					deal := []*proto.OrderChange{&ct.Transaction.TxList[i].Taker, &ct.Transaction.TxList[i].Maker}
					for _, v := range deal {
						_, _ = fmt.Fprintf(lb, "\t\t\t%s\t\t%s\t%s\t%s\t%s\t%s\t%d\t%d\t%s\t%s\t\n",
							v.MarketSide.String(),
							proto.PriceToString(v.DealPrice),
							v.Order.Type.String(), v.Order.Operation.String(), AmountWithSpaces(proto.AmountToString(&v.AmountChange)), proto.PriceToString(v.Order.Price), v.Order.OwnerID, v.Order.ExtID,
							AmountWithSpaces(proto.AmountToString(&v.Order.Amount)), v.OrderStatus.String(),
						)
					}
				}
				if !lb.Next() {
					break
				}
			}
			if i > 0 {
				_, _ = lb.WriteTo(w)
				_ = w.Flush()
			}
			fmt.Printf("total %d tx\n", i)
		}
	}
}

func (t *Storage) Disconnect() {
	if t.methodSubscribeTx != nil {
		fmt.Print("methodSubscribeTx close...")
		switch err := t.methodSubscribeTx.CloseSend(); err {
		case nil, io.EOF:
			md := t.methodSubscribeTx.Trailer()
			t.methodSubscribeTx = nil
			fmt.Println("done. md -> ", md)
		default:
			fmt.Println("error -> ", err)
		}
	}
	if t.clientConn != nil {
		fmt.Print("clientConn & serviceConn close...")
		if err := t.clientConn.Close(); err != nil {
			fmt.Println("error ->", err)
			return
		}
		t.clientConn = nil
		t.serviceConn = nil
		fmt.Println("done")
	}
}

func (t *Storage) Status() {
	if t.clientConn != nil {
		fmt.Println("clientConn -> ", t.clientConn.GetState().String())
	} else {
		fmt.Println("clientConn -> ", "nil")
	}

	fmt.Printf("serviceConn -> %p\n", t.serviceConn)
	fmt.Printf("methodSubscribeTx -> %p\n", t.methodSubscribeTx)
}

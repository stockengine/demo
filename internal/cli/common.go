package cli

import (
	"bytes"
	"io"
	"strings"

	"gitlab.com/stockengine/demo/internal/proto"
)

type Filter struct {
	cond   map[string]struct{}
	invert bool
}

func NewFilter(filter string) (ret Filter) {
	ret.cond = make(map[string]struct{})
	if len(filter) > 0 && filter[0] == '!' {
		ret.invert = true
		filter = filter[1:] //remove first symbol
	}
	if len(filter) > 0 {
		for str, i := strings.Split(filter, ","), 0; i < len(str); i++ {
			ret.cond[str[i]] = struct{}{}
		}
	}
	return ret
}

func (t *Filter) Skip(val string) bool {
	if len(t.cond) > 0 {
		if _, ok := t.cond[val]; ok == t.invert {
			return true
		}
	}
	return false
}

type LimitBuffer struct {
	buf     []bytes.Buffer
	limit   int
	current int
}

func NewLimitBuffer(limit int) *LimitBuffer {
	elems := limit
	switch {
	case limit < 0:
		elems = -elems
	case limit == 0:
		elems = 1
	}
	return &LimitBuffer{
		buf:   make([]bytes.Buffer, elems),
		limit: limit,
	}
}

func (t *LimitBuffer) Write(p []byte) (n int, err error) {
	return t.buf[t.current].Write(p)
}

func (t *LimitBuffer) ResetCurrent() {
	if t.limit != 0 {
		t.buf[t.current%len(t.buf)].Reset()
	}
}

func (t *LimitBuffer) Next() bool {
	switch {
	case t.limit == 0:
		return true
	case t.limit > 0: //пишем всё по кругу
		t.current = (t.current + 1) % len(t.buf)
		return true
	case t.limit < 0: //показываем только N первых
		t.current++
		return t.current != len(t.buf)
	default:
		panic("OMFG!!! its impossible")
	}
}

func (t *LimitBuffer) WriteTo(w io.Writer) (n int64, err error) {
	for index, i := t.current%len(t.buf), 0; i < len(t.buf) && err == nil; i, index = i+1, (index+1)%len(t.buf) {
		tn, te := t.buf[index].WriteTo(w)
		n += tn
		err = te
	}
	return
}

func AmountWithSpaces(amount string) string {
	runes := []rune(amount)
	const spacePosition = 3
	const space = rune(' ')
	var buf []rune
	for i := len(runes) - 1; i >= 0; i-- {
		buf = append(buf, runes[i])
		if (len(amount)-i)%spacePosition == 0 && i != 0 {
			buf = append(buf, space)
		}
	}
	ret := bytes.Buffer{}
	for i := len(buf) - 1; i >= 0; i-- {
		_, _ = ret.WriteRune(buf[i])
	}

	return ret.String()
}

func CommandGetOwnerID(cmd *proto.Command) uint64 {
	switch {
	case cmd.SubCommands.New != nil:
		return cmd.SubCommands.New.OwnerID
	case cmd.SubCommands.Edit != nil:
		return cmd.SubCommands.Edit.OwnerID
	case cmd.SubCommands.Delete != nil:
		return cmd.SubCommands.Delete.OwnerID
	default:
		panic("OMFG!!!")
	}
}

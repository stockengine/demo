package cli

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/stockengine/demo/internal/lib/config"
	"gitlab.com/stockengine/demo/internal/proto"
	"google.golang.org/grpc"
)

type Auth struct {
	clientConn  *grpc.ClientConn
	serviceConn proto.AuthEngineClient
}

func NewAuth() *Auth {
	return &Auth{}
}

func (t *Auth) Description(method string) string {
	desc := map[string]string{
		"Connect": "connect to se.Auth via grpc",
		//"Load":    "load and show all tx",
		//"Show":    "show last tx (received by subscribe)",
	}
	return desc[method]
}

func (t *Auth) Connect(addr *Connect) {
	//connect to order create cmd
	if t.clientConn == nil {
		fmt.Print("grpc.Dial...")

		clientConn, err := config.GRPCClientDial(addr.Addr, true)
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		fmt.Println("done")
		t.clientConn = clientConn
	}
	if t.serviceConn == nil {
		fmt.Print("serviceConn...")
		t.serviceConn = proto.NewAuthEngineClient(t.clientConn)
		fmt.Println("done")
	}
}

func (t *Auth) Disconnect() {
	if t.clientConn != nil {
		fmt.Print("clientConn & serviceConn close...")
		if err := t.clientConn.Close(); err != nil {
			fmt.Println("error ->", err)
			return
		}
		t.clientConn = nil
		t.serviceConn = nil
		fmt.Println("done")
	}
}

func (t *Auth) Status() {
	if t.clientConn != nil {
		fmt.Println("clientConn -> ", t.clientConn.GetState().String())
	} else {
		fmt.Println("clientConn -> ", "nil")
	}

	fmt.Printf("serviceConn -> %p\n", t.serviceConn)
}

//service AuthEngine {
//rpc DecodeAuthToken (AuthTokenCoded) returns (AuthToken);
//rpc DecodeAccessToken (AccessTokenCoded) returns (AccessToken);
//}

type LoginPass struct {
	Login string `cli:"login"`
	Pass  string `cli:"password"`
}

func (t *Auth) CreateUpdatePassword(data *LoginPass) {
	if t.serviceConn == nil {
		fmt.Println("there is no connection")
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		_, err := t.serviceConn.CreateUpdatePassword(ctx, &proto.LoginPass{
			Login:    data.Login,
			Password: data.Pass,
		})
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("done")
	}
}

func (t *Auth) GetToken(data *LoginPass) {
	if t.serviceConn == nil {
		fmt.Println("there is no connection")
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		msg, err := t.serviceConn.GetToken(ctx, &proto.LoginPass{
			Login:    data.Login,
			Password: data.Pass,
		})
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("Auth (refresh) coded token [%s]\n", msg.Auth)
	}
}

type AuthTokenCoded struct {
	Token string `cli:"auth (refresh) token"`
}

func (t *Auth) RefreshToken(data *AuthTokenCoded) {
	if t.serviceConn == nil {
		fmt.Println("there is no connection")
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		msg, err := t.serviceConn.RefreshToken(ctx, &proto.AuthTokenCoded{
			Auth: data.Token,
		})
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("new   auth (refresh) coded token [%s]\n", msg.Auth)
		fmt.Printf("new access (refresh) coded token [%s]\n", msg.Access)
	}
}

type AccessTokenCoded struct {
	Token string `cli:"access token"`
}

func (t *Auth) CheckAccessToken(data *AuthTokenCoded) {
	if t.serviceConn == nil {
		fmt.Println("there is no connection")
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		_, err := t.serviceConn.CheckAccessToken(ctx, &proto.AccessTokenCoded{
			Access: data.Token,
		})
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("done. token valid")
	}
}

func (t *Auth) DecodeAuthToken(data *AuthTokenCoded) {
	if t.serviceConn == nil {
		fmt.Println("there is no connection")
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		msg, err := t.serviceConn.DecodeAuthToken(ctx, &proto.AuthTokenCoded{
			Auth: data.Token,
		})
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("done. token valid. time.Now = %s", time.Now())
		fmt.Printf("Login:\t%s\n", msg.Login)
		fmt.Printf("Role:\t%s\n", msg.Role.String())
		fmt.Printf("Owner:\t%d\n", msg.Owner)
		fmt.Printf("Generation:\t%d\n", msg.Generation)
		fmt.Printf("IssuedAt:\t%s\n", time.Unix(int64(msg.IssuedAt), 0))
		fmt.Printf("Expiration:\t%s\n", time.Unix(int64(msg.Expiration), 0))
	}
}

func (t *Auth) DecodeAccessToken(data *AuthTokenCoded) {
	if t.serviceConn == nil {
		fmt.Println("there is no connection")
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		msg, err := t.serviceConn.DecodeAccessToken(ctx, &proto.AccessTokenCoded{
			Access: data.Token,
		})
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("done. token valid. time.Now = %s", time.Now())
		fmt.Printf("Role:\t%s\n", msg.Role.String())
		fmt.Printf("Owner:\t%d\n", msg.Owner)
		fmt.Printf("IssuedAt:\t%s\n", time.Unix(int64(msg.IssuedAt), 0))
		fmt.Printf("Expiration:\t%s\n", time.Unix(int64(msg.Expiration), 0))
	}
}

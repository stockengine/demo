package cli

import (
	"container/ring"
	"context"
	"fmt"
	"io"
	"os"
	"reflect"
	"strconv"
	"text/tabwriter"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/proto"
	"google.golang.org/grpc"
)

type Dom struct {
	clientConn            *grpc.ClientConn
	serviceConn           proto.DOMClient
	methodSubscribeTop100 proto.DOM_SubscribeShortBookClient
	methodSubscribeDelta  proto.DOM_SubscribeDeltaClient

	lastFullDOM, lastTop100DOM *proto.DOMBook
	last100Delta               *ring.Ring
}

func NewDom() *Dom {
	return &Dom{
		last100Delta: ring.New(100),
	}
}

func (t *Dom) Description(method string) string {
	desc := map[string]string{
		"Connect": "connect to se.Dom via grpc and subscribe to Dom updates",
		"Show":    "show last Dom (received by subscribe)",
	}
	return desc[method]
}

func (t *Dom) Connect(addr *Connect) {
	//connect to order create cmd
	if t.clientConn == nil {
		fmt.Print("grpc.Dial...")
		clientConn, err := config.GRPCClientDial(addr.Addr, true)
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		fmt.Println("done")
		t.clientConn = clientConn
	}
	if t.serviceConn == nil {
		fmt.Print("serviceConn...")
		t.serviceConn = proto.NewDOMClient(t.clientConn)
		fmt.Println("done")
	}

	// ебучая копипаста :(
	if t.methodSubscribeTop100 == nil {
		fmt.Print("methodSubscribeTop100....")
		methodSubscribeTop100, err := t.serviceConn.SubscribeShortBook(context.Background(), &proto.EmptyMessage{})
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		t.methodSubscribeTop100 = methodSubscribeTop100
		//fmt.Printf("context %+v\n", t.methodSubscribeBook.Context())

		go func() {
			//так грязно т.к. этот код ничего полезного не делает и его не жалко
		L:
			for {
				dom, err := t.methodSubscribeTop100.Recv()
				switch err {
				case nil:
				//good
				case io.EOF:
					fmt.Println("methodSubscribeTop100.Recv() = EOF")
					break L
				default:
					fmt.Println("methodSubscribeTop100.Recv() err -> ", err)
					break L
				}
				t.lastTop100DOM = dom
			}
		}()

		fmt.Println("done")
	}

	if t.methodSubscribeDelta == nil {
		fmt.Print("methodSubscribeDelta....")
		methodSubscribeDelta, err := t.serviceConn.SubscribeDelta(context.Background(), &proto.EmptyMessage{})
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		t.methodSubscribeDelta = methodSubscribeDelta
		//fmt.Printf("context %+v\n", t.methodSubscribeBook.Context())

		go func() {
			//так грязно т.к. этот код ничего полезного не делает и его не жалко
		L:
			for {
				delta, err := t.methodSubscribeDelta.Recv()
				switch err {
				case nil:
				//good
				case io.EOF:
					fmt.Println("methodSubscribeDelta.Recv() = EOF")
					break L
				default:
					fmt.Println("methodSubscribeDelta.Recv() err -> ", err)
					break L
				}
				//угу, наплевал на синхронизацию потоков
				t.last100Delta.Value = delta
				t.last100Delta = t.last100Delta.Next()
			}
		}()

		fmt.Println("done")
	}
}

func printDomHelper(dom *proto.DOMBook) {
	if dom == nil {
		fmt.Println("no data")
	} else {
		var w = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.AlignRight)
		defer w.Flush()
		const format = "%s\t%s\t%s\t%s\t\n"
		helper := func(s string, side []proto.DOMLine) {
			_, _ = fmt.Fprintf(w, format, "side", "count", "total amount", "price")
			for _, elem := range side {
				cStr := strconv.FormatUint(elem.Count, 10)
				pStr := proto.PriceToString(elem.Price)
				aStr := proto.AmountToString(&elem.Amount)
				_, _ = fmt.Fprintf(w, format, s, cStr, aStr, pStr)
			}
		}
		helper("Ask", dom.Ask)
		helper("Bid", dom.Bid)
	}
}

func (t *Dom) FullDOM() {
	printDomHelper(t.lastFullDOM)
}

func (t *Dom) Top100DOM() {
	printDomHelper(t.lastTop100DOM)
}

func (t *Dom) Last100Delta() {
	var i int
	t.last100Delta.Do(func(iface interface{}) {
		cx, ok := iface.(*proto.DOMDelta)
		var data string
		switch {
		case iface == nil:
			//do nothing
			return
		case !ok:
			data = "invalid struct ->" + reflect.TypeOf(iface).String()
		default:
			data = fmt.Sprintf("%+v", cx)
		}
		i++
		fmt.Printf("delta[%d] -> %s\n", i, data)
	})
	fmt.Printf("total %d tx\n", i)
}

func (t *Dom) Disconnect() {
	if t.methodSubscribeDelta != nil {
		fmt.Print("methodSubscribeDelta close...")
		switch err := t.methodSubscribeDelta.CloseSend(); err {
		case nil, io.EOF:
			md := t.methodSubscribeDelta.Trailer()
			t.methodSubscribeDelta = nil
			fmt.Println("done. md -> ", md)
		default:
			fmt.Println("error -> ", err)
		}
	}
	if t.methodSubscribeTop100 != nil {
		fmt.Print("methodSubscribeTop100 close...")
		switch err := t.methodSubscribeTop100.CloseSend(); err {
		case nil, io.EOF:
			md := t.methodSubscribeTop100.Trailer()
			t.methodSubscribeTop100 = nil
			fmt.Println("done. md -> ", md)
		default:
			fmt.Println("error -> ", err)
		}
	}
	if t.clientConn != nil {
		fmt.Print("clientConn & serviceConn close...")
		if err := t.clientConn.Close(); err != nil {
			fmt.Println("error ->", err)
			return
		}
		t.clientConn = nil
		t.serviceConn = nil
		fmt.Println("done")
	}
}

func (t *Dom) Status() {
	if t.clientConn != nil {
		fmt.Println("clientConn -> ", t.clientConn.GetState().String())
	} else {
		fmt.Println("clientConn -> ", "nil")
	}

	fmt.Printf("serviceConn -> %v\n", t.serviceConn)
}

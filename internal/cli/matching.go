package cli

import (
	"context"
	"fmt"
	"io"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"google.golang.org/grpc"

	"gitlab.com/stockengine/demo/internal/proto"
)

type Matching struct {
	clientConn        *grpc.ClientConn
	serviceConn       proto.MatchingEngineClient
	methodProceedConn proto.MatchingEngine_ProceedClient
}

func NewMatching() *Matching {
	return &Matching{}
}

func (t *Matching) Description(method string) string {
	switch method {
	case "Connect":
		return "connect to se.Matching via grpc"
	case "Status":
		return "show tx list"
	case "Create":
		return "make order"
	default:
		return ""
	}
}

type Connect struct {
	Addr string
}

func (t *Matching) Connect(addr *Connect) {
	//connect to order create cmd
	if t.clientConn == nil {
		fmt.Print("grpc.Dial...")
		clientConn, err := config.GRPCClientDial(addr.Addr, true)
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		fmt.Println("done")
		t.clientConn = clientConn
	}
	if t.serviceConn == nil {
		fmt.Print("serviceConn...")
		t.serviceConn = proto.NewMatchingEngineClient(t.clientConn)
		fmt.Println("done")
	}

	if t.methodProceedConn == nil {
		fmt.Print("methodProceedOrderConn....")
		methodCreateOrderConn, err := t.serviceConn.Proceed(context.Background())
		if err != nil {
			fmt.Println("error -> ", err)
			return
		}
		t.methodProceedConn = methodCreateOrderConn
		fmt.Println("done")
	}
}

func (t *Matching) Disconnect() {
	if t.methodProceedConn != nil {
		fmt.Print("methodCreateORderConn close...")
		switch _, err := t.methodProceedConn.CloseAndRecv(); err {
		case nil, io.EOF:
			md := t.methodProceedConn.Trailer()
			t.methodProceedConn = nil
			fmt.Println("done. md -> ", md)
		default:
			fmt.Println("error -> ", err)
		}
	}
	if t.clientConn != nil {
		fmt.Print("clientConn & serviceConn close...")
		if err := t.clientConn.Close(); err != nil {
			fmt.Println("error ->", err)
			return
		}
		t.clientConn = nil
		t.serviceConn = nil
		fmt.Println("done")
	}
}

func (t *Matching) Status() {
	if t.clientConn != nil {
		fmt.Println("clientConn -> ", t.clientConn.GetState().String())
	} else {
		fmt.Println("clientConn -> ", "nil")
	}

	fmt.Printf("serviceConn -> %p\n", t.serviceConn)
	fmt.Printf("methodProceedConn -> %p\n", t.methodProceedConn)
}

type CreateParam struct {
	IPLow      uint32
	SrcDstPort uint32
	ConnectTS  uint32
	AcceptCnt  uint64

	Type      string `cli:"Limit"`
	Operation string `cli:"Buy, Sell"`
	Amount    string `cli:"eg: 123456 in minor units"`
	Price     string `cli:"eg: 123.456"`

	Owner uint64
	ExtID uint64
}

func (t *Matching) Create(params *CreateParam) {
	fmt.Print("CreateOrder...")
	if t.methodProceedConn != nil {
		command := &proto.Command{
			ReqID: proto.ReqID{
				IPLow:      params.IPLow,
				SrcDstPort: params.SrcDstPort,
				ConnectTS:  params.ConnectTS,
				AcceptCnt:  params.AcceptCnt,
			},
			SubCommands: proto.SubCommands{
				New: &proto.NewOrder{
					Type:      0,
					Operation: 0,
					Amount:    proto.Amount{},
					Price:     proto.Price{},
					OwnerID:   params.Owner,
					ExtID:     params.ExtID,
				}, Delete: nil,
				Edit: nil,
			},
		}

		//fill type
		if val, ok := proto.Type_value[params.Type]; ok {
			command.SubCommands.New.Type = proto.Type(val)
		} else {
			fmt.Printf("wrong Type[%s]. supported -> ", params.Type)
			for k := range proto.Type_value {
				fmt.Print(k)
			}
			fmt.Println()
			return
		}

		//fill operation
		if val, ok := proto.Operation_value[params.Operation]; ok {
			command.SubCommands.New.Operation = proto.Operation(val)
		} else {
			fmt.Printf("wrong Operation[%s]. supported -> ", params.Operation)
			for k := range proto.Operation_value {
				fmt.Print(k)
			}
			fmt.Println()
			return
		}

		//fill amount
		if err := proto.AmountSetFromString(&command.SubCommands.New.Amount, params.Amount); err != nil {
			fmt.Println("Amount error -> ", err)
			return
		}

		//fill price
		price, err := proto.NewPrice(params.Price)
		if err != nil {
			fmt.Println("Price error -> ", err)
			return
		}
		command.SubCommands.New.Price = price

		if err := t.methodProceedConn.Send(command); err != nil {
			fmt.Println("error -> ", err.Error())
		} else {
			fmt.Println("done")
		}
	} else {
		fmt.Println("fail - no connection. connect at first")
	}
}

type DeleteParam struct {
	IPLow      uint32
	SrcDstPort uint32
	ConnectTS  uint32
	AcceptCnt  uint64

	Owner uint64
	ExtID uint64
}

func (t *Matching) Delete(params *DeleteParam) {
	fmt.Print("DeleteOrder...")
	if t.methodProceedConn != nil {
		command := &proto.Command{
			ReqID: proto.ReqID{
				IPLow:      params.IPLow,
				SrcDstPort: params.SrcDstPort,
				ConnectTS:  params.ConnectTS,
				AcceptCnt:  params.AcceptCnt,
			},
			SubCommands: proto.SubCommands{
				Delete: &proto.DeleteOrder{
					OwnerID: params.Owner,
					ExtID:   params.ExtID,
				},
			},
		}

		if err := t.methodProceedConn.Send(command); err != nil {
			fmt.Println("error -> ", err.Error())
		} else {
			fmt.Println("done")
		}
	} else {
		fmt.Println("fail - no connection. connect at first")
	}
}

type EditParam struct {
	IPLow      uint32
	SrcDstPort uint32
	ConnectTS  uint32
	AcceptCnt  uint64

	Owner uint64
	ExtID uint64

	Amount string `cli:"eg: 123456 in minor units"`
	Price  string `cli:"eg: 123.456"`
}

func (t *Matching) Edit(params *EditParam) {
	fmt.Print("EditOrder...")
	if t.methodProceedConn != nil {
		command := &proto.Command{
			ReqID: proto.ReqID{
				IPLow:      params.IPLow,
				SrcDstPort: params.SrcDstPort,
				ConnectTS:  params.ConnectTS,
				AcceptCnt:  params.AcceptCnt,
			},
			SubCommands: proto.SubCommands{
				Edit: &proto.EditOrder{
					OwnerID:   params.Owner,
					ExtID:     params.ExtID,
					NewAmount: proto.Amount{},
					NewPrice:  proto.Price{},
				},
			}}

		//fill amount
		if err := proto.AmountSetFromString(&command.SubCommands.Edit.NewAmount, params.Amount); err != nil {
			fmt.Println("Amount error -> ", err)
			return
		}

		//fill price
		price, err := proto.NewPrice(params.Price)
		if err != nil {
			fmt.Println("Price error -> ", err)
			return
		}
		command.SubCommands.Edit.NewPrice = price

		if err := t.methodProceedConn.Send(command); err != nil {
			fmt.Println("error -> ", err.Error())
		} else {
			fmt.Println("done")
		}
	} else {
		fmt.Println("fail - no connection. connect at first")
	}
}

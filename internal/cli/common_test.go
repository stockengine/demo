package cli

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewFilter(t *testing.T) {
	tests := []struct {
		filter string
		value  string
		skip   bool
	}{
		{filter: "", value: "123", skip: false},
		{filter: "!", value: "123", skip: false},
		{filter: "123", value: "123", skip: false},
		{filter: "!123", value: "123", skip: true},
		{filter: "123", value: "234", skip: true},
		{filter: "!123", value: "234", skip: false},
		{filter: "123,234", value: "234", skip: false},
		{filter: "123,234", value: "123", skip: false},
		{filter: "123,234", value: "345", skip: true},
		{filter: "!123,234", value: "123", skip: true},
		{filter: "!123,234", value: "234", skip: true},
		{filter: "!123,234", value: "345", skip: false},
	}
	for i, test := range tests {
		filter := NewFilter(test.filter)
		if !assert.Equalf(t, test.skip, filter.Skip(test.value), "case(%d) -> %+v", i, test) {
			break
		}
	}
}

func TestLimitBuffer(t *testing.T) {
	tests := []struct {
		limit  int
		lines  []string
		result string
	}{
		{limit: 1, lines: []string{"123", "234"}, result: "234"},
		{limit: 0, lines: []string{"123", "234"}, result: "123234"},
		{limit: -1, lines: []string{"123", "234"}, result: "123"},
	}
	for i, test := range tests {
		lb := NewLimitBuffer(test.limit)
		for _, line := range test.lines {
			lb.ResetCurrent()
			_, _ = lb.Write([]byte(line))
			if !lb.Next() {
				break
			}
		}
		buf := &bytes.Buffer{}
		_, _ = lb.WriteTo(buf)

		if !assert.Equalf(t, test.result, buf.String(), "case(%d) -> %+v", i, test) {
			break
		}
	}
}

func TestAmountWithSpaces(t *testing.T) {
	tests := []struct {
		a, e string
	}{
		{a: "0", e: "0"},
		{a: "01", e: "01"},
		{a: "012", e: "012"},
		{a: "0123", e: "0 123"},
		{a: "0123456", e: "0 123 456"},
	}

	for i, test := range tests {
		if !assert.Equalf(t, test.e, AmountWithSpaces(test.a), "case(%d) -> %+v", i, test) {
			break
		}
	}
}

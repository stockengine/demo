package storage

import (
	"io"
	"net"
	"reflect"
	"sync"
	"sync/atomic"
	"time"

	prometheus3 "github.com/prometheus/client_golang/prometheus"

	prometheus2 "gitlab.com/stockengine/demo/internal/lib/prometheus"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/proto"
	"google.golang.org/grpc"
)

func NewEngine(opts ...Option) (ret *Engine, err error) {
	se := &Engine{
		quit: make(chan struct{}),
	}
	for _, opt := range opts {
		if err := opt.apply(se); err != nil {
			return nil, err
		}
	}
	se.prom, err = prometheus2.New(se.prometheus)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil && se.prom != nil {
			_ = se.prom.Close()
		}
	}()
	se.promLoadTX = se.prom.NewCounter("loadTx", "loadTx counter")
	se.promStoreTx = se.prom.NewCounter("storeTx", "storeTx counter")
	se.promSendTx = se.prom.NewCounter("sendTx", "sendTx counter")

	logger.Log.Info("loading blockchain", zap.String("file", se.file))
	se.storage, err = NewStorage(se.file, false, true, se.quit)
	if err != nil {
		return
	}
	logger.Log.Info("Storage connected", zap.String("file", se.file))
	defer func() {
		if err != nil {
			logger.Log.Info("there is error, close Storage", zap.String("file", se.file))
			if err2 := se.storage.Close(); err2 != nil {
				logger.Log.Error("there is error, close Storage, but error again", zap.String("file", se.file), zap.Error(err2))
			}
		}
	}()
	se.storage.SetForwardHandler(se.forwarderHadler)

	se.lis, se.srv, err = config.CreateGRPCListen(se.bind)
	if err != nil {
		return nil, err
	}

	proto.RegisterStorageEngineServer(se.srv, se)
	go func() {
		err := se.srv.Serve(se.lis)
		logger.ErrorLogHelper(err, "Serve")
	}()
	logger.Log.Info("server ready", zap.String("addr", se.lis.Addr().String()))
	return se, nil
}

type Engine struct {
	bind string
	file string

	lis net.Listener
	srv *grpc.Server

	storage *Storage

	storeTxBusy uint32
	wg          sync.WaitGroup // не нашёл способа корректно отключить всех подключенных
	quit        chan struct{}
	forwarder   OneToManyForwarder

	prometheus  string
	prom        *prometheus2.Prometheus
	promLoadTX  prometheus3.Counter
	promStoreTx prometheus3.Counter
	promSendTx  prometheus3.Counter
}

func (t *Engine) forwarderHadler(msg *proto.CommitedTransaction) {
	t.forwarder.Forward(msg)
}

func (t *Engine) Stop() {
	logger.Log.Info("going shutdown")
	select {
	case _, ok := <-t.quit:
		if !ok {
			logger.Log.Warn("already shutdown")
			return
		}
	default:
	}
	close(t.quit) //просим завершиться всех grpc
	t.srv.Stop()  // останавливаем приём новых данных и ждём пока все обработчики отработают
	t.wg.Wait()
	if err := t.storage.Close(); err != nil {
		logger.Log.Error("cant close Storage", zap.Error(err))
	}
	logger.ErrorLogHelper(t.prom.Close(), "prom.Close")
	logger.Log.Info("Engine stopped")
}

func (t *Engine) LoadTx(em *proto.EmptyMessage, client proto.StorageEngine_LoadTxServer) error {
	log := logger.LogNewConnect(client.Context(), "LoadTx")
	start := time.Now()
	var txCount int
	it, err := t.storage.Iterator()
	if err != nil {
		log.Info("LoadTx cant create Iterator", zap.Error(err))
		return err
	}
endlessLoop:
	for {
		switch err := it.Next(); err {
		case io.EOF:
			//good
			break endlessLoop
		case nil:
			tx := it.GetTx()
			if err := client.Send(&tx); err != nil {
				log.Error("LoadTxServer.Send", zap.String("type", reflect.TypeOf(err).String()), zap.Error(err))
				return err
			}
			txCount++
			t.promLoadTX.Inc()
		default:
			log.Error("LoadTx it.Next", zap.String("type", reflect.TypeOf(err).String()), zap.Error(err))
			break endlessLoop
		}
	}
	log.Info("LoadTx done", zap.Int("txCount", txCount), zap.Duration("elapsed", time.Since(start)))
	return nil
}

func (t *Engine) StoreTx(client proto.StorageEngine_StoreTxServer) error {
	t.wg.Add(1)
	defer t.wg.Done()
	log := logger.LogNewConnect(client.Context(), "StoreTx")
	defer log.Info("StoreTx closed")
	if isFree := atomic.CompareAndSwapUint32(&t.storeTxBusy, 0, 1); !isFree {
		//занято :(
		if err := client.SendAndClose(&proto.EmptyMessage{}); err != nil {
			log.Error("try to close excess", zap.Error(err))
		} else {
			log.Warn("StoreTx request detected, but its already serve other Tx")
		}
		return ErrorTooManyStoreTx{}
	}

	log.Info("new StoreTx initiated")
	defer func() {
		isFree := atomic.CompareAndSwapUint32(&t.storeTxBusy, 1, 0)
		if !isFree {
			log.Error("StoreTx is done, but cant free", zap.Bool("isFree", isFree))
		} else {
			log.Info("StoreTx is done, ready for new StoreTx")
		}
	}()

endlessLoop:
	for {
		tx, err := client.Recv() // takes time to unmarshall
		if logger.ErrorLogHelper3(err, "client.Recv()", log) {
			break endlessLoop
		}
		t.storage.StoreTx(tx)
		t.promStoreTx.Inc()
		//ToDo: detect StoreTx error and graceful shutdown
	}
	// we never close client. it will do server.Stop :) (btw, context will be closed is case of StoreTx returns
	return nil
}

func (t *Engine) SubscribeTx(em *proto.EmptyMessage, sts proto.StorageEngine_SubscribeTxServer) error {
	t.wg.Add(1)
	defer t.wg.Done()
	chCtx := t.forwarder.AddSubscriber()
	log := logger.LogNewConnect(sts.Context(), "SubscribeTx", zap.Uint64("channel", chCtx.id))
	defer func() {
		log.Info("close SubscribeTx", zap.Uint64("channel", chCtx.id))
		t.forwarder.RemoveSubscriber(chCtx)
	}()
endlessLoop:
	for {
		select {
		case msg := <-chCtx.GetChan():
			if msgBuf, ok := msg.(*proto.CommitedTransaction); ok {
				if err := sts.Send(msgBuf); err != nil {
					log.Warn("client.Send", zap.String("type", reflect.TypeOf(err).String()), zap.Error(err))
					break endlessLoop
				}
				t.promSendTx.Inc()
			} else {
				log.Error("invalid msg format", zap.String("type", reflect.TypeOf(msg).String()))
			}
		case <-t.quit: // толку мало т.к. для StoreTx вместо GracefulStop пришлось использовать Stop, а стоп уже сам отрубит все коннекты не дожидаясь завершения обработки
			break endlessLoop
		}
	}
	return nil
}

package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRouter(t *testing.T) {
	r := OneToManyForwarder{}
	for i := 0; i < 10*routerChanSize; i++ {
		r.Forward("test")
	}
	//shouldn block
	s := r.AddSubscriber()
	assert.Equal(t, 0, len(s.outChan))
	assert.Equal(t, 0, len(s.GetChan()))

	r.Forward("test")
	assert.Equal(t, 1, len(s.outChan))
	assert.Equal(t, 1, len(s.GetChan()))

	s2 := r.AddSubscriber()
	assert.Equal(t, 0, len(s2.outChan))
	assert.Equal(t, 0, len(s2.GetChan()))
	assert.Equal(t, 1, len(s.outChan))
	assert.Equal(t, 1, len(s.GetChan()))

	r.RemoveSubscriber(s)
	assert.Equal(t, 0, len(s2.outChan))
	assert.Equal(t, 0, len(s2.GetChan()))

	for i := 0; i < 10*routerChanSize; i++ {
		r.Forward("test")
	}
	assert.Equal(t, routerChanSize, len(s2.outChan))
	assert.Equal(t, routerChanSize, len(s2.GetChan()))
	assert.Equal(t, 1, len(s.outChan))
	assert.Equal(t, 1, len(s.GetChan()))

	r.RemoveSubscriber(s) //ignore second remove
	r.RemoveSubscriber(s2)

	for i := 0; i < 10*routerChanSize; i++ {
		r.Forward("test")
	}
	s3 := r.AddSubscriber()
	assert.Equal(t, 0, len(s3.outChan))
	assert.Equal(t, 0, len(s3.GetChan()))
}

package storage

import (
	"testing"
)

//array loose :(
// array 400ns vs map 20ns
func BenchmarkRouterAddRemove(b *testing.B) {
	r := OneToManyForwarder{}
	_ = r.AddSubscriber()
	//m := make(map[int]int)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s := r.AddSubscriber()
		r.RemoveSubscriber(s)
		//m[i] = i
		//delete(m, i)
	}
}

//array wins
// array 324ns vs map 600ns (10 subscribers)
// array  65ns vs map 143ns (2 subscribers)
func BenchmarkRouterIterate(b *testing.B) {
	r := OneToManyForwarder{}

	//m := make(map[int]chan interface{})
	for i := 10; i > 0; i-- {
		_ = r.AddSubscriber()
		//m[i] = make(chan interface{}, routerChanSize)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		r.Forward("test")
		//for _, v := range m {
		//	select {
		//	case v <- i:
		//	default:
		//		if log != nil {
		//		}
		//	}
		//}
	}
}

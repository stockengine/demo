// +build heavy

package storage

import (
	"fmt"
	"io/ioutil"
	"os"
	"sync"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/stockengine/demo/internal/proto"
	"google.golang.org/grpc"
)

/*
	итого:
		MarshallTo съедает 1030мс в автотесте
		storeTxBytes съедает 820мс
			hash.Write занимает 500мс
			 Buf.Write занимает 170мс
*/
func BenchmarkStorageEngine_StoreTx(b *testing.B) {
	//b.Logf("gomaxproc -> %d", runtime.GOMAXPROCS(0))
	f, err := ioutil.TempFile("", "storage_test_*.bin")
	if !assert.NoError(b, err) {
		return
	}
	assert.NoError(b, f.Close())

	se, err := NewEngine(File(f.Name()), Bind(""))
	assert.NoError(b, err)
	assert.NotNil(b, se)

	rSe, ok := se.(*Engine)
	assert.True(b, ok)
	assert.NotNil(b, rSe)

	hbc := &benchHelperClass{
		cTx: make(chan *proto.Transaction, 100),
	}

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		assert.NoError(b, rSe.StoreTx(hbc))
		wg.Done()
	}()

	b.ResetTimer()
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		tx := helperGenTx()
		hbc.cTx <- &tx
	}
	close(hbc.cTx)
	wg.Wait() // we dont create real grpc connection, so Stop will not wait till processing complete

	se.Stop()
	b.StopTimer()
	assert.NoError(b, os.Remove(f.Name()))
}

type benchHelperClass struct {
	cTx       chan *proto.Transaction
	processed int64
	grpc.ServerStream
}

func (h *benchHelperClass) SendAndClose(*proto.EmptyMessage) error {
	return nil
}

func (h *benchHelperClass) Recv() (*proto.Transaction, error) {
	retTx, ok := <-h.cTx
	if ok {
		atomic.AddInt64(&h.processed, 1)
		return retTx, nil
	}
	return nil, fmt.Errorf("connection closed")
}

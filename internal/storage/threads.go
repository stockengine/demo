package storage

import (
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/proto"
)

func (t *Storage) marshaller() {
	//runtime.LockOSThread()
	//defer runtime.UnlockOSThread()

	var err error
	for msg := range t.toMarshall {
		msg.buf, err = msg.tx.Marshal()
		if err != nil {
			logger.Log.Error("msg.Marshal", zap.Error(err))
			continue
		}
		t.toWrite <- msg
	}
	close(t.toWrite)
	t.wg.Done()
}

func (t *Storage) writer() {
	//runtime.LockOSThread()
	//defer runtime.UnlockOSThread()

	var err error
	for msg := range t.toWrite {
		msg.cID, err = t.storeTxBytes(msg.buf)
		if err != nil {
			logger.Log.Error("storeTxBytes", zap.Error(err))
			continue
		}
		t.toForward <- msg
	}
	close(t.toForward)
	t.wg.Done()
}

func (t *Storage) forwarder() {
	//runtime.LockOSThread()
	//defer runtime.UnlockOSThread()

	for msg := range t.toForward {
		ctx := &proto.CommitedTransaction{
			CommitID:    msg.cID,
			Transaction: *msg.tx,
		}
		if t.forwardHandler != nil {
			t.forwardHandler(ctx)
		}
	}
	t.wg.Done()
}

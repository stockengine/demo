// build +howtocode
package storage

import (
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCompareAndSwap(t *testing.T) {
	flag := int32(0)

	assert.True(t, atomic.CompareAndSwapInt32(&flag, 0, 1))
	assert.Equal(t, flag, int32(1))

	assert.False(t, atomic.CompareAndSwapInt32(&flag, 0, 1))
	assert.Equal(t, flag, int32(1))

	assert.True(t, atomic.CompareAndSwapInt32(&flag, 1, 0))
	assert.Equal(t, flag, int32(0))
}

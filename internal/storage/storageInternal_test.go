// +build howtocode

package storage

import (
	"sync/atomic"
	"testing"

	orderlib "gitlab.com/stockengine/demo/internal/matching"
)

var lastCtx uint64
var ret orderlib.CommitedTransaction

func helperStructPredefined(tx orderlib.Transaction) (ctx orderlib.CommitedTransaction, err error) {
	ctx.Transaction = tx
	ctx.CommitID.CommitID = atomic.AddUint64(&lastCtx, 1)
	return
}

func helperStructPredefined2(tx orderlib.Transaction) (ctx orderlib.CommitedTransaction, err error) {
	ctx = orderlib.CommitedTransaction{
		CommitID:    orderlib.CommitID{CommitID: atomic.AddUint64(&lastCtx, 1)},
		Transaction: tx,
	}
	return
}

func helperStructNoPredefined(tx orderlib.Transaction) (orderlib.CommitedTransaction, error) {
	return orderlib.CommitedTransaction{
		CommitID:    orderlib.CommitID{CommitID: atomic.AddUint64(&lastCtx, 1)},
		Transaction: tx,
	}, nil
}

func BenchmarkStructNoFill(b *testing.B) {
	tx := orderlib.Transaction{
		ReqID: orderlib.ReqID{
			IPLow:      1234,
			SrcDstPort: 345678,
			ConnectTs:  987654321,
			AcceptCnt:  431,
		},
		TxList: []orderlib.TxDeal{
			{},
			{},
			{},
			{},
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ret, _ = helperStructNoPredefined(tx)
	}
}

func BenchmarkStructFill(b *testing.B) {
	tx := orderlib.Transaction{
		ReqID: orderlib.ReqID{
			IPLow:      1234,
			SrcDstPort: 345678,
			ConnectTs:  987654321,
			AcceptCnt:  431,
		},
		TxList: []orderlib.TxDeal{
			{},
			{},
			{},
			{},
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ret, _ = helperStructPredefined(tx)
	}
}

func BenchmarkStructFill2(b *testing.B) {
	tx := orderlib.Transaction{
		ReqID: orderlib.ReqID{
			IPLow:      1234,
			SrcDstPort: 345678,
			ConnectTs:  987654321,
			AcceptCnt:  431,
		},
		TxList: []orderlib.TxDeal{
			{},
			{},
			{},
			{},
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ret, _ = helperStructPredefined2(tx)
	}
}

// +build howtocode

package storage

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func helperBenchWrite(b *testing.B, sync bool) {
	f, err := ioutil.TempFile("./", "bench*.bin")
	assert.NoError(b, err)
	//assert.NoError(b, f.Stop())
	defer func() {
		assert.NoError(b, os.Remove(f.Name()))
	}()
	st, err := NewStorage(f.Name(), sync, true)
	assert.NoError(b, err)
	if err != nil {
		return
	}
	defer func() {
		assert.NoError(b, st.Close())
	}()
	tx := helperGenTx()
	buf, err := tx.Marshal()
	assert.NoError(b, err)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = st.storeTxBytes(buf)
		assert.NoError(b, err)
	}
}

func BenchmarkWriteSync(b *testing.B) {
	helperBenchWrite(b, true)
}
func BenchmarkWriteNoSync(b *testing.B) {
	helperBenchWrite(b, false)
}

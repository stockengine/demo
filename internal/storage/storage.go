package storage

import (
	"bufio"
	"bytes"
	"crypto/sha512"
	"fmt"
	"hash"
	"io"
	"os"
	"reflect"
	"sync"
	"sync/atomic"
	"time"
	"unsafe"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/proto"
)

/*
структура файла:
[ 8) uint64 version and order metainfo
[ 32 )   prevSnapshotHash
[ 32 )         headerHash
repeated:
[ 8 ) uint64 commitID
[ 8 ) uint64 len of data
[ len(data) ) data (committed tx)
[ 32 ) prevBlockHash or header
[ 32 ) thisBlockHash [0:64+len(data)]

*/

type Storage struct {
	file *os.File
	wBuf *bufio.Writer
	rBuf *bufio.Reader

	lastCtx       uint64
	lastBlockHash []byte

	wg sync.WaitGroup

	toMarshall chan forwarderStruct
	toWrite    chan forwarderStruct
	toForward  chan forwarderStruct

	forwardHandler forwardHandler
}

type forwarderStruct struct {
	tx  *proto.Transaction
	buf []byte
	cID uint64
}

func NewStorage(fName string, sync bool, doRecover bool, quit chan struct{}) (*Storage, error) {
	fInfo, errFinfo := os.Stat(fName)
	switch {
	case os.IsNotExist(errFinfo):
		//create new file
		errFinfo = ErrorNoFile{}
	case errFinfo != nil:
		return nil, errFinfo
	case fInfo.Size() == 0:
		//empty file, fill it
		errFinfo = ErrorEmptyFile{}
	default:
		//normal file
	}

	fFlags := os.O_RDWR | os.O_CREATE | os.O_APPEND
	if sync {
		fFlags |= os.O_SYNC
	}
	f, err := os.OpenFile(fName, fFlags, 0600|os.ModeExclusive)
	if err != nil {
		return nil, err
	}

	bufW := bufio.NewWriterSize(f, 1024*1024)
	bufR := bufio.NewReaderSize(f, 1024*1024)
	st := &Storage{
		file: f,
		wBuf: bufW,
		rBuf: bufR,

		lastBlockHash: make([]byte, prepareHash().Size()),

		toMarshall: make(chan forwarderStruct, chanSize),
		toWrite:    make(chan forwarderStruct, chanSize),
		toForward:  make(chan forwarderStruct, chanSize),
	}

	switch errFinfo.(type) {
	case nil:
		//good, do content check
		//initial load - read file, check content
		it, err := st.Iterator()
		if err != nil {
			return nil, err
		}
		defer func() {
			if err := it.Close(); err != nil {
				logger.Log.Error("cant close it", zap.Error(err))
			}
		}()
	loop:
		for {
			select {
			case <-quit:
				return nil, ErrorStorageClosed{}
			default:
				//good
			}
			switch err := it.Next(); err {
			case io.EOF:
				break loop
			case nil:
			//good
			case ErrorBadHeader{}, ErrorHackedBlock{}, io.ErrUnexpectedEOF:
				if doRecover {
					logger.Log.Warn("corruption detected. will recover, truncate bad data, make a copy of corrupted file")
					if err := st.tryRecover(it); err != nil {
						logger.Log.Error("cant recover", zap.Error(err))
						return nil, err
					}
				} else {
					return nil, err
				}
			default:
				if doRecover {
					logger.Log.Warn("cant recover", zap.String("type", reflect.TypeOf(err).String()), zap.Error(err))
				}
				return nil, err
			}
		}
		st.lastBlockHash = it.storage.lastBlockHash
		st.lastCtx = it.GetTx().CommitID

	case ErrorNoFile, ErrorEmptyFile:
		//fill file, no content checks
		err = st.createHeader()
		if err != nil {
			return nil, err
		}
	default:
		//OMFG....
		return nil, err
	}

	st.wg.Add(3)
	go st.marshaller()
	go st.writer()
	go st.forwarder()

	return st, nil
}

func (t *Storage) Iterator() (*Iterator, error) {
	fName := t.file.Name()
	if err := t.wBuf.Flush(); err != nil {
		return nil, err
	}
	//t.file.Sync()

	f, err := os.OpenFile(fName, os.O_RDONLY, 0600|os.ModeExclusive)
	if err != nil {
		return nil, err
	}

	bufR := bufio.NewReaderSize(f, 1024*1024)
	it := &Iterator{
		lastGoodOffset: 8 + 32 + 32, // очень грязный код :(
		storage: &Storage{
			file: f,
			wBuf: nil,
			rBuf: bufR,

			lastBlockHash: make([]byte, prepareHash().Size()),
		},
	}
	return it, it.storage.readAndValidateHeader()
}

type forwardHandler func(marshalledCTX *proto.CommitedTransaction)

func (t *Storage) SetForwardHandler(fh forwardHandler) {
	t.forwardHandler = fh
}

func (t *Storage) createHeader() (err error) {
	hsh := prepareHash()

	// version
	if err = t.storeBytesAndHashUint64(uint64(FirstVersion), hsh); err != nil {
		return
	}

	// prevSnapshot
	if err = t.storeBytesAndHash(t.lastBlockHash, hsh); err != nil {
		return
	}

	// this header hsh
	t.lastBlockHash = hsh.Sum(t.lastBlockHash[:0])
	if err = t.storeBytesAndHash(t.lastBlockHash, hsh); err != nil {
		return
	}
	return
}

func (t *Storage) readAndValidateHeader() error {
	hsh := prepareHash()
	hashPrevSnapshotBuf := make([]byte, hsh.Size())

	version, err := t.loadBytesAndHashUint64(hsh)
	switch {
	case err == io.EOF:
		return ErrorEmptyFile{}
	case err != nil:
		return err
	case Version(version) != FirstVersion:
		return ErrorBadHeader{}
		//case err == nil: //good
		//default:
		//	panic("OMFG!!!1111 its impossible")
	}
	if err = t.loadBytesAndHash(hashPrevSnapshotBuf, hsh); err != nil {
		return err
	}
	if err = t.loadBytesAndHash(t.lastBlockHash, nil); err != nil {
		return err
	}

	calcHashBuf := hsh.Sum(nil)

	if !bytes.Equal(calcHashBuf, t.lastBlockHash) {
		return ErrorHackedBlock{}
	}

	//move write pointer to prevent header overwrite
	//_, err = t.file.Seek(2, 0)
	return err
}

func (t *Storage) Close() (err error) {
	//wait till goroutines done
	close(t.toMarshall)
	t.wg.Wait()

	if err = t.wBuf.Flush(); err != nil {
		return
	}
	t.wBuf = nil
	if err = t.file.Sync(); err != nil {
		return
	}
	if err = t.file.Close(); err != nil {
		return
	}
	return
}

func prepareHash() hash.Hash {
	return sha512.New512_256()
}

func (t *Storage) StoreTx(tx *proto.Transaction) {
	t.toMarshall <- forwarderStruct{
		tx: tx,
	}
}

func (t *Storage) storeTxBytes(txMarshalled []byte) (commitID uint64, err error) {
	if uint64(len(txMarshalled)) > maxDataLen {
		err = ErrorTooBigDataLen{dataLen: uint64(len(txMarshalled))}
		return
	}

	commitID = atomic.AddUint64(&t.lastCtx, 1)

	hsh := prepareHash()

	//write commitid
	if err = t.storeBytesAndHashUint64(commitID, hsh); err != nil {
		return
	}

	//write data len
	if err = t.storeBytesAndHashUint64(uint64(len(txMarshalled)), hsh); err != nil {
		return
	}

	//write body
	if err = t.storeBytesAndHash(txMarshalled, hsh); err != nil {
		return
	}

	//write prevBlockHash
	if err = t.storeBytesAndHash(t.lastBlockHash, hsh); err != nil {
		return
	}

	//write thisBlockHash
	t.lastBlockHash = hsh.Sum(t.lastBlockHash[:0])
	if err = t.storeBytesAndHash(t.lastBlockHash, hsh); err != nil {
		return
	}

	return
}

func (t *Storage) storeBytesAndHash(buf []byte, hash hash.Hash) (err error) {
	if t.wBuf == nil {
		return ErrorStorageClosed{}
	}
	if hash != nil {
		_, _ = hash.Write(buf) // it never returns error
	}

	var n int
	n, err = t.wBuf.Write(buf)
	if err != nil {
		return
	}
	if n != len(buf) {
		err = fmt.Errorf("write n[%d]!=size[%d]", n, len(buf))
		return
	}
	return
}

func (t *Storage) storeBytesAndHashUint64(data uint64, hash hash.Hash) (err error) {
	bufSlice := &reflect.SliceHeader{
		Data: uintptr(unsafe.Pointer(&data)),
		Len:  8,
		Cap:  8,
	}
	buf := *(*[]byte)(unsafe.Pointer(bufSlice))
	return t.storeBytesAndHash(buf, hash)
}

func (t *Storage) loadBytesAndHash(buf []byte, hash hash.Hash) (err error) {
	var n int
	n, err = io.ReadFull(t.rBuf, buf)
	if err != nil {
		return
	}
	if n != len(buf) {
		err = fmt.Errorf("read n[%d]!=size[%d]", n, len(buf))
		return
	}
	if hash != nil {
		_, _ = hash.Write(buf) // it never returns error
	}
	return
}

func (t *Storage) loadBytesAndHashUint64(hash hash.Hash) (data uint64, err error) {
	bufSlice := &reflect.SliceHeader{
		Data: uintptr(unsafe.Pointer(&data)),
		Len:  8,
		Cap:  8,
	}
	buf := *(*[]byte)(unsafe.Pointer(bufSlice))
	return data, t.loadBytesAndHash(buf, hash)
}

func (t *Storage) tryRecover(it *Iterator) error {
	logger.Log.Info("starting recovery", zap.String("file name", t.file.Name()), zap.Int64("lastGoodOffset", it.lastGoodOffset))
	start := time.Now()
	copyFname := fmt.Sprintf("%s_%0d.bad", t.file.Name(), start.Unix())
	copyFile, err := os.OpenFile(copyFname, os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	copied, err := io.Copy(copyFile, t.file)
	if err != nil {
		return err
	}
	logger.Log.Info("original (corrupted) file copy created", zap.String("copy name", copyFname), zap.Int64("copied bytes", copied), zap.Duration("elapsed", time.Since(start)))
	if err := t.file.Truncate(it.lastGoodOffset); err != nil {
		return err
	}
	if err := t.file.Sync(); err != nil {
		return err
	}
	//t.checkFile()
	logger.Log.Info("recover complete", zap.Duration("elapsed", time.Since(start)))
	return nil
}

package storage

type Option interface {
	apply(*Engine) error
}

func Bind(bind string) Option {
	return optionBind{
		bind: bind,
	}
}

type optionBind struct {
	bind string
}

func (t optionBind) apply(s *Engine) error {
	s.bind = t.bind
	return nil
}

func File(file string) Option {
	return optionFile{
		file: file,
	}
}

type optionFile struct {
	file string
}

func (t optionFile) apply(s *Engine) error {
	s.file = t.file
	return nil
}

func Prometheus(bind string) Option {
	return optionPrometheus{
		prometheus: bind,
	}
}

type optionPrometheus struct {
	prometheus string
}

func (t optionPrometheus) apply(s *Engine) error {
	s.prometheus = t.prometheus
	return nil
}

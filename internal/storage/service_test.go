package storage

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"
)

func TestNewStorageEngine(t *testing.T) {
	// несуществующий файл
	se, err := NewEngine(File("/asd/asd"), Bind("127.0.0.4:666"), Prometheus("127.0.0.10:/metrics"))
	assert.Error(t, err)
	assert.Nil(t, se)

	f, err := ioutil.TempFile("", "storage_test_*.bin")
	require.NoError(t, err)
	require.NoError(t, f.Close())

	// invalid address
	se, err = NewEngine(File(f.Name()), Bind("123.0.0.4:666"), Prometheus("127.0.0.10:/metrics"))
	require.Error(t, err)
	require.Nil(t, se)

	se, err = NewEngine(File(f.Name()), Bind("127.0.0.4:6666"), Prometheus("127.0.0.10:/metrics"))
	require.NoError(t, err)
	require.NotNil(t, se)

	assert.NoError(t, se.prom.Close())

	se2, err := NewEngine(File(f.Name()), Bind("127.0.0.4:6666"), Prometheus("127.0.0.10:/metrics"))
	require.Error(t, err)
	require.Nil(t, se2)

	se.Stop()
	assert.NoError(t, os.Remove(f.Name()))

}

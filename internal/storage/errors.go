package storage

import "fmt"

type ErrorEmptyFile struct{}

func (ErrorEmptyFile) Error() string { return "ErrorEmptyFile" }

type ErrorNoFile struct{}

func (ErrorNoFile) Error() string { return "ErrorNoFile" }

type ErrorBadHeader struct{}

func (ErrorBadHeader) Error() string { return "ErrorBadHeader" }

type ErrorTooBigDataLen struct{ dataLen uint64 }

func (t ErrorTooBigDataLen) Error() string {
	return fmt.Sprintf("ErrorTooBigDataLen len=%d its too big", t.dataLen)
}

type ErrorBadBlockchain struct{}

func (ErrorBadBlockchain) Error() string { return "ErrorBadBlockchain" }

type ErrorHackedBlock struct{}

func (ErrorHackedBlock) Error() string { return "ErrorHackedBlock" }

type ErrorStorageClosed struct{}

func (ErrorStorageClosed) Error() string { return "ErrorStorageClosed" }

type ErrorTooManyStoreTx struct{}

func (ErrorTooManyStoreTx) Error() string { return "ErrorTooManyStoreTx" }

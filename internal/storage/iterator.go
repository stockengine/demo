package storage

import (
	"bytes"

	"gitlab.com/stockengine/demo/internal/proto"
)

type Iterator struct {
	storage        *Storage
	lastGoodOffset int64

	data proto.CommitedTransaction
}

func (t *Iterator) Close() error {
	return t.storage.file.Close()
}

func (t *Iterator) GetTx() proto.CommitedTransaction {
	return t.data
}

func (t *Iterator) Next() (err error) {
	hash := prepareHash()
	prevBlockHash := make([]byte, hash.Size())

	var commitID, dataLen uint64

	//read commitid
	if commitID, err = t.storage.loadBytesAndHashUint64(hash); err != nil {
		return
	}

	//read data len
	if dataLen, err = t.storage.loadBytesAndHashUint64(hash); err != nil {
		return
	}

	if dataLen > maxDataLen {
		return ErrorTooBigDataLen{dataLen}
	}

	txBytes := make([]byte, dataLen)

	//load body
	if err = t.storage.loadBytesAndHash(txBytes, hash); err != nil {
		return
	}

	//load prevBlockHash
	if err = t.storage.loadBytesAndHash(prevBlockHash, hash); err != nil {
		return
	}

	if !bytes.Equal(prevBlockHash, t.storage.lastBlockHash) { //brocken blockchain
		return ErrorBadBlockchain{}
	}

	//read thisBlockHash
	thisBlockHash := make([]byte, 0, hash.Size())
	thisBlockHash = hash.Sum(thisBlockHash)

	loadedBlockHash := make([]byte, hash.Size())
	if err = t.storage.loadBytesAndHash(loadedBlockHash, nil); err != nil {
		return
	}
	if !bytes.Equal(thisBlockHash, loadedBlockHash) { //hacked block
		return ErrorHackedBlock{}
	}

	//try to decode
	t.data.Transaction.Reset()
	if err = t.data.Transaction.Unmarshal(txBytes); err != nil {
		return err
	}

	t.data.CommitID = commitID
	t.storage.lastBlockHash = thisBlockHash

	t.lastGoodOffset += 8 + 8 + int64(dataLen) + int64(hash.Size()+hash.Size())

	return
}

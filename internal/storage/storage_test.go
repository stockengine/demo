package storage

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/stockengine/demo/internal/proto"

	"github.com/stretchr/testify/assert"
)

/*
on server (mirror MP866
BenchmarkWriteSync-24      	  200000	      8624 ns/op
BenchmarkWriteNoSync-24    	  200000	      5995 ns/op

on notebook (nvme)
BenchmarkWriteSync-8              200000              9705 ns/op
BenchmarkWriteNoSync-8            500000              3079 ns/op


*/
func TestStoreLoad(t *testing.T) {
	//case no exists file
	f, err := ioutil.TempFile("./", "store_load_test_*.bin")
	assert.NoError(t, err)
	assert.NoError(t, f.Close())
	assert.NoError(t, os.Remove(f.Name()))
	defer func() { //remove file created during test
		assert.NoError(t, os.Remove(f.Name()))
	}()

	headerSize := int64(8 + 2*prepareHash().Size())
	blockSize := int64(8 + 8 + len(helperGenTxBytes(helperGenTx())) + 2*prepareHash().Size())

	//ожидаем что файл будет создан с коннектным заголовком
	{
		st, err := NewStorage(f.Name(), false, true, make(chan struct{}))
		assert.NoError(t, err)

		it, err := st.Iterator()
		assert.NoError(t, err)
		assert.Equal(t, it.Next(), io.EOF)
		assert.NoError(t, it.Close())
		assert.NoError(t, st.Close())

		fi, err := os.Stat(f.Name())
		assert.NoError(t, err)
		assert.Equal(t, int64(8+2*prepareHash().Size()), fi.Size())

	}

	//ожидаем что файл из одного заголовка успешно прочитается
	{
		st, err := NewStorage(f.Name(), false, true, make(chan struct{}))
		assert.NoError(t, err)

		// проверка итератора по пустому файлу
		it, err := st.Iterator()
		assert.NoError(t, err)
		assert.Equal(t, it.Next(), io.EOF)
		assert.NoError(t, it.Close())

		tx := helperGenTxBytes(helperGenTx())
		_, err = st.storeTxBytes(tx)
		assert.NoError(t, err)

		//итератор покажет наличие транзакций и они совпадут с тестовыми
		it2, err := st.Iterator()
		assert.NoError(t, err)
	forLabel:
		for i := 0; ; i++ {
			switch err = it2.Next(); err {
			case io.EOF:
				//good
				break forLabel
			case nil:
				//also good
			default: //bad :(
				assert.NoError(t, err)
			}
			assert.Equal(t, helperGenTx(), it2.GetTx().Transaction)
			assert.True(t, i < 1) //only one tx
		}

		assert.NoError(t, st.Close())

		// запись в закрытый объект
		_, err = st.storeTxBytes(tx)
		assert.Error(t, err)

		fi, err := os.Stat(f.Name())
		assert.NoError(t, err)
		assert.Equal(t, headerSize+blockSize, fi.Size())

	}

	//ожидаем что файл прочитается, транзакции в файле соответствуют загруженным
	{
		st, err := NewStorage(f.Name(), false, true, make(chan struct{}))
		assert.NoError(t, err)
		//итератор покажет тестовые транзакции
		//итератор покажет наличие транзакций и они совпадут с тестовыми
		it2, err := st.Iterator()
		assert.NoError(t, err)
	forLabel2:
		for i := 0; ; i++ {
			switch err = it2.Next(); err {
			case io.EOF:
				//good
				break forLabel2
			case nil:
				//also good
			default: //bad :(
				assert.NoError(t, err)
			}
			assert.Equal(t, helperGenTx(), it2.GetTx().Transaction)
			assert.True(t, i < 1) //only one tx
		}

		tx := helperGenTxBytes(helperGenTx())
		_, err = st.storeTxBytes(tx)
		assert.NoError(t, err)
		//итератор покажет ещё х2 тестовых транзакций
		//итератор покажет наличие транзакций и они совпадут с тестовыми
		it3, err := st.Iterator()
		assert.NoError(t, err)
	forLabel3:
		for i := 0; ; i++ {
			switch err = it3.Next(); err {
			case io.EOF:
				//good
				break forLabel3
			case nil:
				//also good
			default: //bad :(
				assert.NoError(t, err, i)
			}
			assert.Equal(t, helperGenTx(), it2.GetTx().Transaction)
			assert.True(t, i < 2) //only one tx
		}

		assert.NoError(t, st.Close())

		fi, err := os.Stat(f.Name())
		assert.NoError(t, err)
		assert.Equal(t, headerSize+2*blockSize, fi.Size())

	}

	//ожидаем что в файле х2 тестовых транзакций (т.е. запись аддитивна)
	{
		st, err := NewStorage(f.Name(), false, true, make(chan struct{}))
		assert.NoError(t, err)
		it3, err := st.Iterator()
		assert.NoError(t, err)
	forLabel4:
		for i := 0; ; i++ {
			switch err = it3.Next(); err {
			case io.EOF:
				//good
				break forLabel4
			case nil:
				//also good
			default: //bad :(
				assert.NoError(t, err)
			}
			assert.Equal(t, helperGenTx(), it3.GetTx().Transaction)
			assert.True(t, i < 2) //only one tx
		}

		assert.NoError(t, st.Close())

		fi, err := os.Stat(f.Name())
		assert.NoError(t, err)
		assert.Equal(t, headerSize+2*blockSize, fi.Size())

	}
}

func TestStorageBrokenFile(t *testing.T) {
	//broke header
	f, err := ioutil.TempFile("", "storage_test_*.bin")
	assert.NoError(t, err)
	//assert.NoError(b, f.Stop())

	st, err := NewStorage(f.Name(), false, true, make(chan struct{}))
	assert.NoError(t, err)
	if err != nil {
		return
	}
	assert.NoError(t, st.Close())

	//ErrorBadHeader
	old, err := helperBrokeFile(0, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, true, make(chan struct{}))
	assert.Equal(t, ErrorBadHeader{}, err)

	_, err = helperBrokeFile(0, f.Name(), old)
	assert.NoError(t, err)

	//ErrorHackedBlock
	old, err = helperBrokeFile(8, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, true, make(chan struct{}))
	assert.Equal(t, ErrorHackedBlock{}, err)

	_, err = helperBrokeFile(8, f.Name(), old)
	assert.NoError(t, err)

	//ErrorHackedBlock
	old, err = helperBrokeFile(40, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, true, make(chan struct{}))
	assert.Equal(t, ErrorHackedBlock{}, err)

	_, err = helperBrokeFile(40, f.Name(), old)
	assert.NoError(t, err)

	//незавершенный блок
	err = helperBrockenFileWrite(72, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.Error(t, err)

	//исправляем блок
	_, err = NewStorage(f.Name(), false, true, make(chan struct{}))
	assert.NoError(t, err)

	//читаем исправленный блок
	st, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.NoError(t, err)

	//дописываем транзакции в блок
	tx := helperGenTx()
	assert.NoError(t, err)
	st.StoreTx(&tx)
	assert.NoError(t, st.Close())

	//считываем расширенный блок
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.NoError(t, err)

	//ломаем блок с транзакциями
	//ErrorHackedBlock -> commitID
	old, err = helperBrokeFile(72, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.Equal(t, ErrorHackedBlock{}, err)

	_, err = helperBrokeFile(72, f.Name(), old)
	assert.NoError(t, err)

	//ErrorHackedBlock -> len
	old, err = helperBrokeFile(104, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.Equal(t, ErrorHackedBlock{}, err)

	_, err = helperBrokeFile(104, f.Name(), old)
	assert.NoError(t, err)

	//ErrorHackedBlock -> body
	old, err = helperBrokeFile(138, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.Equal(t, ErrorHackedBlock{}, err)

	_, err = helperBrokeFile(138, f.Name(), old)
	assert.NoError(t, err)

	//ErrorHackedBlock -> body
	offset := 138 + int64(tx.Size()) - 10
	old, err = helperBrokeFile(offset, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.Equal(t, ErrorHackedBlock{}, err)

	_, err = helperBrokeFile(offset, f.Name(), old)
	assert.NoError(t, err)

	//ErrorHackedBlock -> prevblockhash
	offset = 138 + int64(tx.Size()) + 10
	old, err = helperBrokeFile(offset, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.Equal(t, ErrorHackedBlock{}, err)

	_, err = helperBrokeFile(offset, f.Name(), old)
	assert.NoError(t, err)

	//ErrorHackedBlock ann extra data
	offset = 138 + int64(tx.Size()) + 32
	err = helperBrockenFileWrite(offset, f.Name(), 0xFF)
	assert.NoError(t, err)
	_, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.Error(t, err)

	//ErrorHackedBlock ann extra data + do recover
	st, err = NewStorage(f.Name(), false, true, make(chan struct{}))
	assert.NoError(t, err)

	st.StoreTx(&tx)
	assert.NoError(t, st.Close())

	//ErrorHackedBlock ann extra data + do open recovered + 1 more tx
	st, err = NewStorage(f.Name(), false, false, make(chan struct{}))
	assert.NoError(t, err)

	it, err := st.Iterator()
	assert.NoError(t, err)
	var txCount int
endlessLoop:
	for {
		switch err := it.Next(); err {
		case io.EOF:
			//good
			break endlessLoop
		case nil:
			txCount++
		default:
			assert.NoError(t, err)
			break endlessLoop
		}
	}
	assert.Equal(t, 2, txCount)

	assert.NoError(t, os.Remove(f.Name()))
}

func helperBrokeFile(offset int64, fname string, new byte) (old byte, err error) {
	var fd *os.File
	var n int
	fd, err = os.OpenFile(fname, os.O_RDWR, 0600)
	if err != nil {
		return
	}
	rBuf := make([]byte, 1)
	wBuf := []byte{new}
	n, err = fd.ReadAt(rBuf, offset)
	switch {
	case err != nil:
		return
	case n != len(rBuf):
		err = fmt.Errorf("wrong read len = %d", n)
		return
	}
	n, err = fd.WriteAt(wBuf, offset)
	switch {
	case err != nil:
		return
	case n != len(wBuf):
		err = fmt.Errorf("wrong write len = %d", n)
		return
	}

	old = rBuf[0]
	return
}

func helperBrockenFileWrite(offset int64, fname string, new byte) (err error) {
	var fd *os.File
	var n int
	fd, err = os.OpenFile(fname, os.O_WRONLY, 0600)
	if err != nil {
		return
	}
	wBuf := []byte{new}
	n, err = fd.WriteAt(wBuf, offset)
	switch {
	case err != nil:
		return
	case n != len(wBuf):
		err = fmt.Errorf("wrong write len = %d", n)
		return
	}

	return
}

func helperGenTx() proto.Transaction {
	return proto.Transaction{
		Command: proto.Command{
			ReqID: proto.ReqID{
				IPLow:      1234567,
				SrcDstPort: 234567,
				ConnectTS:  uint32(1573958262),
				AcceptCnt:  666,
			},
			SubCommands: proto.SubCommands{
				New: &proto.NewOrder{
					Type:      proto.Type_Limit,
					Operation: proto.Operation_Buy,
					Amount:    proto.NewAmountStringMust("12345678909876654321"),
					Price:     proto.NewPriceMust("123.456"),
					OwnerID:   1234567890,
					ExtID:     9876543210,
				},
			}},
		TxList: []proto.TxDeal{
			{Maker: proto.OrderChange{
				MarketSide:   proto.MarketSide_Maker,
				AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
				OrderStatus:  proto.OrderStatus_Closed,
				DealPrice:    proto.Price{Price: 1234567890},
				Order: proto.NewOrder{
					Type:      proto.Type_Limit,
					Operation: proto.Operation_Sell,
					Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					Price:     proto.Price{Price: 1234567890},
					OwnerID:   1234,
					ExtID:     345,
				},
			},
				Taker: proto.OrderChange{
					MarketSide:   proto.MarketSide_Maker,
					AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					OrderStatus:  proto.OrderStatus_Closed,
					DealPrice:    proto.Price{Price: 1234567890},
					Order: proto.NewOrder{
						Type:      proto.Type_Limit,
						Operation: proto.Operation_Sell,
						Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
						Price:     proto.Price{Price: 1234567890},
						OwnerID:   1234,
						ExtID:     345,
					},
				},
			},
			{Maker: proto.OrderChange{
				MarketSide:   proto.MarketSide_Maker,
				AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
				OrderStatus:  proto.OrderStatus_Closed,
				DealPrice:    proto.Price{Price: 1234567890},
				Order: proto.NewOrder{
					Type:      proto.Type_Limit,
					Operation: proto.Operation_Sell,
					Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					Price:     proto.Price{Price: 1234567890},
					OwnerID:   1234,
					ExtID:     345,
				},
			},
				Taker: proto.OrderChange{
					MarketSide:   proto.MarketSide_Maker,
					AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					OrderStatus:  proto.OrderStatus_Closed,
					DealPrice:    proto.Price{Price: 1234567890},
					Order: proto.NewOrder{
						Type:      proto.Type_Limit,
						Operation: proto.Operation_Sell,
						Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
						Price:     proto.Price{Price: 1234567890},
						OwnerID:   1234,
						ExtID:     345,
					},
				},
			},
			{Maker: proto.OrderChange{
				MarketSide:   proto.MarketSide_Maker,
				AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
				OrderStatus:  proto.OrderStatus_Closed,
				DealPrice:    proto.Price{Price: 1234567890},
				Order: proto.NewOrder{
					Type:      proto.Type_Limit,
					Operation: proto.Operation_Sell,
					Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					Price:     proto.Price{Price: 1234567890},
					OwnerID:   1234,
					ExtID:     345,
				},
			},
				Taker: proto.OrderChange{
					MarketSide:   proto.MarketSide_Maker,
					AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					OrderStatus:  proto.OrderStatus_Closed,
					DealPrice:    proto.Price{Price: 1234567890},
					Order: proto.NewOrder{
						Type:      proto.Type_Limit,
						Operation: proto.Operation_Sell,
						Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
						Price:     proto.Price{Price: 1234567890},
						OwnerID:   1234,
						ExtID:     345,
					},
				},
			},
			{Maker: proto.OrderChange{
				MarketSide:   proto.MarketSide_Maker,
				AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
				OrderStatus:  proto.OrderStatus_Closed,
				DealPrice:    proto.Price{Price: 1234567890},
				Order: proto.NewOrder{
					Type:      proto.Type_Limit,
					Operation: proto.Operation_Sell,
					Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					Price:     proto.Price{Price: 1234567890},
					OwnerID:   1234,
					ExtID:     345,
				},
			},
				Taker: proto.OrderChange{
					MarketSide:   proto.MarketSide_Maker,
					AmountChange: proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
					OrderStatus:  proto.OrderStatus_Closed,
					DealPrice:    proto.Price{Price: 1234567890},
					Order: proto.NewOrder{
						Type:      proto.Type_Limit,
						Operation: proto.Operation_Sell,
						Amount:    proto.Amount{Byte0: 1, Byte1: 2, Byte2: 3, Byte3: 4, Byte4: 5, Byte5: 6},
						Price:     proto.Price{Price: 1234567890},
						OwnerID:   1234,
						ExtID:     345,
					},
				},
			},
		},
	}
}

func helperGenTxBytes(tx proto.Transaction) []byte {
	ret, err := tx.Marshal()
	if err != nil {
		panic(err)
	}
	return ret
}

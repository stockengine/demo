package storage

import (
	"sync"
	"sync/atomic"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"
)

// one more router implementation

type OneToManyForwarder struct {
	subscribers []Subscriber
	lastID      uint64

	mx sync.RWMutex
}

type Subscriber struct {
	id      uint64
	outChan chan interface{}
}

func (t *OneToManyForwarder) AddSubscriber() Subscriber {
	ret := Subscriber{
		id:      atomic.AddUint64(&t.lastID, 1),
		outChan: make(chan interface{}, routerChanSize),
	}

	t.mx.Lock()
	t.subscribers = append(t.subscribers, ret)
	t.mx.Unlock()

	return ret
}

func (t *OneToManyForwarder) RemoveSubscriber(sb Subscriber) {
	t.mx.Lock()
	for i, v := range t.subscribers {
		if v.id == sb.id {
			close(sb.outChan)
			//close(v.outChan) // there are the same channels
			// https://github.com/golang/go/wiki/SliceTricks
			t.subscribers = append(t.subscribers[:i], t.subscribers[i+1:]...) //memory leak is possible? - check last slice element
			break
		}
	}
	t.mx.Unlock()
}

func (t *OneToManyForwarder) Forward(msg interface{}) {
	t.mx.RLock()
	for i, v := range t.subscribers {
		select {
		case v.outChan <- msg:
		//good
		default:
			//bad :(
			logger.Log.Warn("cant forward message to chan is full", zap.Int("subscriber index", i), zap.Uint64("subscriber id", v.id))
		}
	}
	t.mx.RUnlock()
}

func (s *Subscriber) GetChan() <-chan interface{} {
	return s.outChan
}

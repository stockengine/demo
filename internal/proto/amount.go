package proto

import (
	"bytes"
	"errors"
	"fmt"
	"math/big"
	"math/bits"
	"reflect"
	"unsafe"

	"gitlab.com/stockengine/demo/internal/lib/logger"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

var ErrorAmountOverflow = errors.New("ErrorAmountOverflow")

// ToDo: это всё же число с фиксированной точкой. надо бы сделать нормализацию после операций
const AmountBytes = 6

func NewAmount(minor uint64) Amount {
	return Amount{
		Byte0: minor,
	}
}

func NewAmountString(str string) (am Amount, err error) {
	err = AmountSetFromString(&am, str)
	return
}

func NewAmountStringMust(str string) (am Amount) {
	var err error
	if am, err = NewAmountString(str); err != nil {
		panic(err)
	}
	return
}

// a-=b в итоге либо a = 0, в минус не уходит
// a > b => a-=b, deal = b, b = 0
// a = b => a=0, deal = b, b = 0
// a < b => b-=a, deal = a, a=0
//
// deal = min(a,b), a-=deal, b-=deal
// 8ns уходит на cmp
// 2х5ns на вычитание
func AmountSubTillZero(t, b, deal *Amount) {
	// 18 ns/op
	//deal.Min(t, b)
	//
	//t.Sub(deal)
	//b.Sub(deal)

	// 12 ns/op
	switch c := AmountCmp(t, b); c {
	case -1: // t < b => b-=t; t=0;
		*deal = *t
		AmountSub(b, t)
		*t = Amount{Byte0: 0, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0}
	case +1: // t > b
		*deal = *b
		AmountSub(t, b)
		*b = Amount{Byte0: 0, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0}
	case 0: // t == b
		*deal = *t
		*t = Amount{Byte0: 0, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0}
		*b = Amount{Byte0: 0, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0}
	default:
		panic("OMFG!!! Unknown error")
	}
}

//func Min(t, a, b *proto.Amount) {
//	switch c := AmountCmp(a, b); c {
//	case -1, 0:
//		*t = *a
//	default:
//		*t = *b
//	}
//}

func AmountSub(t, a *Amount) {
	//компилятор плохо циклы разворачивает...
	var b uint64
	t.Byte0, b = bits.Sub64(t.Byte0, a.Byte0, 0)
	t.Byte1, b = bits.Sub64(t.Byte1, a.Byte1, b)
	t.Byte2, b = bits.Sub64(t.Byte2, a.Byte2, b)
	t.Byte3, b = bits.Sub64(t.Byte3, a.Byte3, b)
	t.Byte4, b = bits.Sub64(t.Byte4, a.Byte4, b)
	t.Byte5, b = bits.Sub64(t.Byte5, a.Byte5, b)
	if b == 0 {
		return
	}

	logger.Log.Panic("OMFG... sub negative result")
}

func AmountAdd(t, a *Amount) {
	//компилятор плохо циклы разворачивает...
	var b uint64
	t.Byte0, b = bits.Add64(t.Byte0, a.Byte0, 0)
	t.Byte1, b = bits.Add64(t.Byte1, a.Byte1, b)
	t.Byte2, b = bits.Add64(t.Byte2, a.Byte2, b)
	t.Byte3, b = bits.Add64(t.Byte3, a.Byte3, b)
	t.Byte4, b = bits.Add64(t.Byte4, a.Byte4, b)
	t.Byte5, b = bits.Add64(t.Byte5, a.Byte5, b)
	if b == 0 {
		return
	}
	logger.Log.Panic("OMFG... add overflow")
}

// +1 => a>b
//  0 => a=b
// -1 => a<b
//func (t *proto.Amount) Cmp(b *proto.Amount) int {
//	for i := len(t.major) - 1; i >= 0; i-- {
//		c := helperCmp(t.major[i], b.major[i])
//		if c == 0 {
//			continue
//		} else {
//			return c
//		}
//	}
//
//	for i := len(t.minor) - 1; i >= 0; i-- {
//		c := helperCmp(t.minor[i], b.minor[i])
//		if c == 0 {
//			continue
//		} else {
//			return c
//		}
//	}
//	return 0
//}
/*
BenchmarkAmountCmp-8            300000000                5.75 ns/op            0 B/op          0 allocs/op
BenchmarkAmountCmpBigInt-8      300000000                5.74 ns/op            0 B/op          0 allocs/op

//после смены внутренней структуры на uint64s [6]uint64
BenchmarkAmountCmp-8                    200000000                7.86 ns/op            0 B/op          0 allocs/op

//после смены структуры на type [6]uint64 (отказ от вложения)
BenchmarkAmountCmp-8                    200000000                9.05 ns/op            0 B/op          0 allocs/op

//после смены алгоритма на битовые операции и исправлени ошибок
BenchmarkAmountCmp-8                    500000000                3.52 ns/op            0 B/op          0 allocs/op

*/
func AmountCmp(t, b *Amount) int {
	if d, c := bits.Sub64(t.Byte5, b.Byte5, 0); d != 0 {
		return 1 - int(c)<<1
	}
	if d, c := bits.Sub64(t.Byte4, b.Byte4, 0); d != 0 {
		return 1 - int(c)<<1
	}
	if d, c := bits.Sub64(t.Byte3, b.Byte3, 0); d != 0 {
		return 1 - int(c)<<1
	}
	if d, c := bits.Sub64(t.Byte2, b.Byte2, 0); d != 0 {
		return 1 - int(c)<<1
	}
	if d, c := bits.Sub64(t.Byte1, b.Byte1, 0); d != 0 {
		return 1 - int(c)<<1
	}
	if d, c := bits.Sub64(t.Byte0, b.Byte0, 0); d != 0 {
		return 1 - int(c)<<1
	}
	return 0
	////5.49 ns/op
	//i := amountBytes - 1
	//for ; t[i] == b[i] && i > 0; i-- {
	//}
	//switch {
	//case t[i] == b[i]:
	//	return 0
	//case t[i] > b[i]:
	//	return 1
	//default:
	//	return -1
	//}
}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func AmountToString(t *Amount) string {
	res := AmountToBuffer(t)
	return Reverse(res.String())
}

func AmountExtractDigit(t *Amount) (digit uint64) {
	tmp := [AmountBytes]*uint64{
		&t.Byte0,
		&t.Byte1,
		&t.Byte2,
		&t.Byte3,
		&t.Byte4,
		&t.Byte5,
	}
	for i := AmountBytes - 1; i >= 0; i-- {
		*tmp[i], digit = bits.Div64(digit, *tmp[i], 10)
	}
	return
}

func AmountIsZero(t *Amount) bool {
	mix := t.Byte5 | t.Byte4 | t.Byte3 | t.Byte2 | t.Byte1 | t.Byte0
	switch mix {
	case 0:
		return true
	default:
		return false
	}

	// 3.5 & 5.8 ns/op
	//return [AmountBytes]uint64(*t) == [AmountBytes]uint64{0, 0, 0, 0, 0, 0}
}

func AmountToBuffer(t *Amount) bytes.Buffer {
	tmp := *t
	var res bytes.Buffer
	for {
		d := AmountExtractDigit(&tmp)
		res.WriteByte('0' + byte(d))
		if AmountIsZero(&tmp) {
			break
		}
	}
	return res
}

func AmountSetFromString(t *Amount, str string) error {
	*t = Amount{}
	for i := 0; i < len(str); i++ {
		digitStr := str[i]
		d := digitStr - '0'
		if d > 9 {
			return fmt.Errorf("%s at position %d has invalid digit '%v'", str, i, digitStr)
		}
		if err := AmountMul64(t, 10); err != nil {
			return err
		}
		AmountAdd(t, &Amount{Byte0: uint64(d), Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0})
	}
	return nil
}

// проиграл в скорости штатному big.Int т.к. там используется алгоритм Карацубы -
// https://ru.wikipedia.org/wiki/%D0%A3%D0%BC%D0%BD%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5_%D0%9A%D0%B0%D1%80%D0%B0%D1%86%D1%83%D0%B1%D1%8B
func AmountMul64(t *Amount, b uint64) error {
	// 28.6 ns/op
	var hiCur, hiPrev uint64
	hiPrev, t.Byte0 = bits.Mul64(t.Byte0, b)

	hiCur, t.Byte1 = bits.Mul64(t.Byte1, b)
	AmountAdd(t, &Amount{Byte0: 0, Byte1: hiPrev, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: 0})
	hiPrev = hiCur

	hiCur, t.Byte2 = bits.Mul64(t.Byte2, b)
	AmountAdd(t, &Amount{Byte0: 0, Byte1: 0, Byte2: hiPrev, Byte3: 0, Byte4: 0, Byte5: 0})
	hiPrev = hiCur

	hiCur, t.Byte3 = bits.Mul64(t.Byte3, b)
	AmountAdd(t, &Amount{Byte0: 0, Byte1: 0, Byte2: 0, Byte3: hiPrev, Byte4: 0, Byte5: 0})
	hiPrev = hiCur

	hiCur, t.Byte4 = bits.Mul64(t.Byte4, b)
	AmountAdd(t, &Amount{Byte0: 0, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: hiPrev, Byte5: 0})
	hiPrev = hiCur

	hiCur, t.Byte5 = bits.Mul64(t.Byte5, b)
	AmountAdd(t, &Amount{Byte0: 0, Byte1: 0, Byte2: 0, Byte3: 0, Byte4: 0, Byte5: hiPrev})

	if hiCur != 0 {
		return ErrorAmountOverflow
	}
	return nil

	//// 31.7 ns/op
	//h0, l0 := bits.Mul64(t[0], b)
	//h1, l1 := bits.Mul64(t[1], b)
	//h2, l2 := bits.Mul64(t[2], b)
	//h3, l3 := bits.Mul64(t[3], b)
	//h4, l4 := bits.Mul64(t[4], b)
	//h5, l5 := bits.Mul64(t[5], b)
	//
	//*t = [amountBytes]uint64{l0, h0, 0, 0, 0, 0}
	//t.Add(&proto.Amount{0, l1, h1, 0, 0, 0})
	//t.Add(&proto.Amount{0, 0, l2, h2, 0, 0})
	//t.Add(&proto.Amount{0, 0, 0, l3, h3, 0})
	//t.Add(&proto.Amount{0, 0, 0, 0, l4, h4})
	//t.Add(&proto.Amount{0, 0, 0, 0, 0, l5})
	//
	//if h5 != 0 {
	//	panic("OMFG mul64 overflow")
	//}
}

const amountResolutionInternal = 30
const amountResolutionExternal = 20

func (a Amount) MarshalEasyJSON(w *jwriter.Writer) {
	buf := &big.Int{}
	buf.SetBits([]big.Word{
		big.Word(a.Byte0),
		big.Word(a.Byte1),
		big.Word(a.Byte2),
		big.Word(a.Byte3),
		big.Word(a.Byte4),
		big.Word(a.Byte5),
	})

	b, err := buf.MarshalJSON()

	//отрезаем всё что выходит за рамки сетки
	stripPoint := amountResolutionInternal - amountResolutionExternal
	if len(b) < (stripPoint) {
		b = b[:0]
	} else {
		b = b[:len(b)-stripPoint]
	}

	//добавляем точку
	b = helperAddPostDotZeroesRemoveRightZeroes(b, amountResolutionExternal)

	w.Raw(b, err)
}

func helperAddPostDotZeroesRemoveRightZeroes(b []byte, resolution int) []byte {
	//отрезаем справа нули, но оставляем не менее чем resolution символов
	pointPos := len(b) - resolution
	var stripPoint, cPoint int
	if pointPos >= 0 { //оптимизация цикла - что б не по двум условиям идти, а по одному
		cPoint = pointPos
	}
	for stripPoint = len(b) - 1; stripPoint >= cPoint && b[stripPoint] == '0'; stripPoint-- {
	}
	b = b[:stripPoint+1]

	switch {
	case len(b) == 0:
		b = append(b, '0')
	case len(b) == 1 && b[0] == '0':
	//это ноль, ничего с ним не делаем
	case stripPoint < cPoint: //точку ставить ненадо т.к. до неё одни нули, но есть что-то перед точкой
		//ничего не делаем
	case pointPos <= 0:
		//наполняем нулями после точки, добавляем точку и ведущий ноль
		extraZeroesCount := -pointPos
		needCap := extraZeroesCount + 2 + len(b)
		var tmp []byte
		if cap(b) < needCap { //по сути нам нужен новый слайс, филигранно копируем данные
			//need more space
			tmp = make([]byte, needCap)
		} else { //и в текущем достаточно места, нужно только им аккуратно распорядиться, без аллокаций
			tmp = *(*[]byte)(unsafe.Pointer(&reflect.SliceHeader{ //удивлюсь если эта фигня не приводит к аллокации на куче
				Data: uintptr(unsafe.Pointer(&b[0])),
				Len:  needCap,
				Cap:  cap(b),
			}))
		}
		copy(tmp[extraZeroesCount+2:], b)
		tmp[0] = '0'
		tmp[1] = '.'
		for i := 0; i < extraZeroesCount; i++ {
			tmp[i+2] = '0'
		}
		b = tmp
	default:
		//вставляем точку
		b = append(b, 0 /* use the zero value of the element type */)
		copy(b[pointPos+1:], b[pointPos:])
		b[pointPos] = '.'
	}
	return b
}

var ErrAmountToManyDots = errors.New("to many dots")
var ErrAmountToMany = fmt.Errorf("fraction too long. must be %d characters or less", amountResolutionExternal)
var ErrAmountIsEmpty = errors.New("empty field")

func (a *Amount) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	buf := make([]byte, 0, len(raw)+amountResolutionInternal+1)
	buf = append(buf, raw...)

	dotSep := bytes.Split(buf, []byte{'.'})
	if len(dotSep) > 2 {
		in.AddError(ErrAmountToManyDots)
		return
	}

	var zeroesToAdd = amountResolutionInternal
	//если точка есть
	if len(dotSep) > 1 {
		if len(dotSep[1]) > amountResolutionExternal {
			in.AddError(ErrAmountToMany)
			return
		}
		dotIndex := len(dotSep[0])

		copy(buf[dotIndex:], dotSep[1])
		buf = buf[:len(buf)-1]
		zeroesToAdd -= len(dotSep[1])
	}

	for i := 0; i < zeroesToAdd; i++ {
		buf = append(buf, '0')
	}

	//strip first zeroes
	var firstNonZero int
	for ; firstNonZero < len(buf) && buf[firstNonZero] == '0'; firstNonZero++ {
	}
	if firstNonZero == len(buf) {
		//все нули
		if len(buf) > 0 {
			buf = buf[:1]
		} else {
			in.AddError(ErrAmountIsEmpty)
			return
		}
	} else {
		buf = buf[firstNonZero:]
	}

	bufInt := big.Int{}
	err := bufInt.UnmarshalJSON(buf)
	if err != nil {
		in.AddError(err)
		return
	}
	//fmt.Println(bufInt.String())
	bts := bufInt.Bits()
	a.Reset()
	outWords := []*uint64{ //bigendian
		&a.Byte0, &a.Byte1, &a.Byte2, &a.Byte3, &a.Byte4, &a.Byte5,
	}
	if bl := bufInt.BitLen(); bl > 6*64 {
		in.AddError(fmt.Errorf("bitlen is %d, its greater that 6*64", bl))
		return
	}
	for i := 0; i < len(bts) && i < 6; i++ {
		*outWords[i] = uint64(bts[i])
	}
}

package proto

import (
	"bytes"
	"errors"
	"fmt"
	"math"
	"strconv"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

func (a Price) MarshalEasyJSON(w *jwriter.Writer) {
	b := make([]byte, 0, priceMaxLen) //ToDO: change to pool
	b = strconv.AppendUint(b, a.Price, 10)
	b = helperAddPostDotZeroesRemoveRightZeroes(b, priceFixPoint)
	w.Raw(b, nil)
}

func (a *Price) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	if len(raw) > priceMaxLen {
		in.AddError(ErrPriceTooBig)
		return
	}
	p, e := NewPrice(string(raw))
	if e != nil {
		in.AddError(e)
		return
	}
	a.Price = p.Price
}

func (a Price) ToFloat64() float64 {
	return float64(a.Price) / priceFixPointFloat64
}

var ErrPriceTooBig = errors.New("price must be in 10.10 format")

const priceMaxLen = priceFixPoint*2 + 1
const priceFixPoint = 10

var priceFixPointFloat64 = math.Pow10(priceFixPoint)

func NewPriceUint64(p uint64) Price {
	return Price{
		Price: p,
	}
}

func NewPriceMust(str string) Price {
	p, e := NewPrice(str)
	if e != nil {
		panic(e)
	}
	return p
}

func NewPrice(str string) (p Price, err error) {
	var c uint8
	var i int
	for i = 0; i < len(str); i++ {
		c = str[i]
		if c == '.' {
			break
		}
		d := c - '0'
		if d > 9 {
			err = fmt.Errorf("invalid format [%s] at position %d", str, i)
			return
		}
		p.Price *= 10
		p.Price += uint64(d)
	}
	if c == '.' {
		i++ // skip dot
		var j int
		for i < len(str) && j < priceFixPoint {
			c = str[i]
			d := c - '0'
			if d > 9 {
				err = fmt.Errorf("invalid format [%s] at position %d", str, i)
				return
			}
			p.Price *= 10
			p.Price += uint64(d)

			i++
			j++
		}
		for ; j < priceFixPoint; j++ {
			p.Price *= 10
		}
	} else {
		for i = 0; i < priceFixPoint; i++ {
			p.Price *= 10
		}
	}
	return
}

func PriceToString(p Price) string {
	var buf bytes.Buffer
	const maxDigits = 20
	for i := 0; i < maxDigits; i++ {
		d := p.Price % 10
		p.Price /= 10
		buf.WriteRune('0' + rune(d))
		if maxDigits-i == priceFixPoint+1 {
			buf.WriteRune('.')
		}
		if p.Price == 0 && maxDigits-i < priceFixPoint+1 {
			break
		}
	}
	return Reverse(buf.String())
}

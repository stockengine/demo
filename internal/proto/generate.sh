#!/usr/bin/env bash

GENERATED_PATH=./

if [[ "$(uname)" == "Darwin"* ]]; then
   sed="gsed"
else
   sed="sed"
fi

## cleanup
ls "$GENERATED_PATH"/*.pb* 1> /dev/null 2>&1 && rm "$GENERATED_PATH"/*.pb*

protoc -I=. -I="$GOPATH"/pkg/mod/ -I="$GOPATH"/src --gogofaster_out=plugins=grpc:"$GENERATED_PATH" ./*.proto
##protoc -I=. -I=$GOPATH/pkg/mod/ -I=$GOPATH/src --gogofaster_out=plugins=grpc:../order/ ./*v3.proto

matching="SubCommands"
storage="CommitedTransaction"
dom="DOMDelta DOMLine DOMBook DOMDeltaList"
auth="LoginPass RegisterRequest RegisterResult AuthToken AccessToken AuthTokenCoded AccessTokenCoded RefreshTokenCoded"

for template in $matching $storage $dom $auth
do
  $sed -i "/^type $template struct {*/i //easyjson:json" "$GENERATED_PATH"/*.pb.go
done

easyjson -no_std_marshalers "$GENERATED_PATH"/*.pb.go ../auth/recaptcha.go

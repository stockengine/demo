package proto

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrice(t *testing.T) {
	tests := []struct {
		req, res string
	}{
		{
			req: "1234567890.1234567890",
			res: "1234567890.1234567890",
		},
		{
			req: "123.123",
			res: "123.1230000000",
		},
		{
			req: "123",
			res: "123.0000000000",
		},
		{
			req: "0.0000001",
			res: "0.0000001000",
		}}
	for _, tst := range tests {
		p, e := NewPrice(tst.req)
		assert.NoError(t, e)
		assert.Equal(t, tst.res, PriceToString(p))
	}
}

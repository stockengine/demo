package proto

import (
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

func (a Operation) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(a.String())
}

func (a *Operation) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	err := a.UnmarshalJSON(raw)
	if err != nil {
		in.AddError(err)
	}
}

func (a Type) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(a.String())
}

func (a *Type) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	err := a.UnmarshalJSON(raw)
	if err != nil {
		in.AddError(err)
	}
}

func (a MarketSide) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(a.String())
}

func (a *MarketSide) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	err := a.UnmarshalJSON(raw)
	if err != nil {
		in.AddError(err)
	}
}

func (a OrderStatus) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(a.String())
}

func (a *OrderStatus) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	err := a.UnmarshalJSON(raw)
	if err != nil {
		in.AddError(err)
	}
}

func (a DOMDeltaType) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(a.String())
}

func (a *DOMDeltaType) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	err := a.UnmarshalJSON(raw)
	if err != nil {
		in.AddError(err)
	}
}

func (a AuthRole) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(a.String())
}

func (a *AuthRole) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	err := a.UnmarshalJSON(raw)
	if err != nil {
		in.AddError(err)
	}
}

func (a RegisterStatus) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(a.String())
}
func (a *RegisterStatus) UnmarshalEasyJSON(in *jlexer.Lexer) {
	raw := in.Raw()
	err := a.UnmarshalJSON(raw)
	if err != nil {
		in.AddError(err)
	}
}

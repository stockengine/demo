#  локальная разработка

запускается локально, сборка и 4 контейнера с единой локальной сетью.
в каталоге build запускаешь make и он всё делает
нужен docker-compose - возьми последнюю версию с офф сайта
и сам докер - бери любую я, кажется, взял из офф репки
после того как мейкфайл отработает всё запустится с биндом на локальные адреса (см. docker-compose.yaml файл) + сервисы внутри докера композа будут видеть друг-друга 
проверить что всё работает можно через:
 - docker-compose -p stockengine_net ps
 - docker-compose -p stockengine_net images
 - docker-compose -p stockengine_net top
 - docker-compose -p stockengine_net logs
 - и т.п. если сетку не указать, то композ тебе не покажет процессы
стопать тоже штатно: docker-compose -p stockengine_net down

соответственно, для локального кодинга и дебага, убираешь из композа модифицируемый сервис и запускаешь его локально. либо подсматриваешь в мейкфайле команды и пересоздаёшь образ с твоим дорабатываемым сервисов и играешься с docker-compose -p stockengine_net up (без -d оно работает в текущей консоли и по ctrl-c стопается). кстати, запускается весьма шустро.

либо для запуска используй make run

при каждом запуске композера выполняется пересборка бинарников

# Dockerhub

для нужд внешнего тестирования подготовлены образы на docker hub и соответствующий docker-compose файл. см папку dockerhub.
сборка и деплой выполняется как
```
make push_docker
```
пользоваться можно так:
```
docker-compose up
```
(стопать по ctrl+c)
и запуск кли
```
docker exec -it dockerhub_se.cli_1 /opt/stockengine/se.cli
```
и коннект 
```
matching Connect -Addr=se.matching:7111
```
все демоны на 7111 порту 

создание пользователя и проверка работы
- подключаемся к cli ``` docker exec -it dockerhub_se.cli_1 /opt/stockengine/se.cli ```:
    ```
    > Auth Connect -Addr=localhost:7060
    grpc.Dial...done
    serviceConn...done
    
    > Auth CreateUpdatePassword -Login=test123 -Pass=123
    done
    
    > Auth GetToken -Login=test123 -Pass=123
    Auth (refresh) coded token [v2.public.Cgd0ZXN0MTIzEKbT1u8FGKbese8FINOlm-G-qpnzBygBMACe8yRfma6o1EmtZ8hRpqgWdpgtdndZ6zhMgLl3uINANFaqe0CrYb0C-uWv-dHZxG1rUWQWSQDgFSw6224zmJsF.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2MzgwODM4LCJJc3N1ZWRBdCI6MTU3NTc3NjAzOCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0]
    
    > Auth RefreshToken -Token=v2.public.Cgd0ZXN0MTIzEKbT1u8FGKbese8FINOlm-G-qpnzBygBMACe8yRfma6o1EmtZ8hRpqgWdpgtdndZ6zhMgLl3uINANFaqe0CrYb0C-uWv-dHZxG1rUWQWSQDgFSw6224zmJsF.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2MzgwODM4LCJJc3N1ZWRBdCI6MTU3NTc3NjAzOCwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0
    new   auth (refresh) coded token [v2.public.Cgd0ZXN0MTIzEKbT1u8FGL7ese8FINOlm-G-qpnzBygBMAGOCnOkNRIscaHZbqBmUtyI0UNqGsllvn8yi3CZbb8Sy-tg72TRZNN7C6OpTpmVrEHniNFVAVWf1zXLiZCttKwJ.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2MzgwODM4LCJJc3N1ZWRBdCI6MTU3NTc3NjA2MiwiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MX0]
    new access (refresh) coded token [v2.public.CNOlm-G-qpnzBxDq4LHvBRi-3rHvBSABf9OtPslGyZtBn4E8S0b3iOFsTxURT2tb49FlS_o1ZqbVWRgeA-gpzOoCLlX4j1AtcKBldRpKN4IvuwVxoupeDw]
    ```    
- вставляем access токен в curl и убеждаемся в работоспособности
    ```
    curl -v -d'{"Login":"test123","Password":"123"}' http://localhost:8080/auth/login
    *   Trying 127.0.0.1:8080...
    * TCP_NODELAY set
    * Connected to localhost (127.0.0.1) port 8080 (#0)
    > POST /auth HTTP/1.1
    > Host: localhost:8080
    > User-Agent: curl/7.65.3
    > Accept: */*
    > Content-Length: 36
    > Content-Type: application/x-www-form-urlencoded
    > 
    * upload completely sent off: 36 out of 36 bytes
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 200 OK
    < Server: stockengine auth server
    < Date: Sun, 08 Dec 2019 03:42:27 GMT
    < Content-Type: text/plain; charset=utf-8
    < Content-Length: 309
    < Vary: Origin
    < Authorization: Bearer v2.public.Cgd0ZXN0MTIzEKPX1u8FGKPise8FINOlm-G-qpnzBygBMAAhlrLfTdGrvROu8skzbBhX42a-jaxv9XyB9aD_JSrgeNHkykELCYi2jnubIifmltZS1BOGDQyu4onFwaskPccM.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2MzgxMzQ3LCJJc3N1ZWRBdCI6MTU3NTc3NjU0NywiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0
    < Cache-Control: no-store
    < Pragma: no-cache
    < 
    * Connection #0 to host localhost left intact
    {"Auth":"v2.public.Cgd0ZXN0MTIzEKPX1u8FGKPise8FINOlm-G-qpnzBygBMAAhlrLfTdGrvROu8skzbBhX42a-jaxv9XyB9aD_JSrgeNHkykELCYi2jnubIifmltZS1BOGDQyu4onFwaskPccM.eyJMb2dpbiI6InRlc3QxMjMiLCJFeHBpcmF0aW9uIjoxNTc2MzgxMzQ3LCJJc3N1ZWRBdCI6MTU3NTc3NjU0NywiT3duZXIiOjU2OTI1MzgxNDAyNzYwNDY5MSwiUm9sZSI6MSwiR2VuZXJhdGlvbiI6MH0"}
    ```
    ``` 
    curl -v -H'X-Access-Token: v2.public.CNOlm-G-qpnzBxDq4LHvBRi-3rHvBSABf9OtPslGyZtBn4E8S0b3iOFsTxURT2tb49FlS_o1ZqbVWRgeA-gpzOoCLlX4j1AtcKBldRpKN4IvuwVxoupeDw' http://localhost:8080/auth/show
    *   Trying 127.0.0.1:8080...
    * TCP_NODELAY set
    * Connected to localhost (127.0.0.1) port 8080 (#0)
    > GET /show HTTP/1.1
    > Host: localhost:8080
    > User-Agent: curl/7.65.3
    > Accept: */*
    > X-Access-Token: v2.public.CNOlm-G-qpnzBxDq4LHvBRi-3rHvBSABf9OtPslGyZtBn4E8S0b3iOFsTxURT2tb49FlS_o1ZqbVWRgeA-gpzOoCLlX4j1AtcKBldRpKN4IvuwVxoupeDw
    > 
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 200 OK
    < Server: stockengine auth server
    < Date: Sun, 08 Dec 2019 03:35:28 GMT
    < Content-Type: text/plain; charset=utf-8
    < Content-Length: 83
    < Vary: Origin
    < Cache-Control: no-store
    < Pragma: no-cache
    < 
    * Connection #0 to host localhost left intact
    {"Owner":569253814027604691,"Expiration":1575776362,"IssuedAt":1575776062,"Role":1}
    ```

# grpc_cli (se.cli)
## storage Load
пример вывода

```
                 Command                                                                                     Original                                                              Final                   
    ### CommitID    IPLow SrcDstPort  ConnectTS      AcceptCnt  Type Operation                                  Amount          Price Owner ExtID                                  Amount             Status
 319981   319983 ac170001   1bc7832c 1570298855          99977     1      Sell 340282366920938463463374607431768211456   0.0000001234     0     0                                       0             Closed
                               Taker              0.0000012345     1      Sell 340282366920938463463374607431768211456   0.0000001234     0     0                                       0             Closed
                               Maker              0.0000012345     1       Buy 340282366920938463463374607431768211456   0.0000012345     0     0                                       0             Closed
```

Описание вывода.

Каждая подтверждённая транзакция состоит из финального статуса входящего ордера и связаных сделок. Визуально, сделки не имеют поряднового номера, CommitID, имеют чуть другую структуру и сдвинуты вправо.

| колонка      | транзакции                                                                                                                 | сделки                                                                     |
|:-------------|:---------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------|
| ###          | порядковый номер строки выводе. в блокчекн не хранится                                                                     |                                                                            |
| CommitID     | ID коммита в хранилище. Монотонно растущий uint64 счётчик                                                                  |                                                                            |
| IPLow        | hex IP источника запроса. устанавливается se.command, нужно для se.command для поиска REST коннекта отправившего запрос    |                                                                            |
| SrcDstPort   | порт источника и приёмника, устанавливается se.command, нужно ему же                                                       | Сторона сделки - Maker-> был в стакане, Taker-> пришёл забирать из стакана |
| ConnectTS    | ts (unix) открытия соединения по REST                                                                                      |                                                                            |
| AcceptCnt    | счётчик принятых сообщений/ комманд в se.command                                                                           | Price сделки                                                               |
| Type         | Тип ордера - лимитный и т.п.                                                                                               | аналогично транзакции                                                      |
| Operation    | Тип операции - покупка/ продажа                                                                                            | исходный тип операции ордера                                               |
| Amount       | исходный размер ордера (в минорных единицах, uint386)                                                                      | аналогично                                                                 |
| Price        | цена ордера (исходная, uint64, fix point)                                                                                  | аналогично                                                                 |
| Owner        | id владельца ордера. используется в отмене ордеров. 0 = не задан                                                           | аналогично                                                                 |
| ExtID        | вместе с Owner составляет уникальный пользовательский ID ордера. Используется в редактировании, отмене и т.п. 0 = не задан | аналогично                                                                 |
| Final Amount | остаток суммы ордера после сделки                                                                                          | аналогично                                                                 |
| Final Status | статус ордера после сделки                                                                                                 | аналогично                                                                 |

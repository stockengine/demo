FROM ubuntu:latest
RUN mkdir -p /opt/stockengine && apt update -y && apt install -y netcat
ADD se.command /opt/stockengine/
ADD cluster.env /opt/stockengine/
CMD set -o allexport; . /opt/stockengine/cluster.env; set +o allexport; \
    while ! nc -z ${SE_COMMAND_CONNECTORDERCREATE%%:*} ${SE_COMMAND_CONNECTORDERCREATE##*:}; do sleep 0.1; done; \
    while ! nc -z ${SE_COMMAND_CONNECTTRANSACTION%%:*} ${SE_COMMAND_CONNECTTRANSACTION##*:}; do sleep 0.1; done; \
    while ! nc -z ${SE_COMMAND_CONNECTDOM%%:*} ${SE_COMMAND_CONNECTDOM##*:}; do sleep 0.1; done; \
    exec /opt/stockengine/se.command
EXPOSE 7111

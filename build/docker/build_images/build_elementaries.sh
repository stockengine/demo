#!/usr/bin/env sh
set -e
cd "$(dirname "$0")"
docker-compose build --pull elementary
docker-compose push
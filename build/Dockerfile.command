FROM ubuntu:latest
RUN mkdir -p /opt/stockengine && \
    apt update -y && apt install -y netcat
VOLUME /opt/stockengine
CMD sleep 0.1; while ! test -f /opt/stockengine/builded ; do sleep 0.1; done ; \
    while ! nc -z ${SE_COMMAND_CONNECTORDERCREATE%%:*} ${SE_COMMAND_CONNECTORDERCREATE##*:}; do sleep 0.1; done; \
    while ! nc -z ${SE_COMMAND_CONNECTTRANSACTION%%:*} ${SE_COMMAND_CONNECTTRANSACTION##*:}; do sleep 0.1; done; \
    while ! nc -z ${SE_COMMAND_CONNECTDOM%%:*} ${SE_COMMAND_CONNECTDOM##*:}; do sleep 0.1; done; \
    exec /opt/stockengine/se.command
EXPOSE 7111

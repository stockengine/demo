package main

import (
	"gitlab.com/stockengine/demo/internal/notify"

	"gitlab.com/stockengine/demo/internal/lib/config"
	"gitlab.com/stockengine/demo/internal/lib/logger"
)

type Config struct {
	Bind                    string `required:"true" default:"127.0.0.1:7070" desc:"host and port to bind server"`
	PrometheusBind          string `desc:"prometheus interface and port to listen" default:"127.0.0.10:7070/metrics"`
	PublicKey               string `required:"true" desc:"mailjet public key" default:"/run/secrets/notify_public" fromfile:""`
	PrivateKey              string `required:"true" desc:"mailjet private key" default:"/run/secrets/notify_private" fromfile:""`
	PasswordTemplateID      int    `required:"true" desc:"new password template ID" default:"1"`
	ResetPasswordTemplateID int    `required:"true" desc:"reset password template ID" default:"2"`
}

func main() {
	cfg := Config{}
	if !config.LoadConfig(&cfg) {
		return
	}
	defer logger.Sync()
	var eng *notify.Engine
	defer func() {
		if eng != nil {
			eng.Stop()
		}
	}()

	config.WaitTermOld(func() (err error) {
		eng, err = notify.NewEngine(cfg.Bind, cfg.PrometheusBind, cfg.PublicKey, cfg.PrivateKey, cfg.PasswordTemplateID, cfg.ResetPasswordTemplateID)
		return err
	})
}

package main

import (
	"time"

	"gitlab.com/stockengine/demo/internal/lib/prometheus"

	"gitlab.com/stockengine/demo/internal/proto"

	"gitlab.com/stockengine/demo/internal/auth"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"github.com/valyala/fasthttp/reuseport"

	"github.com/fasthttp/router"

	"gitlab.com/stockengine/demo/internal/command"

	"github.com/valyala/fasthttp"
)

type Config struct {
	ListenHTTPRPC      string   `required:"true" desc:"address and port to bind and listen for incoming http requests (tcp4 only!" default:":6666"`
	ListenWSRPC        string   `required:"true" desc:"address and port to bind and listen for incoming WebSocket requests (tcp4 only!" default:":8080"`
	ConnectOrderCreate string   `required:"true" desc:"address and port for GRPC connect to order.engine" default:":6666"`
	ConnectTransaction string   `required:"true" desc:"address and port for GRPC connect to storage" default:":6667"`
	ConnectDOM         string   `required:"true" desc:"address and port for DOM" default:"127.0.0.1:6668"`
	AccessPublicKey    string   `required:"true" desc:"access ed25519 public key filename" default:"/run/secrets/access_public_key" fromfile:""`
	PrometheusBind     string   `desc:"prometheus interface and port to listen" default:"127.0.0.10:7020/metrics"`
	AllowedOrigins     []string `desc:"CORS AllowOrigin domains" default:"https://demo.stockengine.tech"`
}

func main() {
	cfg := Config{}
	if !config.LoadConfig(&cfg) {
		return
	}
	defer logger.Sync()

	ce, err := command.NewService(cfg.ConnectOrderCreate, cfg.ConnectTransaction, cfg.ConnectDOM, cfg.PrometheusBind)
	if err != nil {
		logger.Log.Error("NewService", zap.Error(err))
		return
	}
	defer ce.Stop()

	lnHTTP, err := reuseport.Listen("tcp4", cfg.ListenHTTPRPC)
	if err != nil {
		logger.Log.Error("reuseport listener", zap.Error(err))
		return
	}
	//lnHTTP = garcefullistener.NewGracefulListener(lnHTTP, 3*time.Second)

	lnWS, err := reuseport.Listen("tcp4", cfg.ListenWSRPC)
	if err != nil {
		logger.Log.Error("reuseport listener", zap.Error(err))
		return
	}
	//lnWS = garcefullistener.NewGracefulListener(lnWS, 3*time.Second)

	authMiddleware, err := auth.NewFastHTTPMiddleware(auth.FastHTTPAuthMiddlewareOptions{
		PublicKey:      []byte(cfg.AccessPublicKey),
		AllowedOrigins: cfg.AllowedOrigins,
	})
	if err != nil {
		logger.Log.Error("cant init authMiddleware", zap.Error(err))
		return
	}

	rHTTP := router.New()
	// Matching
	rHTTP.POST("/rest/order", authMiddleware.HandlerWithAccessToken(ce.HandleOrderOperation))
	// DOM
	rHTTP.GET("/rest/dom/full", authMiddleware.HandlerByRole(ce.FullDOM, proto.AuthRole_DemoRole))
	rHTTP.GET("/rest/dom/top100", authMiddleware.HandlerByRole(ce.Top100DOM, proto.AuthRole_DemoRole))

	rWS := router.New()
	rWS.GET("/ws/order", authMiddleware.HandlerWithAccessToken(ce.ServeWSOrder))
	rWS.GET("/ws/dom/delta", authMiddleware.HandlerByRole(ce.ServeWSDOM, proto.AuthRole_DemoRole))

	serverHTTP := &fasthttp.Server{
		Handler: prometheus.MetricsWrapper(authMiddleware.HandlerOnlyCors(rHTTP.Handler), ce.HTTPStatus),
		Name:    "stockengine.tech",
		//DisableKeepalive: true, // отключил, иначе сервер не хочет отрубать соединение если висит keep-alive
		DisableKeepalive:   false, //включил keepalive, но поставил ограничение на число запросов (1000) и таймаут на idle (10s)
		ReadBufferSize:     0,
		WriteBufferSize:    0,
		ReadTimeout:        100 * time.Millisecond, //когда на сервере кончится процессор, это может стать проблемой
		WriteTimeout:       100 * time.Millisecond, //когда на сервере кончится процессор, это может стать проблемой
		IdleTimeout:        1 * time.Second,
		MaxConnsPerIP:      0,           //может пригодиться для защиты от особо активных
		MaxRequestsPerConn: 1000,        //что бы вечного keep-alive не было, а то сервис не сможем грейсфул погасить да и балансировка не будет работать
		MaxRequestBodySize: 1024 * 1024, //ибо нефиг. по дефолту 4мбайт
		Logger:             nil,         //ToDO: сделать обёртку над логгеромм - он выдаёт ошибки когда нет сокетов и т.п.
	}
	serverWS := &fasthttp.Server{
		Handler:            prometheus.MetricsWrapper(authMiddleware.HandlerOnlyCors(authMiddleware.HandlerOnlyCors(rWS.Handler)), ce.HTTPStatus),
		Name:               "stockengine.tech",
		DisableKeepalive:   false,
		ReadBufferSize:     0,
		WriteBufferSize:    0,
		ReadTimeout:        100 * time.Millisecond, //когда на сервере кончится процессор, это может стать проблемой
		WriteTimeout:       100 * time.Millisecond, //когда на сервере кончится процессор, это может стать проблемой
		IdleTimeout:        1 * time.Second,
		MaxConnsPerIP:      0,
		MaxRequestsPerConn: 1000,        //что бы вечного keep-alive не было, а то сервис не сможем грейсфул погасить да и балансировка не будет работать
		MaxRequestBodySize: 1024 * 1024, //ибо нефиг. по дефолту 4мбайт
		Logger:             nil,
		KeepHijackedConns:  false,
	}

	config.WaitTerm(
		[]config.WaitFunction{
			func() error {
				logger.Log.Info("service started for HTTP", zap.String("addr", cfg.ListenHTTPRPC))
				if err := serverHTTP.Serve(lnHTTP); err != nil {
					logger.Log.Error("ListenAndServe", zap.Error(err))
					return err
				}
				return nil
			},
			func() error {
				logger.Log.Info("service started for WebSockets", zap.String("addr", cfg.ListenWSRPC))
				if err := serverWS.Serve(lnWS); err != nil {
					logger.Log.Error("ListenAndServe", zap.Error(err))
					return err
				}
				return nil
			},
		},
		[]config.AfterWaitFunction{
			func() error {
				logger.Log.Debug("serverHTTP shutdown")
				return serverHTTP.Shutdown()
			},
			func() error {
				logger.Log.Debug("serverWS shutdown")
				return serverWS.Shutdown()
			},
		},
	)
}

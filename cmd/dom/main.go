package main

import (
	"time"

	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/dom"
)

type Config struct {
	Bind                 string        `required:"true" desc:"address and port to bind and listen for incoming GRPC requests" default:"127.0.0.1:6668"`
	ConnectStorage       string        `required:"true" desc:"address and port for GRPC connect to se.storage" default:"127.0.0.1:6667"`
	FullUpdateInterval   time.Duration `required:"true" desc:"full DOM update interval" default:"1s"`
	Top100UpdateInterval time.Duration `required:"true" desc:"top100 DOM update interval" default:"100ms"`
	DeltaBuffer          int           `required:"true" desc:"fullDOM, top100DOM, delta to consumer channel size. " default:"10000"`
	UpdateBuffer         int           `required:"true" desc:"storage to DOM channel size" default:"10000"`
	PrometheusBind       string        `desc:"prometheus interface and port to listen" default:"127.0.0.10:7030/metrics"`
}

func main() {
	cfg := Config{}
	if !config.LoadConfig(&cfg) {
		return
	}

	var service *dom.Engine
	var err error

	config.WaitTerm(
		[]config.WaitFunction{
			func() error {
				service, err = dom.NewEngine(dom.Options{
					StorageServiceHostPort: cfg.ConnectStorage,
					BindHostPort:           cfg.Bind,
					FullUpdateInterval:     cfg.FullUpdateInterval,
					Top100UpdateInterval:   cfg.Top100UpdateInterval,
					DeltaBuffer:            cfg.DeltaBuffer,
					UpdateBuffer:           cfg.UpdateBuffer,
					PrometheusBind:         cfg.PrometheusBind,
				})

				if err != nil {
					logger.Log.Error("cant init dom service", zap.Error(err))
					return err
				}
				return nil
			},
		}, []config.AfterWaitFunction{
			func() error {
				if service != nil {
					service.Stop()
				}
				return nil
			},
			func() error {
				logger.Sync()
				return nil
			}},
	)
}

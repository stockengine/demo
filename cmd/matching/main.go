package main

import (
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/config"

	"gitlab.com/stockengine/demo/internal/lib/forwarder"

	"gitlab.com/stockengine/demo/internal/matching"
)

type Config struct {
	Bind           string `default:"127.0.0.1:6666" desc:"host and port to bind server"`
	StorageAddr    string `default:"127.0.0.1:6667" desc:"host and port of storage"`
	CmdBufLen      int    `default:"100" desc:"matching enigne input cmd channel size"`
	TxBufLen       int    `default:"100" desc:"matching enigne output tx channel size per client"`
	ReportLock     bool   `default:"true" desc:"report to log in case of channel full"`
	DropCmd        bool   `default:"false" desc:"drop incoming cmd in case of channel full"`
	DropTx         bool   `default:"false" desc:"drop outgoing tx in case of channel full"`
	PrometheusBind string `desc:"prometheus interface and port to listen" default:"127.0.0.10:7040/metrics"`
}

func main() {
	cfg := Config{}
	if !config.LoadConfig(&cfg) {
		return
	}

	var me *matching.Engine
	config.WaitTerm(
		[]config.WaitFunction{
			func() error {
				var err error
				me, err = matching.NewEngine(
					matching.OptionForwarder{
						ManyToOneOptions: forwarder.ManyToOneOptions{ChannelSize: cfg.CmdBufLen, DropMsg: cfg.DropCmd, ReportLock: cfg.ReportLock},
						OneToManyOptions: forwarder.OneToManyOptions{ChannelSize: cfg.TxBufLen, DropSubscribersMsg: cfg.DropTx, ReportLock: cfg.ReportLock},
					},
					matching.OptionServerAddr{Addr: cfg.Bind},
					matching.OptionStorageAddr{Addr: cfg.StorageAddr},
					matching.OptionPrometheusBind{Addr: cfg.PrometheusBind},
				)
				if err != nil {
					logger.Log.Error("NewEngine", zap.Error(err))
					return err
				}
				return nil
			},
		}, []config.AfterWaitFunction{
			func() error {
				if me != nil {
					me.Stop()
				}
				return nil
			},
			func() error {
				logger.Sync()
				return nil
			},
		},
	)
}

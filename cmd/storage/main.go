package main

import (
	"gitlab.com/stockengine/demo/internal/lib/config"
	"gitlab.com/stockengine/demo/internal/lib/logger"
	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/storage"
)

type Config struct {
	Bind           string `default:"127.0.0.1:6667" desc:"host and port to bind server"`
	File           string `default:"/var/tmp/storage.blkchn" desc:"path to transaction log blockchain"`
	PrometheusBind string `desc:"prometheus interface and port to listen" default:"127.0.0.10:7050/metrics"`
}

func main() {
	cfg := Config{}
	if !config.LoadConfig(&cfg) {
		return
	}
	var se *storage.Engine

	config.WaitTerm(
		[]config.WaitFunction{
			func() error {
				var err error
				se, err = storage.NewEngine(
					storage.Bind(cfg.Bind),
					storage.File(cfg.File),
					storage.Prometheus(cfg.PrometheusBind),
				)
				if err != nil {
					logger.Log.Error("cant init storage", zap.Error(err))
					return err
				}
				return nil
			},
		}, []config.AfterWaitFunction{
			func() error {
				if se != nil {
					se.Stop()
				}
				return nil
			},
			func() error {
				logger.Sync()
				return nil
			},
		})
}

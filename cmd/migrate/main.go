package main

import (
	"gitlab.com/stockengine/demo/internal/migrate"

	"go.uber.org/zap"

	"gitlab.com/stockengine/demo/internal/lib/config"
	"gitlab.com/stockengine/demo/internal/lib/logger"
)

type Config struct {
	User       string `required:"true" desc:"postgres file with user" default:"/run/secrets/postgres_user.txt" fromfile:""`
	Pass       string `required:"true" desc:"postgres file with pass" default:"/run/secrets/postgres_pass.txt" fromfile:""`
	Endpoint   string `required:"true" desc:"postgres endpoint" default:"postgres:5432"`
	Migrations string `required:"true" desc:"path to folder with migrations script in 'golang-migrations' format" default:"/opt/stockengine/migrations"`
	AuthPass   string `required:"true" desc:"auth file with pass" default:"/run/secrets/auth_pass.txt" fromfile:""`
	AuthXTEA   string `required:"true" desc:"file with xtea key" default:"/run/secrets/auth_xtea" fromfile:""`
}

// задача этого сервиса - запуститься, сделать миграцию и завершиться.
// должен уметь прекращать свою работу по ctrl+C
func main() {
	cfg := Config{}
	if !config.LoadConfig(&cfg) {
		return
	}
	defer logger.Sync()

	eng, err := migrate.NewService(cfg.User, cfg.Pass, cfg.Endpoint, cfg.Migrations,
		migrate.UserPass{User: "auth", Pass: cfg.AuthPass},
	)
	if err != nil {
		logger.Log.Error("NewService", zap.Error(err))
		return
	}
	defer eng.Stop()

	config.WaitTermOld(eng.Do)
}

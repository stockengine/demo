package main

import (
	"fmt"
	"os"

	"github.com/c-bata/go-prompt"

	"gitlab.com/stockengine/demo/internal/cli"
	cli2 "gitlab.com/stockengine/demo/pkg/cli"
)

func main() {
	var commands cli2.Commands
	structs := []cli2.CommandGenerateInterface{
		cli.NewMatching(),
		cli.NewStorage(),
		cli.NewDom(),
		cli.NewAuth(),
	}
	for _, s := range structs {
		cmd, err := cli2.Generate(s)
		if err != nil {
			fmt.Println(err)
			return
		}
		commands = append(commands, cmd)
	}

	commands = append(commands, cli2.Command{
		Prompt: prompt.Suggest{
			Text:        "exit",
			Description: "no graceful",
		},
		Flags: nil,
		Operation: func() {
			fmt.Printf("going shutdown...\n")
			os.Exit(0)
		},
		SubCommands: nil,
	})

	cli2.New(commands).Run()
}

go build && \
AUTH_PASS=../../build/secrets/auth_pass.txt \
AUTH_ENDPOINT="localhost:5432/stockengine?sslmode=disable" \
AUTH_XTEAKEY=../../build/secrets/auth_xtea_key \
AUTH_AUTHPRIVATEKEY=../../build/secrets/auth_private_key \
AUTH_AUTHPUBLICKEY=../../build/secrets/auth_public_key \
AUTH_ACCESSPRIVATEKEY=../../build/secrets/access_private_key \
AUTH_ACCESSPUBLICKEY=../../build/secrets/access_public_key \
./auth

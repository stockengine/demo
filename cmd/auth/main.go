package main

import (
	"fmt"

	"gitlab.com/stockengine/demo/internal/auth"
	"gitlab.com/stockengine/demo/internal/lib/config"
	"gitlab.com/stockengine/demo/internal/lib/logger"
)

type Config struct {
	ListenHTTPRPC    string   `required:"true" desc:"address and port to bind and listen for incoming http requests (tcp4 only!" default:":8080"`
	Bind             string   `required:"true" default:"127.0.0.1:7060" desc:"host and port to bind server"`
	User             string   `required:"true" desc:"postgres file with user" default:"auth"`
	Pass             string   `required:"true" desc:"postgres file with pass" default:"/run/secrets/auth_pass" fromfile:""`
	Endpoint         string   `required:"true" desc:"postgres endpoint" default:"postgres:5432"`
	ResetPasswordKey string   `required:"true" desc:"resetpassword encryption key 32-bytes filename" default:"/run/secrets/auth_reset_key" fromfile:""`
	XteaKey          string   `required:"true" desc:"xtea key 16-bytes filename" default:"/run/secrets/auth_xtea_key" fromfile:""`
	AuthPrivateKey   string   `required:"true" desc:"auth ed25519 private key filename" default:"/run/secrets/auth_private_key" fromfile:""`
	AuthPublicKey    string   `required:"true" desc:"auth ed25519 public key filename" default:"/run/secrets/auth_public_key" fromfile:""`
	AccessPrivateKey string   `required:"true" desc:"access ed25519 private key filename" default:"/run/secrets/access_private_key" fromfile:""`
	AccessPublicKey  string   `required:"true" desc:"access ed25519 public key filename" default:"/run/secrets/access_public_key" fromfile:""`
	PrometheusBind   string   `desc:"prometheus interface and port to listen" default:"127.0.0.10:7010/metrics"`
	AllowedOrigins   []string `desc:"CORS AllowOrigin domains" default:"https://demo.stockengine.tech"`
	RecaptchaSecret  string   `required:"true" desc:"recaptcha v3 secret" default:"/run/secrets/auth_recaptcha" fromfile:""`
	NotifyEndpoint   string   `default:"127.0.0.1:7070" desc:"host and port of se.notify"`
}

func main() {
	cfg := Config{}
	if !config.LoadConfig(&cfg) {
		return
	}
	defer logger.Sync()
	var eng *auth.Engine
	defer func() {
		if eng != nil {
			eng.Stop()
		}
	}()

	config.WaitTermOld(func() error {
		var err error
		eng, err = auth.NewEngine(
			fmt.Sprintf("postgres://%s:%s@%s", cfg.User, cfg.Pass, cfg.Endpoint),
			cfg.ListenHTTPRPC, cfg.Bind,
			auth.Keys{
				Auth: auth.Key{
					PrivateKey: []byte(cfg.AuthPrivateKey),
					PublicKey:  []byte(cfg.AuthPublicKey),
				},
				Access: auth.Key{
					PrivateKey: []byte(cfg.AccessPrivateKey),
					PublicKey:  []byte(cfg.AccessPublicKey),
				},
			},
			[]byte(cfg.XteaKey),
			cfg.PrometheusBind,
			cfg.AllowedOrigins,
			cfg.RecaptchaSecret,
			cfg.NotifyEndpoint,
			cfg.ResetPasswordKey,
		)
		if err != nil {
			return err
		}
		return nil
	})
}

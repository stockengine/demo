generate:
	protoc \
		-I proto \
		--gogofaster_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:\
$$GOPATH/src/ \
		--swagger_out=OpenAPI/ \
		serialization/*.proto

install:
	go get \
		github.com/gogo/protobuf/protoc-gen-gogo \

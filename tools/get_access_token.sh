#!/bin/bash

#set -o xtrace

refresh=$(curl -s -d'{"Login":"'$1'", "Password":"'$2'"}' https://api.stockengine.tech/auth/login | jq -r '.Auth')

decoded=$(echo ${refresh}= | awk -F'.' '{printf $4}' | base64 -di`)

echo 'Decoded refresh token -> '$decoded

access=$(curl -XPOST -s -H"Authorization: Bearer "$refresh https://api.stockengine.tech/auth/refresh | jq -r '.Access')
echo $access


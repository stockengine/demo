import random
from functools import partial
from multiprocessing.pool import Pool

import requests
import simplejson


def print_log(owner_id, *args, **kwargs):
    print(f'{owner_id}\t', *args, **kwargs)


def send_new_order(owner_id):
    log = partial(print_log, owner_id)
    log(f'process started')
    try:
        order_url = 'http://127.0.0.1:6665/order'
        order = {
            "New": {
                "Type": 0,
                "Operation": 0,
                "Amount": 0,
                "Price": {
                    "Price": 0
                },
                "Identification": {
                    "Owner": {
                        "Owner": 0
                    },
                    "ExtID": {
                        "ExtID": 0
                    }
                }
            }
        }
        req_session = requests.Session()
        for i in range(1, 100000):
            order['New']['Operation'] = random.randint(0, 1)
            order['New']['Amount'] = random.randint(100, 1000)
            order['New']['Price']['Price'] = random.randint(10, 10000)
            order['New']['Identification']['Owner']['Owner'] = owner_id
            order['New']['Identification']['ExtID']['ExtID'] = i
            response = req_session.post(order_url, json=order)
            log(f'status_code: {response.status_code}, content: {response.content}')
            if response.status_code != 200:
                log(f'error response: {response.content}')
                return
            log(f'count: {i}, order {simplejson.dumps(order)} sent')
        log(f'process finished')
    except Exception as ex:
        log(ex)


if __name__ == '__main__':
    # send_new_order(10)
    processes = 6
    with Pool(processes) as pool:
        res = pool.map_async(send_new_order, [i for i in range(1, processes + 1)])
        try:
            res.wait()
        except Exception as ex:
            print(ex)
        print(f'all threads are done')

CREATE OR REPLACE FUNCTION auth.new_auth_user2(login_ text, key_ bytea, role_ text, pass_hash_ bytea, pass_alg_ bytea,
                                               pass_salt_ bytea, force bool) RETURNS INTEGER AS
$$
DECLARE
    status INTEGER := 0;
BEGIN
    PERFORM * FROM auth.users WHERE login = login_;

    IF NOT FOUND THEN
        status = 1;
        INSERT INTO auth.users (login, owner, role, pass_hash, pass_alg, pass_salt)
        VALUES (login_, abs(auth.xtea(nextval('auth.auth_seq'), key_, true)), role_, pass_hash_, pass_alg_, pass_salt_);
    ELSEIF force THEN
        status = 2;
        UPDATE auth.users
        SET pass_hash = pass_hash_,
            pass_alg  = pass_alg_,
            pass_salt = pass_salt_,
            updated   = now()
        WHERE login = login_;
    ELSE
        status = 3;
    END IF;
    RETURN status;
END;
$$ VOLATILE
   strict language plpgsql;

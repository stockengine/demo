
CREATE SCHEMA auth;
CREATE USER auth;

-- https://wiki.postgresql.org/wiki/XTEA
CREATE OR REPLACE FUNCTION auth.xtea(val bigint, cr_key bytea, encrypt BOOLEAN)
    returns bigint AS
$$
DECLARE
    bk int[4];
    b  bigint; -- unsigned 32 bits
BEGIN
    IF octet_length(cr_key) <> 16 THEN
        RAISE EXCEPTION 'XTEA crypt key must be 16 bytes long.';
  END IF;
  FOR i IN 1..4 LOOP
    b:=0;
    FOR j IN 0..3 LOOP
      -- interpret cr_key as 4 big-endian signed 32 bits numbers
      b:= (b<<8) | get_byte(cr_key, (i-1)*4+j);
    END LOOP;
    bk[i] := CASE WHEN b>2147483647 THEN b-4294967296 ELSE b END;
  END LOOP;
  RETURN auth.xtea(val, bk, encrypt);
END
$$ immutable language plpgsql;

CREATE OR REPLACE FUNCTION auth.xtea(val bigint, key128 int4[4], encrypt BOOLEAN)
returns bigint AS $$
DECLARE
  -- we use bigint (int8) to implement unsigned 32 bits with modulo 32 arithmetic
  -- (in C, uint32_t is used but pg's int4 is signed and would overflow).
  -- the most significant halves of v0,v1,_sum must always be zero
  -- they're AND'ed with 0xffffffff after every operation
  v0 bigint;
  v1 bigint;
  _sum bigint:=0;
  cr_key bigint[4]:=ARRAY[
     CASE WHEN key128[1]<0 THEN key128[1]+4294967296 ELSE key128[1] END,
     CASE WHEN key128[2]<0 THEN key128[2]+4294967296 ELSE key128[2] END,
     CASE WHEN key128[3]<0 THEN key128[3]+4294967296 ELSE key128[3] END,
     CASE WHEN key128[4]<0 THEN key128[4]+4294967296 ELSE key128[4] END
   ];
BEGIN
  v0 := (val>>32)&4294967295;
  v1 := val&4294967295;
  IF encrypt THEN
    FOR i IN 0..63 LOOP
      v0 := (v0 + ((
	     ((v1<<4)&4294967295 # (v1>>5))
	       + v1)&4294967295
		   #
		   (_sum + cr_key[1+(_sum&3)::int])&4294967295
		   ))&4294967295;
      _sum := (_sum + 2654435769) & 4294967295;
      v1 := (v1 + ((
             ((v0<<4)&4294967295 # (v0>>5))
	       + v0)&4294967295
		  #
		  (_sum + cr_key[1+((_sum>>11)&3)::int])&4294967295
		  ))&4294967295;
    END LOOP;
  ELSE
    _sum := (2654435769 * 64)&4294967295;
    FOR i IN 0..63 LOOP
      v1 := (v1 - ((
	      ((v0<<4)&4294967295 # (v0>>5))
		  + v0)&4294967295
		  #
		  (_sum + cr_key[1+((_sum>>11)&3)::int])&4294967295
		  ))&4294967295;

      _sum := (_sum - 2654435769)& 4294967295;

      v0 := (v0 - ((
	     ((v1<<4)&4294967295 # (v1>>5))
	       + v1)&4294967295
		   #
		   (_sum + cr_key[1+(_sum&3)::int])&4294967295
		   ))&4294967295;

    END LOOP;
  END IF;
  RETURN (v0<<32)|v1;
END
$$ immutable strict language plpgsql;

CREATE SEQUENCE auth.auth_seq START 1;

CREATE TABLE auth.users
(
    login     VARCHAR(300) PRIMARY KEY UNIQUE NOT NULL,
    owner     int8 UNIQUE                     NOT NULL, -- ага, если всё же модуль даст 2 одинаковых числа, то пусть падает
    role      VARCHAR(10)                     NOT NULL,
    pass_hash bytea                           NOT NULL,
    pass_alg  bytea                           NOT NULL,
    pass_salt bytea                           NOT NULL,
    created   timestamp                       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated   timestamp                       NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX users_login_idx ON auth.users (login);

CREATE OR REPLACE FUNCTION auth.new_auth_user(login_ text, key_ bytea, role_ text, pass_hash_ bytea, pass_alg_ bytea,
                                              pass_salt_ bytea) RETURNS void AS
$$
DECLARE
BEGIN
    INSERT INTO auth.users (login, owner, role, pass_hash, pass_alg, pass_salt)
    VALUES (login_, abs(auth.xtea(nextval('auth.auth_seq'), key_, true)), role_, pass_hash_, pass_alg_, pass_salt_)
    ON CONFLICT (login) DO UPDATE SET pass_hash = pass_hash_,
                                      pass_alg=pass_alg_,
                                      pass_salt=pass_salt_,
                                      updated=excluded.updated;
END;
$$ VOLATILE
   strict language plpgsql;

CREATE TABLE auth.tokens
(
    token      text PRIMARY KEY UNIQUE NOT NULL,
    login      VARCHAR(300) UNIQUE REFERENCES auth.users (login),
    expiration timestamp               NOT NULL,
    issuedat   timestamp               NOT NULL,
    generation int8                    NOT NULL DEFAULT 0
);

CREATE VIEW auth.token_users AS
(
select *
from auth.users
         join auth.tokens using (login));

CREATE OR REPLACE FUNCTION auth.new_token(login_ text, expiration_ timestamp, issuedat_ timestamp, token_ text) RETURNS void AS
$$
DECLARE
BEGIN
    INSERT INTO auth.tokens (token, login, expiration, issuedat)
    VALUES (token_, login_, expiration_, issuedat_)
    ON CONFLICT (login) DO UPDATE SET token      = token_,
                                      expiration = expiration_,
                                      issuedat   = issuedat_,
                                      generation = 0;
END;
$$ VOLATILE
   strict language plpgsql;

GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA auth TO auth;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA auth TO auth;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA auth TO auth;
GRANT USAGE ON SCHEMA auth TO auth;

commit;

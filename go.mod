module gitlab.com/stockengine/demo

go 1.14

require (
	github.com/c-bata/go-prompt v0.2.3
	github.com/fasthttp/router v0.6.1
	github.com/fasthttp/websocket v1.4.2-0.20200217170420-2f79dd4fe29c
	github.com/gogo/protobuf v1.3.1
	github.com/golang-migrate/migrate/v4 v4.9.1
	github.com/jackc/pgconn v1.3.2
	github.com/jackc/pgmock v0.0.0-20190831213851-13a1b77aafa2
	github.com/jackc/pgproto3/v2 v2.0.1
	github.com/jackc/pgx/v4 v4.4.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/klauspost/compress v1.10.1 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/lab259/cors v0.2.0
	github.com/mailjet/mailjet-apiv3-go v0.0.0-20190724151621-55e56f74078c
	github.com/mailru/easyjson v0.7.1
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/o1egl/paseto v1.0.0
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/prometheus/client_golang v1.4.1
	github.com/prometheus/procfs v0.0.10 // indirect
	github.com/sethvargo/go-password v0.1.3
	github.com/shopspring/decimal v0.0.0-20200105231215-408a2507e114 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/valyala/fasthttp v1.9.0
	go.uber.org/atomic v1.5.1 // indirect
	go.uber.org/multierr v1.4.0 // indirect
	go.uber.org/zap v1.14.0
	golang.org/x/crypto v0.0.0-20200221170553-0f24fbd83dfb
	golang.org/x/net v0.0.0-20200219183655-46282727080f // indirect
	golang.org/x/sys v0.0.0-20200219091948-cb0a6d8edb6c // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	golang.org/x/tools v0.0.0-20200221191710-57f3fb51f507 // indirect
	google.golang.org/genproto v0.0.0-20200218151345-dad8c97a84f5
	google.golang.org/grpc v1.27.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
	honnef.co/go/tools v0.0.1-2020.1.2 // indirect
)

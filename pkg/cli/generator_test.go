package cli

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/c-bata/go-prompt"
)

type TestClass struct {
	m1, m2, m3, m4 int
}

var m2 int

func (t *TestClass) Description(method string) (ret string) {
	switch method {
	case "":
		return "testDesc"
	default:
		return method
	}
}
func (t *TestClass) TestMethod1() { t.m1++ }
func (t TestClass) TestMethod2()  { m2++ }

type testInput struct{}

func (t *TestClass) TestMethod3(*testInput) { t.m3++ }
func (t *TestClass) TestMethod4(*int)       { t.m4++ }

func TestGenerate(t *testing.T) {
	tc := TestClass{}
	cmd, _ := Generate(&tc) //ignore errors
	testFlag1 := testInput{}
	cmdEtalon := Command{
		Prompt: prompt.Suggest{
			Text:        "TestClass",
			Description: tc.Description(""),
		},
		Flags:     nil,
		Operation: nil,
		SubCommands: []Command{
			{
				Prompt: prompt.Suggest{
					Text:        "TestMethod1",
					Description: "TestMethod1"},
				Flags:     nil,
				Operation: nil,
			},
			{
				Prompt: prompt.Suggest{
					Text:        "TestMethod2",
					Description: "TestMethod2"},
				Flags:     nil,
				Operation: nil,
			},
			{
				Prompt: prompt.Suggest{
					Text:        "TestMethod3",
					Description: "TestMethod3"},
				Flags:     &testFlag1,
				Operation: nil,
			},
		},
	}
	//make it comparable
	assert.Equal(t, len(cmdEtalon.SubCommands), len(cmd.SubCommands))

	//assert -> Function equality cannot be determined and will always fail.
	cmd.SubCommands[0].Operation = nil
	cmd.SubCommands[1].Operation = nil
	cmd.SubCommands[2].Operation = nil

	cmdEtalon.SubCommands[2].Flags = cmd.SubCommands[2].Flags

	assert.Equal(t, cmdEtalon, cmd)

	//generate again and check method binding
	cmd, _ = Generate(&tc)
	assert.Equal(t, TestClass{}, tc)
	for _, v := range cmd.SubCommands {
		v.Operation()
	}

	tc.m2 += m2
	assert.Equal(t, TestClass{m1: 1, m2: m2, m3: 1, m4: 0}, tc)
}

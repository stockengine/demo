package cli

import (
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testScan struct {
	ip net.IPAddr
}

func (t testScan) FlagParser(data string) (interface{}, error) {
	if data == " " {
		return nil, nil //only for test purpose
	}
	addr, err := net.ResolveIPAddr("ip4", data)
	if err != nil {
		return nil, err
	}
	return testScan{ip: *addr}, nil
}

func TestScan(t *testing.T) {
	flags := []flagsVal{
		{"Scan": "192.168.1.14"},
		{"Scan": " "},
	}
	testStruct := struct {
		Scan testScan
	}{}
	testExpected := struct {
		Scan testScan
	}{
		Scan: testScan{
			ip: net.IPAddr{
				IP: []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 192, 168, 1, 14},
			},
		},
	}
	assert.NoError(t, fillFlags(&testStruct, flags[0]))
	assert.Equal(t, testExpected, testStruct)

	assert.Error(t, fillFlags(&testStruct, flags[1]))
}

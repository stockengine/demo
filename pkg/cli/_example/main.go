package main

import (
	"fmt"
	"time"

	"github.com/c-bata/go-prompt"
	"github.com/cheggaaa/pb"
	"gitlab.com/stockengine/demo/pkg/cli"
)

type testCommand1 struct {
	Field1 int
}

type testCommand2 struct {
	Field2 int
}

type testCommand3 struct {
	testCommand1
	testCommand2
}

type testCommand4 struct {
	Tc1 testCommand1
	Tc2 testCommand2
}

type cmd struct{}

func (t *cmd) Description(method string) string {
	switch method {
	case "Method1":
		return "method1 description :)"
	case "Call":
		return "imitate connection to other host"
	default:
		return ""
	}
}

func (t *cmd) Method1() {
	fmt.Println("do nothing. cmd -> Method1")
}

type args struct {
	Port int
	Host string
}

func (t *cmd) Call(args *args) {
	fmt.Printf("called with args -> %+v\n", args)
	bar := pb.StartNew(10)
	time.Sleep(time.Second * 10)
	bar.Finish()
}

func main() {
	commands := cli.Commands{
		{
			Prompt: prompt.Suggest{
				Text:        "testCommand1",
				Description: "testDesc1",
			},
			Operation: func() {
				fmt.Println("op1")
			},
			Flags: &testCommand1{},
		},
		{
			Prompt: prompt.Suggest{
				Text:        "testCommand2",
				Description: "testDesc2",
			},
			Operation: func() {
				fmt.Println("op2")
			},
			Flags: &testCommand2{},
		},
		{
			Prompt: prompt.Suggest{
				Text:        "testCommand3",
				Description: "testDesc3",
			},
			Operation: func() {
				fmt.Println("op3")
			},
			Flags: &testCommand3{},
		},
		{
			Prompt: prompt.Suggest{
				Text:        "testXYZ",
				Description: "testXYZ",
			},
			Operation: func() {
				fmt.Println("opXYZ")
			},
			Flags: &testCommand4{},
		},
	}

	cmd2 := &cmd{}
	commands2, err := cli.Generate(cmd2)
	if err != nil {
		fmt.Println(err)
		return
	}
	commands = append(commands, commands2)

	cli.New(commands).Run()
}

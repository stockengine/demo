package cli

import (
	"fmt"
	"reflect"

	"github.com/c-bata/go-prompt"
)

type CommandGenerateInterface interface {
	Description(method string) string
}

func Generate(iface CommandGenerateInterface) (ret Command, err error) {
	if reflect.TypeOf(iface).Kind() != reflect.Ptr ||
		reflect.TypeOf(iface).Elem().Kind() != reflect.Struct {
		return Command{}, fmt.Errorf("must be pointer to struct")
	}

	ret.Prompt.Text = reflect.TypeOf(iface).Elem().Name()
	ret.Prompt.Description = iface.Description("")

	for methodN := 0; methodN < reflect.TypeOf(iface).NumMethod(); methodN++ {
		methodType := reflect.TypeOf(iface).Method(methodN)
		methodImp := reflect.ValueOf(iface).Method(methodN)
		cmd := Command{
			Prompt: prompt.Suggest{
				Text:        methodType.Name,
				Description: iface.Description(methodType.Name),
			},
			Flags:       nil,
			Operation:   nil,
			SubCommands: nil,
		}

		name := methodType.Name
		numIn := methodImp.Type().NumIn()
		numOut := methodImp.Type().NumOut()

		switch {
		case name == "Description":
			continue
		case numOut != 0:
			err = fmt.Errorf("method[%s] has unsupported signature. must return nothing, but returns %d args. skip it",
				name, numOut)
			continue
		case numIn == 0:
			var tmp Operation // so stupid :(
			tmpType := reflect.TypeOf(tmp)

			if op, ok := methodImp.Convert(tmpType).Interface().(Operation); !ok {
				fmt.Printf("method[%s]->%s cant convert to Operation type->%s. skip it", name, methodImp.Type().String(), reflect.TypeOf(tmpType))
				continue
			} else {
				cmd.Operation = op
			}

		case numIn != 1:
			err = fmt.Errorf("method[%s] has unsupported signature. must receive only one ptr to struct, but receive %d args. skip it",
				name, numIn)
			continue
		case methodImp.Type().In(0).Kind() != reflect.Ptr:
			err = fmt.Errorf("method[%s] has unsupported signature. must receive ptr, but receive %s. skip it",
				name, methodImp.Type().In(0).Kind())
			continue
		case methodImp.Type().In(0).Elem().Kind() != reflect.Struct:
			err = fmt.Errorf("method[%s] has unsupported signature. must receive ptr to struct, but receive ptr to %s. skip it",
				name, methodImp.Type().In(0).Elem().Kind())
			continue
		default:
			cmd.Operation = func() {
				_ = methodImp.Call([]reflect.Value{
					reflect.ValueOf(cmd.Flags),
				})
			}
			cmd.Flags = reflect.New(methodImp.Type().In(0).Elem()).Interface()
		}

		ret.SubCommands = append(ret.SubCommands, cmd)
	}

	return
}

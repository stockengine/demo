package cli

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/c-bata/go-prompt"
)

//yep. stupid stateless realization

var historyFileName = func() string {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		fmt.Println("gethome dir err", err)
		return ""
	}
	return homeDir + "/.history.stockengine"
}()

const historyMax = 100

func (t *PromptImp) history() prompt.Option {
	last := make([]string, 0, historyMax)

	if historyFile, err := os.OpenFile(historyFileName, os.O_RDONLY, os.ModePerm); err == nil {
		reader := bufio.NewReader(historyFile)
		var line string
		var err error
		var fillIndex int
		var tmpHistory = make([]string, historyMax)
	L:
		for err == nil {
			line, err = reader.ReadString('\n')
			line = strings.ReplaceAll(line, "\n", "")
			if line == "" {
				continue
			}
			//check doubles and move to bottom
			for i := 0; i < historyMax-1; i++ {
				if line == tmpHistory[i] {
					copy(tmpHistory[i:], tmpHistory[i+1:])
					tmpHistory[(fillIndex-1)%historyMax] = line
					continue L
				}
			}
			tmpHistory[fillIndex%historyMax] = line
			fillIndex++
		}
		if err := historyFile.Close(); err != nil {
			fmt.Println("close error -> ", err)
		}

		//write last historyMax in proper order
		var startIndex int
		if fillIndex >= historyMax {
			startIndex = fillIndex
			fillIndex-- //it will be last filled
		}
		startIndex %= historyMax
		fillIndex %= historyMax

		for i := 0; i < historyMax && startIndex != fillIndex; i++ {
			last = append(last, tmpHistory[startIndex%historyMax])
			startIndex++
		}
	} else {
		fmt.Println("cant open history file.", err)
	}

	//truncate file
	if historyFile, err := os.OpenFile(historyFileName, os.O_RDWR|os.O_TRUNC|os.O_CREATE, os.ModePerm); err == nil {
		writer := bufio.NewWriter(historyFile)
		for i := 0; i < len(last); i++ {
			if _, err := writer.WriteString(last[i] + "\n"); err != nil {
				fmt.Println("write error", err)
				break
			} //else {
			//	fmt.Println("rewrited", i, last[i])
			//}
		}
		if err := writer.Flush(); err != nil {
			fmt.Println("flush error", err)
		}
		if err := historyFile.Close(); err != nil {
			fmt.Println("close error", err)
		}
	} else {
		fmt.Println("cant rewrite history file.", err)
	}

	return prompt.OptionHistory(last)
}

func (t *PromptImp) appendHistory(line string) {
	if writer, err := os.OpenFile(historyFileName, os.O_WRONLY|os.O_APPEND, os.ModePerm); err == nil {
		line = strings.ReplaceAll(line, "\n", "")
		line = strings.ReplaceAll(line, "\r", "")
		if _, err := writer.WriteString(line + "\n"); err != nil {
			fmt.Println("append write error", err)
		}
		if err := writer.Close(); err != nil {
			fmt.Println("append close error", err)
		}
	} else {
		fmt.Println("cant append history", err)
	}
}

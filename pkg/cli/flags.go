package cli

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

type flagsDesc map[string]string //text -> description
type flagsVal map[string]string  // "testFlag" -> "testValue"

const flagPrefix = "-"
const flagTemplate = flagPrefix + "%s="
const descTemplate = "(%s)"
const spacedTemplate = flagPrefix + "%s %s" //FlagParser works with space separated values

func makeFlags(i interface{}) (flgs flagsDesc, err error) {
	if i == nil {
		return
	}
	//идём по структуре, через рефлексию вытаскиваем поля и формируем описания
	v := reflect.ValueOf(i)
	if v.Kind() != reflect.Ptr {
		return nil, errors.New("type not a pointer: " + v.Kind().String())
	}
	t := reflect.TypeOf(i).Elem()

	flgs = make(flagsDesc, t.NumField())
	for fldN := 0; fldN < t.NumField(); fldN++ {
		fld := t.Field(fldN)
		if !v.Elem().Field(fldN).CanSet() {
			continue
		}

		var name = fmt.Sprintf(flagTemplate, fld.Name)
		var desc = fmt.Sprintf(descTemplate, fld.Type.Name())

		if tag := fld.Tag.Get("cli"); tag != "" {
			desc += " " + tag
		}

		flgs[name] = desc
	}
	return flgs, nil
}

func makeFlagsVal(fields []string, suggestion bool) (flgs flagsVal) {
	//fields := strings.Fields(flagString)
	flgs = make(flagsVal, len(fields)) // optimistic
	for _, field := range fields {
		name, val := analyzeField(field, suggestion)
		if name == "" || val == "" {
			continue
		}
		flgs[name] = val
	}
	return
}

func analyzeField(field string, suggestion bool) (name, value string) {
	field = strings.ReplaceAll(field, "=", " ")
	_, _ = fmt.Sscanf(field, spacedTemplate, &name, &value)
	if suggestion {
		name = fmt.Sprintf(flagTemplate, name) // костыль что б было единообразно
	}
	return
}

func fillFlags(i interface{}, flags flagsVal) error {
	//fill struct
	val := reflect.ValueOf(i)
	if val.Kind() != reflect.Ptr {
		return errors.New("type not a pointer: " + val.Type().String())
	}
	val = val.Elem()
	typ := reflect.TypeOf(i).Elem()
	for fldN := 0; fldN < typ.NumField(); fldN++ {
		fld := val.Field(fldN)
		if !fld.CanSet() {
			continue
		}
		fldType := typ.Field(fldN)
		fieldName := fldType.Name
		val, ok := flags[fieldName]
		if !ok || val == "" {
			continue
		}
		switch fld.Interface().(type) {
		case string:
			fld.SetString(val)
		case uint64:
			ival, err := strconv.ParseUint(val, 10, 0)
			if err != nil {
				return fmt.Errorf("filed [%s] has value [%s] but cant convert to filed type [%s] error -> %s", fieldName, val, fldType.Type.String(), err)
			}
			fld.SetUint(ival)
		case int:
			ival, err := strconv.ParseInt(val, 10, 0)
			if err != nil {
				return fmt.Errorf("filed [%s] has value [%s] but cant convert to filed type [%s] error -> %s", fieldName, val, fldType.Type.String(), err)
			}
			fld.SetInt(ival)
		default:
			// try FlagParser interface
			if d, ok := fld.Interface().(FlagParser); ok {
				switch iface, err := d.FlagParser(val); {
				case err != nil:
					return fmt.Errorf("%s[%s] error -> %s", fieldName, val, err)
				case iface == nil:
					return fmt.Errorf("%s[%s] error -> nil FlagParser result", fieldName, val)
				default:
					fld.Set(reflect.ValueOf(iface))
				}
			} else {
				return fmt.Errorf("field [%s] has type [%s]. cant process - there is no processing rules. try support FlagParser interface",
					fldType.Name, fldType.Type.String())
			}
		}
	}
	return nil
}

func splitCmdsFlags(words []string) (cmds, flgs []string) {
	for _, word := range words {
		if strings.HasPrefix(word, flagPrefix) {
			flgs = append(flgs, word)
		} else {
			cmds = append(cmds, word)
		}
	}
	return
}

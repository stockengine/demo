package cli

import (
	"sort"
	"strings"

	"github.com/c-bata/go-prompt"
)

type Command struct {
	Prompt      prompt.Suggest
	Flags       interface{}
	Operation   Operation
	SubCommands Commands
}

type Operation func() //value of Flags will be filled and may be used

type Commands []Command

func (t *Commands) suggestions(sub string) []prompt.Suggest {
	var sug = make([]prompt.Suggest, 0, len(*t))
	for i := range *t {
		sug = append(sug, (*t)[i].Prompt)
	}
	return prompt.FilterHasPrefix(sug, sub, true)
}

func (t *Command) suggestion(flags []string) (ret []prompt.Suggest, lastLevel bool) {
	allFlags, err := makeFlags(t.Flags)
	if err != nil {
		panic(err) // ибо нефиг
	}

	if len(flags) > 0 {
		//проверяем переданные флаги, кроме последнего, фильтруем оставшиеся и если видим что-то непонятное, то расстраиваемся и даём ошибку
		setFlags := makeFlagsVal(flags[:len(flags)-1], true)
		for k := range setFlags {
			if _, ok := allFlags[k]; ok {
				// найден, удаляем
				delete(allFlags, k)
			} else {
				// что то необычное, а такого быть не должно
				return
			}
		}

		//особо анализируем последний элемент - возможно он в процессе заполнения
		prefix := flags[len(flags)-1]
		name, val := analyzeField(prefix, true)
		if _, ok := allFlags[name]; ok {
			if val != "" {
				//полностью корректный флаг, даже со значением - убираем из подсказки
				delete(allFlags, name)
			}
		} else {
			//выполняем поиск по префиксу
			for flag, desc := range allFlags {
				if strings.HasPrefix(flag, prefix) {
					//пишут, пока не дописали полный ключ
					// добавляем в подсказку
					ret = append(ret, prompt.Suggest{
						Text:        flag,
						Description: desc,
					})
					delete(allFlags, flag)
				}
			}
			// нет смысла дальше формировать подсказку т.к. надо работать по введённым буковкам
			return
		}
	}

	//костыль - что пустой список не означает ошибку
	lastLevel = len(allFlags) == 0
	if lastLevel {
		return
	}

	//из оставшегося формируем подсказку
	for text, desc := range allFlags {
		ret = append(ret, prompt.Suggest{
			Text:        text,
			Description: desc,
		})
	}
	return
}

func (t *Commands) sort() {
	sort.SliceStable(*t, t.less)
	for i := range *t {
		(*t)[i].SubCommands.sort()
	}
}

func (t *Commands) less(i, j int) bool {
	return (*t)[i].Prompt.Text < (*t)[j].Prompt.Text
}

func (t *Commands) find(cmd string) *Command {
	indx := sort.Search(len(*t), func(i int) bool {
		return (*t)[i].Prompt.Text >= cmd
	})
	if indx < len(*t) && (*t)[indx].Prompt.Text == cmd {
		// x is present at data[i]
		return &(*t)[indx]
	}
	// x is not present in data,
	// but i is the index where it would be inserted.
	return nil
}

func sortSuggestion(unorder []prompt.Suggest) {
	sort.SliceStable(unorder, func(i, j int) bool {
		return unorder[i].Text < unorder[j].Text
	})
}

package cli

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMakeFlags(t *testing.T) {
	testStruct := struct {
		Int     int
		String  string
		Desc    string `cli:"test description"`
		private string //it will be ignored
	}{}
	expected := flagsDesc{
		"-Int=":    "(int)",
		"-String=": "(string)",
		"-Desc=":   "(string) test description",
	}

	flags, err := makeFlags(&testStruct)
	assert.NoError(t, err)
	assert.Equal(t, expected, flags)
}

func TestMakeFlagsVal(t *testing.T) {
	testString := "-Int=123 -String=asd -Desc=dfg"
	expected := flagsVal{
		"-Int=":    "123",
		"-String=": "asd",
		"-Desc=":   "dfg",
	}

	flagsVal := makeFlagsVal(strings.Fields(testString), true)
	assert.Equal(t, expected, flagsVal)
}

func TestFillFlags(t *testing.T) {
	flags := flagsVal{
		"Int":    "123",
		"String": "asd",
		"Desc":   "dfg",
	}
	testStruct := struct {
		Int    int
		String string
		Desc   string `cli:"test description"`
	}{}
	testExpected := struct {
		Int    int
		String string
		Desc   string `cli:"test description"`
	}{
		Int:    123,
		String: "asd",
		Desc:   "dfg",
	}
	assert.NoError(t, fillFlags(&testStruct, flags))
	assert.Equal(t, testExpected, testStruct)
}

func TestAnalyzeFields(t *testing.T) {
	testData := []struct {
		field, name, val string
		suggestion       bool
	}{
		{"-testFlag=asd", "-testFlag=", "asd", true},
		{"-testFlag=asd", "testFlag", "asd", false},
	}
	for _, test := range testData {
		name, val := analyzeField(test.field, test.suggestion)
		assert.Equal(t, test.name, name)
		assert.Equal(t, test.val, val)
	}
	name, val := analyzeField("-testFlag=asd", true)
	assert.Equal(t, "-testFlag=", name)
	assert.Equal(t, "asd", val)
}

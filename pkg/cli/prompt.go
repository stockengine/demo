package cli

import (
	"fmt"
	"os"
	"strings"

	"github.com/c-bata/go-prompt"
)

func New(cmd Commands) *PromptImp {
	cmd.sort()
	pi := &PromptImp{cmdTree: cmd}
	p := prompt.New(
		pi.executor,
		pi.completer,
		prompt.OptionLivePrefix(pi.livePrefix),
		prompt.OptionAddKeyBind(prompt.KeyBind{
			Key: prompt.ControlC,
			Fn: func(buffer *prompt.Buffer) {
				os.Exit(1)
			},
		}),
		prompt.OptionShowCompletionAtStart(),
		prompt.OptionPrefixTextColor(prompt.Yellow),
		prompt.OptionPreviewSuggestionTextColor(prompt.Blue),
		prompt.OptionSelectedSuggestionBGColor(prompt.LightGray),
		prompt.OptionSuggestionBGColor(prompt.DarkGray),
		prompt.OptionDescriptionBGColor(prompt.LightGray),

		pi.history(),
	)
	pi.prompt = p
	return pi
}

func (t *PromptImp) Run() {
	t.prompt.Run()
}

type PromptImp struct {
	cmdTree  Commands
	prompt   *prompt.Prompt
	statusOk bool
}

func (t *PromptImp) completer(doc prompt.Document) (ret []prompt.Suggest) {
	//show commands list
	var overrideOK bool
	defer func() {
		t.statusOk = overrideOK || len(ret) != 0
	}()
	curLevel := &t.cmdTree
	commands, flags := splitCmdsFlags(strings.Fields(doc.CurrentLineBeforeCursor()))

	for i := 0; i < len(commands)-1; i++ {
		//try to find apopriate level
		if fnd := curLevel.find(commands[i]); fnd != nil {
			curLevel = &fnd.SubCommands
		} else {
			//invalid command/ subcommand - there is no hint
			return
		}
	}

	//last word, try prefix search
	var lastWord string
	if lc := len(commands); lc > 0 {
		lastWord = commands[lc-1]
	}
	var lastCommand *Command
	if fnd := curLevel.find(lastWord); fnd != nil {
		//show next level
		lastCommand = fnd
	} else {
		//return prefix
		ret = curLevel.suggestions(lastWord)
		return
	}

	if len(lastCommand.SubCommands) > 0 {
		ret = lastCommand.SubCommands.suggestions("")
		return
	}

	//wow! its time for flags hint
	ret, overrideOK = lastCommand.suggestion(flags)
	sortSuggestion(ret) // only for flags

	return
}

func (t *PromptImp) executor(callLine string) {
	curLevel := &t.cmdTree
	var lastCmd *Command
	cmds, flags := splitCmdsFlags(strings.Fields(callLine))

	for i := 0; i < len(cmds); i++ {
		//try to find apopriate level
		if fnd := curLevel.find(cmds[i]); fnd != nil {
			lastCmd = fnd
			curLevel = &fnd.SubCommands
		} else {
			fmt.Printf("wrong command %v at [%d] word\n", cmds, i+1)
			return
		}
	}

	switch {
	case lastCmd == nil:
		fmt.Println("there is no command")
		return
	case lastCmd.Flags != nil && lastCmd.Operation != nil:
		flagsVal := makeFlagsVal(flags, false)
		if err := fillFlags(lastCmd.Flags, flagsVal); err != nil {
			fmt.Printf("fillFlags err -> %s\n", err)
		} else {
			lastCmd.Operation() //they will use lastCmd.Flags
		}
	case lastCmd.Operation != nil:
		lastCmd.Operation()
	default:
		//do nothing
		return
	}

	t.appendHistory(callLine)
}

func (t *PromptImp) livePrefix() (prefix string, useLivePrefix bool) {
	useLivePrefix = true
	switch t.statusOk {
	case true:
		prefix = "[✓] > "
	default:
		prefix = "[✗] > "
	}
	return
}
